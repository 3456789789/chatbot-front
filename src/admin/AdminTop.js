import React, { Component}  from 'react';
import jQuery from 'jquery';
import Loading from '../Loading.js';

var config = require("../config.js");

class AdminTop extends Loading {
    render() {
        var s =this.state;
        var icon = "";
        if(s.Preload && s.Preload['Logo'])
        {
            if(s.Preload['Logo'].indexOf("uploads")!=-1) {
                icon = config.Backend + '/content/getFile?';
                icon += '&Link=' +  s.Preload['Logo'];
            }
            else {
                icon = s.Preload['Logo'];
            }
        }
        return (
            <div className="admin_top">
                <div className="admin_logo_top">
                    <a href="/"><img src={icon} /></a>
                </div>
            </div>
        )
    }
}
export default AdminTop;
