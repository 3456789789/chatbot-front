import React, { Component}  from 'react';
import jQuery from 'jquery';
import AdminLeft from "../AdminLeft";
import AdminTop from "../AdminTop";
import Cookies from 'universal-cookie';
import { Scrollbars } from 'react-custom-scrollbars';
import Loading from '../../Loading.js';

const cookies = new Cookies();
var config = require('../../config.js');

class AdminNlu extends Loading {
    constructor(props) {
        super(props);
        this.state = {
            User: {},
            Intents: {},
            Wait: 0
        }
        this.load = this.load.bind(this);
        this.edit = this.edit.bind(this);
        this.change = config.change.bind(this);
    }

    edit(key)
    {
        var s =this.state;
        window.location.href=window.location.origin + '/admin/nlu/edit/'+key;
    }
    add()
    {
        var s =this.state;
        window.location.href=window.location.origin + '/admin/nlu/add';
    }
    load()
    {
        var s =this.state;
        var url = config.Backend +'/users/findById?';
        if (!(cookies.get('Id') === undefined))
        {
            url += 'Id=' + cookies.get('Id').toString();
        }
        this.setState({Wait:++this.state.Wait})
        jQuery.getJSON(url, function (res) {
            if (!res.Error && res.Type=="Super") {
                this.setState({User: res,Wait:--this.state.Wait},()=>{
                    this.setState({Wait:++this.state.Wait});
                    config.getData('nlu/all_entity').then(ok=>{
                       this.setState({Wait:--this.state.Wait,Intents:ok});
                   },err=>{
                       this.setState({Wait:--this.state.Wait});
                       alert(JSON.stringify(err))
                   })
                });
            } else {
                window.location.replace(window.location.origin + '/login');
            }
        }.bind(this));
    }
    componentWillMount() {
        this.preload();
        this.load();
    }

    AdminTop(){
        return(
            <div className="admin_top">
                <div className="admin_logo_top">
                    <a href="/admin/main"></a>
                </div>
            </div>
        )
    }

    TList() {
        var s=this.state;
        var list=[];
        if (s.Intents) {
             for(var k in s.Intents) {
                 const key =k;
                 var arr = s.Intents[key];
                list.push(
                    <tr>
                        <td>
                            {key}
                        </td>
                        <td>
                            {arr[0] || ""}
                        </td>
                        <td>
                            {arr[1] || ""}
                        </td>
                        <td>
                            {arr[2] || ""}
                        </td>
                        <td>
                            {arr[3] || ""}
                        </td>
                        <td>
                            <button className={'btn_save'} onClick={()=>this.edit(key)}>Edit</button>
                        </td>
                    </tr>)
            }
        }
        return (<tbody>{list}</tbody>)
    }

    render(){
        var s =this.state;

        return(
            <div>
                {this.Load()}
                <AdminTop/>
                <AdminLeft/>
                <div className="AdminContent">
                    <div className="chart_full padding">
                        <Scrollbars
                            style={{ height: "90%" }}>
                            <div className="overlay_chart grow_main_post">
                                <table className={"table table-striped mt_25"}>
                                    <thead>
                                    <tr>
                                        <th>Intent</th>
                                        <th>Entity</th>
                                        <th>Entity</th>
                                        <th>Entity</th>
                                        <th>Entity</th>
                                        <th>
                                            <button className={'btn_save'} onClick={()=>this.add()}>Add</button>
                                        </th>
                                    </tr>
                                    </thead>
                                    {this.TList()}
                                </table>
                            </div>
                        </Scrollbars>
                    </div>
                </div>
            </div>
        )
    }
}


export default AdminNlu;
