import React, { Component}  from 'react';
import jQuery from 'jquery';
import AdminLeft from "../AdminLeft";
import AdminTop from "../AdminTop";
import Cookies from 'universal-cookie';
import { Scrollbars } from 'react-custom-scrollbars';
import Loading from '../../Loading.js';

const cookies = new Cookies();
var config = require('../../config.js');

class AdminNlu extends Loading {
    constructor(props) {
        super(props);
        this.state = {
            User: {},
            Intents: {},
            Wait: 0,
            Rows:[],
            Entities:[],
            Intent:""
        }
        this.load = this.load.bind(this);
        this.change = config.change.bind(this);
    }

    add=()=>
    {
        var s =this.state;
        var arr=[];
        for(var j=0;j<4;j++) {
            arr.push({represent:""});
        }
        s.Rows.push({
            obj: {
                textExample: "",
            },
            arr:arr
        })
        this.setState({Rows:s.Rows});
    }
    train=()=>
    {
        var s =this.state;
        if(s.Rows.length<2) {
            alert("You must add at least two rows")
        } else {
            try {
                if(!s.Intent)
                    throw("Intent must be not empty!")
                if(s.Intents[s.Intent])
                    throw("This intent is already create!")

                var rows = JSON.parse(JSON.stringify(s.Rows));
                rows.forEach((t,i)=>{
                    if(!t.obj.textExample) {
                        throw("Example must be not empty!")
                    }
                    var new_arr = [];
                    t.arr.forEach((a,j)=>{
                        a.name = s.Entities[j].text;
                        if(a.represent && !a.name)
                            throw("Entity name must not be empty!")
                        if(a.name && a.represent) {
                            if(t.obj.textExample.indexOf(a.represent)==-1)
                                throw("Entity example must be in intent example!")
                            new_arr.push(a);
                        }
                    })
                    if(new_arr.length==0) {
                        throw("No one entity in row!")
                    }
                    t.obj.name = s.Intent;
                    t.arr = new_arr;
                })
                this.setState({Wait:++this.state.Wait});
                config.getData('nlu/train',{Rows:rows}).then(ok=>{
                    this.setState({Wait:--this.state.Wait,Rows:[]},()=>{
                        this.load();
                    });
                    console.log(ok);
                    alert("Success!")
                },e=>{
                    this.setState({Wait:--this.state.Wait});
                    console.log(e);
                    alert(JSON.stringify(e))
                })
            } catch (e) {
                alert(e.toString())
            }
        }
    }
    del=(x,i)=>
    {
        var s =this.state;
        s.Rows.splice(i,1)
        this.setState({Rows:s.Rows});
    }
    load()
    {
        var s =this.state;
        var url = config.Backend +'/users/findById?';
        if (!(cookies.get('Id') === undefined))
        {
            url += 'Id=' + cookies.get('Id').toString();
        }
        this.setState({Wait:++this.state.Wait})
        var key = this.props.match.params.key;
        jQuery.getJSON(url, function (res) {
            if (!res.Error && res.Type=="Super") {
                this.setState({User: res,Wait:--this.state.Wait},()=>{
                    this.setState({Wait:++this.state.Wait});
                    config.getData('nlu/all_entity').then(ok=>{
                        var ent = [];
                        for(var d=0;d<4;d++) {
                            ent.push({text:"",lock:false});
                        }
                        this.setState({Wait:--this.state.Wait,Intents:ok,Entities:ent});
                    },err=>{
                        this.setState({Wait:--this.state.Wait});
                        alert(JSON.stringify(err))
                    })
                });
            } else {
                window.location.replace(window.location.origin + '/login');
            }
        }.bind(this));
    }
    componentWillMount() {
        this.preload();
        this.load();
    }
    AdminTop(){
        return(
            <div className="admin_top">
                <div className="admin_logo_top">
                    <a href="/admin/main"></a>
                </div>
            </div>
        )
    }
    TList() {
        var s=this.state;
        var header_row=[];
        var arr = s.Entities;
        if (arr.length==4) {

            var ent_headers = [];
            for(var jj=0;jj<4;jj++) {
                const j =jj
                ent_headers.push(<td>
                    {arr[j].lock ? arr[j].text :
                        <input className={'form-control'} value={arr[j].text} onChange={(e)=>{
                            s.Entities[j].text = e.target.value;this.setState({Entities:s.Entities});
                        }} placeholder={"type entity name"}/>}
                </td>);
            }
            header_row.push(
                <tr>
                    <td>
                        Example
                    </td>
                    {ent_headers}
                    <td>
                        <button className={'btn_save'} onClick={()=>this.add()}>Add</button>
                        <button className={'btn_save'} onClick={()=>this.train()}>Train</button>
                    </td>
                </tr>)
        }
        var rows = s.Rows.map(function (elem, ind) {
            const i = ind;
            const x = elem;
            var ent =  s.Rows[i].arr.map(function (elem2, ind2) {
                const i2 = ind2;
                const e = elem2;
                return (<td>
                    <input className={'form-control'}
                           value={s.Rows[i].arr[i2].represent} onChange={(e)=>{
                        s.Rows[i].arr[i2].represent = e.target.value;this.setState({Rows:s.Rows});
                    }}/>
                </td>)
            }.bind(this))
            return (
                <tr>
                    <td>
                        <input className={'form-control'} name={"Rows["+i+"].obj.textExample"}
                               value={s.Rows[i].obj.textExample} onChange={(e)=>{
                            s.Rows[i].obj.textExample = e.target.value;this.setState({Rows:s.Rows});
                        }}/>
                    </td>
                    {ent}
                    <td>
                        <button className={'btn_save'} onClick={()=>this.del(x,i)}>Delete</button>
                    </td>
                </tr>)
        }.bind(this))
        return (<table className={"table table-striped"}>
            <thead>
            {header_row}
            </thead>
            <tbody>{rows}</tbody>
        </table>)
    }

    render(){
        var s =this.state;
        return(
            <div>
                {this.Load()}
                <AdminTop/>
                <AdminLeft/>
                <div className="AdminContent">
                    <div className="chart_full padding">
                        <a className={'btn_all'} href="/admin/nlu/list">Back</a>
                        <div className="form-group mt_25">
                            <label htmlFor="intent">Intent:</label>
                        <input id={'intent'} className={'form-control'} name={"Intent"}
                               value={s.Intent} onChange={this.change}/>
                        </div>
                        <Scrollbars
                            style={{ height: "90%" }}>
                            <div className="overlay_chart grow_main_post">
                                {this.TList()}
                            </div>
                        </Scrollbars>
                    </div>
                </div>
            </div>
        )
    }
}

export default AdminNlu;
