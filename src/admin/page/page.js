import React, { Component}  from 'react';
import jQuery from 'jquery';
import AdminLeft from "../AdminLeft";
import AdminTop from "../AdminTop";
import Cookies from 'universal-cookie';
import { Scrollbars } from 'react-custom-scrollbars';
import Loading from '../../Loading.js';

const cookies = new Cookies();

var config = require('../../config.js');

class Page extends Loading {
    constructor(props) {
        super(props);
        this.state = {
            Pages:[],
        }
        this.load = this.load.bind(this);
        this.change = config.change.bind(this);
    }
    get_or_create_page=(slug) =>{
        var s=this.state;
        this.setState({Wait:++this.state.Wait})
        config.getData("page/get_or_create",{Slug:slug}).then(function (ok) {
            var arr = config.find(s.Pages,{Slug:slug});
            if(arr.length==0) {
                s.Pages.push(ok);
            }
            this.setState({Wait:--this.state.Wait,Pages:s.Pages});
        }.bind(this),function (err) {
            this.setState({Wait:--this.state.Wait})
            alert(JSON.stringify(err));
        }.bind(this))
    }
    save_page=(no_alert) =>{
        var s=this.state;
        this.setState({Wait:++this.state.Wait})
        config.getData("page/edit",{Page:s.Page}).then(function (ok) {
            if(!no_alert) {
                alert("Success!");
            }
            this.setState({Wait:--this.state.Wait})
        }.bind(this),function (err) {
            this.setState({Wait:--this.state.Wait})
            alert(JSON.stringify(err));
        }.bind(this))
    }
    load()
    {
        var s=this.state;
        var url = config.Backend +'/users/findById?';
        if (!(cookies.get('Id') === undefined))
        {
            url += 'Id=' + cookies.get('Id').toString();
        }
        this.setState({Wait:++this.state.Wait})
        jQuery.getJSON(url, function (res) {
            if (!res.Error && res.Type=="Super") {
                this.setState({User: res,Wait:--this.state.Wait});
            } else {
                window.location.replace(window.location.origin + '/login');
            }
        }.bind(this));
        this.get_or_create_page("terms");
        this.get_or_create_page("policy");
    }
    AdminTop(){
        return(
            <div className="admin_top">
                <div className="admin_logo_top">
                    <a href="/admin/main"></a>
                </div>
            </div>
        )
    }
    componentWillMount()
    {
        this.preload();
        this.load();
    }
    onCollapse = (collapsed) => {
        console.log(collapsed);
        this.setState({ collapsed });
    }

    render(){
        var s=this.state;
        var pages = s.Pages.map((elem,ind)=>{
            const e=elem;
            const i = ind;

            return (
                <button onClick={(evt)=>{this.setState({Page:e})}} className={'mr_5 btn_all mt_10'}>{e.Slug}</button>)
        })
        return(
            <div>
            {this.Load()}
                <AdminTop/>
                <AdminLeft/>
                <div className="AdminContent">
                    <div className="chart_full padding">
                            <div className="form-group">
                                {pages}
                            </div>
                            {s.Page ?
                                <div>
                                    <div className="form-group">
                                        <label>Title</label>
                                        <input className={'form-control'} value={s.Page.Title || ""} name={"Page.Title"} onChange={this.change}/>
                                    </div>

                                    <div className="form-group">
                                        <label>Content</label>
                                        <textarea className={'form-control'} rows={3} value={s.Page.Body || ""} name={"Page.Body"} onChange={this.change}/>
                                    </div>
                                    <button onClick={()=>this.save_page()} className={'mt-3 btn_all bg_shadow'}>Save</button>
                                    <a target={"_blank"} href={"/page/"+s.Page.Slug}>
                                        <button className={'mt-3 btn_all bg_shadow'}>Show</button>
                                    </a>
                                </div> : null}
                        </div>
                    </div>
                </div>
        )
    }
}
export default Page;
