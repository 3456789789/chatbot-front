import React, { Component}  from 'react';
import jQuery from 'jquery';
import AdminLeft from "../AdminLeft";
import AdminTop from "../AdminTop";
import Cookies from 'universal-cookie';
import { Scrollbars } from 'react-custom-scrollbars';
import ChatBubble from 'react-chat-bubble';

const cookies = new Cookies();
var config = require('../../config.js');

class Support extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            User: {},
            Bot: {},
            Users: [],
            ChatUser: "",
            NotLogin: false,
            Msgs: [],
        };
        this.openChat = this.openChat.bind(this);
        this.unlockUser = this.unlockUser.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.sendAnswer = this.sendAnswer.bind(this);
        this.load = this.load.bind(this);
    }
    sendAnswer(text)
    {
        var url = config.Backend + "/support/sendToUser?&Id="+this.state.ChatUser;
        if(this.state.NotLogin)
        {
            url+='&NotLogin=true';
        }
        url += '&Text='+text;
        jQuery.getJSON(url, function (res) {
            if (!res.Error) {
                this.state.Msgs.push({"type":0,"text":text})
                this.setState({Msgs:this.state.Msgs});
            } else {
                alert(JSON.stringify(res))
            }
        }.bind(this))
    }
    handleInputChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }
    openChat(user) {
        this.setState({NotLogin:user.Ip})
        this.state.Msgs = [];
        for (var i = 0; i < user.Dialog.length; i++) {
            this.state.Msgs.push({"type": user.Dialog[i].Owner == "Admin" ? 0 : 1, "text": user.Dialog[i].Text})
        }
        this.setState({Msgs: this.state.Msgs,ChatUser:user._id});
    }
    unlockUser() {
        var url = config.Backend + "/users/unlock?";
        if(this.state.NotLogin)
        {
            url+='&NotLogin=true';
        }
        url += '&Id='+this.state.ChatUser;
        jQuery.getJSON(url, function (res) {
            if (!res.Error) {
                this.setState({ChatUser:""},function () {
                    this.load();
                }.bind(this))
            } else {
                alert(JSON.stringify(res))
            }
        }.bind(this))
    }
    load()
    {
        var url = config.Backend + '/users/findById?';
        if (!(cookies.get('Id') === undefined)) {
            url += 'Id=' + cookies.get('Id').toString();
        }
        jQuery.getJSON(url, function (res) {
            if (!res.Error) {
                //if(res.Type=="Super")
                //  window.location.replace(window.location.origin + '/admin/main');
                this.setState({User: res}, () => {
                    var url = config.Backend + '/users/allAdmins?';
                    url+="&ChatWithAdmin=true";
                    url+="&Type=Admin";
                    jQuery.getJSON(url, function (res) {
                        if (!res.Error) {
                            this.setState({Users:res});
                            var url = config.Backend + '/support/getNotLoginUsers?';
                            if (!(cookies.get('Id') === undefined)) {
                                url += 'Id=' + cookies.get('Id').toString();
                            }
                            jQuery.getJSON(url, function (res) {
                                if (!res.Error) {
                                    for(var t=0;t<res.length;t++)
                                    {
                                        this.state.Users.push(res[t]);
                                    }
                                    this.setState({Users:this.state.Users})
                                } else {
                                    alert(JSON.stringify(res));
                                }
                            }.bind(this));
                        } else {
                            alert(JSON.stringify(res));
                        }
                    }.bind(this));
                });
            } else {
                window.location.replace(window.location.origin + '/login');
            }
        }.bind(this));
    }
    componentWillMount() {
        this.load();
    }
    Users() {
        var users;
        if (this.state.Users) {
            users = this.state.Users.map(function (l, i) {
                const name =l.Name || l.Ip;
                return (<button className={"btn_all mr2"} onClick={() => this.openChat(l)}>{name}</button>)
            }.bind(this))
        } else {
            users = null;
        }
        return (<div>{users}</div>)
    }
    Chat() {
        return (<div>
            <ChatBubble messages={this.state.Msgs} onNewMessage={this.sendAnswer}/>
            <button className={"btn_all bg_shadow mt60"} onClick={this.unlockUser}>Unlock</button>
        </div>)
    }
    AdminTop(){
        return(
            <div className="admin_top">
                <div className="admin_logo_top">
                    <a href="/admin/main"></a>
                </div>
            </div>
        )
    }
    render() {
        return (
            <div>
                <AdminTop/>
                <AdminLeft/>
                <div className="AdminContent">
                    <div className="chart_full">
                        <Scrollbars
                            style={{ height: "90%" }}>
                            <div className="overlay_chart grow_main_post">
                            {this.Users()}
                            {this.state.ChatUser ? this.Chat() : null}
                            </div>
                        </Scrollbars>
                        </div>
                    </div>
                </div>
        )
    }
}

export default Support;