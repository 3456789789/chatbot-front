import React, { Component}  from 'react';
import jQuery from 'jquery';
import AdminLeft from "../AdminLeft";
import Cookies from 'universal-cookie';
import TextareaAutosize from 'react-autosize-textarea';
import { Scrollbars } from 'react-custom-scrollbars';

const cookies = new Cookies();
var config = require('../../config.js');

class Text extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            User: {},
            Text: {},
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.load = this.load.bind(this);
        this.save = this.save.bind(this);
    }

    save() {
        var fd = {};
        var obj = this.state.Text;
        for(var key in obj)
        {
            fd[key] = obj[key];
        }
        jQuery.ajax({
            method: "POST",
            contentType: 'application/json',
            url: config.Backend +"/admintext/edit",
            success: function (res) {
                if (!res.Error) {
                    alert(JSON.stringify(res));
                } else {
                    alert(JSON.stringify(res));
                }
            }.bind(this),
            data: JSON.stringify(fd),
        })
    }

    handleInputChange(event) {
        this.state.Text[event.target.name] = event.target.type!="checkbox" ? event.target.value : (event.target.checked ? 1 : 0);
        this.setState({
            Text : this.state.Text,
        })
    }

    load(name)
    {
        var url = config.Backend + '/users/findById?';
        if (!(cookies.get('Id') === undefined)) {
            url += 'Id=' + cookies.get('Id').toString();
        }
        jQuery.getJSON(url, function (res) {
            if (!res.Error) {
                if(res.Type=="Admin")
                    window.location.replace(window.location.origin + '/login');
                this.setState({User: res}, () => {
                    var url = config.Backend + '/admintext/show?Name='+name;
                    jQuery.getJSON(url, function (res) {
                        if (!res.Error) {
                            this.setState({Text:res})
                        } else {
                            alert(JSON.stringify(res));
                        }
                    }.bind(this));
                });
            } else {
                window.location.replace(window.location.origin + '/login');
            }
        }.bind(this));
    }
    componentWillMount() {
        this.load("Main");
    }



    AdminTop(){
        return(
            <div className="admin_top">
                <div className="admin_logo_top">
                    <a href="/admin/main"></a>
                </div>
            </div>
        )
    }

    ManageText()
    {
        return (<div>
            <TextareaAutosize className="api_textarea border_textarea_ mt_5 pl" rows = { 3 } maxRows={99} value={this.state.Text.Text} name="Text" onChange={this.handleInputChange}></TextareaAutosize>
        </div>)
    }
    render() {
        return (
            <div>
                {this.AdminTop()}
                <AdminLeft/>
                <div className="AdminContent">
                    <div className="mainAnalize grow_main">
                        <Scrollbars
                            style={{ height: "90%" }}>
                    <div className="chart_full">
                        <div className="overlay_chart grow_main_post">
                            <button className={"btn_all"} onClick={()=>this.load("Main")}>Главная</button>
                            <button className={"btn_all ml5"} onClick={()=>this.load("TariffSoonEnd")}>Напоминание за 3 дня до окончания тарифа</button>
                            {this.ManageText()}
                            <button className={"btn_all bg_shadow mt_10"} onClick={this.save}>Сохранить изменения</button>
                        </div>
                    </div>
                        </Scrollbars>
                    </div>
                </div>
            </div>
        )
    }
}

export default Text;