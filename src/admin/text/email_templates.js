import React, { Component}  from 'react';
import jQuery from 'jquery';
import AdminLeft from "../AdminLeft";
import Cookies from 'universal-cookie';
import TextareaAutosize from 'react-autosize-textarea';
import { Scrollbars } from 'react-custom-scrollbars';
import RichTextEditor from 'react-rte';

const cookies = new Cookies();
var config = require('../../config.js');

class Text extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            User: {},
            Templates: [],
            Index: 0,
            Saved: true,
            TariffSoonEnd: "5a4ee603148e5b220436fffe"
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.changeText = this.changeText.bind(this);
        this.load = this.load.bind(this);
        this.save = this.save.bind(this);
        this.add = this.add.bind(this);
        this.choose = this.choose.bind(this);
        this.del = this.del.bind(this);
    }
    changeText(value)
    {
        this.state.Templates[this.state.Index].Text = value.toString('html');
        this.state.Templates[this.state.Index].Editor = value;
        this.setState({Templates:this.state.Templates,Saved:false});
    }
    choose(i)
    {
        var temp = this.state.Templates;
        this.setState({Index: i});
    }

    add() {
        jQuery.ajax({
            method: "POST",
            contentType: 'application/json',
            url: config.Backend +"/admintext/add",
            success: function (res) {
                if (!res.Error) {
                    this.state.Templates.push(res);
                    this.setState({Templates:this.state.Templates});
                } else {
                    alert(JSON.stringify(res));
                }
            }.bind(this),
            data: JSON.stringify({Id:cookies.get('Id')}),
        })
    }
    save() {
        jQuery.ajax({
            method: "POST",
            contentType: 'application/json',
            url: config.Backend +"/admintext/editTemplate",
            success: function (res) {
                if (!res.Error) {
                    this.setState({Saved:true});
                } else {
                    alert(JSON.stringify(res));
                }
            }.bind(this),
            data: JSON.stringify({Id:cookies.get('Id'),Template:this.state.Templates[this.state.Index]}),
        })
    }
    del() {
        jQuery.ajax({
            method: "POST",
            contentType: 'application/json',
            url: config.Backend +"/admintext/delete",
            success: function (res) {
                if (!res.Error) {
                    this.state.Templates.splice(this.state.Index,1);
                    this.setState({Templates:this.state.Templates});
                } else {
                    alert(JSON.stringify(res));
                }
            }.bind(this),
            data: JSON.stringify({Id:cookies.get('Id'),Template:this.state.Templates[this.state.Index]}),
        })
    }

    handleInputChange(event) {
        this.state.Templates[this.state.Index][event.target.name] = event.target.type!="checkbox" ? event.target.value : (event.target.checked ? 1 : 0);
        this.setState({
            Templates : this.state.Templates,
            Saved: false,
        })
    }

    load()
    {
        var url = config.Backend + '/users/findById?';
        if (!(cookies.get('Id') === undefined)) {
            url += 'Id=' + cookies.get('Id').toString();
        }
        jQuery.getJSON(url, function (res) {
            if (!res.Error) {
                if(res.Type=="Admin")
                    window.location.replace(window.location.origin + '/login');
                this.setState({User: res}, () => {
                    var url = config.Backend + '/admintext/templates?Id='+cookies.get('Id');
                    jQuery.getJSON(url, function (res) {
                        console.log(res);
                        if (!res.Error) {
                            this.setState({Templates:res})
                        } else {
                            alert(JSON.stringify(res));
                        }
                    }.bind(this));
                });
            } else {
                window.location.replace(window.location.origin + '/login');
            }
        }.bind(this));
    }
    componentWillMount() {
        this.load();
    }



    AdminTop(){
        return(
            <div className="admin_top">
                <div className="admin_logo_top">
                    <a href="/admin/main"></a>
                </div>
            </div>
        )
    }

    ManageText()
    {
        return (<div>
            {this.state.Templates[this.state.Index]._id == this.state.TariffSoonEnd ? null :
            <label>Название шаблона
            <input placeholder={"Название шаблона"} value={this.state.Templates[this.state.Index].Name} name="Name" onChange={this.handleInputChange}/>
            </label> }
            <br/>
            <label>Тема письма
            <input placeholder={"Тема письма"} value={this.state.Templates[this.state.Index].Subject} name="Subject" onChange={this.handleInputChange}/>
            </label>
            <RichTextEditor
                value={this.state.Templates[this.state.Index].Editor ? this.state.Templates[this.state.Index].Editor :
                    ( this.state.Templates[this.state.Index].Text ? RichTextEditor.createValueFromString(this.state.Templates[this.state.Index].Text,'html')
                    : RichTextEditor.createEmptyValue())}
                onChange={this.changeText}
            />
            <TextareaAutosize className="api_textarea border_textarea_ mt_5 pl" rows = { 3 } maxRows={99} value={this.state.Templates[this.state.Index].Text} name="Text" onChange={this.handleInputChange}></TextareaAutosize>
            <button className={"btn_all bg_shadow mt_10"} onClick={this.save}>Сохранить изменения</button>
            {this.state.Templates[this.state.Index]._id == this.state.TariffSoonEnd ? null :
            <button className={"btn_all bg_shadow mt_10"} onClick={this.del}>Удалить шаблон</button>}
        </div>)
    }
    render() {
        var templates = this.state.Templates.map(function (elem,ind) {
            const i = ind;
            const t = elem;

            return (<button className={"btn_all"} onClick={()=>this.choose(i)}>{t.Name || "Безымянный"}</button>);
        }.bind(this))

        return (
            <div>
                {this.AdminTop()}
                <AdminLeft/>
                <div className="AdminContent">
                    <div className="mainAnalize grow_main">
                        <button className={"btn_all bg_shadow mt_10"} onClick={this.add}>Добавить шаблон</button>
                        {this.state.Saved ? "Изменения сохранены" : "Изменения не сохранены"}
                        <Scrollbars
                            style={{ height: "90%" }}>
                            <div className="chart_full">
                                <div className="overlay_chart grow_main_post">
                                    {templates}
                                    {this.state.Templates[this.state.Index] ? this.ManageText() : null}
                                </div>
                            </div>
                        </Scrollbars>
                    </div>
                </div>
            </div>
        )
    }
}

export default Text;