import React, { Component}  from 'react';
import jQuery from 'jquery';
import AdminLeft from "../AdminLeft";
import AdminTop from "../AdminTop";
import TextareaAutosize from 'react-autosize-textarea';
import ImageUploader from 'react-images-upload';
import { Scrollbars } from 'react-custom-scrollbars';
import Cookies from 'universal-cookie';
import Popup from "../../public/Popup";
import Gallery from 'react-grid-gallery';
import Loading from '../../Loading.js';

const cookies = new Cookies();
var config = require('../../config.js');

class Bot extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Name: this.props.Name,
            Status:this.props.Status
        }
        this.handleInputChange = this.handleInputChange.bind(this);
        this.edit = this.edit.bind(this);
        this.save = this.save.bind(this);
        this.change = config.change.bind(this);
    }

    edit(){
        cookies.set('BotId', this.props.Id, {path: '/'});
        window.location.replace(window.location.origin + '/chat/build');
    }


    save(){
        var s =this.state;
        cookies.set('BotId', this.props.id, {path: '/'});
        var url = config.Backend +'/bot/edit?';
        if (!(cookies.get('Id') === undefined))
        {
            url += 'CurrentUserId=' + cookies.get('Id').toString();
        }
        url+='&Id='+this.props.Id;
        url+='&Name='+this.state.Name;
        url+='&Status='+this.state.Status;
        this.props.addWait();
        jQuery.getJSON(url, function (res)
        {
            this.props.delWait();
            if (!res.Error) {
            } else {
                alert(JSON.stringify(res));
            }
        }.bind(this));
    }


    handleInputChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }
    editIcon = (e) =>{
        e.preventDefault();
        let reader = new FileReader();
        let file = e.target.files[0];
        reader.onloadend = () => {
            var url = reader.result;
            if(file) {
                var fd = new FormData;
                fd.append('File', file);
                this.props.addWait();
                jQuery.ajax({
                    method: "POST",
                    processData: false,
                    contentType: false,
                    url: config.Backend + "/content/file",
                    success: function (msg) {
                        config.getData("bot/setup_icon",{Icon:msg,Bot:this.props.Id}).then(function (ok) {
                            window.location.reload();
                            this.props.delWait();
                        }.bind(this),function (err) {
                            this.props.delWait();
                            alert(JSON.stringify(err))
                        }.bind(this))

                    }.bind(this),
                    data: fd,
                })
            }
        }
        reader.readAsDataURL(file)
    }
    clearInput=(e)=> {
        e.target.value = null;
    }
    render() {
        var src = config.Backend + '/content/getFile?';
        if (!(cookies.get('Id') === undefined)) {
            src += '&CurrentUserId=' + cookies.get('Id').toString();
        }
        src += '&Link=' + this.props.Icon;
        return (<div className="admin_bot ">
            <div className="admin_bot_left">
                <div className="admin_bot_images">

                    <img src={src} alt=""/>
                </div>
            </div>

            <div className="admin_bot_right">
                <div className="bot_right_content">
                    <div className="admin_bot_title">
                        <input className="chat_input borderR" type="text" value={this.state.Name} onChange={this.handleInputChange} name={"Name"}/>
                    </div>

                        <label>Show
                            <input className={'ml__5'} type={"checkbox"} checked={this.state.Status} name={"Status"} onChange={this.change}/>
                        </label>
                    <div className="admin_bot_title_icon">
                        <div className="form-group inputDnD">
                            <input type="file" accept="image/*" onChange={(e)=>this.editIcon(e)} onClick={this.clearInput}   className="add_image_avatar_bot text-primary "  />
                        </div>
                        <button className="edit_icon ml__10" onClick={this.edit}></button>
                        <button className="delete_icon" onClick={this.props.onDelete}></button>
                    </div>
                </div>


                <div className="admin_bot_bottom">

                    <div className="admin_bot_bottom_btn">
                        <button className="grid__btn_small bg_shadow" onClick={this.save}>Save</button>
                    </div>


                </div>
            </div>

        </div>)
    }
}

class CreateBot extends Loading {
    constructor(props) {
        super(props);
        this.state = {
            Name: "",
            Bots:[],
            Url: "",
            File: undefined,
            BotId: "",
            ShowDeletePopup: false,
            Wait: 0
        }
        this.handleInputChange = this.handleInputChange.bind(this);
        this.create=this.create.bind(this);
        this.deleteBot = this.deleteBot.bind(this);
        this.clearInput = this.clearInput.bind(this);
        this.addImageContent = this.addImageContent.bind(this);
        this.addWait = this.addWait.bind(this);
        this.delWait = this.delWait.bind(this);
    }
    addWait()
    {
        this.setState({Wait:++this.state.Wait});
    }
    delWait() {
        this.setState({Wait:--this.state.Wait});
    }
        addImageContent(e) {
        e.preventDefault();
        let reader = new FileReader();
        let file = e.target.files[0];
        reader.onloadend = () => {
            this.setState({Url:reader.result,File:file});
        }
        reader.readAsDataURL(file)
    }
    clearInput(e) {
        e.target.value = null;
    }

    deleteBot(){
        var s= this.state;
        var url = config.Backend +'/bot/delete?';
        if (!(cookies.get('Id') === undefined))
        {
            url += 'CurrentUserId=' + cookies.get('Id').toString();
        }
        url+='&Id='+this.state.BotId;
        this.setState({Wait:++this.state.Wait})
        jQuery.getJSON(url, function (res) {
            if (!res.Error) {
                for(var i=0;i<this.state.Bots.length;i++)
                {
                    if(this.state.BotId==this.state.Bots[i]._id)
                        this.state.Bots.splice(i,1);
                }
                this.setState({Bots:this.state.Bots,ShowDeletePopup:false,Wait:--this.state.Wait});
            } else {
                this.setState({Wait:--this.state.Wait})
                alert(JSON.stringify(res));
            }
        }.bind(this));
    }
    wantDeleteBot(id) {
        this.setState({BotId:id,ShowDeletePopup:true})
    }



    create() {
        var path="";
        var s =this.state;

        if(this.state.File) {
            var fd = new FormData;
            fd.append('File', this.state.File);
            this.setState({Wait:++this.state.Wait});
            jQuery.ajax({
                method: "POST",
                async: false,
                processData: false,
                contentType: false,
                url: config.Backend + "/content/file",
                success: function (msg) {
                    path = msg;
                    this.setState({Wait:--this.state.Wait})
                }.bind(this),
                data: fd,
            })
        }

        var url = config.Backend +'/bot/add?';
        if (!(cookies.get('Id') === undefined))
        {
            url += 'CurrentUserId=' + cookies.get('Id').toString();
        }
        url+='&isTemplate=1';
        url+='&Name='+ (this.state.Name ? this.state.Name : 'Template Bot');
        url+='&AiPos=50*50';
        url+='&Icon='+path;
        this.setState({Wait:++this.state.Wait})
        jQuery.getJSON(url, function (res) {
            if (!res.Error) {
                this.state.Bots.push(res.Bot);
                this.setState({Bots:this.state.Bots,Wait:--this.state.Wait});
            } else {
                this.setState({Wait:--this.state.Wait})
                alert(JSON.stringify(res));
            }
        }.bind(this));
    }

    componentWillMount() {
        this.preload();
        var s =this.state;
        var url = config.Backend +'/users/findById?';
        if (!(cookies.get('Id') === undefined))
        {
            url += 'Id=' + cookies.get('Id').toString();
        }
        this.setState({Wait:++this.state.Wait})
        jQuery.getJSON(url, function (res) {
            if (!res.Error  && res.Type=="Super") {
                this.setState({User: res,Wait:--this.state.Wait},()=>{
                    var url = config.Backend + '/bot/dftBots?';
                    if (!(cookies.get('Id') === undefined))
                    {
                        url += 'CurrentUserId=' + cookies.get('Id').toString();
                    }
                    this.setState({Wait:++this.state.Wait})
                    jQuery.getJSON(url, function (res) {
                        if (!res.Error) {
                            //alert(JSON.stringify(res));
                            this.setState({Bots: res.Data,Wait:--this.state.Wait})
                        } else {
                            this.setState({Wait:--this.state.Wait})
                            alert(JSON.stringify(res));
                        }
                    }.bind(this));
                });
            } else {
                window.location.replace(window.location.origin + '/login');
            }
        }.bind(this));
    }

    handleInputChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    AdminTop(){
        return(
            <div className="admin_top">
                <div className="admin_logo_top">
                    <a href="/admin/main"></a>
                </div>
            </div>
        )
    }

    AdminBot(){
        return(
            <div className="admin_bot">
                <div className="admin_bot_left">
                    <div className="admin_bot_images">
                        <img src="/img/1368718796_dostoprimechatelnosti_rima_1.jpg" alt=""/>
                    </div>
                </div>
                <div className="admin_bot_right">
                    <div className="bot_right_content">
                        <div className="admin_bot_title">
                            <span>Name bot5555555555555555555</span>
                            <div className="admin_bot_title_icon">
                                <button className="edit"></button>
                                <button className="delete"></button>
                            </div>
                        </div>
                    </div>
                    <div className="admin_bot_text">
                        <TextareaAutosize  rows={3} />
                    </div>
                </div>
            </div>
        )
    }
    render(){
        var IMAGES = [{src: this.state.Url,
            thumbnail:  this.state.Url,
            thumbnailWidth: 250,
            thumbnailHeight: 100,
            isSelected: false,
            caption: ""}]


        var bots = this.state.Bots.map(function (b) {
            return <Bot Id={b._id} Name={b.Name} Desc={b.Description}  Status={b.Status} addWait={this.addWait} delWait={this.delWait}
                        Icon={b.Icon} onDelete={()=>this.wantDeleteBot(b._id)}/>
        }.bind(this))
        return(
            <div>
                {this.Load()}
                <AdminTop/>
                <AdminLeft/>
                <div className="AdminContent">
                    <div className="chart_full">
                        <div className="overlay_chart">
                            <div className="configurate_main">
                                <div className="configurate_button_chart">
                                    <div className="upload_text">
                                        <input className="chat_input borderR" placeholder="Name" name="Name" value={this.state.Name} type="text"
                                               onChange={this.handleInputChange}></input>
                                    </div>
                                </div>
                                    <div className="upload_settings">
                                        <Gallery images={IMAGES}/>
                                    </div>
                                <div className="form-group inputDnD">
                                    <input type="file" accept="image/*" onChange={this.addImageContent} onClick={this.clearInput}   className="form-control-file text-primary " data-title="Image" />
                                </div>
                                    <button className="btn_all bg_shadow mt_10" onClick={this.create}>Create bot</button>
                                    <div className="settings_bot_list">
                                        <Scrollbars
                                            style={{ height: "90%" }}>
                                            {bots}
                                </Scrollbars>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.ShowDeletePopup ?
                    <Popup text={"Are you sure?"} Yes={this.deleteBot}  isDashboard={true}
                           No={()=>{this.setState({ShowDeletePopup:false})}}/> : null}
            </div>
        )
    }
}
export default CreateBot;
