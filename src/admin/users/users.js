import React, { Component}  from 'react';
import jQuery from 'jquery';
import AdminLeft from "../AdminLeft";
import AdminTop from "../AdminTop";
import Cookies from 'universal-cookie';
import { Scrollbars } from 'react-custom-scrollbars';
import Loading from '../../Loading.js';

const cookies = new Cookies();
var config = require('../../config.js');

class AdminUsers extends Loading {
    constructor(props) {
        super(props);
        this.state = {
            User: {},
            Users: [],
            Page: 0,
            Limit: 50,
            Templates:[],
            Template: "",
            Filter:"Name",
            Order:"asc",
            Wait: 0
        }
        this.handleInputChange = this.handleInputChange.bind(this);
        this.userChange = this.userChange.bind(this);
        this.allChange = this.allChange.bind(this);
        this.next = this.next.bind(this);
        this.prev = this.prev.bind(this);
        this.begin = this.begin.bind(this);
        this.load = this.load.bind(this);
        this.go = this.go.bind(this);
        this.edit = this.edit.bind(this);
        this.enter = this.enter.bind(this);
        this.change = config.change.bind(this);
    }

    handleInputChange(event) {
        this.state[event.target.name] = event.target.type!="checkbox" ? event.target.value : (event.target.checked ? 1 : 0);
        this.setState({
            [event.target.name] : this.state[event.target.name],
        })
    }
    userChange(event,i) {
        this.state.Users[i][event.target.name] = event.target.type!="checkbox" ? event.target.value : (event.target.checked ? 1 : 0);
        this.setState({
            Users : this.state.Users,
        })
    }
    allChange(event) {
        for (var i in this.state.Users) {
            this.state.Users[i].Select = event.target.checked ? 1 : 0;
        }
        this.setState({
            Users : this.state.Users,
        })
    }
    go()
    {
        this.load(this.state.Page)
    }
    begin()
    {
        this.state.Page=0;
        this.setState({Page:this.state.Page});
        this.load(this.state.Page);
    }
    prev()
    {
        this.state.Page--;
        this.setState({Page:this.state.Page});
        this.load(this.state.Page);
    }
    next()
    {
        this.state.Page++;
        this.setState({Page:this.state.Page});
        this.load(this.state.Page);
    }
    enter(u,i)
    {
        cookies.set('Id',u._id, { path: '/' });
        if (u.Type=="Admin") {
            window.location.replace(window.location.origin + '/dashboard');
        } else if (u.Type=="Super") {
            window.location.replace(window.location.origin + '/admin/main');
        }
    }
    edit(u,i)
    {
        var s =this.state;
        u.Password = config.xor(u.Password);
        this.setState({Wait:++this.state.Wait});
        config.getData('users/edit2',{User:u}).then(function (s) {
            this.setState({Wait:--this.state.Wait})
        }.bind(this),function (err) {
            this.setState({Wait:--this.state.Wait});
            alert(JSON.stringify(err));
        }.bind(this))
    }
    load(page)
    {
        var s =this.state;
        var url = config.Backend +'/users/findById?';
        if (!(cookies.get('Id') === undefined))
        {
            url += 'Id=' + cookies.get('Id').toString();
        }
        this.setState({Wait:++this.state.Wait})
        jQuery.getJSON(url, function (res) {
            if (!res.Error && res.Type=="Super") {
                this.setState({User: res,Wait:--this.state.Wait},()=>{
                    var url = config.Backend + '/users/forSuper_l?Page='+page+"&Limit="+this.state.Limit+"&Filter="
                        +this.state.Filter+"&Order="+this.state.Order;
                    if (!(cookies.get('Id') === undefined))
                    {
                        url += '&Id=' + cookies.get('Id').toString();
                    }
                    this.setState({Wait:++this.state.Wait})
                    jQuery.getJSON(url, function (res) {
                        console.log(res);
                        if (!res.Error) {
                            this.setState({Users:res,Wait:--this.state.Wait})
                        } else {
                            this.setState({Wait:--this.state.Wait});
                            alert(JSON.stringify(res));
                        }
                    }.bind(this));
                });
            } else {
                window.location.replace(window.location.origin + '/login');
            }
        }.bind(this));
    }
    componentWillMount() {
        this.preload();
        this.load(0);
    }

    AdminTop(){
        return(
            <div className="admin_top">
                <div className="admin_logo_top">
                    <a href="/admin/main"></a>
                </div>
            </div>
        )
    }

    TList() {
        var users;
        if (this.state.Users) {
            users = this.state.Users.map(function (elem, ind) {
                const i = ind;
                const x = elem;
                var end = x.TariffEnd ? new Date(x.TariffEnd) : "";
                if(end) {
                    var year = end.getFullYear();
                    var month = end.getMonth() + 1;
                    var day = end.getDate();

                    if (day < 10) {
                        day = '0' + day;
                    }
                    if (month < 10) {
                        month = '0' + month;
                    }
                    end = day+"."+month+"."+year;
                }
                return (
                    <tr>
                        <td>
                            {x.Name}
                        </td>
                        <td>
                            {x.Email}
                        </td>
                        <td>
                            <input className={'form-control'} type={"password"} name={"Users.Password"} value={this.state.Users[i].Password} onChange={(e)=>this.change(e,i)}/>
                        </td>
                        <td className={'tac'}>
                            {x.Bots ? x.Bots : 0}
                        </td>
                        <td className={'tac'}>
                            <input type={"checkbox"} checked={this.state.Users[i].Status} name={"Users.Status"} onChange={(e)=>this.change(e,i)}/>
                        </td>
                        <td>
                            <button className={'btn_save'} onClick={()=>this.edit(x,i)}>Save</button>
                            <button className={'btn_all'} onClick={()=>this.enter(x,i)}>View</button>
                        </td>
                    </tr>)
            }.bind(this))
        } else {
            users = null;
        }
        return (<tbody>{users}</tbody>)
    }

    render(){
        var s =this.state;

        return(
            <div>
                {this.Load()}
                <AdminTop/>
                <AdminLeft/>
                <div className="AdminContent">
                    <div className="chart_full padding">
                        <div className={'form-inline'}>
                            <div className="form-group mr_10">
                                <label htmlFor="">Filter by:</label>
                                <select className={'chat_input'} value={s.Filter} name={"Filter"} onChange={this.handleInputChange}>
                                    <option value={"Name"}>Name</option>
                                    <option value={"Email"}>Email</option>
                                    <option value={"Status"}>Active/De-Active</option>
                                    <option value={"Bots"}>Bots</option>
                                </select>
                            </div>
                            <div className="form-group mr_10">
                                <label htmlFor="">order:</label>
                                <select className={'chat_input'} value={s.Order} name={"Order"} onChange={this.handleInputChange}>
                                    <option value={"asc"}>Asc</option>
                                    <option value={"desc"}>Desc</option>
                                </select>
                            </div>

                            <div className="form-group mr_10">
                                <label htmlFor=""> Page</label>
                                <input className={'chat_input'} type={'number'} value={this.state.Page} name={"Page"} onChange={this.handleInputChange}/>
                            </div>

<div className="input-group mr_10">
    <label htmlFor="">Rows for page</label>
    <input className={'chat_input'} type={'number'} value={this.state.Limit} name={"Limit"} onChange={this.handleInputChange}/>
        <span className={'input-group-btn'}>
            <button className={'btn_all btn mt_25 bg_shadow p_user'} onClick={this.go}>Go</button>
        </span>
</div>
                        </div>
                        <Scrollbars
                            style={{ height: "90%" }}>
                            <div className="overlay_chart grow_main_post">
                                <table className={"table table-striped mt_25"}>
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Password</th>
                                        <th>Bots</th>
                                        <th className={'tac'}>Active/De-Active</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    {this.TList()}
                                </table>
                                {this.state.Page>0 ? <button className={'btn_all'} onClick={this.begin}>Begin</button> : null}
                                {this.state.Page>0 ? <button className={'btn_all'} onClick={this.prev}>Prev</button> : null}
                                <button className={'btn_all bg_shadow'} onClick={this.next}>Next</button>
                            </div>
                        </Scrollbars>
                    </div>
                </div>
            </div>
        )
    }
}


export default AdminUsers;
