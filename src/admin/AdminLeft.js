import React, { Component}  from 'react';
import jQuery from 'jquery';
import Cookies from 'universal-cookie';

const cookies = new Cookies();

class AdminLeft extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            Wait: 0
        };
    }

    exit(event) {
        cookies.set('Exit',"true", { path: '/' });
        window.location.replace(window.location.origin + '/login');
    }


    render() {
        /*
        <li className="">
                        <a href="/admin/settings">
                            <div class="block_image configurate"></div>
                            <span>Settings</span>
                        </a>
                    </li>
         */

        return (
            <div className="admin_left_panel">
                <ul className="admin_menu top_navigator">
                    <li className={window.location.href.indexOf("/admin/create-bot")!=-1 ? "active" : ""}>
                        <a href="/admin/create-bot">
                            <div className="block_image build"></div>
                            <span>Create Bot Template</span>
                        </a>
                    </li>
                    <li className={window.location.href.indexOf("/admin/main")!=-1 ? "active" : ""}>
                    <a href="/admin/main">
                        <div class="block_image analyze"></div>
                        <span>Statistics</span>
                    </a>
                </li>
                    <li className={window.location.href.indexOf("/admin/users")!=-1 ? "active" : ""}>
                        <a href="/admin/users">
                            <div class="block_image user"></div>
                            <span>Users</span>
                        </a>
                    </li>
                    <li className={window.location.href.indexOf("/admin/nlu/list")!=-1 ? "active" : ""}>
                    <a href="/admin/nlu/list">
                        <div class="block_image nlu"></div>
                        <span>NLU</span>
                    </a>
                   </li>
                    <li className={window.location.href.indexOf("/admin/pages")!=-1 ? "active" : ""}>
                        <a href="/admin/pages">
                            <div class="block_image page"></div>
                            <span>Pages</span>
                        </a>
                    </li>
                    <li className={window.location.href.indexOf("/admin/settings")!=-1 ? "active" : ""}>
                        <a href="/admin/settings">
                            <div class="block_image configurate"></div>
                            <span>Settings</span>
                        </a>
                    </li>
                </ul>
                <div className="bot_left_bottom_navigator">
                    <ul>
                        <li><a onClick={this.exit}>Log Out</a></li>
                    </ul>
                </div>

            </div>
        )
    }
}
export default AdminLeft;
