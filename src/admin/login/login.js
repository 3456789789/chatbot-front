import React, { Component}  from 'react';
import jQuery from 'jquery';
import Cookies from 'universal-cookie';
const cookies = new Cookies();
// var config = require('../../config.js');




class LoginAdminForm extends Component{
    constructor(props) {
        super(props);
        this.state = {
            Email: '',
            Email_valid: true,
            Password: '',
            Password_valid: true,
            Response_txt: '',
            Error: false,
            Type: "",
            Wait: 0
        }

    }

    Response () {
        const danger = this.state.Error ? <div className="text-danger">{this.state.Response_txt}</div> : <div>{this.state.Response_txt}</div>;
        return (<div className="form-group">
                {danger}
            </div>
        )
    }
    Email () {
        const danger = this.state.Email_valid ? '' : <div className="text-danger">Check your E-mail address.</div>;
        return (
            <div className="form-group">
              <input type="Email" className="" placeholder='Email' value={this.state.Email} name="Email" onChange={this.handleInputChange} onFocus={this.handleInputFocus}/>
                {danger}
            </div>
        )
    }

    Password () {
        const danger = this.state.Password_valid ? '' : <div className="text-danger">Check your Password.</div>;
        return (
            <div className="form-group">
              <input type="Password" className="" placeholder="Password" value={this.state.Password} name="Password" onChange={this.handleInputChange} onFocus={this.handleInputFocus}/>
                {danger}
            </div>
        )
    }

    render(){
      return(
          <div className="background_comein">
            <div className="form_block_login">
              <form  className="login">
                <div className="block_img">
                  <img src="/img/logo_logo.png" alt=""/>
                </div>
                <p>Sign into your account.</p>
                  {this.Response()}
                  {this.Email()}
                  {this.Password()}
                <button type="submit"  className="btn_login mt_15">Sign In</button>

              </form>
            </div>
          </div>
      )
    }

}

export default LoginAdminForm;