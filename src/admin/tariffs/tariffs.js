import React, { Component}  from 'react';
import jQuery from 'jquery';
import AdminLeft from "../AdminLeft";
import { Scrollbars } from 'react-custom-scrollbars';
import Cookies from 'universal-cookie';

const cookies = new Cookies();
var config = require('../../config.js');

class Tariffs extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            User: {},
            Tariffs: [],
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.load = this.load.bind(this);
        this.add = this.add.bind(this);
        this.save = this.save.bind(this);
        this.del = this.del.bind(this);
    }
    del(id,i){
        if(id!=-1) {
            var url = config.Backend + "/tariffs/delete?";
            url += '&CurrentUserId=' + cookies.get('Id');
            url += '&Id=' + id;
            jQuery.getJSON(url, function (res) {
                if (!res.Error) {
                    this.state.Tariffs.splice(i, 1);
                    this.setState({Tariffs: this.state.Tariffs});
                }
                else {
                    alert(JSON.stringify(res));
                }
            }.bind(this))
        }
        else {
            this.state.Tariffs.splice(i, 1);
            this.setState({Tariffs: this.state.Tariffs});
        }
    }
    add() {
        this.state.Tariffs.push({_id:-1,Name:"Новый тариф",Price:500,Cnt:1,Days:"1M",Start:false,Deleted:false});
        this.setState({Tariffs:this.state.Tariffs});
    }

    save() {
        var a = this.state.Tariffs;
        for(var i=0;i<a.length;i++)
        {
            var url = config.Backend+"/tariffs/";
            if(a[i]._id==-1)
            {
                url+='add?';
            }
            else
            {
                url+='edit?';
            }
            for(var key in a[i])
            {
                if(key=="_id")
                {
                    url+='&Id='+a[i][key];
                }
                else {
                    url+='&'+key+'='+a[i][key];
                }
            }
            url+='&CurrentUserId='+cookies.get('Id');
            jQuery.getJSON(url,function (res) {
                if(!res.Error)
                {
                }
                else
                {
                    alert(JSON.stringify(res));
                }
            }.bind(this))
        }
        alert("Изменения успешно сохранены")
    }

    handleInputChange(event,i) {
        this.state.Tariffs[i][event.target.name] = event.target.type!="checkbox" ? event.target.value : (event.target.checked ? 1 : 0);
        this.setState({
            Tariffs : this.state.Tariffs,
        })
    }

    load()
    {
        var url = config.Backend + '/users/findById?';
        if (!(cookies.get('Id') === undefined)) {
            url += 'Id=' + cookies.get('Id').toString();
        }
        jQuery.getJSON(url, function (res) {
            if (!res.Error) {
                if(res.Type=="Admin")
                  window.location.replace(window.location.origin + '/login');
                this.setState({User: res}, () => {
                    var url = config.Backend + '/tariffs/all?';
                    if (!(cookies.get('Id') === undefined)) {
                        url += '&CurrentUserId=' + cookies.get('Id').toString();
                    }
                    jQuery.getJSON(url, function (res) {
                        if (!res.Error) {
                            this.setState({Tariffs:res})
                        } else {
                            alert(JSON.stringify(res));
                        }
                    }.bind(this));
                });
            } else {
                window.location.replace(window.location.origin + '/login');
            }
        }.bind(this));
    }
    componentWillMount() {
        this.load();
    }
    TList() {
        var tariffs;
        if (this.state.Tariffs) {
            tariffs = this.state.Tariffs.map(function (elem, ind) {
                const i = ind;
                const x = elem;

                return (

                    <tr>
                        <td>
                    <input className={"form-control"} placeholder={"Название"} type={"text"} name={"Name"} value={this.state.Tariffs[i].Name} onChange={(e)=>this.handleInputChange(e,i)}/>
                        </td>
                        <td>
                    <input className={"form-control"} placeholder={"Цена"} type={"number"} name={"Price"} value={this.state.Tariffs[i].Price} onChange={(e)=>this.handleInputChange(e,i)}/>
                        </td>
                        <td>
                    <input className={"form-control"} placeholder={"Кол-во ботов для каждой соц сети"} type={"number"} name={"Cnt"} value={this.state.Tariffs[i].Cnt} onChange={(e)=>this.handleInputChange(e,i)}/>
                        </td>
                        <td>
                            <input className={"form-control"} placeholder={"Длительность"} type={"text"} name={"Days"} value={this.state.Tariffs[i].Days} onChange={(e)=>this.handleInputChange(e,i)}/>
                        </td>
                        <td>
                            <div className="wrap_filter_laber">
                            <label>Стартовый тариф</label>
                    <input title={"Стартовый тариф"} type={"checkbox"} name={"Start"} checked={this.state.Tariffs[i].Start} onChange={(e)=>this.handleInputChange(e,i)}/>
                    {this.state.Tariffs[i].Deleted ? <span>Помечен для удаления</span> : null}
                            </div>
                        </td>
                        <td>
                    <button className={"btn_table_delete"} onClick={()=>this.del(x._id,i)}></button>
                        </td>


                </tr>)
            }.bind(this))
        } else {
            tariffs = null;
        }
        return (<tbody>{tariffs}</tbody>)
    }

    AdminTop(){
        return(
            <div className="admin_top">
                <div className="logo">
                    <a href="/dashboard"><img src="/img/logo_chat.png" alt=""/></a>
                </div>
            </div>
        )
    }
    render() {
        return (
            <div>
                {this.AdminTop()}
                <AdminLeft/>
                <div className="AdminContent">
                    <div className="chart_full">

                            <Scrollbars
                                style={{ height: "90%" }}>
                        <div className="overlay_chart grow_main_post">
                            <button className={"btn_all"} onClick={this.add}>Добавить тариф</button>

                            <table className={"table table-striped mt_25"}>
                                <thead>
                                <tr>
                                    <th>Название</th>
                                    <th>Цена</th>
                                    <th>Кол-во ботов для каждой соц сети</th>
                                    <th>Длительность</th>
                                    <th>Стартовый тариф</th>
                                    <th>Удалить</th>
                                </tr>
                                </thead>
                            {this.TList()}
                            </table>

                            <button className={"btn_all bg_shadow"} onClick={this.save}>Сохранить изменения</button>
                        </div>
                            </Scrollbars>
                        </div>

                </div>
            </div>
        )
    }
}

export default Tariffs;