import React, { Component}  from 'react';
import jQuery from 'jquery';
import AdminLeft from "../AdminLeft";
import AdminTop from "../AdminTop";
import Cookies from 'universal-cookie';
import { Scrollbars } from 'react-custom-scrollbars';
import Loading from '../../Loading.js';

const cookies = new Cookies();
var config = require('../../config.js');

class Settings extends Loading {
    constructor(props) {
        super(props);
        this.state = {
            User: null,
            Settings: [],
            Wait: 0,
        }
        this.load = this.load.bind(this);
        this.save_main = this.save_main.bind(this);
        this.setup = this.setup.bind(this);
        this.change = config.change.bind(this);
    }

    save_main()
    {
        var s =this.state;
        for(var i in s.Settings)
        {
            this.setState({Wait:++this.state.Wait})
            config.getData('settings/edit',{Key:s.Settings[i],Value:s[s.Settings[i]]}).then(function (s) {
                this.setState({Wait:--this.state.Wait})
            }.bind(this),function (err) {
                this.setState({Wait:--this.state.Wait})
                alert(JSON.stringify(err));
            }.bind(this))
        }
    }
    setup()
    {
        var s =this.state;
        this.setState({Wait:++this.state.Wait})
        config.getData('settings/setup').then(function (s) {
            this.setState({Wait:--this.state.Wait});
            this.load();
        }.bind(this),function (err) {
            this.setState({Wait:--this.state.Wait});
            alert(JSON.stringify(err));
        }.bind(this))
    }
    load()
    {
        var s =this.state;
        var url = config.Backend +'/users/findById?';
        if (!(cookies.get('Id') === undefined))
        {
            url += 'Id=' + cookies.get('Id').toString();
        }
        this.setState({Wait:++this.state.Wait})
        jQuery.getJSON(url, function (res) {
            if (!res.Error && res.Type=="Super") {
                this.setState({User: res},()=>{
                    config.getData('settings/all').then(function (ss) {
                        for (var i in ss) {
                            this.setState({[ss[i].Key]: ss[i].Value});
                        }
                        this.setState({Wait:--this.state.Wait,Settings:ss.map(e=>e.Key)});
                    }.bind(this),function (err) {
                        this.setState({Wait:--this.state.Wait});
                        alert(JSON.stringify(err));
                    }.bind(this));
                });
            } else {
                window.location.replace(window.location.origin + '/login');
            }
        }.bind(this));
    }
    componentWillMount() {
        this.preload();
        this.load();
    }

    AdminTop(){
        return(
            <div className="admin_top">
                <div className="admin_logo_top">
                    <a href="/admin/main"></a>
                </div>
            </div>
        )
    }
    MailSettings()
    {
        var s =this.state;
        return ( <div>
            <div className="form-group">
                <label>SMTP Host</label>
                <input className={'chat_input'} type={"text"} placeholder={"host"} value={s.Mail.Host} onChange={config.change.bind(this)} name={"Mail.Host"}/>
            </div>
            <div className="form-group">
                <label>SMTP Port</label>
                <input className={'chat_input'} type={"text"} placeholder={"Port"} value={s.Mail.Port} onChange={config.change.bind(this)} name={"Mail.Port"}/>
            </div>
            {/*<div className="form-group">*/}
                {/*<label>SMTP Secure</label>*/}
                {/*<input className={'chat_input'} type={"checkbox"} checked={s.Mail.Secure} onChange={config.change.bind(this)} name={"Mail.Secure"}/>*/}
            {/*</div>*/}

            <div className="checkbox">
                <label>
                    <input type={"checkbox"} checked={s.Mail.Secure} onChange={config.change.bind(this)} name={"Mail.Secure"}/>SMTP Secure
                </label>
            </div>



            <div className="checkbox">
                <label>
                    <input type={"checkbox"} checked={s.Mail.Auth} onChange={config.change.bind(this)} name={"Mail.Auth"}/> SMTP Auth
                </label>
            </div>

            {s.Mail.Auth ? <div>
                <div className="form-group">
                    <label>SMTP User</label>
                        <input className={'chat_input'} type={"text"} placeholder={"user"} value={s.Mail.User} onChange={config.change.bind(this)} name={"Mail.User"}/>

                </div>


                <div className="form-group">
                    <label>SMTP User</label>
                    <input className={'chat_input'} type={"text"} placeholder={"user"} value={s.Mail.User} onChange={config.change.bind(this)} name={"Mail.User"}/>
                </div>
                <div className="form-group">
                    <label>SMTP Password</label>
                    <input className={'chat_input'} type={"password"} placeholder={"password"} value={s.Mail.Pass} onChange={config.change.bind(this)} name={"Mail.Pass"}/>
                </div>
            </div> : null}
            <div className="form-group">
                <label>SMTP From</label>
                <input className={'chat_input'} type={"text"} placeholder={"from"} value={s.Mail.From} onChange={config.change.bind(this)} name={"Mail.From"}/>
            </div>
        </div>)
    }
    LogoSettings()
    {
        // /img/logo_chat.png
        var s =this.state;
        return ( <div>
            <div className="form-group">
                <label>Logo</label>
                <input className={'chat_input'} type={"text"} placeholder={"src"} value={s.Logo} onChange={config.change.bind(this)} name={"Logo"}/>
                <input id={'avatar_settings'} type="file" accept="image/*" onChange={(e)=>this.loadLogo(e)} onClick={e=>{e.target.value=null}}   className="add_image_avatar_bot text-primary "/>
            </div>
        </div>)
    }
    loadLogo = (e) => {
        e.preventDefault();
        let reader = new FileReader();
        let file = e.target.files[0];
        reader.onloadend = () => {
            var url = reader.result;
            if(file) {
                var fd = new FormData;
                fd.append('File', file);
                this.setState({Wait:++this.state.Wait});
                jQuery.ajax({
                    method: "POST",
                    processData: false,
                    contentType: false,
                    url: config.Backend + "/content/file",
                    success: function (msg) {
                        this.state.Logo = msg;
                        this.setState({Wait:--this.state.Wait});
                        this.save_main();
                    }.bind(this),
                    data: fd,
                })
            }
        }
        reader.readAsDataURL(file)
    }
    TitleSettings()
    {
        var s =this.state;
        return ( <div>
            <div className="form-group">
                <label>Title</label>
                <input className={'chat_input'} type={"text"} value={s.Title} onChange={config.change.bind(this)} name={"Title"}/>
            </div>
        </div>)
    }
    render(){
        var s =this.state;

        return(
            <div>
                {this.Load()}
               <AdminTop/>
                <AdminLeft/>
                <div className="AdminContent">
                <Scrollbars
                    style={{ height: "90%" }}>
                    <div className="overlay_chart grow_main_post padding">
                {/*<div className="AdminContent">*/}
                        {s.Mail!==undefined ? this.MailSettings() : null}
                        {s.Logo!==undefined ? this.LogoSettings() : null}
                        {s.Title!==undefined ? this.TitleSettings() : null}
                        <button className={'btn_all bg_shadow  mr_10'} onClick={this.save_main}>Save</button>
                        <button className={'btn_all'} onClick={this.setup}>Reset/Setup Settings</button>
                </div>
                </Scrollbars>
            </div>
            </div>


        )
    }
}


export default Settings;
