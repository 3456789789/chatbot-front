import React, { Component}  from 'react';
import jQuery from 'jquery';
import {Bar, Pie, Line} from 'react-chartjs-2';
import AdminLeft from "../AdminLeft";
import AdminTop from "../AdminTop";
import Cookies from 'universal-cookie';
import { Scrollbars } from 'react-custom-scrollbars';
import ReactTooltip from 'react-tooltip'
import Loading from '../../Loading.js';
import Switch from 'react-toggle-switch';

const cookies = new Cookies();
var config = require('../../config.js');

class MainAdmin extends Loading {
    constructor(props) {
        super(props);
        this.state = {
            User: {},
            DefaultBotsData: [],
            MonthBotsData: [],
            MonthCnt: 7,
            Visits: 0,
            UniqueVisits: 0,
            VisitsArr:[],
            Users: 0,
            Bots: [],
            BotPeriod: "Week",
            VisitPeriod: "Week",
            Wait: 0,
            Tables: false,
            Table:"BotsTable"
        }

        this.change = config.change.bind(this);
        this.load_data = this.load_data.bind(this);
        this.select_period = this.select_period.bind(this);
    }
    load_data(x,route,period)
    {
        var p = config.period_obj(period);
        var s =this.state;

        this.setState({Wait:++this.state.Wait})
        config.getData(route,p).then(function (data) {
            this.setState({[x]:data,Wait:--this.state.Wait});
        }.bind(this),function (err) {
            this.setState({Wait:--this.state.Wait})
            alert(JSON.stringify(err));
        }.bind(this))
    }
    select_period(e)
    {
        this.change(e);
        switch (e.target.name)
        {
            case "BotPeriod":
            this.load_data("Bots","bot/time",e.target.value);
            break;
            case "VisitPeriod":
                this.load_data("VisitsArr","users/time_visits",e.target.value);
                break;
        }
    }
    labels(p_obj)
    {
        var labels = [];
        //var weekdays = ['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'];
        if(p_obj.Period=="Month")
        {
            var now = new Date().getMonth();
            var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July','August','September','October','November','December']
            for(var i=now-p_obj.Times+1;i<now+1;i++) {
                var x = i > 11 ? i - 12 : i;
                x = x <0 ? 12+i : x;
                labels.push(months[x])
            }
        }
        else if(p_obj.Period=="Day")
        {
            var now = new Date().valueOf();
            for(var i=p_obj.Times-1;i>=0;i--) {
                var x = new Date(now - i*1000*60*60*24).toDateString();
                labels.push(x)
            }
        }

        return labels;
    }
    labels_time(p_obj)
    {
        var times = [];
        var now = new Date().valueOf();
        for(var i=p_obj.Times-1;i>=0;i--) {
            if(p_obj.Period=="Month") {
                var x = now - i * 1000 * 60 * 60 * 24 * 31;
            }
            else if(p_obj.Period=="Day") {
                var x = now - i*1000*60*60*24;
            }
            times.push(x)
        }
        return times;
    }
    sort_data(data,labels_times)
    {
        var res = [];
        for(var i in labels_times)
        {
            res.push(0);
        }
        for(var i in data)
        {
            var nearIndex = 0;
            var min = 1000*60*60*24*900;
            for(var j in labels_times)
            {
                if(Math.abs(data[i]-labels_times[j])<min)
                {
                    min =  Math.abs(data[i]-labels_times[j]);
                    nearIndex = j;
                }
            }
            res[nearIndex]+=data[i].Amount || 1;
        }
        return res;
    }
    componentWillMount() {
        this.preload();
        var s =this.state;
        var url = config.Backend +'/users/findById?';
        if (!(cookies.get('Id') === undefined))
        {
            url += 'Id=' + cookies.get('Id').toString();
        }
        this.setState({Wait:++this.state.Wait});
        jQuery.getJSON(url, function (res) {
            if (!res.Error && res.Type=="Super") {
                this.setState({Wait:--this.state.Wait});

                this.setState({User: res},()=>{
                    var url = config.Backend + '/bot/dftBotStat?';
                    if (!(cookies.get('Id') === undefined))
                    {
                        url += 'CurrentUserId=' + cookies.get('Id').toString();
                    }
                    this.setState({Wait:++this.state.Wait});
                    jQuery.getJSON(url, function (res) {
                        if (!res.Error) {
                            this.setState({DefaultBotsData: res.Data,Wait:--this.state.Wait})
                        } else {
                            this.setState({Wait:--this.state.Wait});
                            alert(JSON.stringify(res));
                        }
                    }.bind(this));

                    this.setState({Wait:++this.state.Wait});
                    url = config.Backend + '/users/cnt'
                    jQuery.getJSON(url,function (res) {
                        this.setState({Users:res.Data,Wait:--this.state.Wait})
                    }.bind(this))
                    this.setState({Wait:++this.state.Wait});
                    url = config.Backend + '/users/visits'
                    jQuery.getJSON(url,function (res) {
                        this.setState({Visits:res.Cnt,Wait:--this.state.Wait})
                        this.setState({UniqueVisits:res.Unique})
                    }.bind(this))
                    this.load_data("Bots",'bot/time',this.state.BotPeriod);
                    this.load_data("VisitsArr","users/time_visits",this.state.VisitPeriod);
                });
            } else {
                window.location.replace(window.location.origin + '/login');
            }
        }.bind(this));
    }

    AllDefaultBots(data){
        var data = {
            labels: data.map(e=>e.Name),
            datasets: [
                {
                    label: 'Number of bots created based on templates',
                    backgroundColor: 'rgba(255,99,132,0.2)',
                    borderColor: 'rgba(255,99,132,1)',
                    borderWidth: 1,
                    hoverBackgroundColor: 'rgba(255,99,132,0.4)',
                    hoverBorderColor: 'rgba(255,99,132,1)',
                    data: data.map(e=>e.Cnt)
                }
            ]
        };
        return(
            <div className="chart_block">
                <div className="analyze_header">
                    <div className="analyze_header_caption" style={{width: '370px'}}>
                        <h2 className="pc25">Bots created based on templates</h2>
                    </div>
                    <div className="analyze_header_hint">
                        <span  data-tip data-for='listBots' className="analyze_header_hint_icon">
                            <ReactTooltip id='listBots' aria-haspopup='true' role='example'>
                                <p>list of bots created from templates</p>
                            </ReactTooltip>
                        </span>
                    </div>
                </div>

                <Bar
                    data={data}
                    width={100}
                    height={180}
                    options={{
                        maintainAspectRatio: false
                    }}
                />
            </div>
        )
    }





    AllBots(){
        var s =this.state;
        var p_obj = config.period_obj(s.BotPeriod);
        var cols = this.sort_data(s.Bots.map((e)=>e.created_at),this.labels_time(p_obj));
        var dataLine = {
            labels: this.labels(p_obj),
            datasets: [
                {
                    label: 'Number of created bots',
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: 'rgba(75,192,192,0.4)',
                    borderColor: 'rgba(75,192,192,1)',
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: 'rgba(75,192,192,1)',
                    pointBackgroundColor: '#fff',
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: 'rgba(75,192,192,1)',
                    pointHoverBorderColor: 'rgba(220,220,220,1)',
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: cols,
                }
            ]
        };
        return(
            <div>
                <div className="analyze_header">
                    <div className="analyze_header_caption" style={{width: '120px'}}>
                        <h2 className={'pc25'}>All bots</h2>
                    </div>
                    <div className="analyze_header_hint">
                        <span  data-tip data-for='Popular_buttons' className="analyze_header_hint_icon">
                            <ReactTooltip id='Popular_buttons' aria-haspopup='true' role='example'>
                                <p>list of all bots</p>
                            </ReactTooltip>
                        </span>
                    </div>
                </div>


                <div className="form-group pc25">
                    <select value={s.BotPeriod} name="BotPeriod" onChange={this.select_period} className="form-control">
                        <option value="Week">during the week</option>
                        <option value="15Day">for 15 days</option>
                        <option value="Month">per month</option>
                        <option value="3Month">for 3 months</option>
                        <option value="Half Year">for half a year</option>
                        <option value="Year">in a year</option>
                    </select>
                </div>
                <Line data={dataLine}
                      height={90}
                      options={{
                          maintainAspectRatio: false
                      }}/>
            </div>
        )
    }
    Visits(){
        var s =this.state;
        var p_obj = config.period_obj(s.VisitPeriod);
        var cols = this.sort_data(s.VisitsArr.map((e)=>e.created_at),this.labels_time(p_obj));
        var users = [];
        var data = s.VisitsArr.map((e)=>{return{Time:e.created_at,User:e.Ip}});
        var labels_times = config.labels_time(p_obj);
        for(var k=0;k<labels_times.length;k++)
        {
            users.push({Cnt:0,Users:[]});
        }
        for(var i=0;i<data.length;i++)
        {
            var nearIndex = 0;
            var min = 1000*60*60*24*900;
            for(var j in labels_times)
            {
                if(Math.abs(data[i].Time-labels_times[j])<min)
                {
                    min =  Math.abs(data[i].Time-labels_times[j]);
                    nearIndex = j;
                }
            }
            if(users[nearIndex].Users.indexOf(data[i].User)==-1)
            {
                users[nearIndex].Cnt+=1;
                users[nearIndex].Users.push(data[i].User);
            }
        }
        var dataLine = {
            labels: this.labels(p_obj),
            datasets: [
                {
                    label: 'Visits',
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: 'rgba(75,192,192,0.4)',
                    borderColor: 'rgba(75,192,192,1)',
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: 'rgba(75,192,192,1)',
                    pointBackgroundColor: '#fff',
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: 'rgba(75,192,192,1)',
                    pointHoverBorderColor: 'rgba(220,220,220,1)',
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: cols,
                },
                {
                    label: 'Unique Visits',
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: 'rgba(175,102,50,0.4)',
                    borderColor: 'rgba(175,102,50,1)',
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: 'rgba(175,102,50,1)',
                    pointBackgroundColor: '#fff',
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: 'rgba(175,102,50,1)',
                    pointHoverBorderColor: 'rgba(175,102,50,1)',
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: users.map(e=>e.Cnt),
                }
            ]
        };
        return(
            <div>
                <div className="analyze_header">
                    <div className="analyze_header_caption" style={{width: '120px'}}>
                        <h2 className={'pc25'}>Visits</h2>
                    </div>
                    <div className="analyze_header_hint">
                        <span  data-tip data-for='Visits' className="analyze_header_hint_icon">
                            <ReactTooltip id='Visits' aria-haspopup='true' role='example'>
                                <p>Show visits</p>
                            </ReactTooltip>
                        </span>
                    </div>
                </div>


                <div className="form-group pc25">
                    <select value={s.VisitPeriod} name="VisitPeriod" onChange={this.select_period} className="form-control">
                        <option value="Week">during the week</option>
                        <option value="15Day">for 15 days</option>
                        <option value="Month">per month</option>
                        <option value="3Month">for 3 months</option>
                        <option value="Half Year">for half a year</option>
                        <option value="Year">in a year</option>
                    </select>
                </div>

                <Line data={dataLine}
                      height={90}
                      options={{
                          maintainAspectRatio: false
                      }}

                />

            </div>
        )
    }


    //
    // AdminTop(){
    //     return(
    //         <div className="admin_top">
    //             <div className="admin_logo_top">
    //                 <a href="/admin/main"></a>
    //             </div>
    //         </div>
    //     )
    // }


    BotsTable() {
        var s =this.state;
        var p_obj = config.period_obj(s.BotPeriod,true);
        var labels = config.labels(p_obj);
        labels.reverse();
        var labels_time = config.labels_time(p_obj);
        labels_time.reverse();
        var cols = this.sort_data(s.Bots.map((e)=>e.created_at),labels_time);
        var rows = labels.map((e,i)=>{
            return (<tr>
                <td>{e}</td>
                <td className={'tac'}>{cols[i]}</td>
            </tr>)
        })
        return (<div>
            <div className="form-group">
                <select value={s.BotPeriod} name="BotPeriod" onChange={this.select_period} className="chat_input">
                    <option value="Week">during the week</option>
                    <option value="15Day">for 15 days</option>
                    <option value="Month">per month</option>
                    <option value="3Month">for 3 months</option>
                    <option value="Half Year">for half a year</option>
                    <option value="Year">in a year</option>
                </select>
            </div>
            <table className={'table'}>
                <thead>
                <tr>
                    <th>Time</th>
                    <th className={'tac'}>Bots</th>
                </tr>
                </thead>
                <tbody>
                {rows}
                </tbody>
            </table>
        </div>)
    }
    VisitsTable()
    {
        var s =this.state;
        var p_obj = config.period_obj(s.VisitPeriod,true);
        var labels = config.labels(p_obj);
        labels.reverse();
        var labels_time = config.labels_time(p_obj);
        labels_time.reverse();
        var cols = this.sort_data(s.VisitsArr.map((e)=>e.created_at),labels_time);
        var users = [];
        var data = s.VisitsArr.map((e)=>{return{Time:e.created_at,User:e.Ip}});
        var labels_times = labels_time;
        for(var k=0;k<labels_times.length;k++)
        {
            users.push({Cnt:0,Users:[]});
        }
        for(var i=0;i<data.length;i++)
        {
            var nearIndex = 0;
            var min = 1000*60*60*24*900;
            for(var j in labels_times)
            {
                if(Math.abs(data[i].Time-labels_times[j])<min)
                {
                    min =  Math.abs(data[i].Time-labels_times[j]);
                    nearIndex = j;
                }
            }
            if(users[nearIndex].Users.indexOf(data[i].User)==-1)
            {
                users[nearIndex].Cnt+=1;
                users[nearIndex].Users.push(data[i].User);
            }
        }
        users = users.map(e=>e.Cnt);
        var rows = labels.map((e,i)=>{
            return (
                <tr>
                <td>{e}</td>
                <td className={'tac'}>{cols[i]}</td>
                <td className={'tac'}>{users[i]}</td>
            </tr>)
        })
        return (
            <div>
                <div className="form-group">
                    <select value={s.VisitPeriod} name="VisitPeriod" onChange={this.select_period} className="chat_input">
                        <option value="Week">during the week</option>
                        <option value="15Day">for 15 days</option>
                        <option value="Month">per month</option>
                        <option value="3Month">for 3 months</option>
                        <option value="Half Year">for half a year</option>
                        <option value="Year">in a year</option>
                    </select>
                </div>
            <table className={'table'}>
                <thead>
                <tr>
                    <th>Time</th>
                    <th className={'tac'}>Visits</th>
                    <th className={'tac'}>Unique Visits</th>
                </tr>
                </thead>
                <tbody>
                {rows}
                </tbody>
            </table>
        </div>)
    }
    TemplatesTable() {
        var s =this.state;
        var rows = s.DefaultBotsData.map((e,i)=>{
            return (
                <tr>
                <td>{e.Name}</td>
                <td className={'tac'}>{e.Cnt}</td>
            </tr>)
        })
        return (
            <div>
                <table className={'table'}>
                    <thead>
                    <tr>
                        <th>Template</th>
                        <th className={'tac'}>Bots</th>
                    </tr>
                    </thead>
                    <tbody>
                    {rows}
                    </tbody>
                </table>
            </div>)
    }
    Tables() {
        var s=this.state;
        return (<div className={'chart_full pd_15'}>

            <button className="btn_all mr_2" onClick={() => this.setState({Table:"BotsTable"})}>Bots creation</button>
            <button className="btn_all mr_2" onClick={() => this.setState({Table:"TemplatesTable"})}>Template bots</button>
            <button className="btn_all" onClick={() => this.setState({Table:"VisitsTable"})}>Visits</button>

            {this[s.Table]()}
     </div>)
    }
    render(){
        var s =this.state;
        var ShowDefaultBots = this.state.DefaultBotsData.length>0;

        /*
        <div className="chart_right">
                                </div>
                                                            {this.state.Wait ? "Wait" : null}
         */
        return(
            <div>
                {this.Load()}
               <AdminTop/>
                <AdminLeft/>
                <div className="AdminContent">
                    <div className="chart_full">
                        <div className="grow_main ">
                            <Scrollbars
                                style={{ height: "90%" }}>
                                <div className="form-check form-check-inline pd_15">

                                    <Switch
                                        onClick={()=>this.setState({Tables:!s.Tables})}
                                        on={s.Tables}
                                        className={'form-check-input'}
                                        id={"Tables"}/>
                                    <label  className={'ml_10 form-check-label'} onClick={()=>this.setState({Tables:!s.Tables})}
                                           htmlFor="Tables">Tables/Graphs</label>
                                </div>

                                {s.Tables ? this.Tables() :
                                    <div>

                            {ShowDefaultBots ?
                            <div className="chart_main">
                                    {this.AllDefaultBots(this.state.DefaultBotsData)}
                            </div>

                                : null}
                            <div className="chart_main h450 mt_25">
                                <div className="chart_left">
                                    {this.AllBots()}
                                </div>
                                <div className="chart_right">
                                    {this.Visits()}
                                    {/*<ul class="list-group">
                                        <li class="list-group-item">
                                            <span class="badge">{this.state.Visits}</span>
                                            Number of visitors
                                        </li>
                                        <li class="list-group-item">
                                            <span class="badge">{this.state.UniqueVisits}</span>
                                            Number of unique site visitors
                                        </li>
                                        <li class="list-group-item">
                                            <span class="badge">{this.state.Users}</span>
                                            Number of registered users
                                        </li>
                                    </ul>*/}
                                </div>
                            </div>
                                    </div>}
                            </Scrollbars>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


export default MainAdmin;
