import jQuery from 'jquery';
import Cookies from 'universal-cookie';
const cookies = new Cookies();

//var config = {
    //Backend: 'http://localhost:4800',
//export var Backend= 'https://back.i-solutions.org';
export var Backend= 'https://octavabackend.octacore.io'

    //Backend: 'https://bot-server-side.herokuapp.com',
//export var Frontend='https://front.i-solutions.org';
export var Frontend=window.location.origin;
//export var Frontend='http://localhost:3000';

 export var FbAppId= "643322866004992";
    //FbAppId: "-",
    export var FbApi= "https://graph.facebook.com/v2.11";

    export var SlackId="279361283362.278854196865";
    export var SlackSecret= "99747ae2f6ecf9e7307881aa7d61152a";
    export var SlackToken= "U59SF05yD5zoeiN27hFIM2JK";
    export var SlackApi= 'https://slack.com/api';

    //RedirectLoginUri: 'http://localhost:3000/login',
    //RedirectAddAppUri: 'http://localhost:3000/chat/configure',
    export var  RedirectLoginUri= Frontend+'/login';
    export var  RedirectAddAppUri= Frontend+'/chat/configure';
//}

//module.exports = config;
export function find(arr,search) {
    var res = [];
    arr.forEach(e=>{
        var add = true;
        for(var key in search) {
            if (e[key]!=search[key]) {
                add =false;
                break;
            }
        }
        if(add)
            res.push(e);
    })
    return res;
}
export function accessDenied (uId,bot)
{
    alert("Access denied!")
    var r= firstRight(uId,bot);
    if(r)
    {
        window.location.href = window.location.origin + '/chat/'+r.toLowerCase();
    }
    else
    {
        window.location.href = window.location.origin + '/profile/setting';
    }
}
export function firstRight(uId,bot)
{
    if(bot.User.toString()!=uId)
    {
        if(bot.Share)
        {
            var ind = bot.Share.Users.indexOf(uId);
            if(ind==-1)
                return false;

            var arr = ['Build','Broadcast','Configure','Grow','Analyze','RTC','Share']
            for(var i=0;i<arr.length;i++)
            {
                if(bot.Share.UsersWithRights[ind].Rights.indexOf(arr[i])!=-1)
                    return arr[i];
            }
        }
        else
        {
            return false;
        }
    }
    return false;
}
//module.exports.firstRight = firstRight;
export  function load_data(x,route,period,data)
{
    var s =this.state;
    var p = period_obj(period);
    if(data)//additional data to post query
    {
     for(var key in data)
     {
         p[key] = data[key];
     }
    }
    this.setState({Wait:++this.state.Wait})
    getData(route,p).then(function (data) {
        this.setState({[x]:data,Wait:--this.state.Wait});
    }.bind(this),function (err) {
        this.setState({Wait:--this.state.Wait})
        alert(JSON.stringify(err));
    }.bind(this))
}

export  function labels(p_obj)
{
    var labels = [];
    //var weekdays = ['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'];
    if(p_obj.Period=="Month")
    {
        var now = new Date().getMonth();
        var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July','August','September','October','November','December']
        for(var i=now-p_obj.Times+1;i<now+1;i++) {
            var x = i > 11 ? i - 12 : i;
            x = x <0 ? 12+i : x;
            labels.push(months[x])
        }
    }
    else if(p_obj.Period=="Day")
    {
        var now = new Date().valueOf();
        for(var i=p_obj.Times-1;i>=0;i--) {
            var x = new Date(now - i*1000*60*60*24).toDateString();
            labels.push(x)
        }
    }

    return labels;
}
export  function labels_time(p_obj)
{
    var times = [];
    var now = new Date().valueOf();
    for(var i=p_obj.Times-1;i>=0;i--) {
        if(p_obj.Period=="Month") {
            var x = now - i * 1000 * 60 * 60 * 24 * 31;
        }
        else if(p_obj.Period=="Day") {
            var x = now - i*1000*60*60*24;
        }
        times.push(x)
    }
    return times;
}
export  function sort_data(data,labels_times)
{
    var res = [];
    for(var k=0;k<labels_times.length;k++)
    {
        res.push(0);
    }
    for(var i=0;i<data.length;i++)
    {
        var nearIndex = 0;
        var min = 1000*60*60*24*900;
        for(var j in labels_times)
        {
            if(Math.abs(data[i]-labels_times[j])<min)
            {
                min =  Math.abs(data[i]-labels_times[j]);
                nearIndex = j;
            }
        }
        res[nearIndex]+=data[i].Amount || 1;
    }
    return res;
}
export  function period_obj(period,per_days)
{
    switch (period)
    {
        case "Week": return {Period:"Day",Times:7}
        case "15Day": return {Period:"Day",Times:15}
        case "Month": return {Period:"Day",Times:31}
        case "3Month": if(per_days)return {Period:"Day",Times:93};
            return {Period:"Month",Times:3}
        case "Half Year": if(per_days)return {Period:"Day",Times:180};
            return {Period:"Month",Times:6}
        case "Year":if(per_days)return {Period:"Day",Times:360};
        return {Period:"Month",Times:12}
    }
}
export  function checkRights(uId,bot,right)
{
    if(bot.User.toString()!=uId)
    {
        if(bot.Share)
        {
            var i = bot.Share.Users.indexOf(uId);
            if(i==-1)
                return false;

            if(bot.Share.UsersWithRights[i].Rights.indexOf(right)==-1)
                return false;
        }
        else
        {
            return false;
        }
    }
    return true;
}
export  function checkDialog(uId,bot) {
    if(bot.User.toString()!=uId)
    {
        if(bot.Share)
        {
            var i = bot.Share.Users.indexOf(uId);
            if(i==-1)
                return false;

                return bot.Share.UsersWithRights[i].Chat;
        }
        else
        {
            return false;
        }
    }
    return false;
}
export function getData(route,body) {
    return new Promise(function(resolve, reject) {
        var data = body ? body : {};
        if(!data.Id)
        data.Id = cookies.get("Id");
        if(!data.Bot)
        data.Bot = cookies.get("BotId");
        jQuery.ajax({
            method: "POST",
            contentType: 'application/json',
            url: Backend +"/"+route,
            success: function (res) {
                if (res.Error===undefined) {
                    resolve(res);
                } else {
                    reject(res);
                }
            },
            error:function (err) {
                if(err.readyState!=0)
                {
                    reject(err);
                }
            },
            data: JSON.stringify(data)
        })
    })
}
export  function change(event,i) {
    var arr =event.target.name.split(".");
    if(arr.length==1) {
        this.setState({
            [event.target.name]: event.target.type != "checkbox" ? event.target.value : (event.target.checked ? true : false)
        })
    }
    else
    {
        if(i===undefined) {
            var val = event.target.type != "checkbox" ? event.target.value : (event.target.checked ? true : false);
            function edit(end,num,max,val) {
                if(num==max)
                {
                    end[arr[max]] = val;
                }
                else
                {
                    edit(end[arr[num]],++num,max,val);
                }
            }
            edit(this.state[arr[0]],1,arr.length-1,val);
            //alert(JSON.stringify(this.state[arr[0]));
            this.setState({
                [arr[0]]: this.state[arr[0]]
            })
        }
        else
        {
            var val = event.target.type != "checkbox" ? event.target.value : (event.target.checked ? true : false);
            function edit(end,num,max,val) {
                if(num==max)
                {
                    end[arr[max]] = val;
                }
                else
                {
                    edit(end[arr[num]],++num,max,val);
                }
            }
            edit(this.state[arr[0]][i],1,arr.length-1,val);
            //alert(JSON.stringify(this.state[arr[0]][i]));
            this.setState({
                [arr[0]]: this.state[arr[0]]
            })
        }
    }

}
export function xor(ai) {
    if(ai) {
        var f = "";
        for (var c = 0; c < ai.length; c++) {
            f += String.fromCharCode(ai.charCodeAt(c) ^ (1 + (ai.length - c) % 32))
        }
        return f
    }
    else return ai;
}
export function indexOf(arr,obj) {
    for(var i=0;i<arr.length;i++)
    {
        var find =true;
        for(var key in obj)
        {
            if(obj[key]!=arr[i][key])
                find = false;
        }
        if(find)
            return i;
    }
    return -1;
}

