import jQuery from 'jquery';
import Cookies from 'universal-cookie';
const cookies = new Cookies();
var config = require('./config');

export function getData(route,body) {
    return new Promise(function(resolve, reject) {
        var data = body ? body : {};
        data.Id = cookies.get("Id");
        jQuery.ajax({
            method: "POST",
            contentType: 'application/json',
            url: config.Backend +"/"+route,
            success: function (res) {
                if (res.Error===undefined) {
                    resolve(res);
                } else {
                    reject(res);
                }
            },
            error:function (err) {
                reject(err);
            },
            data: JSON.stringify(data)
        })
    })
}