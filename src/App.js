import React, { Component } from 'react';
import './style/css/main.min.css';
import LoginForm from './public/login/login.js';
import RegisterForm from './public/login/signup.js';
import HomeForm from './public/home.js';
//import UsersForm from './public/users/all.js';
import ResetPasswordForm from './public/login/resetPassword.js';

// settingProfile
import ProfileSettings from './public/chat/ProfileSettings/SettingProfile.js';

// Build
import Build from './public/chat/Build/Build.js';

// Build
import Broadcast from './public/chat/Broadcast/Broadcast.js';

// Bots
import Bots from './public/chat/Bots/Bots.js';

// Bots description
import BotsDescription from './public/chat/Bots/Description.js';

// Configure
import Configure from './public/chat/Configure/Configure.js';

// Grow
import Grow from './public/chat/Grow/Grow.js'

// Grow
import RTC from './public/chat/Grow/RTC.js'

// Analyze
import Analize from './public/chat/Analyze/Alalyze.js';

// Terms
import Terms from './public/login/Terms.js';


// Admin login
import AdminLogin from './admin/login/login.js';

// Admin create bot
import AdminCreateBot from './admin/createBot/createBot.js'

// Admin main
import AdminMain from './admin/chart/chartMain.js'
import AdminNluList from './admin/nlu/list.js'
import AdminNluEdit from './admin/nlu/edit.js'
import AdminNluAdd from './admin/nlu/add.js'
import AdminPage from './admin/page/page.js'

// Admin settings
import AdminSetting from './admin/settings/admin-settings'
import AdminUsers from './admin/users/users'

// Manual
import Manual from './public/chat/manual/manual.js'

import Share from './public/chat/Share/Share.js'

// Manual Build
import ManualBuild from './public/chat/manual/manual_build.js'
import ManualRestrictions from './public/chat/manual/manual_restrictions.js'
import RestrictionsFb from './public/chat/manual/r_fb.js'
import RestrictionsSlack from './public/chat/manual/r_slack.js'
import RestrictionsSkype from './public/chat/manual/r_skype.js'
import RestrictionsSite from './public/chat/manual/r_site.js'

// Manual Build Api skype
import ApiSkype from './public/chat/manual/api_skype_site.js'
import CheckApi from './public/chat/manual/check-api.js'

// Manual Build Api site
import ApiSlack from './public/chat/manual/api_slack.js'
import History from './public/chat/History/History.js'
// Manual Build Api site
import ApiFb from './public/chat/manual/api_fb.js'

import Keywords from './public/chat/manual/keywords.js'

// Manual configure
import ManualConfigure from './public/chat/manual/manual_configure.js'
import Page from './public/page.js'


// Manual configurate facebook
import Entry_facebook from './public/chat/manual/entry_facebook.js'

// Manual configurate skype
import Entry_skype from './public/chat/manual/entry_skype.js'

// Manual configurate slack
import Entry_slack from './public/chat/manual/entry_slack.js'
import Entry_slack_new from './public/chat/manual/entry_slack_new.js'

// Manual configurate site
import Entry_site from './public/chat/manual/entry_site.js'

import Verify from './public/login/verify.js'
import ResetToken from './public/login/resetWithToken.js'

//Groups
import Groups from './public/chat/Build/Groups.js';

import 'bootstrap/dist/js/bootstrap';
import { Switch, Route, Link } from 'react-router-dom'
import jQuery from 'jquery';
import Cookies from 'universal-cookie';
const cookies = new Cookies();
var config = require('./config.js');

class App extends Component {

    constructor(props) {
        super(props);

        this.state = {
            Type: '',
        }

        this.exit = this.exit.bind(this);
    }

    componentWillMount() {
        if (cookies.get('visit') === undefined)
        {
            var url = config.Backend + '/users/visit?';
            if (!(cookies.get('Id') === undefined)) {
                url += 'Id=' + cookies.get('Id').toString();
            }

            jQuery.getJSON(url, function (res) {
                if (!res.Error) {
                    var exp = new Date();
                    exp.setMinutes(exp.getMinutes() + 30);
                    cookies.set('visit', true, {path: '/', expires: exp});
                }
            }.bind(this));
        }

        if (!(cookies.get('Exit') === undefined))
        {
            cookies.remove('Id');
            cookies.remove('Exit');
        }
    }

    exit(event) {

        cookies.remove('Id');
        window.location.replace(window.location.origin + '/login');
    }

    Main() {
        return (
            <main>
                <Switch>
                    <Route exact path='/' component={LoginForm} />
                    <Route path='/login' component={LoginForm} />
                    <Route path='/register' component={RegisterForm} />
                    <Route path='/page/:slug' component={Page} />
                    <Route exact path='/home' component={HomeForm} />
                    <Route exact path='/reset' component={ResetPasswordForm} />
                    <Route exact path='/reset/:token' component={ResetToken} />
                    <Route exact path='/profile/setting' component={ProfileSettings} />
                    <Route exact path='/chat/build' component={Groups} />
                    <Route exact path='/chat/broadcast' component={Broadcast} />
                    <Route exact path='/chat/build/:id' component={Build} />
                    <Route exact path='/dashboard' component={Bots} />
                    <Route exact path='/chat/configure' component={Configure} />
                    <Route exact path='/chat/grow' component={Grow} />
                    <Route exact path='/chat/rtc' component={RTC} />
                    <Route exact path='/chat/share' component={Share} />
                    <Route exact path='/chat/analyze' component={Analize} />
                    <Route exact path='/chat/history' component={History} />
                    <Route exact path='/admin/login' component={AdminLogin} />
                    <Route exact path='/admin/main' component={AdminMain} />
                    <Route exact path='/admin/settings' component={AdminSetting} />
                    <Route exact path='/admin/users' component={AdminUsers} />
                    <Route exact path='/admin/create-bot' component={AdminCreateBot} />
                    <Route exact path='/admin/nlu/list' component={AdminNluList} />
                    <Route exact path='/admin/nlu/edit/:key' component={AdminNluEdit} />
                    <Route exact path='/admin/nlu/add' component={AdminNluAdd} />
                    <Route exact path='/admin/pages' component={AdminPage} />

                    <Route exact path='/chat/bot-description' component={BotsDescription} />



                    {/*manual*/}
                    <Route exact path='/manual' component={Manual} />
                    <Route exact path='/verify' component={Verify} />
                    <Route exact path='/manual/build' component={ManualBuild} />
                    <Route exact path='/manual/build/api-slack' component={ApiSlack} />
                    <Route exact path='/manual/build/api-skype' component={ApiSkype} />
                    <Route exact path='/manual/build/api-fb' component={ApiFb} />
                    <Route exact path='/manual/build/keywords' component={Keywords} />
                    <Route exact path='/manual/build/check-api' component={CheckApi} />

                    <Route exact path='/manual/configure' component={ManualConfigure} />
                    <Route exact path='/manual/configure/facebook' component={Entry_facebook} />
                    <Route exact path='/manual/configure/skype' component={Entry_skype} />
                    <Route exact path='/manual/configure/slack' component={Entry_slack_new} />
                    <Route exact path='/manual/configure/site' component={Entry_site} />

                    <Route exact path='/manual/restrictions' component={ManualRestrictions} />
                    <Route exact path='/manual/restrictions/fb' component={RestrictionsFb} />
                    <Route exact path='/manual/restrictions/slack' component={RestrictionsSlack}/>
                    <Route exact path='/manual/restrictions/skype' component={RestrictionsSkype}/>
                    <Route exact path='/manual/restrictions/site' component={RestrictionsSite} />

                </Switch>
            </main>
        );
    }



    render() {
        const Header = <div/>;

        return (
            <div className="App">

                {this.Main()}
            </div>
        );
    }
}

export default App;
