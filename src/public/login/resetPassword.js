import React, { Component}  from 'react';
import jQuery from 'jquery'; 
import Cookies from 'universal-cookie';
import Loading from '../../Loading.js';
const cookies = new Cookies();
var config = require('../../config.js');

class ResetPasswordForm extends Loading{
    constructor(props) {
    super(props);

    this.state = {
                        Email: "",
                        Email_valid: true,
                        Response_txt: '',
                        Error: false,
        Wait: 0
      
    }
    this.submitFormHandler = this.submitFormHandler.bind(this);
                this.handleInputChange = this.handleInputChange.bind(this);
                this.handleInputFocus = this.handleInputFocus.bind(this);
  }
  
  
  
  
  submitFormHandler(event) {
        const Email_filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
               if (! Email_filter.test(this.state.Email)) {
        this.setState({Email_valid: false});
        } else {
        var url = config.Backend+'/users/resetPassword?Email='+this.state.Email;
        
               
        this.setState({Wait:++this.state.Wait});jQuery.getJSON(url,function (res){
                       this.setState({Wait:--this.state.Wait});

                       if (res.Error)
            {
                this.setState({Response_txt : res.Error});
                this.setState({Error : true});
            } else {
               this.setState({Error: false});
               
               this.setState({Response_txt : res.Ok}); 
            }
            console.log(res);
        }.bind(this));
        
        }
        event.preventDefault();
        }

        handleInputChange(event) {
        this.setState({
        [event.target.name]: event.target.value
        })
        }

        handleInputFocus(event) {
        this.setState({
        [event.target.name + '_valid'] : true
        })
        }
  
Response () {
        const danger = this.state.Error ? <div className="text-danger">{this.state.Response_txt}</div> : <div>{this.state.Response_txt}</div>;
        return (<div className="form-group">
    {danger}
    </div>
                        )
        }
Email () {
        const danger = this.state.Email_valid ? '' : <div className="text-danger">Check E-mail address.</div>;
                return (
<div className="form-group">
    <input type="Email" className="" placeholder="Email" value={this.state.Email} name="Email" onChange={this.handleInputChange} onFocus={this.handleInputFocus} required/>
    {danger}
</div>
                        )
        }



        render () {
        return (
            <div className="background_comein">
                {this.Load()}
                <div className="form_block_login">
                    <p>Forgot something?<br />Type email to recover pass</p>
                        <form onSubmit={this.submitFormHandler} className="login">
                            {this.Response()}
                            {this.Email()}
                            <button type="submit" className="btn_facebook ">Reset password</button>
                        </form>
                    <p>Back to <a href="/login">Login</a></p>
                </div>
            </div>
                );
                    }
                        }

export default ResetPasswordForm;
