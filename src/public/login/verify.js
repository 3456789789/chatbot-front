import React, { Component}  from 'react';
import jQuery from 'jquery';
import Cookies from 'universal-cookie';
import Loading from '../../Loading.js';
const cookies = new Cookies();
var config = require('../../config.js');

class Verify extends Loading{
    constructor(props) {
        super(props);

        this.state = {
            Wait: 0
        }

    }

    componentWillMount() {
        this.preload();
        const params = new URLSearchParams(this.props.location.search);
        const id = params.get('Id');
        const code = params.get('Code');
        if(code && id)
        {
            var url = config.Backend + '/users/verify?Id='+id+'&Code='+code;
            this.setState({Wait:++this.state.Wait});jQuery.getJSON(url,function (res) {
            this.setState({Wait:--this.state.Wait});
                if(!res.Error)
                {
                    alert("You email was verifed!");
                    var exp = new Date();
                    exp.setDate(exp.getDate() + 3);
                    cookies.set('Id',res.User._id, { path: '/' ,expires:exp});
                    if (res.User.Type=="Admin") {
                                window.location.replace(window.location.origin + '/dashboard');
                            } else if (res.User.Type=="Super") {
                                window.location.replace(window.location.origin + '/admin/main');
                            }
                }
                else{
                    alert(JSON.stringify(res))
                }
            }.bind(this))
        }

    }
    render () {
        return (
            <div className="background_comein">
                {this.Load()}
            </div>
        );
    }
}

export default Verify;
