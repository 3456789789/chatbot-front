import React, { Component}  from 'react';
import Cookies from 'universal-cookie';
import Loading from '../../Loading.js';
const cookies = new Cookies();
var config = require('../../config.js');

class ResetPasswordForm extends Loading{
    constructor(props) {
        super(props);

        this.state = {
            Password: "",
            RPassword: "",
            Response_txt: '',
            Error: false,
            Wait: 0
        }
        this.submitFormHandler = this.submitFormHandler.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleInputFocus = this.handleInputFocus.bind(this);
    }

    submitFormHandler(event) {
        var s =this.state;
        if(s.Password.length<8)
        {
            this.setState({Error: true,Response_txt : "Password too short"});
        }
        else if(s.Password != s.RPassword)
        {
            this.setState({Error: true,Response_txt : "Passwords not match"});
        }
        else
        {
            this.setState({Wait:++this.state.Wait})
            config.getData("users/resetByToken",{Password:config.xor(s.Password),Token:this.props.match.params.token}).then(function (res) {
                this.setState({Wait:--this.state.Wait,Error: false,Response_txt : "Password was changed!"});
            }.bind(this),function (err) {
                this.setState({Wait:--this.state.Wait,Error: true,Response_txt : JSON.stringify(err)});
            }.bind(this))
        }
        event.preventDefault();
    }

    handleInputChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    handleInputFocus(event) {
        this.setState({
            [event.target.name + '_valid'] : true
        })
    }

    Response () {
        const danger = this.state.Error ? <div className="text-danger">{this.state.Response_txt}</div> : <div>{this.state.Response_txt}</div>;
        return (<div className="form-group">
                {danger}
            </div>
        )
    }

    render () {
        return (
            <div className="background_comein">
                {this.Load()}
                <div className="form_block_login">
                    <p>Type new password </p>
                    <form onSubmit={this.submitFormHandler} className="login">
                        {this.Response()}
                        <div className="form-group">
                                <input type="password" className="" placeholder="password" value={this.state.Password} name="Password" onChange={this.handleInputChange} onFocus={this.handleInputFocus} required/>
                                <input type="password" className="" placeholder="repeat password" value={this.state.RPassword} name="RPassword" onChange={this.handleInputChange} onFocus={this.handleInputFocus} required/>
                        </div>
                        <button type="submit" className="btn_facebook ">Reset password</button>
                    </form>
                    <p>Back to <a href="/login">Login</a></p>
                </div>
            </div>
        );
    }
}

export default ResetPasswordForm;
