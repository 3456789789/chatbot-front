import React, { Component}  from 'react';
import jQuery from 'jquery';
import Cookies from 'universal-cookie';
import FacebookLogin from 'react-facebook-login';
import Loading from '../../Loading.js';

const cookies = new Cookies();
var config = require('../../config.js');

class LoginForm extends Loading{
    constructor(props) {
        super(props);
        this.state = {
            Email: '',
            Email_valid: true,
            Password: '',
            Password_valid: true,
            Response_txt: '',
            Error: false,
            Type: "",
            Wait: 0
        }
        this.submitFormHandler = this.submitFormHandler.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleInputFocus = this.handleInputFocus.bind(this);
        this.resetPass = this.resetPass.bind(this);
        this.facebook = this.facebook.bind(this)
    }

    facebook(res){
        console.log(res)
        const Email_filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if(res.id!==undefined) {
            var url = config.Backend+'/fbLogin?'
                + '&Name=' + res.name
                + '&accessToken=' + res.accessToken
                + '&Fb_id=' + res.id;
                url += '&Email=' + res.email;
            if (!(cookies.get('Id') === undefined)) {
                url += '&Id=' + cookies.get('Id').toString();
            }
            this.setState({Wait:++this.state.Wait});jQuery.getJSON(url, function (res2) {
                if (res2.Error) {
                    this.setState({Wait:--this.state.Wait})

                    this.setState({Response_txt: res2.Error});
                    this.setState({Error: true});
                } else {
                    this.setState({Wait:--this.state.Wait})

                    this.setState({Error: false});
                    this.setState({Response_txt: res2.Ok});
                    var exp = new Date();
                    exp.setDate(exp.getDate() + 3);
                    cookies.set('Id',res2.User._id, { path: '/' ,expires:exp});
                    if (res2.User.Type=="Admin") {
                        window.location.replace(window.location.origin + '/dashboard');
                    } else if (res2.User.Type=="Super") {
                        window.location.replace(window.location.origin + '/admin/main');
                    }
                }
            }.bind(this));
        }
    }
    componentWillMount() {
        this.preload();
        const params = new URLSearchParams(this.props.location.search);
        const code = params.get('code');
        if(code)
        {
            var url = config.SlackApi + '/oauth.access?client_id='+
                config.SlackId+'&client_secret='+config.SlackSecret+'&code='+code
            +'&redirect_uri='+config.RedirectLoginUri;
            this.setState({Wait:++this.state.Wait});jQuery.getJSON(url,function (res) {
                if(res.ok)
                {
                    this.setState({Wait:--this.state.Wait})

                    var url = config.Backend + '/slackLogin?Slack_id='+res.user.id
                        +'&Name='+res.user.name
                        +'&Email='+res.user.email
                        +'&Token='+res.access_token;
                    this.setState({Wait:++this.state.Wait});jQuery.getJSON(url,function (res) {
                        if (!res.Error) {
                            this.setState({Wait:--this.state.Wait})

                            this.setState({Error: false});
                            this.setState({Response_txt: res.Ok});
                            cookies.set('Id', res.User._id, {path: '/'});
                            if (res.User.Type=="Admin") {
                                window.location.replace(window.location.origin + '/dashboard');
                            } else if (res.User.Type=="Super") {
                                window.location.replace(window.location.origin + '/admin/main');
                            }
                        }
                        else
                        {
                            this.setState({Wait:--this.state.Wait})

                            this.setState({Response_txt: res.Error});
                            this.setState({Error: true});
                        }
                    }.bind(this))
                }
                else{
                    this.setState({Wait:--this.state.Wait})

                    alert(JSON.stringify(res))
                }
            }.bind(this))
        }

        var url = config.Backend + '/users/findById?';
        if (!(cookies.get('Id') === undefined))
        {
            url += 'Id='+ cookies.get('Id').toString();
        }
        this.setState({Wait:++this.state.Wait});jQuery.getJSON(url,function (res){
            this.setState({Wait:--this.state.Wait})
            if (!res.Error) {
                this.setState({Type: res.Type});
                if (this.state.Type !== '')
                {
                    if (res.Type=="Admin") {
                        window.location.replace(window.location.origin + '/dashboard');
                    } else if (res.Type=="Super") {
                        window.location.replace(window.location.origin + '/admin/main');
                    }
                }
            }
        }.bind(this));
    }
    xor(ai) {
        var f = "";
        for (var c = 0; c < ai.length; c++) {
            f += String.fromCharCode(ai.charCodeAt(c) ^ (1 + (ai.length - c) % 32))
        }
        return f
    }

    submitFormHandler(event) {
        const Email_filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if ( ! Email_filter.test(this.state.Email) ) {
            this.setState({Email_valid: false});
        } else if ( this.state.Password === "" ) {
            this.setState({Password_valid: false});
        } else {
            var fd = {};
            fd.Email = this.state.Email;
            fd.Password = this.xor(this.state.Password);
            var url = config.Backend+'/login';
            this.setState({Wait:++this.state.Wait});jQuery.ajax({
                method:"POST",
                url:url,
                contentType: 'application/json',
                success: function (res) {
                    this.setState({Wait:--this.state.Wait})
                    if (res.Error)
                    {
                        this.setState({Response_txt : res.Error});
                        this.setState({Error : true});
                    } else {
                        this.setState({Error: false});
                        this.setState({Response_txt : res.Ok});
                        const Type = res.User.Type;
                        var exp = new Date();
                        exp.setDate(exp.getDate() + 3);
                        cookies.set('Id',res.Id, { path: '/' ,expires:exp});
                        if (Type=="Admin") {
                            window.location.replace(window.location.origin + '/dashboard');
                        } else if (Type=="Super") {
                            window.location.replace(window.location.origin + '/admin/main');
                        }
                    }
                }.bind(this),
                data: JSON.stringify(fd),
            })
        }
        event.preventDefault();
    }

    resetPass() {
        window.location.replace(window.location.origin + '/reset');
    }

    handleInputChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    handleInputFocus(event) {
        this.setState({
            [event.target.name+'_valid'] : true
        })
    }

    Response () {
        const danger = this.state.Error ? <div className="text-danger">{this.state.Response_txt}</div> : <div>{this.state.Response_txt}</div>;
        return (<div className="form-group">
                {danger}
            </div>
        )
    }
    Email () {
        const danger = this.state.Email_valid ? '' : <div className="text-danger">Check your E-mail address.</div>;
        return (
            <div className="form-group">
                <input type="Email" className="" placeholder='Email' value={this.state.Email} name="Email" onChange={this.handleInputChange} onFocus={this.handleInputFocus}/>
                {danger}
            </div>
        )
    }

    Password () {
        const danger = this.state.Password_valid ? '' : <div className="text-danger">Check your Password.</div>;
        return (
            <div className="form-group">
                <input type="Password" className="" placeholder="Password" value={this.state.Password} name="Password" onChange={this.handleInputChange} onFocus={this.handleInputFocus}/>
                {danger}
            </div>
        )
    }

    render () {
        /*<a href={"https://slack.com/oauth/authorize?scope=identity.basic,identity.email&client_id="+config.SlackId
                        +'&redirect_uri='+config.RedirectLoginUri}><img src="https://api.slack.com/img/sign_in_with_slack.png" /></a>*/
        var s =this.state;
        var icon = "";
        if(s.Preload && s.Preload['Logo'])
        {
            if(s.Preload['Logo'].indexOf("uploads")!=-1) {
                icon = config.Backend + '/content/getFile?';
                icon += '&Link=' +  s.Preload['Logo'];
            }
            else {
                icon = s.Preload['Logo'];
            }
        }
        return (
            <div className="background_comein">
                {this.Load()}
                <div className="form_block_login">
                    <form onSubmit={this.submitFormHandler} className="login">
                        <a href="/">
                        <div className="block_img">
                            <img src={icon} alt=""/>
                        </div>
                        </a>
                        <p>Sign into your account.</p>
                        {this.Response()}
                        {this.Email()}
                        {this.Password()}
                        <button type="submit"  className="btn_login mt_15">Sign In</button>
                        <div className="facebook">
                            <FacebookLogin
                                appId={config.FbAppId}
                                autoLoad={false}
                                fields="name,email,picture"
                                cssClass = "btn_facebook"
                                reAuthenticate={true}
                                scope={"pages_show_list,manage_pages,email,pages_messaging,pages_messaging_subscriptions,public_profile"}
                                returnScopes={true}
                                callback={this.facebook} />
                        </div>

                    </form>
                    <div className="login_block_form">
                        <a href="/register">Sign up</a>
                        <button type="submit" onClick={this.resetPass} className="forgot_password">Forget your password?</button>
                    </div>
                </div>
            </div>
        );
    }
}

export default LoginForm;
