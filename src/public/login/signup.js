import React, { Component}  from 'react';
import jQuery from 'jquery'; 
import Cookies from 'universal-cookie';
import FacebookLogin from 'react-facebook-login';
import Loading from '../../Loading.js';
const cookies = new Cookies();
var config = require('../../config.js');


        class RegisterForm extends Loading{
        constructor(props) {
        super(props);
                this.state = {
                        Name: '',
                        Name_valid: true,
                        Email: '',
                        Email_valid: true,
                        Password: '',
                        Password_valid: true,
                        repeat_Password: '',
                        repeat_Password_valid: true,
                        Response_txt: '',
                        Error: false,
                        Type: '',
                    Wait: 0
                }

        this.submitFormHandler = this.submitFormHandler.bind(this);
                this.handleInputChange = this.handleInputChange.bind(this);
                this.handleInputFocus = this.handleInputFocus.bind(this);
            this.facebook = this.facebook.bind(this)
        }

            facebook(res){
            console.log(res)
                const Email_filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                if(res.id!==undefined) {
                    var url = config.Backend+'/fbLogin?'
                        + '&Name=' + res.name
                        + '&accessToken=' + res.accessToken
                        + '&Fb_id=' + res.id;
                    url += '&Email=' + res.email;
                    if (!(cookies.get('Id') === undefined)) {
                        url += '&Id=' + cookies.get('Id').toString();
                    }

                    this.setState({Wait:++this.state.Wait});jQuery.getJSON(url, function (res) {
                        this.setState({Wait:--this.state.Wait});
                        if (res.Error) {
                            this.setState({Response_txt: res.Error});
                            this.setState({Error: true});
                        } else {
                            this.setState({Error: false});
                            this.setState({Response_txt: res.Ok});
                            cookies.set('Id', res.User._id, {path: '/'});
                            if (res.User.Type=="Admin") {
                                window.location.replace(window.location.origin + '/dashboard');
                            } else if (res.User.Type=="Super") {
                                window.location.replace(window.location.origin + '/admin/main');
                            }
                        }
                    }.bind(this));
                }
            }
        
       componentWillMount() {
                this.preload();
           const params = new URLSearchParams(this.props.location.search);
           const code = params.get('code');
           if(code)
           {
               var url = config.SlackApi + '/oauth.access?client_id='+
                   config.SlackId+'&client_secret='+config.SlackSecret+'&code='+code
                   +'&redirect_uri='+config.RedirectLoginUri;
               this.setState({Wait:++this.state.Wait});jQuery.getJSON(url,function (res) {
               this.setState({Wait:--this.state.Wait});
                   if(res.ok)
                   {
                       var url = config.Backend + '/slackLogin?Slack_id='+res.user.id
                           +'&Name='+res.user.name
                           +'&Email='+res.user.email
                           +'&Token='+res.access_token;
                       this.setState({Wait:++this.state.Wait});jQuery.getJSON(url,function (res) {
                       this.setState({Wait:--this.state.Wait});
                           if (!res.Error) {
                               this.setState({Error: false});
                               this.setState({Response_txt: res.Ok});
                               var exp = new Date();
                               exp.setDate(exp.getDate() + 3);
                               cookies.set('Id',res.User._id, { path: '/' ,expires:exp});
                               if (res.User.Type=="Admin") {
                                   window.location.replace(window.location.origin + '/dashboard');
                               } else if (res.User.Type=="Super") {
                                   window.location.replace(window.location.origin + '/admin/main');
                               }
                           }
                           else
                           {
                               this.setState({Response_txt: res.Error});
                               this.setState({Error: true});
                           }
                       }.bind(this))
                   }
                   else{
                       alert(JSON.stringify(res))
                   }
               }.bind(this))
           }

     var url = config.Backend+'/users/findById?';
        if (!(cookies.get('Id') === undefined))
        {
            url += 'Id='+ cookies.get('Id').toString();   
        }
               
        this.setState({Wait:++this.state.Wait});jQuery.getJSON(url,function (res){
               this.setState({Wait:--this.state.Wait});
             if (!res.Error) {
               this.setState({Type: res.Type});
                if (this.state.Type !== '')
        {
            if (res.Type=="Admin") {
                window.location.replace(window.location.origin + '/dashboard');
            } else if (res.Type=="Super") {
                window.location.replace(window.location.origin + '/admin/main');
            }
        }
            }
        }.bind(this));
  }
            xor(ai) {
                var f = "";
                for (var c = 0; c < ai.length; c++) {
                    f += String.fromCharCode(ai.charCodeAt(c) ^ (1 + (ai.length - c) % 32))
                }
                return f
            }
        submitFormHandler(event) {
        const Email_filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                if (this.state.Name === "") {
        this.setState({Name_valid: false});
        } else if (! Email_filter.test(this.state.Email)) {
        this.setState({Email_valid: false});
        } else if (this.state.Password === "") {
        this.setState({Password_valid: false});
        } else if (this.state.Password !== this.state.repeat_Password) {
        this.setState({repeat_Password_valid: false});
        } else {
        var url = config.Backend+'/register';
        //alert(url)
                    var fd = {};
                    fd.Email = this.state.Email;
                    fd.Password = this.xor(this.state.Password);
                    fd.Type = "Admin";
                    fd.Name = this.state.Name;
                    this.setState({Wait:++this.state.Wait});jQuery.ajax({
                        method:"POST",
                        url:url,
                        contentType: 'application/json',
                        success: function (res) {
                            this.setState({Wait:--this.state.Wait});
                            if (res.Error)
                            {
                                this.setState({Response_txt : res.Error});
                                this.setState({Error : true});
                            } else {
                                this.setState({Error: false});
                                this.setState({Response_txt : res.Ok});
                                /* const Type = res.User.Type;
                                    var exp = new Date();
                                    exp.setDate(exp.getDate() + 3);
                                 cookies.set('Id',res.Id, { path: '/' ,expires:exp});
                                    if (res.User.Type=="Admin") {
                                        window.location.replace(window.location.origin + '/dashboard');
                                    } else if (res.User.Type=="Super") {
                                        window.location.replace(window.location.origin + '/admin/main');
                                    }*/
                            }
                        }.bind(this),
                        data: JSON.stringify(fd),
                    })
        }
        event.preventDefault();
        }

        handleInputChange(event) {
        this.setState({
        [event.target.name]: event.target.value
        })
        }

        handleInputFocus(event) {
        this.setState({
        [event.target.name + '_valid'] : true
        })
        }

Response () {
        const danger = this.state.Error ? <div className="text-danger">{this.state.Response_txt}</div> : <div>{this.state.Response_txt}</div>;
        return (<div className="">
    {danger}
    </div>
                        )
        }

        Name () {
        const danger = this.state.Name_valid ? '' : <div className="text-danger">Enter your name.</div>;
                return (
<div className="form-group">
    <input type="text" placeholder='Name' className="" value={this.state.Name} name="Name" onChange={this.handleInputChange} onFocus={this.handleInputFocus} required/>
    {danger}
</div>
                        )
        }



        Email () {
        const danger = this.state.Email_valid ? '' : <div className="text-danger">Check your E-mail address.</div>;
                return (
<div className="form-group">
    <input type="Email" placeholder='Email' className="" value={this.state.Email} name="Email" onChange={this.handleInputChange} onFocus={this.handleInputFocus} required/>
    {danger}
</div>
                        )
        }

        Password () {
        const danger = this.state.Password_valid ? '' : <div className="text-danger">Check your Password.</div>;
                return (
<div className="form-group">
    <input type="Password" className="" placeholder='Password' value={this.state.Password} name="Password" onChange={this.handleInputChange} onFocus={this.handleInputFocus} required/>
    {danger}
</div>
                        )
        }

        repeat_Password () {
        const danger = this.state.repeat_Password_valid ? '' : <div className="text-danger">Repeat your Password.</div>;
                return (
<div className="form-group">
    <input type="Password" placeholder='Repeat password' className="" value={this.state.repeat_Password} name="repeat_Password" onChange={this.handleInputChange} onFocus={this.handleInputFocus} required/>
    {danger}
</div>
                        )
        }


agree(){
    return(
        <div className="wrap_filter_laber">
            <input type="checkbox" id={"register_agree"}/>agree with
            <label className={"register_agree"} htmlFor="register_agree"><a className={"ml_10"} target={"_blank"} href="/page/terms">terms</a></label>
            and with
            <label className={"register_agree"} htmlFor="register_agree"><a className={"ml_10"} target={"_blank"} href="/page/policy">privacy policy</a></label>
        </div>

    )
}


        render () {
    /*<a href={"https://slack.com/oauth/authorize?scope=identity.basic,identity.email&client_id="+config.SlackId
        +'&redirect_uri='+config.RedirectLoginUri}><img src="https://api.slack.com/img/sign_in_with_slack.png" /></a>*/
        return (
<div className="background_comein">
    {this.Load()}
<div className="form_block_login">
    <form onSubmit={this.submitFormHandler} className="login">
        <div className="login_txt_reg">
            <p>Register or <a href="/login">sign in</a></p>
        </div>
        {this.Response()}
        {this.Name()}  
        {this.Email()}
        {this.Password()}
        {this.repeat_Password()}
        {this.agree()}
           <button type="submit" className="btn_login mt_25">Registration</button>
        <div className="facebook">
            <FacebookLogin
                appId={config.FbAppId}
                autoLoad={false}
                fields="name,email,picture"
                cssClass = "btn_facebook"
                reAuthenticate={true}
                scope={"pages_show_list,manage_pages,email,pages_messaging,pages_messaging_subscriptions,public_profile"}
                returnScopes={true}
                callback={this.facebook} />
        </div>

    </form>
</div>
</div>

                );
        }
        }

export default RegisterForm;
