import React, { Component}  from 'react';
import jQuery from 'jquery';
import Cookies from 'universal-cookie';
import Loading from '../Loading.js';
import { Nav, NavItem, Dropdown, DropdownItem, DropdownToggle, DropdownMenu, NavLink } from 'reactstrap';
const cookies = new Cookies();
var config = require('../config.js');




class Link extends React.Component {
    state = {
        open: false
    }
    handleClick = () => {
        this.setState({ open: !this.state.open });
    }

    exit(event) {
        cookies.set('Exit',"true", { path: '/' });
        window.location.replace(window.location.origin + '/login');
    }

    render () {
        var s =this.state;
        const { open } = this.state;
        return (
            <div className="link">
                <span onClick={this.handleClick}>
                    <div className={`img_picture_user ${open ? 'borderR2' : ''}`}>
                                            <img src={this.props.icon || "/img/profile/images.png"} alt=""/>
                                        </div></span>
                <div className={`menu ${open ? 'open' : ''}`}>
                    <ul className={'list_top_menu'}>
                        <li><a href="/profile/setting"><span class="ti-settings mr_2"></span>Setting</a></li>
                        <li><a target={'_blank'} href="/manual"><span className={'ti-help-alt mr_2'}></span>Help</a></li>
                        <li><a href={'#'} onClick={this.exit}><span className={'ti-power-off mr_2'}></span>Logout</a></li>
                    </ul>
                </div>
            </div>
        )
    }
}


class TopPanel extends Loading {
    constructor(props) {
        super(props);
        this.state = {
            Bot: null,
            dropdownOpen: false
        };
        this.check = this.check.bind(this);
        this.toggle = this.toggle.bind(this);
    }

    exit(event) {
        cookies.set('Exit',"true", { path: '/' });
        window.location.replace(window.location.origin + '/login');
    }

    componentWillMount() {
        this.preload();
        var url = config.Backend + '/users/showBots?';
        if (!(cookies.get('Id') === undefined)) {
            url += 'Id=' + cookies.get('Id').toString();
        }
        jQuery.getJSON(url, function (res) {
            if (!res.Error) {
                for(var t=0;t<res.length;t++)
                {
                    if(res[t]._id==cookies.get('BotId'))
                    {
                        this.setState({Bot: res[t]});
                    }
                }
            } else {
                alert(JSON.stringify(res));
            }
        }.bind(this));

        var url = config.Backend + '/users/findById?';
        if (!(cookies.get('Id') === undefined)) {
            url += 'Id=' + cookies.get('Id').toString();
        }
        jQuery.getJSON(url, function (res) {
            if (!res.Error) {
                this.setState({User: res});
            }
        }.bind(this));
    }

    check()
    {
        var b =this.state.Bot;
        if(!b.BindTo || b.BindTo=="-")
        {
            alert("Bot not connect!");
        }
    }

    toggle() {
        this.setState({
            dropdownOpen: !this.state.dropdownOpen
        });
    }


    render(){
        var test =  window.location.origin + '/chat/configure';
        var s = this.state;
        var b =this.state.Bot;
        if(b)
        {
            if(b.BindTo=="Skype")
            test="https://join.skype.com/bot/"+b.SkypeId;
            if(b.BindTo=="FB")
                test="https://www.messenger.com/t/"+b.Page;
            if(b.BindTo=="Standalone")
                test="https://dev.botframework.com/bots?id="+b.SkypeBotName;
            if(b.BindTo=="Slack")
                test="https://"+b.SlackDomain+".slack.com/messages/";
        }
        var icon = ""
        if(s.User && s.User.Icon)
        {
            if(s.User.Icon.indexOf("uploads")!=-1) {
                icon = config.Backend + '/content/getFile?';
                if (!(cookies.get('Id') === undefined)) {
                    icon += '&CurrentUserId=' + cookies.get('Id').toString();
                }
                icon += '&Link=' + s.User.Icon;
            }
            else {
                icon = s.User.Icon;
            }
        }
        var logo = "";
        if(s.Preload && s.Preload['Logo'])
        {
            if(s.Preload['Logo'].indexOf("uploads")!=-1) {
                logo = config.Backend + '/content/getFile?';
                logo += '&Link=' +  s.Preload['Logo'];
            }
            else {
                logo = s.Preload['Logo'];
            }
        }
        return (
            <div className="chatbot">
                <div className="topnav">
                    <div className="topnav_left">
                        <div className="logo">
                            <a href="/dashboard"><img src={logo} alt=""/></a>
                        </div>

                        <div className="name_block">
                            <span>
                                {this.props.Text ?
                                    this.props.Text : (b ? b.Name : "")}
                            </span>
                        </div>
                    </div>

                    <div className="topnav_right">
                        <div className="nav_top_menu">
<span>
     {b && b.isTemplate ?
         <a href={test} target={"_blank"} onClick={this.check} className="btn_tchatbot">Test drive your Chatbot</a>
         : null}
</span>
                            <Link icon={icon}/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default TopPanel;
