import React, { Component}  from 'react';
import jQuery from 'jquery'; 
import Cookies from 'universal-cookie';
const cookies = new Cookies();
var config = require('../config.js');

class HomeForm extends Component{
  constructor(props) {
    super(props);

    this.state = {
      Name: '',
    }
  }

// вызывается при создании компонента
  componentWillMount() {
     var url = config.Backend+'/users/findById?';
        if (!(cookies.get('Id') === undefined))
        {
            url += 'Id='+ cookies.get('Id').toString();   
        }
               
        jQuery.getJSON(url,function (res){
             if (!res.Error) {
               this.setState({Name: res.Name});
               console.log(res);
            }
        }.bind(this));
  }

  render () {
    return (
      <div className="container">
          <h1>Hello {this.state.Name}!</h1>
      </div>
    );
  }
}

export default HomeForm;