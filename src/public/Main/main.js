import React, { Component}  from 'react';
import jQuery from 'jquery';


class MainPage extends Component{
    constructor(props) {
        super(props);

    }

    render () {
        return (
            <div className="main_deliver">
            <header className="main_header">
                <div className="main_top_header">
                        <div className="container">
                            <nav class="main_menu_header collapse navbar-collapse" id="bs-navbar">
                                <ul class="nav navbar-nav">
                                    <li> <a href="getting-started/"><img src="/img/logo_main.png" alt=""/></a>
                                    </li>
                                </ul>

                                <ul class="nav navbar-nav navbar-right">
                                    <li><a href="#">О сервисе</a></li>
                                    <li><a href="#">Функционал</a></li>
                                    <li><a href="#">Тарифы</a></li>
                                    <li className="main_top_btn"><a href="/login">Войти</a></li>
                                </ul>
                            </nav>
                    </div>
                </div>

                <div className="content_header">
                    <div className="container">
                        <div className="row">
                            <div className="items-center">
                                <div className="col-md-6">
                                    <h1>Создайте комплекс ботов для работы с клиентами без навыков программирования</h1>
                                    <div className="items_text_header">
                                        <p>Запустите полнофункциональных ботов<br/> в <span>7-ми</span> самых популярных социальных системах<br/><span>менее чем за 1 день</span></p>
                                    </div>
                                </div>

                                <div className="col-md-6">
                                    <div className="img_items_center">
                                        <img src="/img/robocop.png" alt=""/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>

                <div className="container">
                    <div className="row">
                        <div className="comfortable">
                            <h2>Почему мы?</h2>
                            <div className="col-sm-3">
                                <div className="items_comfort">
                                    <div className="items_img">
                                        <img src="/img/items_com_1.png"/>
                                    </div>
                                    <h4>Техническая поддержка ваших ботов 24/7</h4>
                                </div>
                            </div>

                            <div className="col-sm-3">
                                <div className="items_comfort">
                                    <div className="items_img">
                                        <img src="/img/items_com_2.png"/>
                                    </div>
                                    <h4>Актуальная статистическая информация</h4>

                                </div>
                            </div>

                            <div className="col-sm-3">
                                <div className="items_comfort">
                                    <div className="items_img">
                                        <img src="/img/items_com_3.png"/>
                                    </div>
                                    <h4>Создание бота не требует навыков программирования</h4>
                                </div>
                            </div>

                            <div className="col-sm-3">
                                <div className="items_comfort">
                                    <div className="items_img">
                                        <img src="/img/items_com_4.png"/>
                                    </div>
                                    <h4>Потому что мы первые</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="program">
                    <div className="container">
                        <div className="row">
                            <div className="program_text_container">
                                <h2>Мы предлагаем</h2>
                            </div>

                            <div className="programm_content">
                                <div className="col-sm-6">
                                    <div className="programm_item col-sm-12 p0">
                                        <div className="col-sm-3 p0">
                                            <div className="programm_item_img">
                                                <img src="/img/programm_items.png"/>
                                            </div>
                                        </div>
                                        <div className="col-sm-9 p0">
                                            <div className="programm_item_text">
                                                <h4>Простоту управления ботами</h4>
                                            </div>
                                        </div>
                                </div>

                                    <div className="programm_item col-sm-12 p0">
                                        <div className="col-sm-3 p0">
                                            <div className="programm_item_img">
                                                <img src="/img/programm_items.png"/>
                                            </div>
                                        </div>
                                        <div className="col-sm-9 p0">
                                            <div className="programm_item_text">
                                                <h4>Создание ботов для 7 самых популярных социальных систем</h4>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="programm_item col-sm-12 p0">
                                        <div className="col-sm-3 p0">
                                            <div className="programm_item_img">
                                                <img src="/img/programm_items.png"/>
                                            </div>
                                        </div>
                                        <div className="col-sm-9 p0">
                                            <div className="programm_item_text">
                                                <h4>Автоматизацию рассылки по Вашим подписчикам</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="programm_item col-sm-12 p0">
                                        <div className="col-sm-3 p0">
                                            <div className="programm_item_img">
                                                <img src="/img/programm_items.png"/>
                                            </div>
                                        </div>
                                        <div className="col-sm-9 p0">
                                            <div className="programm_item_text">
                                                <h4>Качественную техническую поддержку</h4>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div className="col-sm-6">
                                    <div className="programm_item col-sm-12 p0">
                                        <div className="col-sm-3 p0">
                                            <div className="programm_item_img">
                                                <img src="/img/programm_items.png"/>
                                            </div>
                                        </div>
                                        <div className="col-sm-9 p0">
                                            <div className="programm_item_text">
                                                <h4>Конструктор для создания ботов не требующий навыков в программировании</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="programm_item col-sm-12 p0">
                                        <div className="col-sm-3 p0">
                                            <div className="programm_item_img">
                                                <img src="/img/programm_items.png"/>
                                            </div>
                                        </div>
                                        <div className="col-sm-9 p0">
                                            <div className="programm_item_text">
                                                <h4>Ботов, способных понять ответы пользователя, даже если они написаны с ошибками</h4>
                                            </div>
                                        </div>

                                    </div>

                                    <div className="programm_item col-sm-12 p0">
                                        <div className="col-sm-3 p0">
                                            <div className="programm_item_img">
                                                <img src="/img/programm_items.png"/>
                                            </div>
                                        </div>
                                        <div className="col-sm-9 p0">
                                            <div className="programm_item_text">
                                                <h4>Средство передачи управления человеку, если бот не в состоянии ответить на поставленный вопрос</h4>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="programm_item col-sm-12 p0">
                                        <div className="col-sm-3 p0">
                                            <div className="programm_item_img">
                                                <img src="/img/programm_items.png"/>
                                            </div>
                                        </div>
                                        <div className="col-sm-9 p0">
                                            <div className="programm_item_text">
                                                <h4>Расширение базовых возможностей бота сторонними средствами</h4>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="tariffs">
                    <div className="container">
                        <div className="row">
                            <h2 className="mb60">Тарифы на использование бота</h2>
                                <div className="tariffs_container col-sm-10  col-sm-offset-1">
                                   <div className="col-sm-4">
                                       <div className="tariffs_item ">
                                           <h3 className="yel">Бесплатно</h3>
                                           <p>Для создания и тестирования<br/>
                                               Вашего первого бота</p>
                                           <p>1 чат бот</p>
                                           <div className="btn_overlay mt_15">
                                               <a className="tariffs_btn" href="#">Попробуйте бесплатно</a>
                                           </div>
                                       </div>
                                   </div>

                                    <div className="col-sm-4">
                                        <div className="tariffs_item">
                                            <h3 className="blue">400руб</h3>
                                            <p>Для создания и тестирования<br/>
                                                Вашего первого бота</p>
                                            <p>1 чат бот</p>
                                            <div className="btn_overlay mt_15">
                                                <a className="tariffs_btn" href="#">Начать</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="col-sm-4">
                                        <div className="tariffs_item">
                                            <h3 className="purple">600руб</h3>
                                            <p>Для создания и тестирования<br/>
                                                Вашего первого бота</p>
                                            <p>1 чат бот</p>
                                            <div className="btn_overlay mt_15">
                                                <a className="tariffs_btn" href="#">Начать</a>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
                <footer className="main_footer">
                    <div className="container">
                        <div className="row">
                            <div className="main_footer_top">
                            <ul class="bs-docs-footer-links">
                                <li><img src="/img/main_logo_footer.png" alt=""/></li>
                                <li><a href="#">Политика</a></li>
                                <li><a href="#">О нас</a></li>
                                <li><a href="#">Контакты</a></li>
                            </ul>
                            </div>
                            <div className="main_footer_bottom">
                                <ul class="nav navbar-nav">
                                    <li>
                                    <p className="fz12">CBFlow 2018 Все права защищены</p>
                                    </li>
                                </ul>

                                <ul class="navbar-right">
                                    <li><a className="fz12" href="#">Сделано в i-solution</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        );
    }
}

export default MainPage;

