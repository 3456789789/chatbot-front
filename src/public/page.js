import React, { Component}  from 'react';
import Cookies from 'universal-cookie';
import jQuery from 'jquery';
import Loading from '../Loading.js';

const cookies = new Cookies();
var config = require('../config.js');

class Page extends Loading{
    constructor(props) {
        super(props);

        this.state = {
            Wait: 0,
            Page:null,
            head: "",
            body:"",
        }
        this.load = this.load.bind(this);
        this.change = config.change.bind(this);
    }
    componentWillMount()
    {

    }
    componentDidMount()
    {
       this.preload();
       this.load();
    }
    load()
    {
        var slug = this.props.match.params.slug;
        config.getData('page/get',{Slug:slug}).then(function (p) {
            this.setState({Page:p});
            var head = document.getElementsByTagName('HEAD')[0].innerHTML;
            var body = document.getElementsByTagName('BODY')[0].innerHTML;
            this.setState({body:this.state.body});
            var footer_text="";
            var footer= "<footer></footer>";
            var s = this.state;
            document.title = p.Title;
            document.getElementsByTagName('HEAD')[0].innerHTML  =head + p.Head;
            document.getElementsByTagName('BODY')[0].insertAdjacentHTML('beforeend', '<div class="container">' +  p.Body+footer + '</div>');
        }.bind(this),function (err) {
            alert(JSON.stringify(err));
        }.bind(this))
    }

    render () {
        var s = this.state;
        var logo = "";
        if(s.Preload && s.Preload['Logo'])
        {
            if(s.Preload['Logo'].indexOf("uploads")!=-1) {
                logo = config.Backend + '/content/getFile?';
                logo += '&Link=' +  s.Preload['Logo'];
            }
            else {
                logo = s.Preload['Logo'];
            }
        }
        return (
            <div>
                <nav className="navbar navbar-default">
                    <div className="container-fluid">
                        <div className="navbar-header">
                            <a className={'logo__pulic--a'}>
                                <img style={{maxWidth: '55px',display: 'inline-block'}} className={'logo__public'} src={logo}/>
                            </a>
                        </div>
                    </div>
                </nav>
            </div>)
    }
}

export default Page;
