import React, {Component} from 'react';
import jQuery from 'jquery';
import Cookies from 'universal-cookie';
import LeftPanel from "../../LeftPanel";
import TopPanel from "../../TopPanel";
import { Scrollbars } from 'react-custom-scrollbars';
import Loading from '../../../Loading.js';

const cookies = new Cookies();
var config = require('../../../config.js');


class History extends Loading {
    constructor(props) {
        super(props);
        this.state = {
            User: null,
            Bot: null,
            Users: [],
            History:[],
            Wait: 0,
            Show:null,
        };
        this.show = this.show.bind(this);
        this.load = this.load.bind(this);
    }
   /* componentWillUpdate (p,s)
    {
console.log(this.state.Wait);
  }*/
    show(u,i)
    {
        this.setState({Wait:++this.state.Wait});
        config.getData("bot/history",{Period:"All",Times:1,Bot:this.state.Bot._id,User:u.User}).then(function (h) {
            this.setState({Wait:--this.state.Wait,History:h,Show:true})
        }.bind(this),function (err) {
            this.setState({Wait:--this.state.Wait});alert(JSON.stringify(err));
        }.bind(this))
    }
    load()
    {
        var url = config.Backend + '/bot/findById?';
        if (!(cookies.get('Id') === undefined)) {
            url += 'CurrentUserId=' + cookies.get('Id').toString();
        }
        url += '&Id=' + cookies.get('BotId').toString();
        this.setState({Wait:++this.state.Wait});jQuery.getJSON(url, function (res) {
        if (!res.Error) {
            this.setState({Bot: res}, function () {
            }.bind(this));
            config.getData("bot/time_users",{Period:"All",Times:1,Bot:res._id}).then(function (u) {
                this.setState({Wait:--this.state.Wait,Users:u})
            }.bind(this),function (err) {
                this.setState({Wait:--this.state.Wait});alert(JSON.stringify(err));
            }.bind(this))
        } else {
            this.setState({Wait:--this.state.Wait});alert(JSON.stringify(res));
        }
    }.bind(this));
    }
    componentWillMount() {
        this.preload();
        var url = config.Backend + '/users/findById?';
        if (!(cookies.get('Id') === undefined)) {
            url += 'Id=' + cookies.get('Id').toString();
        }
        this.setState({Wait:++this.state.Wait});jQuery.getJSON(url, function (res) {
            if (!res.Error) {
                this.setState({User: res,Wait:--this.state.Wait}, () => {
                    this.load();
                });
            } else {
                window.location.replace(window.location.origin + '/login');
            }
        }.bind(this));
    }
    Users()
    {
        var users = this.state.Users.map(function (s,i) {
            const ind = i;
            const  obj = s;
            return (
                <tbody>
                    <tr>
                        <td>{obj.User}</td>
                        <td>{obj.Name}</td>
                        <td className={'tac'}><button className={'btn_all'} onClick={()=>this.show(obj,ind)}>Show</button></td>
                    </tr>
                </tbody>)
        }.bind(this))
        return (<table className={'table'}>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th className={'tac'}>Control</th>
            </tr>
            {users}
        </table>)
    }

    History()
    {
        var msgs = this.state.History.map(function (s,i) {
            const ind = i;
            const  obj = s;
            return (
                <tr>
                <td>{obj.Owner=="Admin" ? obj.Owner+"("+obj.Name+")" : obj.Owner}</td>
                <td>{obj.Text}</td>
                <td>date{new Date(obj.created_at).toLocaleString()}</td>

            </tr>
               )
        }.bind(this))
        return (
            <div>
                <button className={'btn_all'} onClick={()=>this.setState({Show:null})}>Back</button>
                <table className={'table'}>
            <thead>
            <tr>
                <th>Owner</th>
                <th>Text</th>
                <th className={'tac'}>Time</th>
            </tr>
            </thead>
                    <tbody>
            {msgs}
                    </tbody>
        </table>
        </div>)
    }

    render() {
        var s = this.state;
        return (
            <div>
                {this.Load()}
                <TopPanel/>
                <LeftPanel User={this.state.User}/>
                <div className="mainBroadCast broad_main_2">
                    <Scrollbars
                        style={{ height: "90%" }}>
                        <div className="overlay_chart padding ">
                            {s.Show ? this.History() : this.Users()}
                        </div>
                    </Scrollbars>
                </div>
            </div>
        )
    }
}

export default History;