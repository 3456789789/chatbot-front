import React, {Component} from 'react';
import jQuery from 'jquery';
import Cookies from 'universal-cookie';
import LeftPanel from "../../LeftPanel";
import TopPanel from "../../TopPanel";
import Loading from '../../../Loading.js';

const cookies = new Cookies();
var config = require('../../../config.js');


class Share extends Loading {
    constructor(props) {
        super(props);
        this.state = {
            User: null,
            Bot: null,
            Users: [],
            Wait: 0,
            ShowSharePopup: false,
            ShareEmail: "",
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.delete = this.delete.bind(this);
        this.share = this.share.bind(this);
        this.changeEmail = this.changeEmail.bind(this);
        this.save = this.save.bind(this);
        this.load = this.load.bind(this);
    }
    changeEmail(e)
    {
        this.setState({ShareEmail:e.target.value});
    }
    share()
    {
        this.setState({Wait:this.state.Wait+1})
        jQuery.ajax({
            method: "POST",
            contentType: 'application/json',
            url: config.Backend + '/share/add',
            success: function (res) {
                this.setState({Wait:this.state.Wait-1})
                if (!res.Error) {
                    this.load();
                    this.setState({ShowSharePopup:false});
                } else {
                    alert(JSON.stringify(res));
                }
            }.bind(this),
            data: JSON.stringify({Email:this.state.ShareEmail,
                BotId: this.state.Bot._id,
                Rights:"RTC"}),
        })
    }
    save()
    {
        var fd = this.state.Bot.Share;
        this.setState({Wait:this.state.Wait+1});jQuery.ajax({
        method: "POST",
        contentType: 'application/json',
        url: config.Backend + '/share/editFull',
        success: function (res) {
            if (!res.Error) {
                this.setState({Wait:--this.state.Wait})
            } else {
                this.setState({Wait:--this.state.Wait});alert(JSON.stringify(res));
            }
        }.bind(this),
        data: JSON.stringify(fd),
    })
    }
    delete(s,i){
        var fd = {};
        fd.ShareId = this.state.Bot.Share._id;
        fd.UserId = s.User;
        this.setState({Wait:this.state.Wait+1});jQuery.ajax({
            method: "POST",
            contentType: 'application/json',
            url: config.Backend + '/share/delete',
            success: function (res) {
                if (!res.Error) {
                    this.setState({Wait:--this.state.Wait})

                    this.state.Bot.Share.UsersWithRights.splice(i,1);
                    this.setState({Bot:this.state.Bot})

                } else {
                    this.setState({Wait:--this.state.Wait});alert(JSON.stringify(res));
                }
            }.bind(this),
            data: JSON.stringify(fd),
        })
    }
    handleInputChange(event,i) {
        this.state.Bot.Share.UsersWithRights[i].Rights = (event.target.checked ? (this.state.Bot.Share.UsersWithRights[i].Rights+event.target.name)
            : this.state.Bot.Share.UsersWithRights[i].Rights.replace(event.target.name,''));
        if(event.target.name=="RTC")
        {
            this.state.Bot.Share.UsersWithRights[i].Chat = undefined;
        }
        this.setState({
            Bot:this.state.Bot,
        })
    }
    load()
    {
        var url = config.Backend + '/bot/findById?';
        if (!(cookies.get('Id') === undefined)) {
            url += 'CurrentUserId=' + cookies.get('Id').toString();
        }
        url += '&Id=' + cookies.get('BotId').toString();
        this.setState({Wait:this.state.Wait+1});
        jQuery.getJSON(url, function (res) {
        if (!res.Error) {
            if(!config.checkRights(cookies.get('Id').toString(),res,"Share"))
            {
                config.accessDenied(cookies.get('Id').toString(),res);
            }
            this.setState({Bot: res,Users:res.Users}, function () {
            }.bind(this));
            this.setState({Wait:--this.state.Wait})

        } else {
            this.setState({Wait:--this.state.Wait});alert(JSON.stringify(res));
        }
    }.bind(this));
    }
    componentWillMount() {
        this.preload();
        var url = config.Backend + '/users/findById?';
        if (!(cookies.get('Id') === undefined)) {
            url += 'Id=' + cookies.get('Id').toString();
        }
        this.setState({Wait:++this.state.Wait});
        jQuery.getJSON(url, function (res) {
            this.setState({Wait:--this.state.Wait})
            if (!res.Error) {
                //if(res.Type=="Super")
                //  window.location.replace(window.location.origin + '/admin/main');
                this.setState({User: res}, () => {
                    this.load();
                });
            } else {
                window.location.replace(window.location.origin + '/login');
            }
        }.bind(this));
    }
    Users()
    {
        var users = this.state.Bot.Share.UsersWithRights.map(function (s,i) {
            var rights = (this.state.Bot.Share.UsersWithRights[i].Rights);
            var u = null;
            const ind = i;
            const  obj = s;
            for(var i=0;i<this.state.Users.length;i++)
            {
                if(this.state.Users[i]._id==s.User)
                    u = this.state.Users[i];
            }

            return (
                <tr>
                    <td>{u.Name}</td>
                    <td>{u.Email}</td>
                    <td className={'tac'}><input name={'Build'} checked={rights.indexOf('Build')!=-1} onChange={(e)=>this.handleInputChange(e,ind)} type={'checkbox'}/></td>
                    <td className={'tac'}><input name={'Broadcast'} checked={rights.indexOf('Broadcast')!=-1} onChange={(e)=>this.handleInputChange(e,ind)} type={'checkbox'}/></td>
                    <td className={'tac'}><input name={'Configure'} checked={rights.indexOf('Configure')!=-1} onChange={(e)=>this.handleInputChange(e,ind)} type={'checkbox'}/></td>
                    <td className={'tac'}><input name={'Grow'} checked={rights.indexOf('Grow')!=-1} onChange={(e)=>this.handleInputChange(e,ind)} type={'checkbox'}/></td>
                    <td className={'tac'}><input name={'Analyze'} checked={rights.indexOf('Analyze')!=-1} onChange={(e)=>this.handleInputChange(e,ind)} type={'checkbox'}/></td>
                    <td className={'tac'}><input name={'RTC'} checked={rights.indexOf('RTC')!=-1} onChange={(e)=>this.handleInputChange(e,ind)} type={'checkbox'}/></td>
                    <td className={'tac'}><input name={'Share'} checked={rights.indexOf('Share')!=-1} onChange={(e)=>this.handleInputChange(e,ind)} type={'checkbox'}/></td>
                    <td className={'tac'}><button className={'btn_delete'} onClick={()=>this.delete(obj,ind)}>Delete</button></td>
                </tr>)

        }.bind(this))
        return (<table className={'table'}>
            <thead>
            <tr>
                <th>Username</th>
                <th>Email</th>
                <th className={'tac'}>Build</th>
                <th className={'tac'}>Broadcast</th>
                <th className={'tac'}>Integrate</th>
                <th className={'tac'}>Plugins</th>
                <th className={'tac'}>Analytics</th>
                <th className={'tac'}>RTC</th>
                <th className={'tac'}>Share</th>
                <th className={'tac'}>Delete</th>
            </tr>
            </thead>
            <tbody>
            {users}
            </tbody>
        </table>)
    }

    sharePopup()
    {
        return ( <div>
            <div className='filterPopup'>
                <div className='filter_delete'>
                    <h1>{"Share this bot"}</h1>
                    <input className="chat_input mb10" type="text" value={this.state.ShareEmail}
                           name="ShareEmail" placeholder={'Input user email for share...'}
                           onChange={this.changeEmail}/>
                    <button className={ "btn_all bg_shadow"}
                            onClick={this.share}>
                        Ok
                    </button>
                    <button className="btn_all" onClick={()=>{this.setState({ShowSharePopup:false})}}> {"Cancel"}</button>
                </div>
            </div>
        </div>);
    }

    render() {
        var s = this.state;
        var somebodyGetShare = s.Bot && s.Bot.Share && s.Bot.Share.UsersWithRights.length>0 && s.Users.length>0;
        return (
            <div>
                {this.Load()}
                <TopPanel/>
                <LeftPanel User={this.state.User}/>
                <div className="mainBroadCast">
                    <div className="padding">
                        {somebodyGetShare ? this.Users() : null}
                        {somebodyGetShare ? <button className={'btn_all bg_shadow mr_10'} onClick={this.save}>Save changes</button> : null}
                        {somebodyGetShare ? null: <p>{"For now nobody get share access to your bot"}</p>}
                        <button className={"btn_all"} onClick={()=>this.setState({ShowSharePopup:true})}>Share</button>
                    </div>
                </div>
                {this.state.ShowSharePopup ?
                    this.sharePopup() : null}
            </div>
        )
    }
}

export default Share;