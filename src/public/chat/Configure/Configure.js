import React, { Component}  from 'react';
import jQuery from 'jquery';
import Cookies from 'universal-cookie';
import LeftPanel from "../../LeftPanel";
import TopPanel from "../../TopPanel";
import { Scrollbars } from 'react-custom-scrollbars';
import Loading from '../../../Loading.js';
import Gallery from 'react-grid-gallery';
const cookies = new Cookies();
var config = require('../../../config.js');


class PagesList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            SkypeId:  this.props.Bot.BindTo=="Skype" ? this.props.Bot.SkypeId : '',
            SkypePass: "",
            SkypeId2:  '',
            SkypePass2: "",
            SkypeBotName: this.props.Bot.BindTo=="Standalone" ? this.props.Bot.SkypeBotName : '',
            SkypeFrameSecret: this.props.Bot.BindTo=="Standalone" ? this.props.Bot.SkypeFrameSecret : '',
            ClientSecret:"",
            ClientId:"",
            SlackBtn:false,
            SlackVerificationToken:"",
        };
        this.connectSkype = this.connectSkype.bind(this);
        this.connectWeb = this.connectWeb.bind(this);
        this.connectSlack = this.connectSlack.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.unbind = this.unbind.bind(this);
    }
    unbind()
    {
        var s =this.state;
        this.props.addWait();
        config.getData("bot/unbind").then(function (ok) {
            this.props.delWait();
            this.props.loadBot();
        }.bind(this),function (err) {
            this.props.delWait();
            alert(JSON.stringify(err));
        }.bind(this))
    }
    componentWillReceiveProps(props)
    {
        this.setState({SkypeId:this.props.Bot.BindTo=="Skype" ? props.Bot.SkypeId : '',SkypeBotName:this.props.Bot.BindTo=="Standalone" ?
            props.Bot.SkypeBotName : ''})
    }


    connectWeb() {
        if(this.state.SkypeBotName && this.state.SkypeFrameSecret) {
            var ok = true;
            if(this.props.Bot.BindTo)
            {
                ok = window.confirm("Previous channel settings will be lost!");
            }
            if(ok) {
                var fd = {};
                fd.CurrentUserId = cookies.get('Id').toString();
                fd.Id = this.props.Bot._id;
                fd.SkypeId = this.state.SkypeId2;
                fd.SkypePass = this.state.SkypePass2;
                fd.SkypeBotName = this.state.SkypeBotName;
                fd.SkypeFrameSecret = this.state.SkypeFrameSecret;
                this.props.addWait();jQuery.ajax({
                    method: "POST",
                    contentType: 'application/json',
                    url: config.Backend + '/bot/bindTo',
                    success: function (res) {
                        this.props.delWait();
                        if (!res.Error) {
                            alert("Bot was connected");
                            this.props.Bot.BindTo = "Standalone";
                             window.location.href= window.location.origin+"/chat/configure";
                        } else {
                             alert(JSON.stringify(res));
                        }
                    }.bind(this),
                    data: JSON.stringify(fd),
                })
            }
        } else {
            alert("One or more fields of Skype data is empty");
        }
    }
    connectSlack()
    {
        var s=this.state;
        if(!s.ClientId)
        {
            alert("Client Id is empty!")
        }
        else if(!s.ClientSecret)
        {
            alert("Client Secret is empty!")
        }
        else if(!s.SlackVerificationToken)
        {
            alert("Verification Token is empty!")
        }
        else {
            cookies.set('ClientSecret',s.ClientSecret, { path: '/'});
            cookies.set('ClientId',s.ClientId, { path: '/'});
            cookies.set('SlackVerificationToken',s.SlackVerificationToken, { path: '/'});
            window.location.replace("https://slack.com/oauth/authorize?client_id="+s.ClientId+"&scope=users:read,users.profile:read,bot"
                +'&redirect_uri='+config.RedirectAddAppUri)
        }
    }
    connectSkype() {
        if(this.state.SkypeId && this.state.SkypePass) {
            var ok = true;
            if(this.props.Bot.BindTo)
            {
                ok = window.confirm("Previous channel settings will be lost!");
            }
            if(ok) {
                var fd = {};
                fd.CurrentUserId = cookies.get('Id').toString();
                fd.Id = this.props.Bot._id;
                fd.SkypeId = this.state.SkypeId;
                fd.SkypePass = this.state.SkypePass;
                this.props.addWait();jQuery.ajax({
                    method: "POST",
                    contentType: 'application/json',
                    url: config.Backend + '/bot/bindTo',
                    success: function (res) {
                        this.props.delWait();
                        if (!res.Error) {
                            alert("Bot was connected");
                            this.props.Bot.BindTo = "Skype";
                            var test="https://join.skype.com/bot/"+fd.SkypeId;
                          // window.location.href= window.location.origin+"/chat/configure";
                            window.location.href= test;
                        } else {
                             alert(JSON.stringify(res));
                        }
                    }.bind(this),
                    data: JSON.stringify(fd),
                })
            }
        } else {
            alert("One or more fields of Skype data is empty");
        }
    }
    handleInputChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    Disconnect(bindTo)
    {
        var b =this.props.Bot || {};
        if(this.props.Bot.BindTo==bindTo)
        {
            if(bindTo=="Skype")
            {
                return (<div>
                    <p>App id:{b.SkypeId}</p>
                    {/*<p>App secret:{b.SkypePass}</p>*/}
                    <button className={'btn_all bg_shadow'} onClick={this.unbind}>Disconnect</button>
                </div>)
            }
            else if(bindTo=="Standalone") {
                return (<div>
                    <p>App id:{b.SkypeId}</p>
                    <p>Skype bot name:{b.SkypeBotName}</p>
                    <p>Skype iframe secret:{b.SkypeFrameSecret}</p>
                    {/*<p>App secret:{b.SkypePass}</p>*/}
                    <button className={'btn_all bg_shadow'} onClick={this.unbind}>Disconnect</button>
                </div>)
            }
            else if(bindTo=="FB")
            {
                return (<div>
                    <p>Page id:{b.Page}</p>
                    <button className={'btn_all bg_shadow'} onClick={this.unbind}>Disconnect</button>
                </div>)
            }
            else if(bindTo=="Slack")
            {
                return (<div>
                    <p>Team:{b.SlackTeamName}</p>
                    <p>Domain:{b.SlackDomain}</p>
                    <button className={'btn_all bg_shadow'} onClick={this.unbind}>Disconnect</button>
                </div>)
            }
        }
        else
        {
            return <span>First of all you should disconnect</span>;
        }
    }
/*
<p>Token:{b.SlackBotToken}</p>
                    <p>Verification Token:{b.SlackVerificationToken}</p>
 */
    render() {
        /*
       <div className="configure_list_header">
                                                <div className="configure_left_bottom">
                                                    <span>FirstAction</span>
                                                </div>
                                                <div className="configure_right_bottom">
                                                    <button className="btn_connect_page">Connect to page</button>
                                                </div>
                                            </div>
                                            <li>
                                                <div className="configure_bot_left">
                                                    <a href="#">Name bot</a>
                                                </div>
                                                <div className="configure_bot_right">
                                                    <span>connect to</span>
                                                    <a href>Chat bot</a>
                                                </div>
                                            </li>
        */
        var b =  this.props.Bot.BindTo;
        var soc = "";
        if(b=="FB")
        {
            var soc = "Facebook";
        }
        else if(b=="Slack")
        {
            var soc = "Slack";
        }
        else if(b=="Skype")
        {
            var soc = "Skype";
        }
        else if(b=="Standalone")
        {
            var soc = "standalone site";
        }

        return (
            <div className="overlay_chart">
                {/* Facebook connect*/}

                <div className="configure_connect_top">
                    <div className="configure_facebook_connect col-sm-6">
                        <div className={this.props.Bot.BindTo=="FB" ? "connect_wrap active_border_config" : "connect_wrap"}>
                            <h2>Connect with Facebook</h2>
                            {this.props.Bot.BindTo ? this.Disconnect("FB") :
                                <div>
                            <div className="configure_publishing">
                                <div className="configure_list_page">
                                    <div className="publishing_left">
                                        <h1>Bot Publishing</h1>
                                        <p>Your facebook pages</p>
                                    </div>
                                    <div className="publishing_right">
                                        <button onClick={()=>{window.open("https://www.facebook.com/pages/create/",'_blank')}} >Create facebook page</button>
                                        {/*<span onClick={this.props.OpenFacebook} className="question_popup"><span className="icon_question"></span></span>*/}
                                    </div>
                                </div>
                            </div>

                            <div className="configure_list_bottom">
                                <div className="configure_list_bottom_page">
                                    <Scrollbars
                                        style={{ height: "90%" }}>
                                        <ul className="configure_list_connect">
                                            {this.props.pages}
                                        </ul>
                                    </Scrollbars>
                                </div>
                            </div>
                                </div> }
                            {/*End facebook connect*/}
                        </div>
                    </div>

                    <div className="configure_skype_connect  col-sm-6">

                        <div className={this.props.Bot.BindTo=="Skype" ? "connect_wrap active_border_config" : "connect_wrap"}>
                            <h2>Connect from Skype</h2>
                            {/*<span onClick={this.props.OpenSkype} className="question_popup"><span className="icon_question"></span></span>*/}

                            {this.props.Bot.BindTo ? this.Disconnect("Skype") :
                           <div>
                               <input className="chat_input borderR" type="text" placeholder="Enter your skype app id" name="SkypeId" value={this.state.SkypeId} onChange={this.handleInputChange}/>
                               <input className="chat_input borderR" type="password" placeholder="Enter your skype app password" name="SkypePass" value={this.state.SkypePass} onChange={this.handleInputChange}/>
                            <button className="btn_all bg_shadow mt_10" onClick={this.connectSkype}>Connect you bot with Skype bot</button>
                           </div> }
                        </div>
                    </div>
                </div>
                <div className="configure_connect_bottom">
                    <div className="configure_slack_connect col-sm-6">
                        <div className={this.props.Bot.BindTo=="Slack" ? "connect_wrap active_border_config" : "connect_wrap"}>

                            <h2>Connect with Slack</h2>
                            {/*<span onClick={this.props.OpenSlack} className="question_popup"><span className="icon_question"></span></span>*/}
                            {this.props.Bot.BindTo ? this.Disconnect("Slack") :
                                <div>

                                    <input className="chat_input borderR" type="text" placeholder="Enter your client id" name="ClientId" value={this.state.ClientId} onChange={this.handleInputChange}/>
                                    <input className="chat_input borderR" type="password" placeholder="Enter your client secret" name="ClientSecret" value={this.state.ClientSecret} onChange={this.handleInputChange}/>
                                    <input className="chat_input borderR" type="text" placeholder="Enter your verification token" name="SlackVerificationToken" value={this.state.SlackVerificationToken} onChange={this.handleInputChange}/>
                            <button className={"btn_all bg_shadow mt_10"} onClick={this.connectSlack}>Connect you bot with Slack bot
                                </button>
                                </div> }
                        </div>
                    </div>
                    <div className="configure_site_connect col-sm-6">
                        <div className={this.props.Bot.BindTo=="Standalone" ? "connect_wrap active_border_config" : "connect_wrap"}>
                            <h2>Connect with Web chat</h2>
                            {/*<span onClick={this.props.OpenSite} className="question_popup"><span className="icon_question"></span></span>*/}
                            {this.props.Bot.BindTo ? this.Disconnect("Standalone") :
                                <div>
                                    <input className="chat_input borderR" type="text" placeholder="Enter your skype app id" name="SkypeId2" value={this.state.SkypeId2} onChange={this.handleInputChange}/>
                                    <input className="chat_input borderR" type="password" placeholder="Enter your skype app password" name="SkypePass2" value={this.state.SkypePass2} onChange={this.handleInputChange}/>
                            <input className="chat_input borderR" type="text" placeholder="Enter your azure bot name" name="SkypeBotName" value={this.state.SkypeBotName} onChange={this.handleInputChange}/>
                            <input className="chat_input borderR" type="text" placeholder="Enter your secret key for web chat" name="SkypeFrameSecret" value={this.state.SkypeFrameSecret} onChange={this.handleInputChange}/>
                            <button className="btn_all bg_shadow mt_10" onClick={this.connectWeb}>Connect you bot with Web chat</button>
                                </div> }
                        </div>
                    </div>
                </div>

<div className="overlay_chart mt_25">
    <div className="configure_connect_top">
        {b ? <span>Connected to {soc}</span> : null}
    </div>
</div>
            </div>
        );
    }
}

class Configure extends Loading {
    constructor(props) {
        super(props);
        this.state = {
            User: {},
            Bots: [],
            Bot: {},
            Pages: [],
            SkypeTut: false,
            SlackTut: false,
            SiteTut: false,
            FacebookTut: false,
            FbPageToken: "",
            FbPage:"",
            Wait: 0
        };
        this.chooseBot= this.chooseBot.bind(this);
        this.connect = this.connect.bind(this);
        this.disconnect = this.disconnect.bind(this);
        this.loadData = this.loadData.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.connectFbAdminBot = this.connectFbAdminBot.bind(this);
    }

    connectFbAdminBot() {
        var url = config.Backend +'/bot/edit?';
        if (!(cookies.get('Id') === undefined))
        {
            url += 'CurrentUserId=' + cookies.get('Id').toString();
        }
        url+='&Id='+this.state.Bot._id;
        url+='&Page='+this.state.FbPage;
        url+='&PageToken='+this.state.FbPageToken;

        this.setState({Wait:++this.state.Wait});jQuery.getJSON(url, function (res) {
            if (!res.Error) {this.setState({Wait:--this.state.Wait});
                alert("Bot was connected");
                this.state.Bot.BindTo="FB";
                window.location.href= window.location.origin+"/chat/configure";
            } else {
                 this.setState({Wait:--this.state.Wait});alert(JSON.stringify(res));
            }
        }.bind(this));
    }

    FbAdminBot() {
        return (<div className="overlay_chart">
            <div className="configure_connect_top">
                <div className="configure_skype_connect  col-sm-6">
                    <div className="connect_wrap active_border_config">
                        <h2>Connect Facebook admin bot</h2>
                        {/*<span onClick={this.props.OpenSkype} className="question_popup"><span className="icon_question"></span></span>*/}
                        <input className="chat_input borderR" type="text" placeholder="Enter your facebook page id" name="FbPage" value={this.state.FbPage} onChange={this.handleInputChange}/>
                        <input className="chat_input borderR" type="text" placeholder="Enter your facebook page token" name="FbPageToken" value={this.state.FbPageToken} onChange={this.handleInputChange}/>
                        <button className="btn_all bg_shadow mt_10" onClick={this.connectFbAdminBot}>Connect you bot with Facebook page</button>
                    </div>
                </div>
            </div>
        </div>)
    }

    PopupManualFacebook(){
        return(
            <div className="popup_manual">
                <div className="popup_manual_edit">
                    <button className="cansel" onClick={()=>this.setState({FacebookTut:false})}></button>
                    <div className="popup_content">
                        <h3>Facebook</h3>
                        <h3>2: Connect you bot with our webhook</h3>
                        <div className="manual_image">
                            <div className="manual_img border_img">
                                <img src="/img/Manual/skype/2.png"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    PopupManual(){
        return(
            <div className="popup_manual">
                <div className="popup_manual_edit">
                    <button className="cansel" onClick={()=>this.setState({SkypeTut:false})}></button>
                    <div className="popup_scroll">
                        <Scrollbars
                            style={{ height: "90%" }}>
                            <div className="popup_content">
                                <h3>1. Register a Microsoft record</h3>
                                <h3>2: Create a microsoft bot</h3>
                                <div className="manual_image">
                                    <div className="manual_img border_img">
                                        <img src="/img/Manual/site/1.png"/>
                                    </div>
                                </div>
                                <div className="manual_image">
                                    <div className="manual_img border_img">
                                        <img src="/img/Manual/skype/1.png"/>
                                    </div>
                                </div>
                                <h3>3. Go to the "Skype" tab</h3>
                                <div className="manual_image">
                                    <div className="manual_img border_img">
                                        <img src="/img/Manual/skype/3.png"/>
                                    </div>
                                </div>
                                <h3>4: Connect you bot with our webhook</h3>
                                <div className="manual_image">
                                    <div className="manual_img border_img">
                                        <img src="/img/Manual/skype/2.png"/>
                                    </div>
                                </div>
                                <h3>5: Copy you skype application Id and password and press the connect button</h3>
                            </div>
                        </Scrollbars>
                    </div>
                </div>
            </div>
        )
    }

    PopupManualSlack(){
        return(
            <div className="popup_manual">
                <div className="popup_manual_edit">
                    <button className="cansel" onClick={()=>this.setState({SlackTut:false})}></button>
                    <div className="popup_scroll">
                        <Scrollbars
                            style={{ height: "90%" }}>
                            <div className="popup_content">
                                <h3>
                                    If you do not have an account, create an account
                                    and your channel slack.
                                </h3>
                                <h3>
                                    Please log in.</h3>
                                <div className="manual_image">
                                    <div className="manual_img border_img">
                                        <img src="/img/Manual/slack/1.png"/>
                                    </div>
                                </div>
                                <h3>2: Authorize</h3>
                                <h3>3: You will see in the applications slack, the added application ChatBot</h3>
                                <div className="manual_image">
                                    <div className="manual_img border_img">
                                        <img src="/img/Manual/slack/2.png"/>
                                    </div>
                                </div>
                            </div>
                        </Scrollbars>
                    </div>
                </div>
            </div>
        )
    }

    PopupManualSite(){
        return(
            <div className="popup_manual">
                <div className="popup_manual_edit">
                    <button className="cansel" onClick={()=>this.setState({SiteTut:false})}></button>
                    <div className="popup_scroll">
                        <Scrollbars
                            style={{ height: "90%" }}>
                            <div className="popup_content">
                                <h3>
                                    Before you add a bot to your site, you must install a skype bot.</h3>
                                <br/>

                                <h3>1. Go to the Web Chat tab.</h3>
                                <div className="manual_image">
                                    <div className="manual_img border_img">
                                        <img src="/img/Manual/site/2.png"/>
                                    </div>
                                </div>
                                <h3>2. Copy the "Secret keys" as shown in the image</h3>
                                <br/>
                                <p>The <b>YourIDnumber</b> line is copied and pasted onto the chatbot site</p>
                                <p>After clicking on the button "Connect you bot with Web chat"</p>
                                <div className="manual_image">
                                    <div className="manual_img border_img">
                                        <img src="/img/Manual/site/3.png"/>
                                    </div>
                                </div>
                            </div>
                        </Scrollbars>
                    </div>
                </div>
            </div>
        )
    }

    handleInputChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }


    disconnect()
    {
        var url = config.Backend +'/bot/edit?';
        if (!(cookies.get('Id') === undefined))
        {
            url += 'CurrentUserId=' + cookies.get('Id').toString();
        }
        url+='&Id='+this.state.Bot._id;
        url+='&Page=-';

        this.setState({Wait:++this.state.Wait});jQuery.getJSON(url, function (res) {
            if (!res.Error) {this.setState({Wait:--this.state.Wait});
                this.loadData();
            } else {
                 this.setState({Wait:--this.state.Wait});alert(JSON.stringify(res));
            }
        }.bind(this));
    }

    connect(pageId,token)
    {
        var x = config.Backend + "/fb/subscribe?"
            +"PageId="+pageId
            +"&BotId="+this.state.Bot._id
            +"&accessToken="+token;
        this.setState({Wait:++this.state.Wait});jQuery.getJSON(x,function (res) {
        this.setState({Wait:--this.state.Wait});
        });


        var url = config.Backend +'/bot/edit?';
        if (!(cookies.get('Id') === undefined))
        {
            url += 'CurrentUserId=' + cookies.get('Id').toString();
        }
        url+='&Id='+this.state.Bot._id;
        url+='&Page='+pageId;
        url+='&PageToken='+token;

        this.setState({Wait:++this.state.Wait});jQuery.getJSON(url, function (res) {
            if (!res.Error) {this.setState({Wait:--this.state.Wait});
                for(var i=0;i<this.state.Pages.length;i++)
                {
                    if(this.state.Pages[i].id==pageId)
                    {
                        for(var j=0;j<this.state.Bots.length;j++)
                        {
                            if(this.state.Bots[j]._id==this.state.Bot._id)
                            {
                                var arr = this.state.Pages;
                                arr[i].Bot=j+"";
                                this.setState({Pages:[]});
                                this.setState({Pages:arr});
                                window.location.replace(window.location.origin + '/chat/configure');
                            }
                        }
                    }
                }
                this.setState({Bots:this.state.Bots,ShowRenamePopup:false});
                this.state.Bot.BindTo="FB";
            } else {
                 this.setState({Wait:--this.state.Wait});alert(JSON.stringify(res));
            }
        }.bind(this));
    }

    chooseBot(id) {
        cookies.set('BotId', id, {path: '/'});
        window.location.replace(window.location.origin + '/chat/build');
    }

    loadData()
    {
        var url = config.Backend +'/users/findById?';
        if (!(cookies.get('Id') === undefined))
        {
            url += 'Id=' + cookies.get('Id').toString();
        }
        this.setState({Wait:++this.state.Wait});jQuery.getJSON(url, function (res) {
            if (!res.Error) {this.setState({Wait:--this.state.Wait});
                // if(res.Type=="Super")
                //   window.location.replace(window.location.origin + '/admin/main');
                this.setState({User: res},()=>{
                    var url = config.Backend + '/bot/findById?';
                    if (!(cookies.get('Id') === undefined))
                    {
                        url += 'CurrentUserId=' + cookies.get('Id').toString();
                    }
                    url += '&Id=' + cookies.get('BotId').toString();
                    this.setState({Wait:++this.state.Wait});jQuery.getJSON(url, function (bot) {
                        this.setState({Wait:--this.state.Wait})
                        if (!bot.Error) {
                            if(!config.checkRights(cookies.get('Id').toString(),bot,"Configure"))
                            {
                                config.accessDenied(cookies.get('Id').toString(),res);

                            }
                                    this.setState({Bot: bot},()=>{
                                        this.setState({FbPage:this.state.Bot.Page,FbPageToken:this.state.Bot.PageToken})
                                        this.setState({Bots: bot},()=>{
                                            if(res.Fb_id && res.accessToken) {
                                                var url = config.FbApi + '/' + res.Fb_id
                                                    + "/accounts?access_token=" + res.accessToken;
                                                this.setState({Wait:++this.state.Wait});jQuery.getJSON(url, function (pages) {
                                                    this.setState({Wait:--this.state.Wait})
                                                    console.log(pages)
                                                    var arr = []
                                                    for (var i = 0; i < pages.data.length; i++) {
                                                        if (pages.data[i].perms.indexOf("ADMINISTER") != -1) {
                                                            pages.data[i].Bot = "-";
                                                            for (var j = 0; j < this.state.Bots.length; j++) {
                                                                if (this.state.Bots[j].Page == pages.data[i].id) {
                                                                    pages.data[i].Bot = j + "";
                                                                }
                                                            }
                                                            arr.push(pages.data[i]);
                                                        }
                                                    }
                                                    this.setState({Pages: arr});
                                                }.bind(this));
                                            }
                                        });
                                    });
                        } else {
                        }
                    }.bind(this));
                });
            } else {
                window.location.replace(window.location.origin + '/login');
            }
        }.bind(this));
    }

    componentWillMount() {
        const params = new URLSearchParams(this.props.location.search);
        const code = params.get('code');
        var ClientSecret =cookies.get("ClientSecret");
        var ClientId =cookies.get("ClientId");
        var SlackVerificationToken =cookies.get("SlackVerificationToken");
        if(code && ClientSecret && ClientId && SlackVerificationToken)
        {
            var url = config.SlackApi + '/oauth.access?client_id='+
                ClientId+'&client_secret='+ClientSecret+'&code='+code
                +'&redirect_uri='+config.RedirectAddAppUri;
            this.setState({Wait:++this.state.Wait});jQuery.getJSON(url,function (res) {
            this.setState({Wait:--this.state.Wait})
                if(res.ok)
                {
                    var url = config.Backend + '/bot/bindTo?';
                    if (!(cookies.get('Id') === undefined))
                    {
                        url += 'CurrentUserId=' + cookies.get('Id').toString();
                    }
                    url+= '&Id='+cookies.get('BotId').toString();
                    url+= '&SlackVerificationToken='+SlackVerificationToken;
                    url+= '&SlackTeamId='+res.team_id;
                    url+= '&SlackTeamName='+res.team_name;
                    url+= '&SlackBotToken='+res.bot.bot_access_token;
                    url+= '&SlackBotId='+res.bot.bot_user_id;

                    this.setState({Wait:++this.state.Wait});jQuery.getJSON(url,function (res) {
                    this.setState({Wait:--this.state.Wait})
                        alert("Bot was connected")
                    window.location.href= window.location.origin+"/chat/configure";
                        //alert(JSON.stringify(res))
                    }.bind(this))
                }
                else{
                    alert(JSON.stringify(res))
                }
            }.bind(this))
        }
        this.loadData();
    }


    render() {
        var pages = this.state.Pages.map(function (page) {
            return (
                <li>
                    <span>{page.name} </span>
                    {page.Bot=="-" ? <button className={'btn_all'} onClick={()=>this.connect(page.id,page.access_token)}>Connect to page</button> : <span>Connected to </span>}
                    {page.Bot=="-" ? null: <button onClick={()=>this.chooseBot(this.state.Bots[+page.Bot]._id)}>{this.state.Bots[+page.Bot].Name}</button>}
                </li>
            )
        }.bind(this));

        var PageName = "-";
        for(var i=0;i<this.state.Pages.length;i++)
        {
            if(this.state.Pages[i].id==this.state.Bot.Page)
            {
                PageName = this.state.Pages[i].name;
            }
        }

        var page = <li>
            <span>{PageName} </span>
            <button className={'btn_all'} onClick={()=>this.disconnect()}>Disconnect</button>
        </li>

        //<TopPanel />
        //<LeftPanel User={this.state.User}/>
        return <div>
            {this.Load()}
            {this.state.FacebookTut ? this. PopupManualFacebook() : null}
            {this.state.SkypeTut ? this.PopupManual() : null}
            {this.state.SlackTut ? this.PopupManualSlack() : null}
            {this.state.SiteTut ? this.PopupManualSite() : null}
            <TopPanel/>
            <LeftPanel User={this.state.User}/>
            <div className="mainBroadCast grow_main">
                <Scrollbars
                    style={{ height: "90%" }}>
                    <div className="grow_height">
                {this.state.Bot && this.state.Bot.BindTo=="Standalone" ?
                    <iframe height={500} width={"100%"} src={'https://webchat.botframework.com/embed/'+this.state.Bot.SkypeBotName+'?s='+this.state.Bot.SkypeFrameSecret}></iframe> : null}
                {this.state.User && this.state.User.Type=="Super" ? this.FbAdminBot() : null}
                {this.state.User && this.state.User.Type=="Admin" ?<PagesList
                    loadBot={this.loadData} addWait={()=>this.setState({Wait:++this.state.Wait})}
                    delWait={()=>this.setState({Wait:this.state.Wait-1})}
                    Bot={this.state.Bot} pages={PageName!="-" ? page : pages} OpenFacebook={()=>this.setState({FacebookTut:true})}
                    Bot={this.state.Bot} pages={PageName!="-" ? page : pages} OpenSkype={()=>this.setState({SkypeTut:true})}
                    Bot={this.state.Bot} pages={PageName!="-" ? page : pages} OpenSlack={()=>this.setState({SlackTut:true})}
                    Bot={this.state.Bot} pages={PageName!="-" ? page : pages} OpenSite={()=>this.setState({SiteTut:true})}
                /> : null}
                    </div>
                </Scrollbars>
            </div>
        </div>
    }
}



export default Configure;