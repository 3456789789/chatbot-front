import React, { Component}  from 'react';
import jQuery from 'jquery';
import Cookies from 'universal-cookie';
import TopPanel from "../../TopPanel";
import { Scrollbars } from 'react-custom-scrollbars';
import Loading from '../../../Loading.js';

const cookies = new Cookies();
var config = require('../../../config.js');


class Tariffs extends Loading {
    constructor(props) {
        super(props);
        this.state = {
            User: {},
            Tariffs: [],
            Wait:0,
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.load = this.load.bind(this);
        this.buy = this.buy.bind(this);
    }
    buy(id,i) {
        var url = config.Backend + '/tariffs/bind?Id='+id+'&CurrentUserId='+cookies.get("Id").toString();
        jQuery.getJSON(url,function (res) {
            if(!res.Error)
            {
                alert("Тариф был успешно подключен(продлен)");
                this.load();
            }
            else
            {
                alert(JSON.stringify(res));
            }
        }.bind(this))
    }

    handleInputChange(event,i) {
        this.state.Tariffs[i][event.target.name] = event.target.type!="checkbox" ? event.target.value : (event.target.checked ? 1 : 0);
        this.setState({
            Tariffs : this.state.Tariffs,
        })
    }

    load()
    {
        var url = config.Backend + '/users/findById?';
        if (!(cookies.get('Id') === undefined)) {
            url += 'Id=' + cookies.get('Id').toString();
        }
        jQuery.getJSON(url, function (res) {
            if (!res.Error) {
                this.setState({User: res}, () => {
                    var url = config.Backend + '/tariffs/all?';
                    if (!(cookies.get('Id') === undefined)) {
                        url += '&CurrentUserId=' + cookies.get('Id').toString();
                    }
                    jQuery.getJSON(url, function (res) {
                        if (!res.Error) {
                            this.setState({Tariffs:res})
                        } else {
                            alert(JSON.stringify(res));
                        }
                    }.bind(this));
                });
            } else {
                window.location.replace(window.location.origin + '/login');
            }
        }.bind(this));
    }
    componentWillMount() {
        this.load();
    }
    TList() {
        var tariffs;
        if (this.state.Tariffs) {
            tariffs = this.state.Tariffs.map(function (elem, ind) {
                const x= elem;
                const i = ind;
                var days = "";
                var what = "";
                if(x.Days.indexOf("M") != -1)
                {
                    days = x.Days.replace("M","");
                    what = "(в месяцах)";
                }
                else {
                    days = x.Days.replace("D","");
                    what = "(в днях)";
                }
                var end = this.state.User.TariffEnd.toString();
                if(!x.Adv) {
                    return (
                        <div className={"tariffs_items"}>
                            <ul className={"tariffs_list_item"}>
                                <li><h3>{x.Name}</h3></li>
                                <li>Цена: {x.Price} руб.</li>
                                <li>Максимальное кол-во ботов:<span> {x.Cnt}</span></li>
                                <li>Длительность <span>{what}: {days}</span></li>
                                {x.Start ? <li>Этот тариф является стартовым(он активируется сразу после
                                    регистрации)</li> : null}
                                {x._id == this.state.User.Tariff ? <li>Вы подключены к этому тарифу<br/>(истекает:{
                                    end.substr(0, end.indexOf("T"))})</li> : null}

                                {x.Deleted || x.Start ? null :
                                    <button className={"btn_all btn_all bg_shadow mt_15"}
                                            onClick={() => this.buy(x._id, i)}>{x._id == this.state.User.Tariff ? "Продлить" : "Подключить"}</button>}
                            </ul>
                        </div>)
                }
            }.bind(this))
        } else {
            tariffs = null;
        }
        return (<div className={"list_tariffs_plan"}>{tariffs}</div>)
    }


    render() {
        return (
            <div>
                <TopPanel />
                {/*<div className="list_tariffs_plan">*/}
                    <div className="grow_main_tariffs">
                        <Scrollbars
                            style={{ height: "90%" }}>
                            <div className="col-sm-12">
                                <h1>Управление услугами</h1>
                                <p>Вы можете в любой момент перейти на другой тарифный план или добавить к текущему тарифу дополнительные услуги.</p>
                            </div>
                            {this.TList()}
                        </Scrollbars>

                    </div>
                </div>



                // </div>

        )
    }
}

export default Tariffs;