import React, { Component}  from 'react';
import jQuery from 'jquery';
import Cookies from 'universal-cookie';
import Popup from "../../Popup";
import LeftPanel from "../../LeftPanel";
import TopPanel from "../../TopPanel";
import Content from "../Build/Content";
import {Bar, Pie, Line} from 'react-chartjs-2';
import { Scrollbars } from 'react-custom-scrollbars';
import ReactTooltip from 'react-tooltip'
import Switch from 'react-toggle-switch';
import Loading from '../../../Loading.js';

const cookies = new Cookies();
var config = require('../../../config.js');
// require `react-d3-core` for Chart component, which help us build a blank svg and chart title.

class AlalyzeMain extends Loading{
    constructor(props) {
        super(props);
        this.state = {
            User: {},
            Bot: {},
            BotUsers: [],
            BlocksData: [],
            BtnsData: [],
            WordsData: [],
            Msgs: [],
            UniqueUsers:[],
            Wait: 0,
            Tables: false,
            Table:"MsgsTable",
            MsgsPeriod: "Week",
        }
        this.change = config.change.bind(this);
        this.load_data = config.load_data.bind(this);
        this.select_period = this.select_period.bind(this);
        this.getData = this.getData.bind(this);
        this.reset = this.reset.bind(this);
        this.hidePopups = this.hidePopups.bind(this);
    }
    hidePopups()
    {
        this.setState({ShowPopularBlockPopup:false,ShowPopularWordsPopup:false,ShowPopularButtonsPopup:false});
    }
     select_period(e)
    {
        this.change(e);
        switch (e.target.name)
        {
            case "MsgsPeriod":
                this.load_data("Msgs","bot/time_msgs",e.target.value,{Bot:cookies.get('BotId')});
                this.load_data("UniqueUsers","bot/time_users",e.target.value,{Bot:cookies.get('BotId')});
                break;
        }
    }
    reset() {
        var url = config.Backend + '/bot/resetStat?';
        if (!(cookies.get('Id') === undefined))
        {
            url += 'CurrentUserId=' + cookies.get('Id').toString();
        }
        url+= '&Id=' + this.state.Bot._id;
        jQuery.getJSON(url, function (res) {
            if (!res.Error) {
                alert("Success!")
                this.load();
            } else {
                alert(JSON.stringify(res))
            }
        }.bind(this))
    }

    getData(attr) {
        var datas = {}

            var labels = [];
            var data = [];
            var colors = [];
            /*[
                        '#97cc64',
                        '#ffd963',
                    ]*/

            for (var i = 0; i < this.state.BotUsers.length; i++) {
                var ind = labels.indexOf(this.state.BotUsers[i][attr]);
                if (ind == -1) {
                    labels.push(this.state.BotUsers[i][attr])
                    data.push(1);
                    var r = Math.floor(Math.random() * (256));
                    var g = Math.floor(Math.random() * (256));
                    var b = Math.floor(Math.random() * (256));
                    var c = '#' + r.toString(16) + g.toString(16) + b.toString(16);
                    colors.push(c);
                } else {
                    data[ind]++;
                }
            }
            datas = {
                labels: labels,
                datasets: [{
                    data: data,
                    backgroundColor: colors,
                    hoverBackgroundColor: colors
                }]
            };

            return datas;
    }




    pieGraphic(){


        return(
            <div>
                <h2 className="">Floor</h2>

                <div className="listPie">
                    {this.state.Bot.BindTo == "Skype" ?
                    <div className="pieWrap">
                        <div className="pie">
                            <Pie
                                data={this.getData("platform")}
                                height={100}
                                width={100}
                            />
                        </div>
                        <div className="pie">
                            <Pie data={this.getData("locale")}
                                 height={100}
                                 width={100}
                            />
                        </div>
                        <div className="pie">
                            <Pie data={this.getData("country")}
                                 height={100}
                                 width={100}
                            />
                        </div>
                    </div> : null}
                    {this.state.Bot.BindTo == "FB" ?
                        <div className="pieWrap">
                            <div className="pie">
                                <Pie data={this.getData("gender")}
                                     height={100}
                                     width={100}
                                />
                            </div>
                            <div className="pie">
                                <Pie data={this.getData("locale")}
                                     height={100}
                                     width={100}
                                />
                            </div>
                        </div> : null}
                </div>
            </div>
        )
    }

    popularWords(data){
        var labels = [];
        var arr = [];
        for(var i=0;i<data.length;i++)
        {
            if(data[i].Select)
            {
                labels.push(data[i].Name)
                arr.push(data[i].Cnt)
            }
        }
        var data = {
            labels: labels,
            datasets: [
                {
                    label: 'Popular answers',
                    backgroundColor: 'rgba(255,99,132,0.2)',
                    borderColor: 'rgba(255,99,132,1)',
                    borderWidth: 1,
                    hoverBackgroundColor: 'rgba(255,99,132,0.4)',
                    hoverBorderColor: 'rgba(255,99,132,1)',
                    data: arr
                }
            ]
        };
        return(
            <div>
                <div className="analyze_header">
                    <div className="analyze_header_caption">
                        <h2>Popular answers</h2>
                    </div>
                    <div className="analyze_header_hint">
                        <span  data-tip data-for='Popular_answers' className="analyze_header_hint_icon">
                            <ReactTooltip id='Popular_answers' aria-haspopup='true' role='example'>
                                <p>This graph displays the most frequently used messages</p>
                            </ReactTooltip>
                        </span>
                    </div>
                    <button className={'btn btn-default btn-sm ml_10 mt_10'} onClick={()=>this.setState({ShowPopularWordsPopup:true})}>Select</button>
                </div>

                <Bar
                    data={data}
                    width={100}
                    height={160}
                    options={{
                        maintainAspectRatio: false
                    }}
                />
            </div>
        )
    }


    line(data){
      /*  var datasets = data.map(function (e,i) {
            return {
                label: e.Name,
                backgroundColor: 'rgba(255,99,132,0.2)',
                borderColor: 'rgba(255,99,132,1)',
                borderWidth: 1,
                hoverBackgroundColor: 'rgba(255,99,132,0.4)',
                hoverBorderColor: 'rgba(255,99,132,1)',
                data: [e.Cnt]
            }
        })*/
        var labels = [];
        var arr = [];
      for(var i=0;i<data.length;i++)
      {
          if(data[i].Select)
          {
              labels.push(data[i].Name)
              arr.push(data[i].Cnt)
          }
      }
        var data = {
            labels: labels,
            datasets: [
                {
                    label: 'Popular blocks',
                    backgroundColor: 'rgba(255,99,132,0.2)',
                    borderColor: 'rgba(255,99,132,1)',
                    borderWidth: 1,
                    hoverBackgroundColor: 'rgba(255,99,132,0.4)',
                    hoverBorderColor: 'rgba(255,99,132,1)',
                    data: arr
                }
            ]
        };
        return(
            <div>
                <div className="analyze_header">
                    <div className="analyze_header_caption">
                        <h2>Popular blocks</h2>
                    </div>
                    <div className="analyze_header_hint">

                        <span  data-tip data-for='Popular_blocks' className="analyze_header_hint_icon">

                            <ReactTooltip id='Popular_blocks' aria-haspopup='true' role='example'>
                                <p>This graph displays the most commonly used message blocks</p>
                            </ReactTooltip>

                        </span>
                    </div>
                    <button className={'btn btn-default btn-sm ml_10 mt_10'} onClick={()=>this.setState({ShowPopularBlockPopup:true})}>Select</button>
                </div>

                <Bar
                    data={data}
                    width={100}
                    height={160}
                    options={{
                        maintainAspectRatio: false
                    }}
                />
            </div>
        )
    }


    UserActivity(){

        var s =this.state;
        var p_obj = config.period_obj(s.MsgsPeriod);
        var msgs = config.sort_data(s.Msgs.map((e)=>e.created_at),config.labels_time(p_obj));
        var users = [];
        var data = s.Msgs.map((e)=>{return{Time:e.created_at,User:e.User}});
        var labels_times = config.labels_time(p_obj);
        for(var k=0;k<labels_times.length;k++)
        {
            users.push({Cnt:0,Users:[]});
        }
        for(var i=0;i<data.length;i++)
        {
            var nearIndex = 0;
            var min = 1000*60*60*24*900;
            for(var j in labels_times)
            {
                if(Math.abs(data[i].Time-labels_times[j])<min)
                {
                    min =  Math.abs(data[i].Time-labels_times[j]);
                    nearIndex = j;
                }
            }
            if(users[nearIndex].Users.indexOf(data[i].User)==-1)
            {
                users[nearIndex].Cnt+=1;
                users[nearIndex].Users.push(data[i].User);
            }
        }
        var dataLine = {
            labels: config.labels(p_obj),
            datasets: [
                {
                    label: 'Unique users',
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: 'rgba(75,192,192,0.4)',
                    borderColor: 'rgba(75,192,192,1)',
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: 'rgba(75,192,192,1)',
                    pointBackgroundColor: '#fff',
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: 'rgba(75,192,192,1)',
                    pointHoverBorderColor: 'rgba(220,220,220,1)',
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: users.map(e=>e.Cnt)
                },
                {
                    label: 'Messages was sent',
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: 'rgba(75,0,192,0.4)',
                    borderColor: 'rgba(75,0,192,1)',
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: 'rgba(75,0,192,1)',
                    pointBackgroundColor: '#fff',
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: 'rgba(75,0,192,1)',
                    pointHoverBorderColor: 'rgba(220,220,220,1)',
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: msgs
                }
            ]
        };
        return(
            <div>
                <div className="analyze_header">
                    <div className="analyze_header_caption">
                        <h2>User activity</h2>
                    </div>
                    <div className="analyze_header_hint">
                        <span  data-tip data-for='User_activity' className="analyze_header_hint_icon">
                            <ReactTooltip id='User_activity' aria-haspopup='true' role='example'>
                                <p>This graph shows the number of new users, as well as the number of messages sent in accordance with the timeline</p>
                            </ReactTooltip>
                        </span>
                    </div>
                </div>
                <div className="form-group">
                    <select value={s.MsgsPeriod} name="MsgsPeriod" onChange={this.select_period} className="chat_input">
                        <option value="Week">during the week</option>
                        <option value="15Day">for 15 days</option>
                        <option value="Month">per month</option>
                        <option value="3Month">for 3 months</option>
                        <option value="Half Year">for half a year</option>
                        <option value="Year">in a year</option>
                    </select>
                </div>
                <Line
                    data={dataLine}
                    height={70}
                />
            </div>
        )
    }

    PopularBlockButton(data){
        var labels = [];
        var arr = [];
        for(var i=0;i<data.length;i++)
        {
            if(data[i].Select)
            {
                labels.push(data[i].Name)
                arr.push(data[i].Cnt)
            }
        }
        var data = {
            labels: labels,
            datasets: [
                {
                    label: 'Popular buttons',
                    backgroundColor: 'rgba(255,99,132,0.2)',
                    borderColor: 'rgba(255,99,132,1)',
                    borderWidth: 1,
                    hoverBackgroundColor: 'rgba(255,99,132,0.4)',
                    hoverBorderColor: 'rgba(255,99,132,1)',
                    data: arr,
                }
            ]
        };
        return(
            <div>
                <div className="analyze_header">
                    <div className="analyze_header_caption">
                        <h2>Popular buttons</h2>
                    </div>
                    <div className="analyze_header_hint">
                        <span  data-tip data-for='Popular_buttons' className="analyze_header_hint_icon">
                            <ReactTooltip id='Popular_buttons' aria-haspopup='true' role='example'>
                                <p>This graph shows frequently used buttons in blocks</p>
                            </ReactTooltip>
                        </span>
                    </div>
                    <button className={'btn btn-default btn-sm ml_10 mt_10'} onClick={()=>this.setState({ShowPopularButtonsPopup:true})}>Select</button>
                </div>

                <Bar
                    data={data}
                    width={100}
                    height={160}
                    options={{
                        maintainAspectRatio: false
                    }}
                />
            </div>
        )
    }

    load() {
        var s =this.state;
        var url = config.Backend +'/users/findById?';
        if (!(cookies.get('Id') === undefined))
        {
            url += 'Id=' + cookies.get('Id').toString();
        }
        this.setState({Wait:++this.state.Wait})
        jQuery.getJSON(url, function (res) {
            if (!res.Error) {
               // if(res.Type=="Super")
                 //   window.location.replace(window.location.origin + '/admin/main');
                this.setState({User: res,Wait:--this.state.Wait},()=>{
                    var url = config.Backend + '/bot/findById?';
                    if (!(cookies.get('Id') === undefined))
                    {
                        url += 'CurrentUserId=' + cookies.get('Id').toString();
                    }
                    url += '&Id=' + cookies.get('BotId').toString();
                    this.setState({Wait:++this.state.Wait})
                    jQuery.getJSON(url, function (res) {
                        if (!res.Error) {
                            if(!config.checkRights(cookies.get('Id').toString(),res,"Analyze"))
                            {
                                config.accessDenied(cookies.get('Id').toString(),res);

                            }
                                    this.setState({Bot: res,Wait:--this.state.Wait},() => {
                                        var route = "fb";
                                        if(this.state.Bot.BindTo=="Skype")
                                        {
                                            route="skype";
                                        }
                                        else if(this.state.Bot.BindTo=="Slack")
                                        {
                                            route="slack";
                                        }
                                        var url = config.Backend + "/"+route+"/getUsers?BotId="+this.state.Bot._id;
                                        if (!(cookies.get('Id') === undefined))
                                        {
                                            url += '&CurrentUserId=' + cookies.get('Id').toString();
                                        }
                                        this.setState({Wait:++this.state.Wait})
                                        jQuery.getJSON(url,function (res) {
                                            this.setState({Wait:--this.state.Wait})
                                            if(!res.Error)
                                            {
                                                this.setState({BotUsers:res})
                                            }
                                            else alert(JSON.stringify(res))
                                        }.bind(this));

                                        this.load_data("Msgs","bot/time_msgs",this.state.MsgsPeriod,{Bot:cookies.get("BotId")});
                                        this.load_data("UniqueUsers","bot/time_users",this.state.MsgsPeriod,{Bot:cookies.get("BotId")});

                                        var getBlockData=config.Backend + '/bot/getBlocksData?Id=' + this.state.Bot._id;
                                        if (!(cookies.get('Id') === undefined)) {
                                            getBlockData += '&CurrentUserId=' + cookies.get('Id').toString();
                                        }
                                        this.setState({Wait:++this.state.Wait})

                                        jQuery.getJSON(getBlockData,function (res) {
                                            if(!res.Error)
                                            {
                                                var blocks = res.Blocks.map(e=>{ return {Name:e.Name,Cnt:e.Cnt,Select:true}})
                                                this.setState({BlocksData:blocks,Wait:--this.state.Wait})
                                                console.log(res)
                                            }
                                            else
                                            {
                                                this.setState({Wait:--this.state.Wait})
                                                alert(JSON.stringify(res))
                                            }
                                        }.bind(this));
                                        getBlockData=config.Backend + '/bot/getBtnsData?Id=' + this.state.Bot._id;
                                        if (!(cookies.get('Id') === undefined)) {
                                            getBlockData += '&CurrentUserId=' + cookies.get('Id').toString();
                                        }
                                        this.setState({Wait:++this.state.Wait})

                                        jQuery.getJSON(getBlockData,function (res) {
                                            if(!res.Error)
                                            {
                                                var blocks = res.Blocks.map(e=>{ return {Name:e.Name,Cnt:e.Cnt,Select:true}})
                                                this.setState({BtnsData:blocks,Wait:--this.state.Wait})
                                            }
                                            else
                                            {
                                                this.setState({Wait:--this.state.Wait})
                                                alert(JSON.stringify(res))
                                            }
                                        }.bind(this));
                                        getBlockData=config.Backend + '/bot/getWordsData?Id=' + this.state.Bot._id;
                                        if (!(cookies.get('Id') === undefined)) {
                                            getBlockData += '&CurrentUserId=' + cookies.get('Id').toString();
                                        }
                                        this.setState({Wait:++this.state.Wait})

                                        jQuery.getJSON(getBlockData,function (res) {
                                            if(!res.Error)
                                            {
                                                var blocks = res.Blocks.map(e=>{ return {Name:e.Name,Cnt:e.Cnt,Select:true}})
                                                this.setState({WordsData:blocks,Wait:--this.state.Wait})
                                            }
                                            else
                                            {
                                                this.setState({Wait:--this.state.Wait})
                                                alert(JSON.stringify(res))
                                            }
                                        }.bind(this));
                                    });

                        } else {
                            this.setState({Wait:--this.state.Wait})
                            alert(JSON.stringify(res));
                        }
                    }.bind(this));
                });
            } else {
                window.location.replace(window.location.origin + '/login');
            }
        }.bind(this));
    }

    componentWillMount() {
        this.preload();
     this.load();
    }

    Content(){
        var s=this.state;
        var labels = [];
        var now = new Date().getMonth();
        var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July','August','September','October','November','December']
        for(var i=now-this.state.MonthCnt+1;i<now+1;i++) {
            var x = i > 11 ? i - 12 : i;
            x = x <0 ? 12+i : x;
            labels.push(months[x])
        }

        var ShowUserAct = this.state.Bot.BindTo;
        var ShowBlocks = this.state.BlocksData.length>0;
        var ShowBtns = this.state.BtnsData.length>0;
        var ShowWords = this.state.WordsData.length>0;
        //                        {!ShowUserAct && !ShowBlocks && !ShowBtns ? null : <button className="btn_all mt_25 " onClick={this.reset}>Reset statistics</button>}

        return(
            <div className="mainContent">
                {!ShowUserAct && !ShowBlocks && !ShowBtns ? <div className="no_data"><div className="content_none_data"><h1>No one has written to<br/> your bot yet</h1></div></div> : null}

                <div className="chart_full grow_main">
                    <div className="analyze_top_block">
                    </div>
                    <div className="form-check form-check-inline">
                        <Switch
                            onClick={()=>this.setState({Tables:!s.Tables})}
                            on={s.Tables}
                            className={'form-check-input'}
                            id={"Tables"}/>
                        <label className={'form-check-label ml_10'} onClick={()=>this.setState({Tables:!s.Tables})}
                               htmlFor="Tables">Tables/Graphs</label></div>
                    {s.Tables ? this.Tables() :
                    <div>
                       <div className="chart_main">
                           <div className="chart_one">
                               {this.UserActivity()}
                           </div>

                           {(this.state.Bot.BindTo=="Skype" || this.state.Bot.BindTo=="FB") && false ?
                               <div className="chart_right">
                                   {this.pieGraphic()}
                               </div> : null}
                       </div>
                       <div className="chart_main">
                           {ShowBlocks ?
                               <div className="chart_three">
                                   {this.line(this.state.BlocksData)}
                               </div> : null}
                           {ShowWords ?
                               <div className="chart_three">
                                   {this.popularWords(this.state.WordsData)}
                               </div> : null}

                           {ShowBtns ?
                               <div className="chart_three">
                                   {this.PopularBlockButton(this.state.BtnsData)}
                               </div> : null}
                       </div>
                   </div>}
                </div>
            </div>
        )
    }
    PopularBlockPopup(x)
    {
        var blocks = this.state[x].map(function (e,i) {

            return (<tr>
                <td className={'tl'}>{e.Name}</td>
                <td className={'tac'}>{e.Cnt}</td>
                <td><input type={"checkbox"} name={x+".Select"} checked={e.Select} onChange={(e)=>this.change(e,i)}/></td>
            </tr>)
        }.bind(this))

        return (
            <div className="preloader_bots">
            <div className="preloader_bots_2 popupW">
                    <div className="analyse_content">
                        <Scrollbars
                            style={{ height: "90%" }}>
                        <div className="analyse_content_2">
                <table className={'table'}>
                    <thead>
                    <tr>
                        <th className={'tl'}>Name</th>
                        <th className={'tac'}>Count</th>
                        <th className={'tac'}>Select</th>
                    </tr>
                    </thead>
                    <tbody>
                        {blocks}
                    </tbody>
                </table>
                {/*<button onClick={this.hidePopups}>Close</button>*/}
                    </div>
                        </Scrollbars>
                        <div className="close_popup_btn">
                            <button onClick={this.hidePopups} type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                    </div>
            </div>
        </div>)
    }
    PopularTable(x) {
        var blocks = this.state[x].map(function (e,i) {
            return (<tr>
                <td className={'tl'}>{e.Name}</td>
                <td className={'tac'}>{e.Cnt}</td>
            </tr>)
        }.bind(this))
        return (
                    <div className="analyse_content">
                            <div className="">
                                <table className={'table'}>
                                    <thead>
                                    <tr>
                                        <th className={'tl'}>Name</th>
                                        <th className={'tac'}>Count</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {blocks}
                                    </tbody>
                                </table>
                            </div>
                    </div>
        )
    }
    PopularBlocksTable() {
        return this.PopularTable("BlocksData");
    }
    PopularAnswersTable() {
        return this.PopularTable("WordsData");
    }
    PopularButtonsTable() {
        return this.PopularTable("BtnsData");
    }
    MsgsTable() {
        var s=this.state;
        var p_obj = config.period_obj(s.MsgsPeriod,true);
        var labels = config.labels(p_obj);
        labels.reverse();
        var labels_time = config.labels_time(p_obj);
        labels_time.reverse();
        var msgs = config.sort_data(s.Msgs.map((e)=>e.created_at),labels_time);
        var users = [];
        var data = s.Msgs.map((e)=>{return{Time:e.created_at,User:e.User}});
        var labels_times = labels_time;
        for(var k=0;k<labels_times.length;k++)
        {
            users.push({Cnt:0,Users:[]});
        }
        for(var i=0;i<data.length;i++)
        {
            var nearIndex = 0;
            var min = 1000*60*60*24*900;
            for(var j in labels_times)
            {
                if(Math.abs(data[i].Time-labels_times[j])<min)
                {
                    min =  Math.abs(data[i].Time-labels_times[j]);
                    nearIndex = j;
                }
            }
            if(users[nearIndex].Users.indexOf(data[i].User)==-1)
            {
                users[nearIndex].Cnt+=1;
                users[nearIndex].Users.push(data[i].User);
            }
        }
        users = users.map(e=>e.Cnt);
        var rows = labels.map((e,i)=>{
            return (<tr>
                <td>{e}</td>
                <td className={'tac'}>{msgs[i]}</td>
                <td className={'tac'}>{users[i]}</td>
            </tr>)
        })
        return (<div>
            <div className="form-group">
                <select value={s.MsgsPeriod} name="MsgsPeriod" onChange={this.select_period} className="chat_input">
                    <option value="Week">during the week</option>
                    <option value="15Day">for 15 days</option>
                    <option value="Month">per month</option>
                    <option value="3Month">for 3 months</option>
                    <option value="Half Year">for half a year</option>
                    <option value="Year">in a year</option>
                </select>
            </div>
            <table className={'table'}>
                <thead>
                <tr>
                    <th>Time</th>
                    <th className={'tac'}>Messages</th>
                    <th className={'tac'}>Unique Users</th>
                </tr>
                </thead>
                <tbody>
                {rows}
                </tbody>
            </table>
        </div>)
    }
    Tables() {
        var s=this.state;
        return (<div className={'mt_20'}>
            <button className="btn_all mr_2" onClick={() => this.setState({Table:"MsgsTable"})}>Users Activity</button>
            <button className="btn_all mr_2" onClick={() => this.setState({Table:"PopularBlocksTable"})}>Popular Blocks</button>
            <button className="btn_all mr_2" onClick={() => this.setState({Table:"PopularAnswersTable"})}>Popular Answers</button>
            <button className="btn_all" onClick={() => this.setState({Table:"PopularButtonsTable"})}>Popular Buttons</button>
            {this[s.Table]()}
        </div>)
    }
    render() {
        var s=this.state;
        return (
            <div>
                {this.Load()}
                {s.ShowPopularBlockPopup ? this.PopularBlockPopup('BlocksData') : null}
                {s.ShowPopularWordsPopup ? this.PopularBlockPopup('WordsData') : null}
                {s.ShowPopularButtonsPopup ? this.PopularBlockPopup('BtnsData') : null}
                <TopPanel />
                <LeftPanel User={this.state.User}/>
                <div className="mainAnalize grow_main">

                <Scrollbars
                    style={{ height: "90%" }}>
                    <div className="grow_height">
                {this.Content()}
                    </div>
                </Scrollbars>
                    </div>
            </div>

        )
    }
}
export default AlalyzeMain;