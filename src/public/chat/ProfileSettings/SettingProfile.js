import React, { Component}  from 'react';
import jQuery from 'jquery';
import Cookies from 'universal-cookie';
import LeftPanel from "../../LeftPanel";
import TopPanel from "../../TopPanel";
import FacebookLogin from 'react-facebook-login';
import Loading from '../../../Loading.js';

const cookies = new Cookies();
var config = require('../../../config.js');


class SettingProfile extends Loading {
    constructor(props) {

        super(props);
        this.state = {
            User: {},
            Name: '',
            Name_valid: true,
            Email: '',
            Email_valid: true,
            Password: '',
            Password_valid: true,
            repeat_Password: '',
            repeat_Password_valid: true,
            OldPassword: '',
            OldPassword_valid: true,
            Response_txt: '',
            Error: false,
            Wait: 0
        }

        this.submitFormHandler = this.submitFormHandler.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleInputFocus = this.handleInputFocus.bind(this);
        this.addIcon = this.addIcon.bind(this);
        this.clearInput = this.clearInput.bind(this);
    }
    addIcon(e) {
        e.preventDefault();
        let reader = new FileReader();
        let file = e.target.files[0];
        reader.onloadend = () => {
            var url = reader.result;
            if(file) {
                var fd = new FormData;
                fd.append('File', file);
                this.setState({Wait:++this.state.Wait});
                jQuery.ajax({
                    method: "POST",
                    processData: false,
                    contentType: false,
                    url: config.Backend + "/content/file",
                    success: function (msg) {
                        config.getData("users/setup_icon",{Icon:msg,Id:this.state.User._id}).then(function (ok) {
                            this.setState({Wait:--this.state.Wait});
                            window.location.reload();
                        }.bind(this),function (err) {
                            this.setState({Wait:--this.state.Wait})
                            alert(JSON.stringify(err))
                        }.bind(this))

                    }.bind(this),
                    data: fd,
                })
            }
        }
        reader.readAsDataURL(file)
    }
    clearInput(e) {
        e.target.value = null;
    }
    componentWillMount() {
        this.preload();
        var url = config.Backend +'/users/findById?';
        if (!(cookies.get('Id') === undefined))
        {
            url += 'Id=' + cookies.get('Id').toString();
        }

        this.setState({Wait:++this.state.Wait});
        jQuery.getJSON(url, function (res) {
            if (!res.Error) {
                this.setState({User: res});
                this.setState({Name:res.Name})
                this.setState({Email:res.Email})
                this.setState({Wait:--this.state.Wait})
            } else {
                window.location.replace(window.location.origin + '/login');
            }
        }.bind(this));
    }

    handleInputChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    handleInputFocus(event) {
        this.setState({
            [event.target.name + '_valid']: true
        })
    }

    submitFormHandler(event) {
        const Email_filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!Email_filter.test(this.state.Email) && this.state.Email.trim() != "") {
            this.setState({Email_valid: false});
        } else if (this.state.Password !== this.state.repeat_Password) {
            this.setState({repeat_Password_valid: false});
        } else {

            var url =config.Backend + '/users/edit?';

            if (this.state.Email.trim() != "")
                url += '&Email=' + this.state.Email
            if (this.state.Password.trim() != "")
                url+='&Password=' + this.state.Password;
            if (this.state.Name.trim() != "")
                url+='&Name=' + this.state.Name;
            if (this.state.OldPassword.trim() != "")
                url+='&OldPassword=' + this.state.OldPassword;

            if (!(cookies.get('Id') === undefined))
            {
                url += '&Id=' + cookies.get('Id').toString();
                url += '&CurrentUserId=' + cookies.get('Id').toString();
            }

            this.setState({Wait:++this.state.Wait});jQuery.getJSON(url, function (res) {
                if (res.Error)
                {
                    this.setState({Response_txt: res.Error});
                    this.setState({Error: true});
                    this.setState({Wait:--this.state.Wait})

                } else {
                    this.setState({Wait:--this.state.Wait})

                    this.setState({Error: false});
                    //this.setState({Response_txt: res.Ok});
                    this.setState({User: res.User})
                }
            }.bind(this));

        }
        event.preventDefault();
    }


    Email() {
        const danger = this.state.Email_valid ? '' : <div className="text-danger">Check your E-mail address.</div>;
        return (
                <div className="form-group">
                    <label htmlFor="optionsEmail">Email address</label>
                    <input type="Email"  className="chat_input" id="optionsEmail" value={this.state.Email} name="Email" onChange={this.handleInputChange} onFocus={this.handleInputFocus} />
                    {danger}
                </div>
                )
    }

    Name() {
        return (
                <div className="form-group">
                    <label htmlFor="optionsName">Name</label>
                    <input type="text" className="chat_input" name="Name" value={this.state.Name} onChange={this.handleInputChange} onFocus={this.handleInputFocus} id="optionsName" />
                </div>
                )
    }

    Password() {
        const danger = this.state.Password_valid ? '' : <div className="text-danger">Check your Password.</div>;
        return (
                <div className="form-group">
                    <label htmlFor="optionsPassword">Password</label>
                    <input type="password" className="chat_input"  id="optionsPassword" value={this.state.Password} name="Password" onChange={this.handleInputChange} onFocus={this.handleInputFocus} />
                    {danger}
                </div>
                )
    }

    OldPassword() {
        const danger = this.state.OldPassword_valid ? '' : <div className="text-danger">Check your Password.</div>;
        return (
            <div className="form-group">
                <label htmlFor="optionsPassword">Old Password</label>
                <input type="password" className="chat_input"  id="optionsPassword" value={this.state.OldPassword} name="OldPassword" onChange={this.handleInputChange} onFocus={this.handleInputFocus} />
                {danger}
            </div>
        )
    }

    repeat_Password() {
        const danger = this.state.repeat_Password_valid ? '' : <div className="text-danger">Repeat your Password.</div>;
        return (
                <div className="form-group">
                    <label htmlFor="optionsPassword">Confirm password</label>
                    <input type="password"  className="chat_input" id="optionsPassword" value={this.state.repeat_Password} name="repeat_Password" onChange={this.handleInputChange} onFocus={this.handleInputFocus} />
                    {danger}
                </div>
                )
    }

    Response() {
        const danger = this.state.Error ? <div className="text-danger">{this.state.Response_txt}</div> : <div>{this.state.Response_txt}</div>;
        return (<div className="">
            {danger}
        </div>
                )
    }

    MainContent() {
        /*
         <div className="col-sm-6 tr mt_15">
                                <button onClick={()=>{window.open("https://www.facebook.com/pages/create/",'_blank')}} className="btn btn-default CreatePage">Create page Facebook</button>
                            </div>
         */

        return (

                            <div className="overlay_chart profile_settings mt_15">
                                <label className={'avatar_settings'} htmlFor={'avatar_settings'}>Upload avatar</label>
                                <div className="user_avatar_profile mb_15">
                                    <input id={'avatar_settings'} type="file" accept="image/*" onChange={(e)=>this.addIcon(e)} onClick={this.clearInput}   className="add_image_avatar_bot text-primary "/>
                                </div>

                                <form onSubmit={this.submitFormHandler}>
                                    {this.Name()}
                                    {this.Email()}
                                    {this.Password()}
                                    {this.repeat_Password()}
                                    {this.OldPassword()}
                                    {this.Response()}
                                    <a className="btn_all mr_2" href={config.Frontend+"/dashboard"}>Cancel</a>
                                    <button type="submit" className="btn btn_setting btn-default">Save</button>
                                </form>

                            </div>
                )
    }

    render() {
        var s =this.state;
        var text="";
        if(s.Preload && s.Preload['Title'])
            text = s.Preload['Title'];
        return (
                <div>
                    {/*{this.Load()}*/}
                    <TopPanel Text={text}/>
                    {this.MainContent()}
                    {/*<LeftPanel User={this.state.User}/>*/}
                </div>
                )
    }
}
;
export default SettingProfile;