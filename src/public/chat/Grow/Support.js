import React, {Component} from 'react';
import jQuery from 'jquery';
import Cookies from 'universal-cookie';
import Popup from "../../Popup";
import LeftPanel from "../../LeftPanel";
import TopPanel from "../../TopPanel";
import ChatBubble from 'react-chat-bubble';
import { Scrollbars } from 'react-custom-scrollbars';
import Loading from '../../../Loading.js';

const cookies = new Cookies();
var config = require('../../../config.js');


class Support extends Loading {
    constructor(props) {
        super(props);
        this.state = {
            User: {},
            Msgs: [],
            Wait: 0
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.sendAnswer = this.sendAnswer.bind(this);
    }
    sendAnswer(text)
    {
        var url = config.Backend + "/support/sendToAdmin?";
        url += '&Text='+text;
        if (!(cookies.get('Id') === undefined)) {
            url += '&Id=' + cookies.get('Id').toString();
        }
        jQuery.getJSON(url, function (res) {
            if (!res.Error) {
                this.state.Msgs.push({"type":0,"text":text})
                this.setState({Msgs:this.state.Msgs});
            } else {
                alert(JSON.stringify(res))
            }
        }.bind(this))
    }
    handleInputChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }
    componentWillMount() {
        var url = config.Backend + '/users/findById?';
        if (!(cookies.get('Id') === undefined)) {
            url += 'Id=' + cookies.get('Id').toString();
        }
        jQuery.getJSON(url, function (res) {
            if (!res.Error) {
                //if(res.Type=="Super")
                //  window.location.replace(window.location.origin + '/admin/main');
                this.setState({User: res},()=>{
                    for (var i = 0; i < res.Dialog.length; i++) {
                        this.state.Msgs.push({"type": res.Dialog[i].Owner == "User" ? 0 : 1, "text": res.Dialog[i].Text})
                    }
                    this.setState({Msgs:this.state.Msgs});
                });
            } else {
                window.location.replace(window.location.origin + '/login');
            }
        }.bind(this));
    }
    Chat() {
        return (
            <div className={"support_chat_message"}>
                    <ChatBubble messages={this.state.Msgs} onNewMessage={this.sendAnswer}/>

            </div>)
    }
    render() {
        return (
            <div>
                <TopPanel />
                <div className="grow_main_tariffs">
                    <Scrollbars
                        style={{ height: "90%" }}>
                        <div className="col-sm-12 z1000">
                            <h1>Помощь и поддержка</h1>
                            <p>Если Ваш вопрос технического плана, то желательно предоставить более подробную информацию о проблеме, такую как, например, номер ошибки  или при каких обстоятельствах проявляются ошибки.</p>
                            <p>Данная информация значительно ускорит время ответа и избавит от лишних вопросов. Для более подробной информации загляните в раздел
                                <a target={"_blank"} href="https://botkits.ru/manual.html"> Помощь.</a></p>
                            {this.Chat()}
                        </div>

                    </Scrollbars>
                </div>
            </div>




        )
    }
}

export default Support;