import React, {Component} from 'react';
import jQuery from 'jquery';
import Cookies from 'universal-cookie';
import Popup from "../../Popup";
import LeftPanel from "../../LeftPanel";
import TopPanel from "../../TopPanel";
import ChatBubble from 'react-chat-bubble';
import { Scrollbars } from 'react-custom-scrollbars';
import Loading from '../../../Loading.js';
import TextareaAutosize from 'react-autosize-textarea';
const cookies = new Cookies();
var config = require('../../../config.js');


class RTC extends Loading {
    constructor(props) {
        super(props);
        this.state = {
            User: null,
            Bot: null,
            BotUsers: [],
            ChatUser: "",
           // Answer: "",
            Wait:0,
            Msgs: [],
            Text:"",
        };
        this.openChat = this.openChat.bind(this);
        this.unlockUser = this.unlockUser.bind(this);
        this.refresh = this.refresh.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.sendAnswer = this.sendAnswer.bind(this);
        this.takeUser = this.takeUser.bind(this);
        this.freeUser = this.freeUser.bind(this);
        this.change = config.change.bind(this);
    }
    takeUser()  
    {
        var fd = this.state.Bot.Share;
        for(var i=0;i<fd.UsersWithRights.length;i++)
        {
            if(fd.UsersWithRights[i].User==cookies.get('Id').toString())
            fd.UsersWithRights[i].Chat = this.state.ChatUser;
        }
        this.setState({Wait:++this.state.Wait});jQuery.ajax({
            method: "POST",
            contentType: 'application/json',
            url: config.Backend + '/share/editFull',
            success: function (res) {
                if (!res.Error) {this.setState({Wait:--this.state.Wait});
                    this.state.Bot.Share = fd;
                    this.setState({Bot:this.state.Bot})
                     
                } else {
                    this.setState({Wait:--this.state.Wait});alert(JSON.stringify(res));
                }
            }.bind(this),
            data: JSON.stringify(fd),
        })
    }
    freeUser()
    {
        var fd = this.state.Bot.Share;
        if(fd) {
            for (var i = 0; i < fd.UsersWithRights.length; i++) {
                if (fd.UsersWithRights[i].User == cookies.get('Id').toString())
                    fd.UsersWithRights[i].Chat = undefined;
            }
            this.setState({Wait:++this.state.Wait});jQuery.ajax({
                method: "POST",
                contentType: 'application/json',
                url: config.Backend + '/share/editFull',
                success: function (res) {
                    if (!res.Error) {this.setState({Wait:--this.state.Wait});
                        this.state.Bot.Share = fd;
                        this.setState({Bot: this.state.Bot})
                         
                    } else {
                        this.setState({Wait:--this.state.Wait});alert(JSON.stringify(res));
                    }
                }.bind(this),
                data: JSON.stringify(fd),
            })
        }
    }
    sendAnswer()
    {
        var s=this.state;
        var text = this.state.Text;
        var route = "";//facebook app.js
        if (this.state.Bot.BindTo == "Skype" || this.state.Bot.BindTo == "Standalone") {
            route = "skype/";
        }
        else if (this.state.Bot.BindTo == "Slack") {
            route = "slack/";
        }
        var url = config.Backend + "/" + route + "sendMsgToAdminDialog?BotId=" + this.state.Bot._id;
        url += '&UserId='+this.state.ChatUser;
        url += '&Text='+text;
        url += '&From='+(this.state.User.Email || this.state.User.Name);
        if (!(cookies.get('Id') === undefined)) {
            url += '&CurrentUserId=' + cookies.get('Id').toString();
        }
        this.setState({Wait:++this.state.Wait});jQuery.getJSON(url, function (res) {
            if (!res.Error) {this.setState({Wait:--this.state.Wait});
            var user =s.ChatUser || {};
                var id = user.recipientId || user.Id || user.Psid;
                this.state.Msgs.push({"Bot":s.Bot._id,"Text":text,User:id,Owner:"Admin",Name:"Admin",
                    created_at:new Date().valueOf()})
                this.setState({Msgs:this.state.Msgs,Text:""});
            } else {
                alert(JSON.stringify(res))
            }
        }.bind(this))
    }
    handleInputChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }
    /*openChat(user) {
        this.state.Msgs = [];
        for (var i = 0; i < user.Dialog.length; i++) {
            this.state.Msgs.push({"type": user.Dialog[i].Owner == "Admin" ? 0 : 1, "text": user.Dialog[i].Text})
        }
        this.setState({Msgs: this.state.Msgs,ChatUser:user._id});
    }*/

openChat(user) {
    //this.setState({Wait:++this.state.Wait});
    var id = user.recipientId || user.Id || user.Psid;
    config.getData("bot/history",{Period:"All",Times:1,Bot:this.state.Bot._id,User:id}).then(function (h) {
      //  this.setState({Wait:--this.state.Wait})
        this.setState({Msgs:h,ChatUser:user._id});
    }.bind(this),function (err) {
        this.setState({Wait:--this.state.Wait});alert(JSON.stringify(err));
    }.bind(this))
}
    unlockUser() {
        var route = "fb";
        if (this.state.Bot.BindTo == "Skype" || this.state.Bot.BindTo == "Standalone") {
            route = "skype";
        }
        else if (this.state.Bot.BindTo == "Slack") {
            route = "slack";
        }
        var url = config.Backend + "/" + route + "/unlockUser?BotId=" + this.state.Bot._id;
        url += '&UserId='+this.state.ChatUser;
        if (!(cookies.get('Id') === undefined)) {
            url += '&CurrentUserId=' + cookies.get('Id').toString();
        }
        this.setState({Wait:++this.state.Wait});jQuery.getJSON(url, function (res) {
            if (!res.Error) {this.setState({Wait:--this.state.Wait});
                this.freeUser();
                this.setState({ChatUser:""},function () {
                    this.refresh();
                }.bind(this))
            } else {
                alert(JSON.stringify(res))
            }
        }.bind(this))
    }
    refresh() {
        var route = "fb";
        if (this.state.Bot.BindTo == "Skype" || this.state.Bot.BindTo=="Standalone") {
            route = "skype";
        }
        else if (this.state.Bot.BindTo == "Slack") {
            route = "slack";
        }
        var url = config.Backend + "/" + route + "/getUsers?BotId=" + this.state.Bot._id;
        url += '&ChatWithAdmin=1';
        if (!(cookies.get('Id') === undefined)) {
            url += '&CurrentUserId=' + cookies.get('Id').toString();
        }
        //this.setState({Wait:++this.state.Wait})
        jQuery.getJSON(url, function (res) {
            //this.setState({Wait:--this.state.Wait});
            if (!res.Error) {
                this.setState({BotUsers: res})
                if (this.state.ChatUser) {
                    for (var i = 0; i < res.length; i++) {
                        if(this.state.ChatUser==res[i]._id)
                        {
                            this.openChat(res[i]);
                        }
                    }
                }
            }
            else alert(JSON.stringify(res))
        }.bind(this));

    }
    componentWillMount() {
        this.preload();
        var url = config.Backend + '/users/findById?';
        if (!(cookies.get('Id') === undefined)) {
            url += 'Id=' + cookies.get('Id').toString();
        }
        this.setState({Wait:++this.state.Wait});jQuery.getJSON(url, function (res) {
            if (!res.Error) {this.setState({Wait:--this.state.Wait});
                //if(res.Type=="Super")
                //  window.location.replace(window.location.origin + '/admin/main');
                this.setState({User: res}, () => {
                    var url = config.Backend + '/bot/findById?';
                    if (!(cookies.get('Id') === undefined)) {
                        url += 'CurrentUserId=' + cookies.get('Id').toString();
                    }
                    url += '&Id=' + cookies.get('BotId').toString();
                    this.setState({Wait:++this.state.Wait});jQuery.getJSON(url, function (res) {
                        if (!res.Error) {this.setState({Wait:--this.state.Wait});
                            if(!config.checkRights(cookies.get('Id').toString(),res,"RTC"))
                            {
                                config.accessDenied(cookies.get('Id').toString(),res);

                            }
                                    this.setState({Bot: res}, function () {
                                        this.refresh();
                                        setInterval(this.refresh,10000);
                                    }.bind(this));

                        } else {
                            this.setState({Wait:--this.state.Wait});alert(JSON.stringify(res));
                        }
                    }.bind(this));
                });
            } else {
                window.location.replace(window.location.origin + '/login');
            }
        }.bind(this));
    }

    Users() {
        var b = this.state.Bot;
        var u = this.state.User;
        var chats = [];
        if(b && b.Share) {
            for (var i = 0; i < b.Share.UsersWithRights.length; i++) {
                if (b.Share.UsersWithRights[i].Chat)
                    chats.push(b.Share.UsersWithRights[i].Chat)
            }
        }
        var chat = null;
        if(u && b)
        {
            chat = config.checkDialog(u._id,b);
        }
        var users;
        if (this.state.BotUsers) {
            users = this.state.BotUsers.map(function (l, i) {
                var name ="";
                if(l.name)
                    name = l.name;
                else if(l.first_name)
                    name = l.first_name;
                else if(l._id)
                    name = l._id;

                if((!chat && chats.indexOf(l._id.toString())==-1) || (chat && chat==l._id)) {
                    return (
                        <div className="cell pl mt_15">
                            <button className="btn_all mr_5 mt_5" onClick={() => this.openChat(l)}>{name}</button>
                        </div> )
                }
                else {
                    return (
                        <div>
                        </div> )
                }

            }.bind(this))
        } else {
            users = null;
        }
        return (<div>{users}</div>)
    }
    Chat() {
    var s=this.state;
        var b = this.state.Bot;
        var u = this.state.User;
        var chat = null;
        if(u && b)
        {
            chat = config.checkDialog(u._id,b);
        }
        var msgs = s.Msgs.map((elem,ind)=>{
            const e =elem;
            const i =ind;
            var name=e.Name || "User";
            var photo="/img/avatar_chat/user.svg";
            switch (e.Owner) {
                case "Bot": photo = "/img/avatar_chat/robot-face.svg";
                name = "Bot";break;
                case "Admin": photo = "/img/avatar_chat/admin.svg";
                    name = "Admin";break;
            }

            return (<div className={'chat_message col__10'}>
                <div className="row__grid">
                    <div className="col__1 row__grid grid__justify--content grid__align--center">
                        <div className="chat_message_avatar row__grid justify-content-center">
                            {/*<img src="/img/avatar_chat/robot-face.svg"/>*/}
                            {/*<img src="/img/avatar_chat/admin.svg"/>*/}
                            <img src={photo}/>
                        </div>
                    </div>

                    <div className="col__9">
                        <div className="row__grid">
                            <div className="col__4">
                                <div className={'chat_name'}>{name}</div>
                            </div>
                            <div className="col__6 justify__content--end row__grid">
                                <div className={'chat_date'}>{new Date(e.created_at).toLocaleString()}</div>
                            </div>
                        </div>
                        <div className={'chat_text'}>{e.Text}</div>
                    </div>
                </div>
            </div>)
        })
        return (

            <div className={'col__5'}>
            <div
                className="MessageList pl"
                style={{height: '63vh',position:'relative'}}
                ref={(div) => {
                    this.messageList = div;
                }}
            >
                    {/*<ChatBubble messages={this.state.Msgs} onNewMessage={this.sendAnswer}/>*/}
                    <div className={'row__grid w-100'}>
                        {msgs}
                    </div>


            </div>
                <div className="form-group bd pd-t-20 pd-b-20 pd-x-20 mt_5">
                    <div className="row__grid">
                        <div className="col__6 row__grid grid__justify--content align-content-center">

                            <TextareaAutosize
                                placeholder={'message...'}
                                rows = {0}
                                onChange={this.change}
                                value={this.state.Text}
                                className={'button__message_textarea w-100'}
                                name="Text">
                            </TextareaAutosize>

                        </div>
                        <div className="col__3 row__grid justify-content-center align-content-center">
                            <button onClick={this.sendAnswer} className={'button__message'}>
                                <span className={'mr_5'}>Sent</span>
                                <i className="fz16 far fa-paper-plane"></i>
                            </button>
                        </div>
                        {/*<div className="col__1 row__grid justify-content-center align-content-center">
                            <input type="file" onChange={this.loadFile} onClick={this.clearInput} className="file_message" />
                        </div>*/}


                        {/* <div className="col__3 row__grid">
                            <button className="btn__all--small mr_5">On</button>
                            <button className="btn__all--small">Off</button>
                        </div>*/}
                    </div>
                </div>





                <button className="btn_all mr_5" onClick={this.unlockUser}>Unlock</button>
                {!chat && u._id!=b.User  ? <button className="btn_all" onClick={this.takeUser}>Take this dialog</button> : null}
                {chat ? <button className="btn_all" onClick={this.freeUser}>Free user</button> : null}
            </div>)
    }


    handleScroll() {
        var s=this.state;
        const  scrollContainer = this.messageList.scrollTop;
        if(scrollContainer < 50 && this.state.LoadMoreEnable) {
            const height = this.messageList.clientHeight;
            var msgs =s.History.length;
            this.messageList.scrollTop = height*(msgs/(msgs+10));
            this.show(s.ChatUser,false,true);
            this.setState({LoadMoreEnable:false},()=>{
                setTimeout(()=>{this.setState({LoadMoreEnable:true})},3000)
            })
        }
    }

    scrollToBottom=()=> {
        try {
            var scrollHeight = this.messageList.scrollHeight;
            const height = this.messageList.clientHeight;
            const maxScrollTop = scrollHeight - height;
            this.messageList.scrollTop = maxScrollTop > 0 ? maxScrollTop : 0;

        } catch (e) {
            console.log(e.toString())
        }
    }


    Empty()
    {
     return (<div className={'message_block_center'}>
             <div class="grid__text_block">
                 <div className="grid__row">
                 <div className="grid__col--3">
                     <div className="image__icon">
                         <img src="/img/user_rts.svg" width={70} height={70} alt=""/>
                     </div>
                 </div>
                     <div className="grid__col--7 grid__align--center">
                         <h4 className={'mt_25'}>No users</h4>
                     </div>
                 </div>
             </div>

        </div>)
    }
    render() {
        var s =this.state;
        return (
            <div>
                {this.Load()}
                <TopPanel/>
                <LeftPanel User={this.state.User}/>
                <div className="mainBroadCast grow_main">
                    {s.BotUsers.length ?
                    <Scrollbars
                        style={{ height: "100%" }}>

                    <div className={'row_grid'}>
                        {this.Users()}
                        {this.state.ChatUser ? this.Chat() : null}
                    </div>
                    </Scrollbars> : this.Empty()}
                </div>
            </div>
        )
    }
}

export default RTC;