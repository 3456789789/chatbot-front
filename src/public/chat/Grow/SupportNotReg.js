import React, {Component} from 'react';
import jQuery from 'jquery';
import Cookies from 'universal-cookie';
import Popup from "../../Popup";
import LeftPanel from "../../LeftPanel";
import TopPanel from "../../TopPanel";
import ChatBubble from 'react-chat-bubble';
import { Scrollbars } from 'react-custom-scrollbars';
import Loading from '../../../Loading.js';

const cookies = new Cookies();
var config = require('../../../config.js');


class SupportNotReg extends Loading {
    constructor(props) {
        super(props);
        this.state = {
            Msgs: [],
            Wait: 0
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.sendAnswer = this.sendAnswer.bind(this);
    }
    sendAnswer(text)
    {
        var url = config.Backend + "/support/sendToAdminNotReg?";
        url += '&Text='+text;
        if (!(cookies.get('Id') === undefined)) {
            url += '&Id=' + cookies.get('Id').toString();
        }
        jQuery.getJSON(url, function (res) {
            if (!res.Error) {
                this.state.Msgs.push({"type":0,"text":text})
                this.setState({Msgs:this.state.Msgs});
            } else {
                alert(JSON.stringify(res))
            }
        }.bind(this))
    }
    handleInputChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }
    componentWillMount() {
        var url = config.Backend + '/support/getUser?';
        if (!(cookies.get('Id') === undefined)) {
            url += 'Id=' + cookies.get('Id').toString();
        }
        jQuery.getJSON(url, function (res) {
            if (!res.Error) {
                this.setState({User: res},()=>{
                    for (var i = 0; i < res.Dialog.length; i++) {
                        this.state.Msgs.push({"type": res.Dialog[i].Owner == "User" ? 0 : 1, "text": res.Dialog[i].Text})
                    }
                    this.setState({Msgs:this.state.Msgs});
                });
            } else {
                alert(JSON.stringify(res))
            }
        }.bind(this));
    }
    Chat() {
        return (<div><ChatBubble messages={this.state.Msgs} onNewMessage={this.sendAnswer}/>
        </div>)
    }
    render() {
        return (
            <div>
                <header class="content_page_chat_bot_header">
                    <div class="container">
                        <div class="row">
                            <div class="main_top_header">
                                <div class="main_top_header_logo">
                                    <a href="/">
                                        <img src="/img/logo_header.png"></img>
                                    </a>
                                </div>
                                <div class="main_top_header_menu">
                                    <ul>
                                        <li><a href="/about.html">Возможности</a></li>
                                        <li><a href="https://dashboard.botkits.ru/login">Войти</a></li>
                                        <li><a href="https://dashboard.botkits.ru/register">Регистрация</a></li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                    </div>
                </header>
                <div className="grow_main_tariffs">
                <Scrollbars
                    style={{ height: "90%" }}>
                <div className="container">
                    <div className="row">
                        <div className=" content_page_chatbot">
                                <div className="overlay_chart grow_main_post">
                                    <h1>Предложить идею</h1>
                                    <p>Предложите нам свои идеи по улучшению , и мы с удовольствием реализуем те из них, которые сделают вашу работу  еще быстрее и комфортнее.</p>
                                    {this.Chat()}
                                </div>
                        </div>

                    </div>
                </div>

                <footer class="main_footer mt60">
                    <div class="container">
                        <div class="row">
                            <div class="main_footer_top">
                                <ul class="bs-docs-footer-links">
                                    <li><img src="/img/footer_logo.png"></img></li>
                                    <li><a href="policies.html">Политика</a></li>
                                    <li><a href="about.html">О сервисе</a></li>
                                    <li><a href="manual.html">Помощь</a></li>
                                    <li><a href="contact.html">Контакты</a></li>
                                </ul>
                            </div>
                            <div class="main_footer_bottom">
                                <ul class="main_footer_bottom_left">
                                    <li>
                                        <span class="fz12">Botkits 2018 Все права защищены</span>
                                    </li>
                                </ul>
                                <ul class="main_footer_bottom_right">
                                    <li><a class="fz12" target="_blank" href="https://i-solutions.org/">Сделано в i-solution</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </footer>
                </Scrollbars>

                </div>
            </div>
        )
    }
}

export default SupportNotReg;