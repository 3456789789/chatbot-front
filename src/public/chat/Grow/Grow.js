import React, { Component}  from 'react';
import jQuery from 'jquery';
import Cookies from 'universal-cookie';
import Popup from "../../Popup";
import LeftPanel from "../../LeftPanel";
import TopPanel from "../../TopPanel";
import Content from "../Build/Content";
import Tags from "./Tags";
import TimeTags from "./TimeTags";
import { Scrollbars } from 'react-custom-scrollbars';
import Loading from '../../../Loading.js';

const cookies = new Cookies();
var config = require('../../../config.js');


class GrowMain extends Loading {
    constructor(props) {
        super(props);
        this.state = {
            User: {},
            Bot: {},
            Time:false,
            condition: false,
            Wait: 0,
            Commands: [],
            CommandIndex: 0,
        };
        this.onDeleteLink = this.onDeleteLink.bind(this);
        this.onDeleteTag = this.onDeleteTag.bind(this);
        this.onAddTag = this.onAddTag.bind(this);
        this.linkChanged = this.linkChanged.bind(this);
        this.formatChanged = this.formatChanged.bind(this);
        this.addWord = this.addWord.bind(this);
        this.pushCommand = this.pushCommand.bind(this);
        this.stateBot = this.stateBot.bind(this);
        this.ctrl = this.ctrl.bind(this);
    }
    stateBot(b)
    {
        this.setState({Wait:++this.state.Wait})
        config.getData('bot/edit',{Bot:b}).then(function (ok) {
            this.setState({Wait:--this.state.Wait})
            var b = this.state.Bot;
            b.Words = [];
            b.TimeWords = [];
            this.setState({Bot:b},()=>this.setState({Bot:ok}));
        }.bind(this),function (err) {
            this.setState({Wait:--this.state.Wait})
            alert(JSON.stringify(err))
        }.bind(this))
    }
    pushCommand(undo,redo)
    {
        var arr = [];
        for(var i =0;i<this.state.CommandIndex;i++)
        {
            arr.push(this.state.Commands[i]);
        }
        arr.push({Redo:redo,Undo:undo});
        this.setState({Commands:arr,CommandIndex:this.state.CommandIndex+1})
    }
    ctrl(e)
    {
        var s =this.state;
        if( e.ctrlKey && s.Commands.length>0) {
            if (e.keyCode == 90 && s.CommandIndex>0)//ctrl+z
            {
                this.setState({CommandIndex:--this.state.CommandIndex});
                s.Commands[s.CommandIndex].Undo();
            }
            else if (e.keyCode == 89 && s.CommandIndex<s.Commands.length)//ctrl+y
            {
                s.Commands[s.CommandIndex].Redo();
                this.setState({CommandIndex:s.CommandIndex+1});
            }
        }
    }
    onDeleteTag(i,ti) {
        var url = config.Backend + '/bot/edit?';
        if (!(cookies.get('Id') === undefined))
        {
            url += '&CurrentUserId=' + cookies.get('Id').toString();
        }
        url+= '&Id='+ this.state.Bot._id;
        url+= this.state.Time ? '&EditTimeWord=1': '&EditWord=1';
        url+= '&DeleteTag=1';
        url+= '&Index='+i;
        url+= '&TagIndex='+ti;
        this.setState({Wait:++this.state.Wait});jQuery.getJSON(url, function (res) {
            if (!res.Error) {this.setState({Wait:--this.state.Wait});
                const bot = JSON.parse(JSON.stringify(this.state.Bot));
                const new_bot = JSON.parse(JSON.stringify(res));
                this.pushCommand(()=>this.stateBot(bot),()=>this.stateBot(new_bot))
                this.setState({Bot:res});
            } else {
                 this.setState({Wait:--this.state.Wait});alert(JSON.stringify(res));
            }
        }.bind(this));
    }
    onAddTag(i,tag)
    {
        var url = config.Backend + '/bot/edit?';
        if (!(cookies.get('Id') === undefined))
        {
            url += '&CurrentUserId=' + cookies.get('Id').toString();
        }
        url+= '&Id='+ this.state.Bot._id;
        url+= this.state.Time ? '&EditTimeWord=1': '&EditWord=1';
        url+= '&Tag='+tag;
        url+= '&Index='+i;
        this.setState({Wait:++this.state.Wait});jQuery.getJSON(url, function (res) {
            if (!res.Error) {this.setState({Wait:--this.state.Wait});
                const bot = JSON.parse(JSON.stringify(this.state.Bot));
                const new_bot = JSON.parse(JSON.stringify(res));
                this.pushCommand(()=>this.stateBot(bot),()=>this.stateBot(new_bot))
                this.setState({Bot:res});
            } else {
                 this.setState({Wait:--this.state.Wait});alert(JSON.stringify(res));
            }
        }.bind(this));
    }
    onDeleteLink(i) {
        var url = config.Backend + '/bot/edit?';
        if (!(cookies.get('Id') === undefined))
        {
            url += '&CurrentUserId=' + cookies.get('Id').toString();
        }
        url+= '&Id='+ this.state.Bot._id;
        url+= this.state.Time ? '&DeleteTimeWord=1': '&DeleteWord=1';
        url+= '&Index='+i;
        this.setState({Wait:++this.state.Wait});
        jQuery.getJSON(url, function (res) {
            if (!res.Error) {this.setState({Wait:--this.state.Wait});
                const bot = JSON.parse(JSON.stringify(this.state.Bot));
                const new_bot = JSON.parse(JSON.stringify(res));
                this.pushCommand(()=>this.stateBot(bot),()=>this.stateBot(new_bot))
                var b = this.state.Bot;
                b.Words = [];
                b.TimeWords = [];
                this.setState({Bot:b},()=>this.setState({Bot:res}));
            } else {
                 this.setState({Wait:--this.state.Wait});alert(JSON.stringify(res));
            }
        }.bind(this));
    }

    formatChanged(i,newFormat) {
        var url = config.Backend + '/bot/edit?';
        if (!(cookies.get('Id') === undefined))
        {
            url += '&CurrentUserId=' + cookies.get('Id').toString();
        }
        url+= '&Id='+ this.state.Bot._id;
        url+= this.state.Time ? '&EditTimeWord=1': '&EditWord=1';
        url+= '&Index='+i;
        url+= '&Format='+newFormat;
        this.setState({Wait:++this.state.Wait});jQuery.getJSON(url, function (res) {
            if (!res.Error) {this.setState({Wait:--this.state.Wait});
                const bot = JSON.parse(JSON.stringify(this.state.Bot));
                const new_bot = JSON.parse(JSON.stringify(res));
                this.pushCommand(()=>this.stateBot(bot),()=>this.stateBot(new_bot))
                this.setState({Bot:res});
            } else {
                 this.setState({Wait:--this.state.Wait});alert(JSON.stringify(res));
            }
        }.bind(this));
    }

    linkChanged(i,newLink) {
        var url = config.Backend + '/bot/edit?';
        if (!(cookies.get('Id') === undefined))
        {
            url += '&CurrentUserId=' + cookies.get('Id').toString();
        }
        url+= '&Id='+ this.state.Bot._id;
        url+= this.state.Time ? '&EditTimeWord=1': '&EditWord=1';
        url+= '&Index='+i;
        url+= '&Link='+newLink;
        //this.setState({Wait:++this.state.Wait});
        jQuery.getJSON(url, function (res) {
            //this.setState({Wait:--this.state.Wait});
            if (!res.Error) {
                this.setState({Bot:res});
            } else {
                 alert(JSON.stringify(res));
            }
        }.bind(this));
    }
    addWord() {
        var url = config.Backend + '/bot/edit?';
        if (!(cookies.get('Id') === undefined))
        {
            url += '&CurrentUserId=' + cookies.get('Id').toString();
        }
        url+= '&Id='+ this.state.Bot._id;
        url+= this.state.Time ? '&AddTimeWord=1': '&AddWord=1';

        this.setState({Wait:++this.state.Wait});jQuery.getJSON(url, function (res) {
            if (!res.Error) {this.setState({Wait:--this.state.Wait});
                const bot = JSON.parse(JSON.stringify(this.state.Bot));
                const new_bot = JSON.parse(JSON.stringify(res));
                this.pushCommand(()=>this.stateBot(bot),()=>this.stateBot(new_bot))
                this.setState({Bot:res});
            } else {
                 this.setState({Wait:--this.state.Wait});alert(JSON.stringify(res));
            }
        }.bind(this));
    }
    componentWillMount() {
        document.onkeydown = this.ctrl;
        this.preload();
        var url = config.Backend +'/users/findById?';
        if (!(cookies.get('Id') === undefined))
        {
            url += 'Id=' + cookies.get('Id').toString();
        }

        this.setState({Wait:++this.state.Wait});jQuery.getJSON(url, function (res) {
            if (!res.Error) {this.setState({Wait:--this.state.Wait});
                //if(res.Type=="Super")
                  //  window.location.replace(window.location.origin + '/admin/main');
                this.setState({User: res},()=>{
                    var url = config.Backend + '/bot/findById?';
                    if (!(cookies.get('Id') === undefined))
                    {
                        url += 'CurrentUserId=' + cookies.get('Id').toString();
                    }
                    url += '&Id=' + cookies.get('BotId').toString();

                    this.setState({Wait:++this.state.Wait});jQuery.getJSON(url, function (res) {
                        if (!res.Error) {this.setState({Wait:--this.state.Wait});
                            if(!config.checkRights(cookies.get('Id').toString(),res,"Grow"))
                            {
                                config.accessDenied(cookies.get('Id').toString(),res);

                            }
                            this.setState({Bot: res});
                        } else {
                             this.setState({Wait:--this.state.Wait});alert(JSON.stringify(res));
                        }
                    }.bind(this));
                });
            } else {
                window.location.replace(window.location.origin + '/login');
            }
        }.bind(this));
    }


    TimeWords(){
        var words;
        if(this.state.Bot.TimeWords) {
            words = this.state.Bot.TimeWords.map((l, i) => <TimeTags onDeleteLink={this.onDeleteLink}
                                                             Link={l.Link} onAddTag={this.onAddTag} formatChanged={this.formatChanged}
                                                             onDeleteTag={this.onDeleteTag} Format={l.Format}
                                                             Word={l} Index={i} linkChanged={this.linkChanged}
            />)
        } else {
            words = null;
        }
        return(
            <div>
                <div className="EditAddBlock">
                    <div className="form_group_right_panel">
                        <button className="btnSave" onClick={this.addWord}>Add time word</button>
                    </div>
                    {words}
                </div>

            </div>
        )
    }
    handleClick() {
        this.setState({
            condition: !this.state.condition
        });
    }

    Words(){
        var words;
        if(this.state.Bot.Words) {
            words = this.state.Bot.Words.map((l, i) => <Tags onDeleteLink={this.onDeleteLink}
                                                               Link={l.Link} onAddTag={this.onAddTag}
                                                               onDeleteTag={this.onDeleteTag}
                                                               Word={l} Index={i} linkChanged={this.linkChanged}
                                                               />)
        } else {
            words = null;
        }
        return(
            <div>
                <div className="EditAddBlock">
                    <div className="form_group_right_panel">
                        <button className="btnSave" onClick={this.addWord}>Add word</button>
                    </div>
                    {words}
                </div>

            </div>
        )
    }

    render() {
        return (
            <div>
                {this.Load()}
                <TopPanel />
                <LeftPanel User={this.state.User}/>
                <div className="mainBroadCast">
                            <div className="col-sm-5 grow_main ">
                                <Scrollbars
                                    style={{ height: "90%" }}>
                                <div className="grow_height">
                                <div className="cell mt_15">
                                    <button onClick={()=>this.setState({Time:false},this.handleClick)} className={this.state.condition ? "btn_all mr_5" : "btn_all Gactive mr_5"}>Word understanding</button>
                                    <button onClick={()=>this.setState({Time:true},this.handleClick)} className={ this.state.condition ? "btn_all Gactive mr_5" : "btn_all mr_5"}>Time understanding</button>
                                    {this.state.Time ? this.TimeWords() : this.Words()}
                                </div>
                            </div>
                                </Scrollbars>
                            </div>
                </div>
            </div>
        )
    }
}

export default GrowMain;

