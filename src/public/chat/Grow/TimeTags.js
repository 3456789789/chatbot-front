import React, { Component}  from 'react';
import jQuery from 'jquery';
import Cookies from 'universal-cookie';
import { WithContext as ReactTags } from 'react-tag-input';

const cookies = new Cookies();
var config = require('../../../config.js');


class TimeTags extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            tags: [],
            Link: this.props.Link,
            Format: this.props.Format,
            Index:this.props.Index,
        };
        this.handleDelete = this.handleDelete.bind(this);
        this.handleAddition = this.handleAddition.bind(this);
        this.handleDrag = this.handleDrag.bind(this);
        this.linkChanged = this.linkChanged.bind(this);
        this.formatChanged = this.formatChanged.bind(this);
    }

    formatChanged(e) {
        this.setState({Format:e.target.value});
        this.props.formatChanged(+this.props.Index,e.target.value);
    }

    linkChanged(e) {
        this.setState({Link:e.target.value});
        this.props.linkChanged(+this.props.Index,e.target.value);
    }

    componentWillMount()
    {
        for(var i =0;i<this.props.Word.Tags.length;i++)
        {
            this.state.tags.push({id:i,text:this.props.Word.Tags[i]})
        }
    }

    handleDelete(i) {
        let tags = this.state.tags;
        tags.splice(i, 1);
        this.setState({tags: tags});
        this.props.onDeleteTag(this.state.Index,i);
    }

    handleAddition(tag) {
        let tags = this.state.tags;
        let ok = true;
        for(let i=0;i<tags.length;i++)
        {
            if(tags[i].text==tag)
                ok = false;
        }
        if(ok) {
            tags.push({
                id: tags.length + 1,
                text: tag
            });
            this.setState({tags: tags});
            this.props.onAddTag(this.state.Index, tag);
        }
    }

    handleDrag(tag, currPos, newPos) {
        let tags = this.state.tags;

        // mutate array
        tags.splice(currPos, 1);
        tags.splice(newPos, 0, tag);

        // re-render
        this.setState({ tags: tags });
    }
    Condition() {
        const { tags } = this.state;

        return(
            <div className="condition EditMessage">
                <div className="mb10 ">
                    <ReactTags type="text" className="hello" maxLength ="30" tags={tags}
                               handleDelete={this.handleDelete}
                               handleAddition={this.handleAddition}
                    />
                </div>
                <span className="condition_over mt_15">
                    <div className="condition_left">BOT UNDERSTAND AS: current_day + </div>
                    </span>
                    <input className="borderR chat_input" type="number" value={this.state.Link} onChange={this.linkChanged} />
                <span className="condition_over mt_15">
                    <div className="condition_left">TIME FORMAT</div>
                    </span>
                <select className="chat_input borderR" name="Format" value={this.state.Format} onChange={this.formatChanged}>
                    <option value="ISO_String">ISO_String</option>
                    <option value="Unixtime"> Unixtime</option>
                    <option value="yyyyMMddhhmm"> yyyyMMddhhmm</option>
                </select>
            </div>
        )
    }

    render() {
        //handleDrag={this.handleDrag}


        return (
            <div>

                {this.Condition()}

                <div className="button_delete_block">
                    <span className="right_delete_icon" onClick={()=>this.props.onDeleteLink(this.state.Index)}>
                    </span></div>
            </div>
        )
    }
}

export default TimeTags;