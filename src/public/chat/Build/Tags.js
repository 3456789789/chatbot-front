import React, { Component}  from 'react';
import jQuery from 'jquery';
import Cookies from 'universal-cookie';
import { WithContext as ReactTags } from 'react-tag-input';

const cookies = new Cookies();
var config = require('../../../config.js');


class Tags extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            tags: [],
            Link: this.props.Link,
            Index:this.props.Index,
            Text:true,
        };
        this.handleDelete = this.handleDelete.bind(this);
        this.handleAddition = this.handleAddition.bind(this);
        this.handleDrag = this.handleDrag.bind(this);
        this.linkChanged = this.linkChanged.bind(this);
        this.txtOrBlockChange = this.txtOrBlockChange.bind(this);
    }

    txtOrBlockChange(e) {
        this.setState({Text:e.target.value=="Text"})
        if(e.target.value=="Text") {
            this.setState({Link: "-"});
            this.props.linkChanged(+this.props.Index,"-");
        }
        else{
            this.setState({Link: this.props.Groups[0]._id});
            this.props.linkChanged(+this.props.Index,this.props.Groups[0]._id);
        }
    }

    linkChanged(e) {
        this.setState({Link:e.target.value});
        this.props.linkChanged(+this.props.Index,e.target.value);
    }

    componentWillMount()
    {
        for(var i =0;i<this.props.AiLink.Tags.length;i++)
        {
            this.state.tags.push({id:i,text:this.props.AiLink.Tags[i]})
        }
        for(var i =0;i<this.props.Groups.length;i++)
        {
            if(this.props.Groups[i]._id==this.props.Link)
                this.setState({Text:false})
        }
    }

    handleDelete(i) {
        let tags = this.state.tags;
        tags.splice(i, 1);
        this.setState({tags: tags});
        this.props.onDeleteTag(this.state.Index,i);
    }

    handleAddition(tag) {
        let tags = this.state.tags;
        let ok = true;
        for(let i=0;i<tags.length;i++)
        {
            if(tags[i].text==tag)
                ok = false;
        }
        if(ok) {
            tags.push({
                id: tags.length + 1,
                text: tag
            });
            this.setState({tags: tags});
            this.props.onAddTag(this.state.Index, tag);
        }
    }

    handleDrag(tag, currPos, newPos) {
        let tags = this.state.tags;

        // mutate array
        tags.splice(currPos, 1);
        tags.splice(newPos, 0, tag);

        // re-render
        this.setState({ tags: tags });
    }

    Condition() {
        var options = this.props.Groups.map((g)=>
            <option value={g._id}>{g.Name +"(#"+g._id+")"}</option>
        )
        const { tags } = this.state;

        return(
            <div className="condition EditMessage">
                <div className="mb10 ">
                <ReactTags type="text" className="hello" maxLength ="30" tags={tags}
                           handleDelete={this.handleDelete}
                           handleAddition={this.handleAddition}

                />
                </div>
                <span className="condition_over mt_15">
                    <div className="condition_left">BOT REPLIES WITH</div>
                    <select className="chat_input borderR" name="TextOrBlock" value={this.state.Text ? "Text" : "Group"} onChange={this.txtOrBlockChange}>
                        <option value="Text">Text</option>
                        <option value="Group"> Group</option>
                    </select>

                    </span>
                { this.state.Text ?
                <input className="borderR chat_input" type="text" placeholder={"your response"} value={this.state.Link} onChange={this.linkChanged} /> :

                <select className="chat_input borderR" name="Link"
                        value={this.state.Link || ""} onChange={this.linkChanged}>
                {options}
            </select>
                    }
            </div>
        )
    }

    render() {
        //handleDrag={this.handleDrag}


        return (
            <div>

                {this.Condition()}

                <div className="button_delete_block">
                    <span className="right_delete_icon" onClick={()=>this.props.onDeleteLink(this.state.Index)}>

                    </span></div>
            </div>
        )
    }
}

export default Tags;