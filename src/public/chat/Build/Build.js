import React, { Component}  from 'react';
import jQuery from 'jquery';
import Cookies from 'universal-cookie';
import Draggable, {DraggableCore} from 'react-draggable'; // Both at the same time
import Audio from "./AudioPlayerClass.js";
import { Player } from 'video-react';
import Gallery from 'react-grid-gallery';
import { Scrollbars } from 'react-custom-scrollbars';
import TextareaAutosize from 'react-autosize-textarea';
import SvgComponent from "./SvgComponent";
import LeftPanel from "../../LeftPanel";
import TopPanel from "../../TopPanel";
import Popup from "../../Popup";
import Content from "./Content";
import Loading from '../../../Loading.js';
import 'video-react/dist/video-react.css';
import ReactTooltip from 'react-tooltip'
import Loader from 'react-loader-spinner';

const cookies = new Cookies();
var config = require('../../../config.js');



const btnBlue = {
    background: 'orange',
    color: 'white'
}


class BotMsg extends Component {
    constructor(props) {
        super(props);
        this.state = {
            x: +this.props.x,
            y: +this.props.y,
            Name: this.props.Name,
            _id: this.props._id,
            BotMsg: this.props.BotMsg,
        };

        this.onStop=this.onStop.bind(this);
    }

    handleDrag = (e, { deltaX, deltaY }) => {
        this.setState({
            x: this.state.x + deltaX,
            y: this.state.y + deltaY,
        });

    };

    onStop()
    {
        var url = config.Backend + '/botMsg/edit?';
        if (!(cookies.get('Id') === undefined))
        {
            url += '&CurrentUserId=' + cookies.get('Id').toString();
        }

        url+='&Id='+this.state._id;
        url+='&Pos='+parseFloat(this.state.x)+"*"+parseFloat(this.state.y);

        jQuery.getJSON(url, function (res) {
            if (!res.Error) {
            } else {
                alert(JSON.stringify(res));
            }
        }.bind(this));
    }

    render() {
        var className = this.props.BotMsg.Condition ? "canva-block conditions" : "canva-block";
        if(this.props.BotMsg.Start)
            className +=  " first";
        return (
            <foreignObject>
                <div className={ className} >

                    <h2 onClick={this.props.onEdit} className="title">
                        <a>{this.props.Name}</a>
                    </h2>
                    <div className="canva_btn">
                        {this.props.BotMsg.Condition ? <span  className="canva_condition_icon"></span> : <span  className="btn_icon"></span>}
                        <button onClick={this.props.onEdit} className="Canva_btn_edit">Edit</button>
                        <button onClick={this.props.onDelete} className="Canva_btn_del">Delete</button>
                    </div>
                </div></foreignObject>
        )
    }
}

class chatBuild extends Loading {
    constructor(props) {
        super(props);
        this.state = {
            User: {},
            typingTime:3,
            Bot: {},
            Group: "",
            Groups: [],
            BotMsgs: [],
            BotMsg: "-1",
            BotMsgName:"",
            TextContents: [],
            AudioContents: [],
            VideoContents: [],
            ImageContents: [],
            CondContents: [],
            BtnContents: [],
            ApiContents: [],
            ShowDeletebotMsgPopup: false,
            hideDefaultLink: false,
            Wait: 0,
            Commands: [],
            CommandIndex: 0,
        }

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleInputFileChange = this.handleInputFileChange.bind(this);
        this.addGroup = this.addGroup.bind(this);
        this.addBotMsg = this.addBotMsg.bind(this);
        this.wantDeleteGroup = this.wantDeleteGroup.bind(this);
        this.wantRenameGroup = this.wantRenameGroup.bind(this);
        this.chooseBlock = this.chooseBlock.bind(this);
        this.deleteBotMsg = this.deleteBotMsg.bind(this);
        this.saveBotMsg = this.saveBotMsg.bind(this);
        this.wantDeleteBotMsg = this.wantDeleteBotMsg.bind(this);
        this.hideAllPopups = this.hideAllPopups.bind(this);
        this.addAudioContent = this.addAudioContent.bind(this);
        this.addVideoContent = this.addVideoContent.bind(this);
        this.addImageContent = this.addImageContent.bind(this);
        this.addApiContent = this.addApiContent.bind(this);
        this.addBtnContent = this.addBtnContent.bind(this);
        this.addCondContent = this.addCondContent.bind(this);
        this.saveContents = this.saveContents.bind(this);
        this.handleForContent = this.handleForContent.bind(this);
        this.deleteContent = this.deleteContent.bind(this);
        this.refreshBotMsgs = this.refreshBotMsgs.bind(this);
        this.chooseGroup = this.chooseGroup.bind(this);
        this.loadGroups = this.loadGroups.bind(this);
        this.changeWaitAnswer = this.changeWaitAnswer.bind(this);
        this.changeStart = this.changeStart.bind(this);
        this.changeAnswerName = this.changeAnswerName.bind(this);
        this.dragLink = this.dragLink.bind(this);
        this.undragLink = this.undragLink.bind(this);
        this.checkLink = this.checkLink.bind(this);
        this.ctrl = this.ctrl.bind(this);
        this.change = config.change.bind(this);
        this.pushCommand = this.pushCommand.bind(this);
        this.restore = this.restore.bind(this);
    }
    restore(id)
    {
        var url = config.Backend + '/botMsg/restore?';
        if (!(cookies.get('Id') === undefined))
        {
            url += '&CurrentUserId=' + cookies.get('Id').toString();
        }
        url+= '&Group='+this.state.Group;
        url+= '&Id='+id;
        this.setState({Wait:++this.state.Wait});jQuery.getJSON(url, function (res) {
        this.setState({Wait:--this.state.Wait})
        if (!res.Error) {
            //this.state.BotMsgs.push(res.BotMsg);
            //this.setState({BotMsgs: this.state.BotMsgs});
            this.loadBotMsgs();
        } else {
            alert(JSON.stringify(res));
        }
    }.bind(this));
    }
    pushCommand(undo,redo)
    {
        var arr = [];
        for(var i =0;i<this.state.CommandIndex;i++)
        {
            arr.push(this.state.Commands[i]);
        }
        arr.push({Redo:redo,Undo:undo});
        this.setState({Commands:arr,CommandIndex:this.state.CommandIndex+1})
    }
    ctrl(e)
    {
        var s =this.state;
        if( e.ctrlKey && s.Commands.length>0) {
            if (e.keyCode == 90 && s.CommandIndex>0)//ctrl+z
            {
                this.setState({CommandIndex:--this.state.CommandIndex});
                s.Commands[s.CommandIndex].Undo();
            }
            else if (e.keyCode == 89 && s.CommandIndex<s.Commands.length)//ctrl+y
            {
                s.Commands[s.CommandIndex].Redo();
                this.setState({CommandIndex:s.CommandIndex+1});
            }
        }
    }

    dragLink(from,to)
    {
        for(var i=0;i<this.state.BotMsgs.length;i++)
        {
            if(from==this.state.BotMsgs[i]._id &&  this.state.BotMsgs[i].DefaultLink!=to)
            {
                this.state.BotMsgs[i].DefaultLink = to;
                this.setState({BotMsgs:this.state.BotMsgs});
                var url = config.Backend + '/botMsg/edit?';
                if (!(cookies.get('Id') === undefined))
                {
                    url += '&CurrentUserId=' + cookies.get('Id').toString();
                }
                url+= '&Id='+this.state.BotMsgs[i]._id;
                url+= '&Name='+this.state.BotMsgs[i].Name;
                url += '&DefaultLink=' + this.state.BotMsgs[i].DefaultLink;
                this.setState({Wait:++this.state.Wait});jQuery.getJSON(url, function (res) {
                    if (!res.Error) {
                        this.setState({Wait:--this.state.Wait})
                    } else {
                        this.setState({Wait:--this.state.Wait});alert(JSON.stringify(res));
                    }
                }.bind(this));
            }
        }
    }
    checkLink(from,to)
    {
        for(var i=0;i<this.state.BotMsgs.length;i++)
        {
            if(from==this.state.BotMsgs[i]._id &&  this.state.BotMsgs[i].DefaultLink==to)
            {
               return true;
            }
        }
        return false;
    }
    undragLink(from,to)
    {
        for(var i=0;i<this.state.BotMsgs.length;i++)
        {
            if(from==this.state.BotMsgs[i]._id)
            {
                this.state.BotMsgs[i].DefaultLink = "-";
                this.setState({BotMsgs:this.state.BotMsgs});
                var url = config.Backend + '/botMsg/edit?';
                if (!(cookies.get('Id') === undefined))
                {
                    url += '&CurrentUserId=' + cookies.get('Id').toString();
                }
                url+= '&Id='+this.state.BotMsgs[i]._id;
                url+= '&Name='+this.state.BotMsgs[i].Name;
                url += '&DefaultLink=-';
                this.setState({Wait:++this.state.Wait});jQuery.getJSON(url, function (res) {
                if (!res.Error) {
                    this.setState({Wait:--this.state.Wait})
                } else {
                    this.setState({Wait:--this.state.Wait});alert(JSON.stringify(res));
                }
            }.bind(this));
            }
        }
    }
    changeAnswerName(e) {
        for(var i=0;i<this.state.BotMsgs.length;i++)
        {
            if(this.state.BotMsg==this.state.BotMsgs[i]._id)
            {
                this.state.BotMsgs[i].AnswerName = e.target.value;
                this.setState({BotMsgs:this.state.BotMsgs});
            }
        }
    }
    changeStart(e) {
        for(var i=0;i<this.state.BotMsgs.length;i++)
        {
            if(this.state.BotMsg==this.state.BotMsgs[i]._id)
            {
                this.state.BotMsgs[i].Start = e.target.checked;
                this.setState({BotMsgs:this.state.BotMsgs});
            }
        }
    }
    changeWaitAnswer(e) {
        for(var i=0;i<this.state.BotMsgs.length;i++)
        {
            if(this.state.BotMsg==this.state.BotMsgs[i]._id)
            {
                this.state.BotMsgs[i].WaitAnswer = e.target.checked;
                this.setState({BotMsgs:this.state.BotMsgs});
            }
        }
    }

    refreshBotMsgs(id,newPos) {
        for(var i=0;i<this.state.BotMsgs.length;i++)
        {
            if(id==this.state.BotMsgs[i]._id)
            {
                this.state.BotMsgs[i].Pos = newPos;
            }
        }
    }

    clearInput(e) {
        e.target.value = null;
    }

    deleteContent(i,type,not_save_state) {
        if(type=="Image")
        {
            if(this.state.ImageContents[i]._id!="-1")
            {
                var url = config.Backend + '/content/delete?';
                if (!(cookies.get('Id') === undefined))
                {
                    url += '&CurrentUserId=' + cookies.get('Id').toString();
                }

                url+='&Id='+this.state.ImageContents[i]._id

                this.setState({Wait:++this.state.Wait});jQuery.getJSON(url, function (res) {
                    if (!res.Error) {
                        this.setState({Wait:--this.state.Wait})
                    } else {
                        this.setState({Wait:--this.state.Wait});alert(JSON.stringify(res));
                    }
                }.bind(this));
                if(not_save_state!==1)
                {
                    this.pushCommand(()=>this.restore_content(i,"ImageContents"),
                        ()=>this.deleteContent(i,type,1));
                }
            }
            else
            {
                this.pushCommand(()=>this.restoreTempContent(i,"ImageContents"),
                    ()=>this.deleteTempContent(i,"ImageContents"));
            }
            this.state.ImageContents[i].Deleted=true;
            this.setState({ImageContents:this.state.ImageContents});
        }
        else if(type=="Video")
        {
            if(this.state.VideoContents[i]._id!="-1")
            {
                var url = config.Backend + '/content/delete?';
                if (!(cookies.get('Id') === undefined))
                {
                    url += '&CurrentUserId=' + cookies.get('Id').toString();
                }

                url+='&Id='+this.state.VideoContents[i]._id

                this.setState({Wait:++this.state.Wait});jQuery.getJSON(url, function (res) {
                    if (!res.Error) {
                        this.setState({Wait:--this.state.Wait})
                    } else {
                        this.setState({Wait:--this.state.Wait});alert(JSON.stringify(res));
                    }
                }.bind(this));
                if(not_save_state!==1)
                {
                    this.pushCommand(()=>this.restore_content(i,"VideoContents"),
                        ()=>this.deleteContent(i,type,1));
                }
            }
            else
            {
                this.pushCommand(()=>this.restoreTempContent(i,"VideoContents"),
                    ()=>this.deleteTempContent(i,"VideoContents"));
            }
            this.state.VideoContents[i].Deleted=true;
            this.setState({VideoContents:this.state.VideoContents});
        }
        else if(type=="Audio")
        {
            if(this.state.AudioContents[i]._id!="-1")
            {
                var url = config.Backend + '/content/delete?';
                if (!(cookies.get('Id') === undefined))
                {
                    url += '&CurrentUserId=' + cookies.get('Id').toString();
                }

                url+='&Id='+this.state.AudioContents[i]._id

                this.setState({Wait:++this.state.Wait});jQuery.getJSON(url, function (res) {
                    if (!res.Error) {
                        this.setState({Wait:--this.state.Wait})
                    } else {
                        this.setState({Wait:--this.state.Wait});alert(JSON.stringify(res));
                    }
                }.bind(this));
                if(not_save_state!==1)
                {
                    this.pushCommand(()=>this.restore_content(i,"AudioContents"),
                        ()=>this.deleteContent(i,type,1));
                }
            }
            else
            {
                this.pushCommand(()=>this.restoreTempContent(i,"AudioContents"),
                    ()=>this.deleteTempContent(i,"AudioContents"));
            }
            this.state.AudioContents[i].Deleted=true;
            this.setState({AudioContents:this.state.AudioContents});
        }
        else if(type=="Api")
        {
            if(this.state.ApiContents[i]._id!="-1")
            {
                var url = config.Backend + '/content/delete?';
                if (!(cookies.get('Id') === undefined))
                {
                    url += '&CurrentUserId=' + cookies.get('Id').toString();
                }
                url+='&Id='+this.state.ApiContents[i]._id

                this.setState({Wait:++this.state.Wait});jQuery.getJSON(url, function (res) {
                    if (!res.Error) {
                        this.setState({Wait:--this.state.Wait})
                    } else {
                        this.setState({Wait:--this.state.Wait});alert(JSON.stringify(res));
                    }
                }.bind(this));
                if(not_save_state!==1)
                {
                    this.pushCommand(()=>this.restore_content(i,"ApiContents"),
                        ()=>this.deleteContent(i,type,1));
                }
            }
            else
            {
                this.pushCommand(()=>this.restoreTempContent(i,"ApiContents"),
                    ()=>this.deleteTempContent(i,"ApiContents"));
            }
            this.state.ApiContents[i].Deleted=true;
            this.setState({ApiContents:this.state.ApiContents});
        }
        else if(type=="Btn")
        {
            if(this.state.BtnContents[i]._id!="-1")
            {
                var url = config.Backend + '/botMsg/edit?';
                if (!(cookies.get('Id') === undefined))
                {
                    url += '&CurrentUserId=' + cookies.get('Id').toString();
                }
                url+='&Id='+this.state.BotMsg;
                url+='&Btn='+this.state.BtnContents[i].Name;
                url+='&Redirect='+this.state.BtnContents[i].Redirect;
                url+='&Index='+i;
                url+='&Delete=true';

                this.setState({Wait:++this.state.Wait});jQuery.getJSON(url, function (res) {
                    if (!res.Error) {
                        this.setState({Wait:--this.state.Wait})
                    } else {
                        this.setState({Wait:--this.state.Wait});alert(JSON.stringify(res));
                    }
                }.bind(this));
                if(not_save_state!==1)
                {
                    this.pushCommand(()=>this.restoreTempContent(i,"BtnContents"),
                        ()=>this.deleteContent(i,type,1));
                }
            }
            else
            {
                this.pushCommand(()=>this.restoreTempContent(i,"BtnContents"),
                    ()=>this.deleteTempContent(i,"BtnContents"));
            }
            this.state.BtnContents[i].Deleted=true;
            this.setState({BtnContents:this.state.BtnContents});
        }
        else if(type=="Cond")
        {
            if(this.state.CondContents[i]._id!="-1")
            {
                var url = config.Backend + '/botMsg/edit?';
                if (!(cookies.get('Id') === undefined))
                {
                    url += '&CurrentUserId=' + cookies.get('Id').toString();
                }
                url+='&Id='+this.state.BotMsg;
                url+='&Condition='+this.state.CondContents[i].Name;
                url+='&Redirect='+this.state.CondContents[i].Redirect;
                url+='&Index='+i;
                url+='&Delete=true';

                this.setState({Wait:++this.state.Wait});jQuery.getJSON(url, function (res) {
                if (!res.Error) {
                    this.setState({Wait:--this.state.Wait})
                } else {
                    this.setState({Wait:--this.state.Wait});alert(JSON.stringify(res));
                }
                if(not_save_state!==1)
                {
                    this.pushCommand(()=>this.restoreTempContent(i,"CondContents"),
                        ()=>this.deleteContent(i,type,1));
                }
            }.bind(this));
            }
            else
            {
                this.pushCommand(()=>this.restoreTempContent(i,"CondContents"),
                    ()=>this.deleteTempContent(i,"CondContents"));
            }
            this.state.CondContents[i].Deleted=true;
            this.setState({CondContents:this.state.CondContents});
        }
    }
    restore_content = (i,content) => {
        var url = config.Backend + '/content/restore?';
        url += '&CurrentUserId=' + cookies.get('Id');
        url+='&Id='+this.state[content][i]._id;

        this.setState({Wait:++this.state.Wait});jQuery.getJSON(url, function (res) {
            if (!res.Error) {
                this.state[content][i].Deleted=undefined;
                this.setState({[content]:this.state[content]});
                this.setState({Wait:--this.state.Wait})
            } else {
                this.setState({Wait:--this.state.Wait});alert(JSON.stringify(res));
            }
        }.bind(this));
    }
    addCondContent() {
        var btn = {Condition: "", Redirect: "-", _id: "-1"}
        this.state.CondContents.push(btn);
        var ind  = this.state.CondContents.length-1;
        this.pushCommand(()=>this.deleteTempContent(ind,"CondContents")
            ,()=>this.restoreTempContent(ind,"CondContents"));
        this.setState({CondContents: this.state.CondContents})
    }
    addBtnContent() {
        if(this.state.BtnContents.filter((v)=>{return !v.Deleted}).length==3)
        {
            alert("Error: Maximum number of buttons is 3")
        }
        else {
            var btn = {Name: "Button", Redirect: "-", _id: "-1",Type:"postBack",Url:"http://"}
            this.state.BtnContents.push(btn);
                var ind  = this.state.BtnContents.length-1;
                this.pushCommand(()=>this.deleteTempContent(ind,"BtnContents")
                    ,()=>this.restoreTempContent(ind,"BtnContents"));
            this.setState({BtnContents: this.state.BtnContents})
        }
    }
    addApiContent() {
        var macros = '';
        var Content = {_id:"-1",Type:"Api",Url:"",ReqType:"Get",Field:'',Macros: macros,Headers:"",Save:""};
        this.state.ApiContents.push(Content);
        var ind  = this.state.ApiContents.length-1;
        this.pushCommand(()=>this.deleteTempContent(ind,"ApiContents")
            ,()=>this.restoreTempContent(ind,"ApiContents"));
        this.setState({ApiContents:this.state.ApiContents});
    }


    addVideoContent(e) {
        e.preventDefault();
        let reader = new FileReader();
        let file = e.target.files[0];
        reader.onloadend = () => {
            var Content = {_id:"-1",Type:"Video",Link:"-1",File:file,Url:reader.result}
            this.state.VideoContents.push(Content);
            this.setState({VideoContents:this.state.VideoContents});
        }
        reader.readAsDataURL(file)
    }


    addImageContent(e) {
        e.preventDefault();
        let reader = new FileReader();
        let file = e.target.files[0];
        reader.onloadend = () => {
            var Content = {_id:"-1",Type:"Image",Link:"-1",File:file,Url:reader.result}
            this.state.ImageContents.push(Content);
            this.setState({ImageContents:this.state.ImageContents});
        }
        reader.readAsDataURL(file)
    }

    addAudioContent(e) {
        e.preventDefault();
        let reader = new FileReader();
        let file = e.target.files[0];
        reader.onloadend = () => {
            var Content = {_id:"-1",Type:"Audio",Link:"-1",File:file,Url:reader.result}
            this.state.AudioContents.push(Content);
            this.setState({AudioContents:this.state.AudioContents});
        }
        reader.readAsDataURL(file)
    }
    deleteTempContent = (i,content) => {
        this.state[content][i].Deleted=true;
        this.setState({[content]:this.state[content]});
    }
    restoreTempContent = (i,content) => {
        this.state[content][i]._id="-1";
        this.state[content][i].Deleted=undefined;
        this.setState({[content]:this.state[content]});
    }
    handleForContent(index,Text,Time,Link,File,Type,Url,ReqType,Field,Refresh,Macros,BtnType,BtnUrl,Headers,Save,Cond,CondRedirect) {
        if(Type=="Text")
        {
            this.state.TextContents[index].Text = Text;
            this.state.TextContents[index].Time = Time;
        }
        else if (Type=="Image")
        {
            this.state.ImageContents[index].Link = Link;
            this.state.ImageContents[index].File = File;
        }
        else if (Type=="Audio")
        {
            this.state.AudioContents[index].Link = Link;
            this.state.AudioContents[index].File = File;
        }
        else if (Type=="Video")
        {
            this.state.VideoContents[index].Link = Link;
            this.state.VideoContents[index].File = File;
        }
        else if (Type=="Api")
        {
            this.state.ApiContents[index].Url = Url;
            this.state.ApiContents[index].ReqType = ReqType;
            this.state.ApiContents[index].Field = Field;
            this.state.ApiContents[index].Macros = Macros;
            this.state.ApiContents[index].Headers = Headers;
            this.state.ApiContents[index].Save = Save;
        }
        else if(Type=="Btn")
        {
            this.state.BtnContents[index].Name = Text;
            if(this.state.BtnContents[index].Redirect != Url) {
                this.state.BtnContents[index].Redirect = Url;
                for (var i = 0; i < this.state.BotMsgs.length; i++) {
                    if (this.state.BotMsgs[i]._id == this.state.BotMsg) {
                        this.state.BotMsgs[i].Btns = this.state.BtnContents;
                    }
                }
            }
            this.state.BtnContents[index].Type = BtnType;
            this.state.BtnContents[index].Url = BtnUrl;
        }
        else if(Type=="Cond")
        {
            this.state.CondContents[index].Condition = Cond;
            if(this.state.CondContents[index].Redirect != CondRedirect) {
                this.state.CondContents[index].Redirect = CondRedirect;
                for (var i = 0; i < this.state.BotMsgs.length; i++) {
                    if (this.state.BotMsgs[i]._id == this.state.BotMsg) {
                        this.state.BotMsgs[i].Links = this.state.CondContents;
                    }
                }
            }
        }
        else if(Type=="DefaultLink")
        {
            for(var i=0;i<this.state.BotMsgs.length;i++)
            {
                if(this.state.BotMsgs[i]._id==this.state.BotMsg)
                {
                    this.state.BotMsgs[i].DefaultLink = Url;
                }
            }
        }
        if(Refresh==1)
        {
            var arr = this.state.BotMsgs;
            this.setState({BotMsgs:arr})
        }
    }


    handleInputFileChange(event) {
        this.setState({Image:event.target.files[0]});
    }

    saveContents() {
        /*  var url = config.Backend + '/content/edit?';
          if (!(cookies.get('Id') === undefined)) {
              url += '&CurrentUserId=' + cookies.get('Id').toString();
          }
          url += '&BotMsg='+this.state.BotMsg;
          url += '&Id=' +this.state.TextContents[0]._id;
          url += '&Text='+this.state.TextContents[0].Text;
          url += '&Time='+this.state.TextContents[0].Time;
          url += '&Link='+this.state.TextContents[0].Link;

          this.setState({Wait:++this.state.Wait});jQuery.getJSON(url, function (res) {
              if (!res.Error) {
                  this.loadBotMsgs();
              } else {
                  this.setState({Wait:--this.state.Wait});alert(JSON.stringify(res));
              }
          }.bind(this));*/

        var fd = {};
        fd.Id=this.state.TextContents[0]._id;
        fd.Text= this.state.TextContents[0].Text;
        fd.Type= "TextContent";
        fd.Time= this.state.TextContents[0].Time;
        fd.Link= this.state.TextContents[0].Link;
        this.setState({Wait:++this.state.Wait});jQuery.ajax({
            method: "POST",
            contentType: 'application/json',
            url: config.Backend +"/content/edit",
            success: function (res) {
                if (!res.Error) {
                    this.setState({Wait:--this.state.Wait})
                    this.loadBotMsgs();
                } else {
                    this.setState({Wait:--this.state.Wait});alert(JSON.stringify(res));
                }
            }.bind(this),
            data: JSON.stringify(fd),
        })

        for(var i=0;i<this.state.ImageContents.length;i++) {
            if(this.state.ImageContents[i].Deleted!=true) {


                if (this.state.ImageContents[i]._id == "-1") {
                    var fd = new FormData;
                    fd.append('File', this.state.ImageContents[i].File);
                    /*if (this.state.ImageContents[i].Link!="-1") {
                        this.setState({Wait:++this.state.Wait});jQuery.ajax({
                            async: false,
                            dataType: "json",
                            url: "http://localhost:4800/content/deleteFile?Link="+this.state.ImageContents[i].Link,
                        })
                    }*/
                    this.setState({Wait: ++this.state.Wait});
                    jQuery.ajax({
                        method: "POST",
                        async: false,
                        processData: false,
                        contentType: false,
                        url: config.Backend + "/content/file",
                        success: function (msg) {
                            this.setState({Wait: --this.state.Wait})
                            this.state.ImageContents[i].Link = msg;
                        }.bind(this),
                        data: fd,
                    })
                    var url = config.Backend + '/content/add?';
                    if (!(cookies.get('Id') === undefined)) {
                        url += '&CurrentUserId=' + cookies.get('Id').toString();
                    }
                    url += '&BotMsg=' + this.state.BotMsg;
                    url += '&Type=' + this.state.ImageContents[i].Type;
                    url += '&Text=' + this.state.ImageContents[i].Text;
                    url += '&Time=' + this.state.ImageContents[i].Time;
                    url += '&Link=' + this.state.ImageContents[i].Link;

                    this.setState({Wait: ++this.state.Wait});
                    jQuery.getJSON(url, function (res) {
                        if (!res.Error) {
                            this.setState({Wait: --this.state.Wait})
                        } else {
                            this.setState({Wait: --this.state.Wait});
                            alert(JSON.stringify(res));
                        }
                    }.bind(this));
                }
            }
        }

        for(var i=0;i<this.state.AudioContents.length;i++) {
            if(this.state.AudioContents[i].Deleted!=true) {


                if (this.state.AudioContents[i]._id == "-1") {
                    var fd = new FormData;
                    fd.append('File', this.state.AudioContents[i].File);

                    this.setState({Wait: ++this.state.Wait});
                    jQuery.ajax({
                        method: "POST",
                        async: false,
                        processData: false,
                        contentType: false,
                        url: config.Backend + "/content/file",
                        success: function (msg) {
                            this.setState({Wait: --this.state.Wait})
                            this.state.AudioContents[i].Link = msg;
                        }.bind(this),
                        data: fd,
                    })
                    var url = config.Backend + '/content/add?';
                    if (!(cookies.get('Id') === undefined)) {
                        url += '&CurrentUserId=' + cookies.get('Id').toString();
                    }
                    url += '&BotMsg=' + this.state.BotMsg;
                    url += '&Type=' + this.state.AudioContents[i].Type;
                    url += '&Text=' + this.state.AudioContents[i].Text;
                    url += '&Time=' + this.state.AudioContents[i].Time;
                    url += '&Link=' + this.state.AudioContents[i].Link;

                    this.setState({Wait: ++this.state.Wait});
                    jQuery.getJSON(url, function (res) {
                        if (!res.Error) {
                            this.setState({Wait: --this.state.Wait})
                        } else {
                            this.setState({Wait: --this.state.Wait});
                            alert(JSON.stringify(res));
                        }
                    }.bind(this));
                }
            }
        }

        for(var i=0;i<this.state.VideoContents.length;i++) {
            if(this.state.VideoContents[i].Deleted!=true) {
                if (this.state.VideoContents[i]._id == "-1") {
                    var fd = new FormData;
                    fd.append('File', this.state.VideoContents[i].File);

                    this.setState({Wait: ++this.state.Wait});
                    jQuery.ajax({
                        method: "POST",
                        async: false,
                        processData: false,
                        contentType: false,
                        url: config.Backend + "/content/file",
                        success: function (msg) {
                            this.setState({Wait: --this.state.Wait})
                            this.state.VideoContents[i].Link = msg;
                        }.bind(this),
                        data: fd,
                    })
                    var url = config.Backend + '/content/add?';
                    if (!(cookies.get('Id') === undefined)) {
                        url += '&CurrentUserId=' + cookies.get('Id').toString();
                    }
                    url += '&BotMsg=' + this.state.BotMsg;
                    url += '&Type=' + this.state.VideoContents[i].Type;
                    url += '&Text=' + this.state.VideoContents[i].Text;
                    url += '&Time=' + this.state.VideoContents[i].Time;
                    url += '&Link=' + this.state.VideoContents[i].Link;

                    this.setState({Wait: ++this.state.Wait});
                    jQuery.getJSON(url, function (res) {
                        if (!res.Error) {
                            this.setState({Wait: --this.state.Wait})

                        } else {
                            this.setState({Wait: --this.state.Wait});
                            alert(JSON.stringify(res));
                        }
                    }.bind(this));
                }
            }
        }

        for(var i=0;i<this.state.ApiContents.length;i++) {
            if(this.state.ApiContents[i].Deleted!=true) {
                if (this.state.ApiContents[i]._id == "-1") {

                    var fd = {};
                    if (!(cookies.get('Id') === undefined)) {
                        fd.CurrentUserId = cookies.get('Id').toString();
                    }
                    fd.BotMsg = this.state.BotMsg;
                    fd.Type = this.state.ApiContents[i].Type;
                    fd.ReqType = this.state.ApiContents[i].ReqType;
                    fd.Field = this.state.ApiContents[i].Field;
                    fd.Macros = this.state.ApiContents[i].Macros;
                    fd.Headers = this.state.ApiContents[i].Headers;
                    fd.Save = this.state.ApiContents[i].Save;
                    fd.Url = this.state.ApiContents[i].Url;
                    this.setState({Wait: ++this.state.Wait});
                    jQuery.ajax({
                        method: "POST",
                        contentType: 'application/json',
                        url: config.Backend + "/content/add",
                        success: function (res) {
                            if (!res.Error) {
                                this.setState({Wait: --this.state.Wait})
                            } else {
                                this.setState({Wait: --this.state.Wait});
                                alert(JSON.stringify(res));
                            }
                        }.bind(this),
                        data: JSON.stringify(fd),
                    })
                } else {
                    var fd = {};
                    if (!(cookies.get('Id') === undefined)) {
                        fd.CurrentUserId = cookies.get('Id').toString();
                    }
                    fd.BotMsg = this.state.BotMsg;
                    fd.Id = this.state.ApiContents[i]._id;
                    fd.ReqType = this.state.ApiContents[i].ReqType;
                    fd.Field = this.state.ApiContents[i].Field;
                    fd.Macros = this.state.ApiContents[i].Macros;
                    fd.Headers = this.state.ApiContents[i].Headers;
                    fd.Save = this.state.ApiContents[i].Save;
                    fd.Url = this.state.ApiContents[i].Url;
                    this.setState({Wait: ++this.state.Wait});
                    jQuery.ajax({
                        method: "POST",
                        contentType: 'application/json',
                        url: config.Backend + "/content/edit",
                        success: function (res) {
                            if (!res.Error) {
                                this.setState({Wait: --this.state.Wait})
                            } else {
                                this.setState({Wait: --this.state.Wait});
                                alert(JSON.stringify(res));
                            }
                        }.bind(this),
                        data: JSON.stringify(fd),
                    })
                }
            }
        }

        for(var i=0;i<this.state.BtnContents.length;i++) {
            if (this.state.BtnContents[i].Deleted != true) {
                if (this.state.BtnContents[i]._id != "-1") {
                    var url = config.Backend + '/botMsg/edit?';
                    if (!(cookies.get('Id') === undefined)) {
                        url += '&CurrentUserId=' + cookies.get('Id').toString();
                    }
                    url += '&Id=' + this.state.BotMsg;
                    url += '&Btn=' + this.state.BtnContents[i].Name;
                    url += '&Redirect=' + this.state.BtnContents[i].Redirect;
                    url += '&Type=' + this.state.BtnContents[i].Type;
                    url += '&Url=' + this.state.BtnContents[i].Url;
                    url += '&Index=' + i;
                    //alert(url);
                    this.setState({Wait: ++this.state.Wait});
                    jQuery.getJSON(url, function (res) {
                        if (!res.Error) {
                            this.setState({Wait: --this.state.Wait})
                        } else {
                            this.setState({Wait: --this.state.Wait});
                            alert(JSON.stringify(res));
                        }
                    }.bind(this));
                } else {
                    var url = config.Backend + '/botMsg/edit?';
                    if (!(cookies.get('Id') === undefined)) {
                        url += '&CurrentUserId=' + cookies.get('Id').toString();
                    }
                    url += '&Id=' + this.state.BotMsg;
                    url += '&Btn=' + this.state.BtnContents[i].Name;
                    url += '&Redirect=' + this.state.BtnContents[i].Redirect;
                    url += '&Type=' + this.state.BtnContents[i].Type;
                    url += '&Url=' + this.state.BtnContents[i].Url;
                    this.setState({Wait: ++this.state.Wait});
                    jQuery.getJSON(url, function (res) {
                        if (!res.Error) {
                            this.setState({Wait: --this.state.Wait})
                        } else {
                            this.setState({Wait: --this.state.Wait});
                            alert(JSON.stringify(res));
                        }
                    }.bind(this));
                }
            }
        }
            for (var i = 0; i < this.state.CondContents.length; i++) {
                if (this.state.CondContents[i].Deleted != true) {
                    if (this.state.CondContents[i]._id != "-1") {
                        var url = config.Backend + '/botMsg/edit?';
                        if (!(cookies.get('Id') === undefined)) {
                            url += '&CurrentUserId=' + cookies.get('Id').toString();
                        }
                        url += '&Id=' + this.state.BotMsg;
                        url += '&Condition=' + this.state.CondContents[i].Condition;
                        url += '&Redirect=' + this.state.CondContents[i].Redirect;
                        url += '&Index=' + i;
                        //alert(url);
                        this.setState({Wait: ++this.state.Wait});
                        jQuery.getJSON(url, function (res) {
                            if (!res.Error) {
                                this.setState({Wait: --this.state.Wait})
                            } else {
                                this.setState({Wait: --this.state.Wait});
                                alert(JSON.stringify(res));
                            }
                        }.bind(this));
                    } else {
                        var url = config.Backend + '/botMsg/edit?';
                        if (!(cookies.get('Id') === undefined)) {
                            url += '&CurrentUserId=' + cookies.get('Id').toString();
                        }
                        url += '&Id=' + this.state.BotMsg;
                        url += '&Condition=' + this.state.CondContents[i].Condition;
                        url += '&Redirect=' + this.state.CondContents[i].Redirect;
                        this.setState({Wait: ++this.state.Wait});
                        jQuery.getJSON(url, function (res) {
                            if (!res.Error) {
                                this.setState({Wait: --this.state.Wait})
                            } else {
                                this.setState({Wait: --this.state.Wait});
                                alert(JSON.stringify(res));
                            }
                        }.bind(this));
                    }
                }
            }

    }

    hideAllPopups() {
        this.setState({ShowDeletebotMsgPopup:false})
    }




    wantDeleteBotMsg(id) {
        this.hideAllPopups();
        this.setState({ShowDeletebotMsgPopup:true});
        this.setState({BotMsg:id});
    }

    saveBotMsg(id) {
        var url = config.Backend + '/botMsg/edit?';
        if (!(cookies.get('Id') === undefined))
        {
            url += '&CurrentUserId=' + cookies.get('Id').toString();
        }
        url+= '&Id='+this.state.BotMsg;
        url+= '&Name='+this.state.BotMsgName;

        for(var i=0;i<this.state.BotMsgs.length;i++)
        {
            if(this.state.BotMsgs[i]._id == this.state.BotMsg) {
                url += '&DefaultLink=' + this.state.BotMsgs[i].DefaultLink;
                if(this.state.BotMsgs[i].WaitAnswer) {
                    url += '&WaitAnswer=true';
                } else {
                    url += '&WaitAnswer=false';
                }
                if(this.state.BotMsgs[i].Start) {
                    url += '&Start=1';
                } else {
                    url += '&Start=0';
                }
                if(this.state.BotMsgs[i].Process) {
                    url += '&Process=1';
                } else {
                    url += '&Process=0';
                }
                if(this.state.BotMsgs[i].WaitApi) {
                    url += '&WaitApi=1';
                } else {
                    url += '&WaitApi=0';
                }
                url += '&AnswerName=' + this.state.BotMsgs[i].AnswerName;
            }
        }

        this.setState({Wait:++this.state.Wait});jQuery.getJSON(url, function (res) {
            if (!res.Error) {
                this.setState({BotMsg:"-1"});
                this.setState({Wait:--this.state.Wait})
            } else {
                this.setState({Wait:--this.state.Wait});alert(JSON.stringify(res));
            }
        }.bind(this));

        this.saveContents();
    }

    deleteBotMsg(not_save_state,id) {
        var url = config.Backend + '/botMsg/delete?';
        if (!(cookies.get('Id') === undefined))
        {
            url += '&CurrentUserId=' + cookies.get('Id').toString();
        }
        var gId = id || this.state.BotMsg;
        url+='&Id='+gId;

        this.setState({Wait:++this.state.Wait});jQuery.getJSON(url, function (res) {
            this.setState({Wait:--this.state.Wait})
            if (!res.Error) {
                this.setState({ShowDeletebotMsgPopup:false})
                this.setState({BotMsg:"-1"})
                this.loadBotMsgs();
                if(!(not_save_state===1)) {
                    this.pushCommand(()=>this.restore(gId),()=>this.deleteBotMsg(1,gId));
                }
            } else {
                //this.setState({Wait:--this.state.Wait});alert(JSON.stringify(res));
            }
        }.bind(this));
    }

    addBotMsg(logical) {
        var s =this.state;
        var url = config.Backend + '/botMsg/add?';
        if (!(cookies.get('Id') === undefined))
        {
            url += '&CurrentUserId=' + cookies.get('Id').toString();
        }
        url+= '&Pos='+Math.floor(Math.random() * (200))+'*'+Math.floor(Math.random() * (200));
        url+= '&Group='+this.state.Group;
        if(logical===true) {
            url += '&Condition=' + "true";
        }
        var caption = "";
        var g = this.state.BotMsgs;
        if(logical==true)
        {
            var names = [];
            for(var i=0;i<g.length;i++)
            {
                names.push(g[i].Name);
            }
            var newName = caption+"ConditionBlock";
            var x = 1;
            do
            {
                newName =caption+"ConditionBlock" + (x++);
            }
            while(names.indexOf(newName)!=-1);
            url += '&Name='+newName;
        }
        else {
            var names = [];
            for(var i=0;i<g.length;i++)
            {
                names.push(g[i].Name);
            }
            var newName = caption+"Block";
            var x = 1;
            do
            {
                newName =caption+"Block" + (x++);
            }
            while(names.indexOf(newName)!=-1);
            url+= '&Name='+(this.state.BotMsgs.length>0 ? newName : "StartBlock");
        }
        url+= '&Start='+(this.state.BotMsgs.length>0 ? "0" : "1");
        this.setState({Wait:++this.state.Wait});jQuery.getJSON(url, function (res) {
            if (!res.Error) {
                this.state.BotMsgs.push(res.BotMsg);
                this.setState({BotMsgs: this.state.BotMsgs});
                this.pushCommand(()=>this.deleteBotMsg(1,res.BotMsg._id),()=>this.restore(res.BotMsg._id));
                this.setState({Wait:--this.state.Wait})
            } else {
                this.setState({Wait:--this.state.Wait});alert(JSON.stringify(res));
            }
        }.bind(this));
    }

    chooseBlock(props) {
        this.setState({BotMsg:props},function(){
            var url = config.Backend + '/botMsg/findById?';
            if (!(cookies.get('Id') === undefined))
            {
                url += '&CurrentUserId=' + cookies.get('Id').toString();
            }
            url+='&Id=' + props;

            this.setState({Wait:++this.state.Wait});jQuery.getJSON(url, function (res) {

                if(!res.Error) {
                    for(var i=0;i<this.state.BotMsgs.length;i++)
                    {
                        if(this.state.BotMsgs[i]._id==props)
                        {
                            this.setState({BotMsgName:this.state.BotMsgs[i].Name},()=>{
                                if(this.state.BotMsgs[i].Condition) {
                                    this.state.CondContents = [];
                                    this.state.BotMsgs[i].Links = res.Links;
                                    this.setState({CondContents: this.state.CondContents}, () => {
                                        this.setState({CondContents: res.Links}, () => {
                                            document.getElementById("rightName").focus();
                                        });
                                    });
                                }
                                else {
                                    this.state.CondContents = [];
                                    this.setState({BotMsgs: this.state.BotMsgs}, () => {
                                        this.state.BtnContents = [];
                                        this.state.BotMsgs[i].Btns = res.Btns;
                                        this.setState({BtnContents: this.state.BtnContents,CondContents:this.state.CondContents},
                                            () => {
                                                this.setState({BtnContents: res.Btns}, () => {
                                                    document.getElementById("rightName").focus();
                                                });
                                            });
                                    });
                                }
                            });
                        }
                    }
                    this.setState({Wait:--this.state.Wait})
                } else {
                    this.setState({Wait:--this.state.Wait});alert(JSON.stringify(res));
                }
            }.bind(this))

        }.bind(this));


        var url = config.Backend + '/botMsg/showContents?';
        if (!(cookies.get('Id') === undefined))
        {
            url += '&CurrentUserId=' + cookies.get('Id').toString();
        }
        url+='&Id=' + props;

        this.setState({Wait:++this.state.Wait});jQuery.getJSON(url, function (res) {
            if (!res.Error) {
                this.state.TextContents = [];
                this.state.ImageContents = [];
                this.state.VideoContents = [];
                this.state.AudioContents = [];
                this.state.ApiContents = [];
                this.setState({TextContents: this.state.TextContents});
                this.setState({ImageContents: this.state.ImageContents});
                this.setState({VideoContents: this.state.VideoContents});
                this.setState({AudioContents: this.state.AudioContents});
                this.setState({ApiContents: this.state.ApiContents});

                res.forEach(function (content,i,arr) {
                    if(content.Type=="Text")
                        this.state.TextContents.push(content)
                    if(content.Type=="Image")
                        this.state.ImageContents.push(content)
                    if(content.Type=="Video")
                        this.state.VideoContents.push(content)
                    if(content.Type=="Audio")
                        this.state.AudioContents.push(content)
                    if(content.Type=="Api")
                        this.state.ApiContents.push(content)
                }.bind(this))
                this.setState({TextContents: this.state.TextContents});
                this.setState({ImageContents: this.state.ImageContents});
                this.setState({VideoContents: this.state.VideoContents});
                this.setState({AudioContents: this.state.AudioContents});
                this.setState({ApiContents: this.state.ApiContents});
                this.setState({Wait:--this.state.Wait})
            } else {
                this.setState({Wait:--this.state.Wait});alert(JSON.stringify(res));
            }
        }.bind(this));

    }


    loadBotMsgs() {
        var url = config.Backend + '/group/showMsgs?';
        if (!(cookies.get('Id') === undefined))
        {
            url += '&CurrentUserId=' + cookies.get('Id').toString();
        }
        url+='&Bot=' + this.state.Bot._id;
        url+='&Id='+this.props.match.params.id;

        this.setState({Wait:++this.state.Wait});jQuery.getJSON(url, function (res) {
            this.setState({Wait:--this.state.Wait})
            if (!res.Error) {
                this.setState({BotMsgs:res },()=>{
                    this.setState({hideDefaultLink:true},()=>this.setState({hideDefaultLink:false}))
                });
                this.setState({Group:this.props.match.params.id});
                this.hideAllPopups();
            } else {
                alert(JSON.stringify(res));
            }
        }.bind(this));

    }

    componentWillMount() {
        document.onkeydown = this.ctrl;
        var url = config.Backend + '/users/findById?';
        if (!(cookies.get('Id') === undefined))
        {
            url += 'Id=' + cookies.get('Id').toString();
        }

        this.setState({Wait:++this.state.Wait});jQuery.getJSON(url, function (res) {
            this.setState({Wait:--this.state.Wait})
            if (!res.Error) {
                this.setState({User: res});
            } else {
                window.location.replace(window.location.origin + '/login');
            }
        }.bind(this));

        url = config.Backend + '/bot/findById?';
        if (!(cookies.get('Id') === undefined))
        {
            url += 'CurrentUserId=' + cookies.get('Id').toString();
        }
        url+= '&Id='+cookies.get('BotId').toString();

        this.setState({Wait:++this.state.Wait});jQuery.getJSON(url, function (res) {
            if (!res.Error) {
                this.setState({Bot: res},()=>{
                    this.setState({Wait:--this.state.Wait})
                    this.loadBotMsgs();
                    this.loadGroups();
                });
                if(!config.checkRights(cookies.get('Id').toString(),res,"Build"))
                {
                    config.accessDenied(cookies.get('Id').toString(),res);
                }
            } else {
                this.setState({Wait:--this.state.Wait});alert(JSON.stringify(res));
            }
        }.bind(this));
    }

    handleInputChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    loadGroups() {
        var url = config.Backend + '/bot/groups?';
        if (!(cookies.get('Id') === undefined))
        {
            url += '&CurrentUserId=' + cookies.get('Id').toString();
        }
        url+='&Id=' + this.state.Bot._id;

        this.setState({Wait:++this.state.Wait});jQuery.getJSON(url, function (res) {
            if (!res.Error) {
                this.setState({Groups: res});
                if(res.length>0) {
                }
                else {
                }
                this.setState({Wait:--this.state.Wait})
            } else {
                this.setState({Wait:--this.state.Wait});alert(JSON.stringify(res));
            }
        }.bind(this));
    }
    RightBar() {
        /*

         */
        return (
                <div className="overflow_main_bot">
                    <LeftPanel User={this.state.User}/>
                    <div className={'overflow_main_bot_center'}>
                {this.Canva()}
                    </div>
                <div class="rightPanel">
                    <Scrollbars
                        style={{ height: "90%" }}>
                        <div className="right_container_content">
                            {this.Right()}
                        </div>
                    </Scrollbars>
                </div>
                </div>

        )
    }

    Right() {
        if(this.state.BotMsg != "-1" && !this.state.ShowDeletebotMsgPopup) return this.EditPanel();
        else return this.RightGroup();
    }
//Anton
    addGroup() {
        var url = config.Backend + '/group/add?';
        if (!(cookies.get('Id') === undefined))
        {
            url += '&CurrentUserId=' + cookies.get('Id').toString();
        }
        url+='&Bot=' + this.state.Bot._id;
        url+= '&Pos='+Math.floor(Math.random() * (200))+'*'+Math.floor(Math.random() * (200));
        url+='&Name=Group';

        this.setState({Wait:++this.state.Wait});jQuery.getJSON(url, function (res) {
            if (!res.Error) {
                this.state.Groups.push(res.Group);
                this.setState({Groups:this.state.Groups });
                this.setState({Wait:--this.state.Wait})
            } else {
                this.setState({Wait:--this.state.Wait});alert(JSON.stringify(res));
            }
        }.bind(this));
    }


    wantRenameGroup(id) {
        this.hideAllPopups();
        this.setState({ShowRenameGroupPopup:true})
        this.setState({Group:id})
    }

    wantDeleteGroup(id) {
        this.hideAllPopups();
        this.setState({ShowDeleteGroupPopup:true});
        this.setState({Group:id});
    }
    chooseGroup(id) {
        //var stateObj = { id: id  };
        //window.history.pushState(stateObj, "page", window.location.origin + '/chat/build/'+id);
        window.location.replace(window.location.origin + '/chat/build/'+id);
    }
    /*{Right group}*/
    RightGroup() {
        const groups = this.state.Groups.map((group)=><li key={group._id}>
            <div className="nameBlock">
                <span  onClick={()=>this.chooseGroup(group._id)}>{group.Name}</span>
            </div>
        </li>);


        return (
                <div className="right_group">
                    <div className="topnav_left_navigator">
                        <a className='chat_prev' onClick={()=>window.location.replace(window.location.origin + '/chat/build')}>Back to AI setup</a>
                    </div>
                    <div className="right_add_message_block">
                        <button onClick={this.addBotMsg}>Add message block</button>
                    </div>
                    <div className="right_add_condition_block">
                        <button onClick={()=>this.addBotMsg(true)}>Add condition block</button>
                    </div>

                    <ul className="rightNav">
                        {null}
                    </ul>
                </div>

        )
    }

    /*{End Right group}*/



    /*{Right edit panel}*/
    make_arr = (arr) => {
        var s =this.state;
        var res =[];
        for(var x =0;x<s[arr].length;x++)
        {
            if(!s[arr][x].Deleted)
            {
                s[arr][x].Index = x;
                res.push(s[arr][x]);
            }
        }
        return res;
    }
    EditPanel(){
        var link="";
        var ind = 0;
        for(var i=0;i<this.state.BotMsgs.length;i++)
        {
            if(this.state.BotMsgs[i]._id==this.state.BotMsg) {
                link = this.state.BotMsgs[i].DefaultLink;
                ind = i;
            }
        }
        var ic = this.make_arr("ImageContents");
        var vc = this.make_arr("VideoContents");
        var auc = this.make_arr("AudioContents");
        var ac = this.make_arr("ApiContents");
        var bc = this.make_arr("BtnContents");
        var cc =  this.make_arr("CondContents");
        const txtContents = this.state.TextContents.map((content,i)=><Content Index={i} handleForContent={this.handleForContent}
                                                                              Text={content.Text} Type={content.Type}
                                                                              File={content.File} Url={content.Url}
                                                                              onDelete={this.deleteContent}           Link={content.Link} typingTime={content.Time}/>);
        const imgContents = ic.map((content)=><Content Index={content.Index} handleForContent={this.handleForContent}
                                                                               onDelete={this.deleteContent}              File={content.File} Url={content.Url}
                                                                               Text={content.Text} Type={content.Type} Link={content.Link} typingTime={content.Time}/>);
        const videoContents = vc.map((content)=><Content Index={content.Index} handleForContent={this.handleForContent}
                                                                                 onDelete={this.deleteContent}      Text={content.Text} Type={content.Type}
                                                                                 File={content.File} Url={content.Url}
                                                                                 Link={content.Link} typingTime={content.Time}/>);

        const audioContents = auc.map((content)=><Content Index={content.Index} handleForContent={this.handleForContent}
                                                                                 onDelete={this.deleteContent}     Text={content.Text} Type={content.Type}
                                                                                 File={content.File} Url={content.Url}
                                                                                 Link={content.Link} typingTime={content.Time}/>);
        const apiContents = ac.map((content)=><Content Index={content.Index} handleForContent={this.handleForContent}
                                                                             Headers={content.Headers}  Macros={content.Macros}       onDelete={this.deleteContent}     Text={content.Text} Type={content.Type}
                                                                             Save={content.Save}   ReqType={content.ReqType} Url={content.Url}
                                                                             Bot={this.state.Bot}     Field={content.Field} typingTime={content.Time}/>);

        const btnContents =!this.state.BotMsgs[ind].Condition ? bc.map(function(content){ return <Content Index={content.Index} handleForContent={this.handleForContent}
                                                                                            BtnUrl={content.Url}  BtnType={content.Type}  onDelete={this.deleteContent}     Text={content.Name} Type={"Btn"}
                                                                                            id={content._id} Url={content.Redirect}
                                                                                            BotMsg={this.state.BotMsg}  BotMsgs={this.state.BotMsgs} typingTime={content.Time}/>}.bind(this)) : null;

        const condContents =this.state.BotMsgs[ind].Condition && this.state.CondContents  ? cc.map(function(content){ return <Content Index={content.Index} handleForContent={this.handleForContent}
                                                                                                                                                             BtnUrl={content.Url}  BtnType={content.Type}  onDelete={this.deleteContent}     Text={content.Name} Type={"Cond"}
                                                                                                                                                             id={content._id} Url={content.Redirect} Cond={content.Condition} CondRedirect={content.Redirect}
                                                                                                                                                             BotMsg={this.state.BotMsg}  BotMsgs={this.state.BotMsgs} typingTime={content.Time}/>}.bind(this)) : null;
        var link="";
        var ind = 0;
        for(var i=0;i<this.state.BotMsgs.length;i++)
        {
            if(this.state.BotMsgs[i]._id==this.state.BotMsg) {
                link = this.state.BotMsgs[i].DefaultLink;
                ind = i;
            }
        }
        var defaultLink =  <Content handleForContent={this.handleForContent} Type={"DefaultLink"}
                                    Url={link}
                                    BotMsg={this.state.BotMsg}  BotMsgs={this.state.BotMsgs}/>

        //                    <label htmlFor="enable_filter_2">Send data to api right after message was received</label>

        return(
            <div>
                <label htmlFor="rightName"><span>Name</span> </label>
                <input className="borderR chat_input mb10" id="rightName" type="text" onChange={this.handleInputChange} value={this.state.BotMsgName} name="BotMsgName" />
                <div className="wrap_filter_laber">
                    {this.state.BotMsgs[ind] ? <label htmlFor="enable_filter_2">Start block</label>
                        : null}
                    {this.state.BotMsgs[ind] ?
                        <input id="enable_filter_2" type={"checkbox"} checked={this.state.BotMsgs[ind].Start} onChange={this.changeStart}/>
                        : null}
                </div>

                <div className="wrap_filter_laber">
                    {this.state.BotMsgs[ind] && this.state.ApiContents.length>0? <label htmlFor="wait_answer_build">Wait api</label>
                        : null}
                    {this.state.BotMsgs[ind] && this.state.ApiContents.length>0 ?
                            <input id="wait_answer_build" type={"checkbox"} name={"BotMsgs.WaitApi"} checked={this.state.BotMsgs[ind].WaitApi} onChange={(e)=>this.change(e,ind)}/>
                        : null}
                </div>
                        <div className="wrap_filter_laber">
                    {this.state.BotMsgs[ind] && !this.state.BotMsgs[ind].Condition ? <label htmlFor="wait_answer_answer">Wait answer</label>
                        : null}

                    {this.state.BotMsgs[ind] && !this.state.BotMsgs[ind].Condition ?
                        <input id="wait_answer_answer" type={"checkbox"} checked={this.state.BotMsgs[ind].WaitAnswer} onChange={this.changeWaitAnswer}/>

                        : null}
                        </div>

                {this.state.BotMsgs[ind] && this.state.BotMsgs[ind].WaitAnswer ?
                    <div>

                        <label className="mt_15"  htmlFor="param"><span>Context.</span> </label>
                        <input className="borderR chat_input mb6" id="param" type="text" onChange={this.changeAnswerName} value={this.state.BotMsgs[ind].AnswerName} />

                        <div className="wrap_filter_laber">
                            <label htmlFor="enable_module">Process answer with plugins</label>
                            <input id="enable_module" type={"checkbox"} name={"BotMsgs.Process"} checked={this.state.BotMsgs[ind].Process} onChange={(e)=>this.change(e,ind)}/>
                        </div>
                    </div>

                    : null}
                {this.state.BotMsgs[ind] && !(this.state.BotMsgs[ind].Condition && true)  ? txtContents : null}
                {this.state.BotMsgs[ind] && !(this.state.BotMsgs[ind].Condition && true) && !this.state.hideDefaultLink ?
                    <div className="test">
                        {defaultLink}
                    </div> : null}
                <div className="testAudio">
                    {imgContents}
                </div>
                <div className="right_video mt_c15">
                    {videoContents}
                </div>
                <div className="audio"> {audioContents}</div>

                <div className="test">
                    {apiContents}
                </div>

                <div className="test">
                    {btnContents}
                </div>
                <div className="test">
                    {condContents}
                </div>
                {this.AddItems()}
            </div>
        )
    }

    /*{Message block}*/
    TypingTime(){
        return (
            <div className="TypingTime">
                <span>Typing time</span>
                <input type="range" min="0" max="10" value={this.state.typingTime} onChange={this.handleInputChange} name="typingTime"/>
                <span>{this.state.typingTime}s</span>
            </div>
        )
    }
    MessageBlock(){
        return(
            <div className="EditMessage">
                {this.TypingTime()}
                <textarea value="Bot creation" placeholder="Write your message" defaultValue={""} />
            </div>
        )
    }

    cancel() {
        this.setState({BotMsg:"-1",Contents:[]},()=>this.loadBotMsgs());
    }

    AddItems(){
        var b = this.state.Bot ? this.state.Bot.BindTo : "-";
        var cond = false;
        for(var i =0;i<this.state.BotMsgs.length;i++)
        {
            if(this.state.BotMsg==this.state.BotMsgs[i]._id)
            {
                cond = this.state.BotMsgs[i].Condition;
            }
        }
        var showBtn = true && !cond;
        var showApi =  true && !cond;
        var showVideo = true && !cond;
        var showAudio = true && !cond;
        var showImage = true && !cond;
        return(
            <div className="EditAddBlock">
                <span>Add items</span>

                <div className="list_button">
                    {showVideo ?
                        <div className="form-group inputDnD">
                            <input type="file" accept="video/*" onChange={this.addVideoContent} onClick={this.clearInput} className="form-control-file text-primary "  data-title="Video"/>
                        </div> : null}
                    {showAudio ?
                        <div className="form-group inputDnD">
                            <input type="file" accept="audio/*" onChange={this.addAudioContent} onClick={this.clearInput}  className="form-control-file text-primary " data-title="Audio" />
                        </div> : null}
                    {showImage ?
                        <div className="form-group inputDnD">
                            <input type="file" accept="image/*" onChange={this.addImageContent} onClick={this.clearInput}   className="form-control-file text-primary " data-title="Image" />
                        </div> : null}
                    {showApi ?
                        <div className="form-group inputDnD">
                            <span className="form-control-file text-primary " data-title="Api"  onClick={this.addApiContent} >Api</span>
                        </div> : null}
                    {showBtn ?
                        <div className="form-group inputDnD">
                            <span className="form-control-file text-primary " data-title="Button"  onClick={this.addBtnContent} >Button</span>
                        </div> : null}
                    {cond ?
                        <div className="form-group inputDnD">
                            <span className="form-control-file text-primary " data-title="Condition"  onClick={this.addCondContent} >Condition</span>
                        </div> : null}
                </div>

                <div className="form_group_right_panel">
                    <button onClick={()=>this.saveBotMsg(this.state.BotMsg)} className="btnSave">Save</button>
                    <button onClick={this.cancel.bind(this)} className="btnCansel">Cancel</button>
                </div>

            </div>)
    }

    /*{End edit panel}*/


    /*{Canva block}*/

    Canva() {
        const msgs = this.state.BotMsgs.map((msg,i)=><BotMsg x={msg.Pos.split('*')[0]} y={msg.Pos.split('*')[1]} key={msg._id}
                                                             _id={msg._id} Name={msg.Name} Index={i} BotMsg={msg}
                                                             onDelete={()=>this.wantDeleteBotMsg(msg._id)}
                                                             onEdit={()=>this.chooseBlock(msg._id)}/>);


        return (
                <SvgComponent undragLink={this.undragLink} pushCommand={this.pushCommand} checkLink={this.checkLink}
                              dragLink={this.dragLink} isGroups={false} Bot={this.state.Bot} childrens={msgs} arr={this.state.BotMsgs}  Refresh={this.refreshBotMsgs}/>


        )
    }

    /*{End Canva block}*/


    refreshBotMsgs(id,newPos) {
        for(var i=0;i<this.state.BotMsgs.length;i++)
        {
            if(id==this.state.BotMsgs[i]._id)
            {
                this.state.BotMsgs[i].Pos = newPos;
            }
        }
    }
    /*{End Setting RightBat}*/

    render() {
        var caption = "";
        for(var i=0;i<this.state.Groups.length;i++)
        {
            if(this.state.Groups[i]._id==this.props.match.params.id)
            {
                caption= this.state.Groups[i].Name;
                //caption+= "(#"+this.state.Group + ")";
            }
        }
        return (
            <div>
                {this.Load()}
                <TopPanel Text={caption}/>
                {this.RightBar()}
                {this.state.ShowDeleteGroupPopup ?
                    <Popup text={"Are you sure?"} Yes={this.deleteGroup}
                           No={()=>{this.setState({ShowDeleteGroupPopup:false})}}/> : null}
                {this.state.ShowDeletebotMsgPopup ?
                    <Popup text={"Are you sure?"} Yes={this.deleteBotMsg}
                           No={()=>{this.setState({ShowDeletebotMsgPopup:false}); this.setState({BotMsg:"-1"})}}/> : null}
                {this.state.ShowRenameGroupPopup ?
                    <Popup text={"Enter new name"} Yes = {this.deleteBotMsg} Bot = {this.state.Bot} Group = {this.state.Group}
                           RenameGroup={true} loadGroups={this.loadGroups} Hide={this.hideAllPopups}
                           chooseGroup={this.chooseGroup}  Name={this.getGroupName()}     No={()=>{this.setState({ShowRenameGroupPopup:false})}}/> : null}
            </div>

        )
    }
};

export default chatBuild;
