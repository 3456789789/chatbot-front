import React, { Component}  from 'react';
import jQuery from 'jquery';
import Cookies from 'universal-cookie';
import Audio from "./AudioPlayerClass.js";
import { Player } from 'video-react';
import Gallery from 'react-grid-gallery';
import { Scrollbars } from 'react-custom-scrollbars';
import TextareaAutosize from 'react-autosize-textarea';
import Select from 'react-select';
import Loader from 'react-loader-spinner';
import ReactTooltip from 'react-tooltip'
const cookies = new Cookies();
var config = require('../../../config.js');


class Content extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Field: this.props.Field,
            ReqType: this.props.ReqType,
            Type: this.props.Type,
            Text: this.props.Text,
            typingTime: +this.props.typingTime,
            Link: this.props.Link,
            Index: this.props.Index,
            myFile: this.props.File,
            Macros: this.props.Macros,
            Url: this.props.Url,
            BtnUrl: this.props.BtnUrl,
            BtnType: this.props.BtnType,
            Headers: this.props.Headers,
            Save: this.props.Save,
            Cond: this.props.Cond,
            CondRedirect: this.props.CondRedirect,
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleInputFileChange = this.handleInputFileChange.bind(this);
        this.changeSelect = this.changeSelect.bind(this);
        this.onCheckMsg = this.onCheckMsg.bind(this);
        this.onCheckGroup = this.onCheckGroup.bind(this);
    }

    componentWillReceiveProps(props)
    {
        if(props.Type=="Video" || props.Type=="Audio" || props.Type=="Image") {
            this.setState({
                Field: props.Field,
                Index: props.Index,
                Url: props.Url,
                Link: props.Link,
                myFile: props.File,
                Headers: props.Headers,
                Save: props.Save,
                Cond: props.Cond,
                CondRedirect: props.CondRedirect,
            }, () => {
                if (this.state.Type != "Text" && this.state.Type != "Btn" && this.state.Type != "BtnToGroup"
                    && this.state.Type != "Api" && this.state.Link != "-1" && this.state.Type != "DefaultLink" && this.state.Type != "DefaultLinkToGroup") {
                    var url = config.Backend + '/content/getFile?';
                    if (!(cookies.get('Id') === undefined)) {
                        url += '&CurrentUserId=' + cookies.get('Id').toString();
                    }
                    url += '&Link=' + this.state.Link;
                    this.setState({Url: url,})
                }
            })
        } else {
            this.setState({
                Url: props.Url,
                Link: props.Link,
                Cond: props.Cond,
                CondRedirect: props.CondRedirect,
            })
        }
    }

    componentWillMount() {
        if(this.state.Type!="Text" && this.state.Type!="Btn" && this.state.Type!="BtnToGroup"
            && this.state.Type!="Api" && this.state.Link!="-1" && this.state.Type!="DefaultLink" && this.state.Type!="DefaultLinkToGroup") {
            var url = config.Backend+'/content/getFile?';
            if (!(cookies.get('Id') === undefined)) {
                url += '&CurrentUserId=' + cookies.get('Id').toString();
            }
            url += '&Link=' + this.state.Link;
            this.setState({Url: url})
        }
    }

    handleInputChange(event) {
        //alert(JSON.stringify(event.target.value))
        this.setState({
            [event.target.name]: event.target.value
        },() => {
            this.props.handleForContent(this.state.Index,this.state.Text
                ,this.state.typingTime,this.state.Link,this.state.myFile,this.state.Type,
                this.state.Url,this.state.ReqType,this.state.Field,this.state.Type == "Btn" || this.state.Type == "DefaultLink"|| this.state.Type=="Cond" ? 1 : 0,this.state.Macros
                ,this.state.BtnType,this.state.BtnUrl,this.state.Headers,this.state.Save,this.state.Cond,this.state.CondRedirect);
        })
    }

    onCheckGroup(e)
    {
        this.setState({BtnType : (e.target.checked ?  "openUrl" : "postBack")}, () => {
            this.props.handleForContent(this.state.Index,this.state.Text
                ,this.state.typingTime,this.state.Link,this.state.myFile,this.state.Type,
                this.state.Url,this.state.ReqType,this.state.Field,this.state.Type == "Btn" || this.state.Type == "DefaultLink"|| this.state.Type=="Cond" ? 1 : 0,
                this.state.Macros,this.state.BtnType,this.state.BtnUrl,this.state.Headers,this.state.Save,this.state.Cond,this.state.CondRedirect);
        })
    }

    onCheckMsg(e)
    {
        this.setState({BtnType : (e.target.checked ?  "openUrl" : "postBack")}, () => {
            this.props.handleForContent(this.state.Index,this.state.Text
                ,this.state.typingTime,this.state.Link,this.state.myFile,this.state.Type,
                this.state.Url,this.state.ReqType,this.state.Field,this.state.Type == "Btn" || this.state.Type == "DefaultLink"|| this.state.Type=="Cond" ? 1 : 0,
                this.state.Macros,this.state.BtnType,this.state.BtnUrl,this.state.Headers,this.state.Save,this.state.Cond,this.state.CondRedirect);
        })
    }


    changeSelect(e)
    {
        if(e) {
            this.setState({
                Field: e.value
            }, () => {
                this.props.handleForContent(this.state.Index, this.state.Text
                    , this.state.typingTime, this.state.Link, this.state.myFile, this.state.Type,
                    this.state.Url, this.state.ReqType, this.state.Field, this.state.Type == "Btn" || this.state.Type == "DefaultLink" || this.state.Type=="Cond"
                        ? 1 : 0, this.state.Macros,
                    this.state.BtnType,this.state.BtnUrl,this.state.Headers,this.state.Save,this.state.Cond,this.state.CondRedirect);
            })
        }
        else {
            this.state.Field = "";
        }
    }

    handleInputFileChange(event) {
        this.setState({myFile:event.target.files[0]},()=>{
            this.props.handleForContent(this.state.Index,this.state.Text
                ,this.state.typingTime,this.state.Link,this.state.myFile,this.state.Type,this.state.Url,this.state.ReqType,this.state.Field,0,this.state.Macros)});
    }

    /*{Upload image}*/
    UploadImage(){
        const IMAGES = [{src: this.state.Url,
            thumbnail:  this.state.Url,
            thumbnailWidth: 250,
            thumbnailHeight: 100,
            isSelected: false,
            caption: ""}]
        return(
            <Gallery images={IMAGES}/>
        )
    }


    /*{End upload image}*/

    ApiBlock() {
        /*
        var attrs = ["Last message","Dialog history",].map(function (attr) {
            return {value: attr, label: attr}
        });*/
        //alert(JSON.stringify(this.props.Bot))
        /*
        if(this.props.Bot.BindTo=="FB") {
            attrs = ["text","first_name", "last_name", "gender", "locale", "timezone"].map(function (attr) {
                return {value: attr, label: attr}
            });
        } else if(this.props.Bot.BindTo=="Skype") {
            attrs = ["text","name", "platform","locale","country"].map(function (attr) {
                return {value: attr, label: attr}
            });
        } else if(this.props.Bot.BindTo=="Slack") {
            attrs = ["text"].map(function (attr) {
                return {value: attr, label: attr}
            });
        } else {
            attrs = ["text"].map(function (attr) {
                return {value: attr, label: attr}
            });
        }
        */
        //                    <input className="chat_input" placeholder="field" type="text" name="Field" value={this.state.Field} onChange={this.handleInputChange}/>

        return (
            <form>
                <div className="api_block">
                    <input type="text" className="chat_input" name="Url" placeholder="url" value={this.state.Url} onChange={this.handleInputChange}/>
                    <select className="chat_input" name="ReqType" value={this.state.ReqType} onChange={this.handleInputChange}>
                        <option value="Get">Get</option>
                        <option value="Post">Post</option>
                    </select>

                    <div className="textare_block_size api_textarea border_textarea_ mt_5 pl">
                        <div className="textarea_help">
                            <span className={'ti-info'}
                                  data-tip data-for='textarea_1'
                            ></span>
                        </div>
                        <div className="textarea_text">
                            <TextareaAutosize className="api_textarea border_textarea_ mt_5 pl" placeholder={""} rows = { 3 } maxRows={99} value={this.state.Headers} name="Headers" onChange={this.handleInputChange}></TextareaAutosize>
                        </div>
                    </div>

                    <ReactTooltip delayShow={100}  id='textarea_1' aria-haspopup='true' role='example'>
                        <p>Set headers for your request</p>
                        <p>Example:HeaderName:HeaderValue; HeaderName2:HeaderValue2</p>
                    </ReactTooltip>


                    <div className="textare_block_size api_textarea border_textarea_ mt_5 pl">
                        <div className="textarea_help">
                            <span className={'ti-info'}
                                  data-tip data-for='textarea_2'
                            ></span>
                        </div>
                        <div className="textarea_text">
                            <TextareaAutosize className="api_textarea border_textarea_ mt_5 pl" placeholder={''} rows = { 3 } maxRows={99} value={this.state.Field} name="Field" onChange={this.handleInputChange}></TextareaAutosize>
                        </div>
                    </div>

                    <ReactTooltip delayShow={100}  id='textarea_2' aria-haspopup='true' role='example'>
                        <p>Set query params for you request</p>
                        <p>Example:{'{"field1":"<Context.param>,"field2":"<Context.param2>"}'}</p>
                    </ReactTooltip>


                    <div className="textare_block_size api_textarea border_textarea_ mt_5 pl">
                        <div className="textarea_help">
                            <span className={'ti-info'}
                                  data-tip data-for='textarea_3'
                            ></span>
                        </div>
                        <div className="textarea_text">
                            <TextareaAutosize className="api_textarea border_textarea_ mt_5 pl" placeholder={""} rows = { 3 } maxRows={99} value={this.state.Macros} name="Macros" onChange={this.handleInputChange}></TextareaAutosize>
                        </div>
                    </div>

                    <ReactTooltip delayShow={100}  id='textarea_3' aria-haspopup='true' role='example'>
                        <p>Set macros for bot answer</p>
                        <p>See examples on help page</p>
                    </ReactTooltip>

                    <div className="textare_block_size api_textarea border_textarea_ mt_5 pl">
                        <div className="textarea_help">
                            <span className={'ti-info'}
                                  data-tip data-for='textarea_4'
                            ></span>
                        </div>
                        <div className="textarea_text">
                            <TextareaAutosize className="api_textarea border_textarea_ mt_5 pl" placeholder={""} rows = { 3 } maxRows={99} value={this.state.Save} name="Save" onChange={this.handleInputChange}></TextareaAutosize>
                        </div>
                    </div>

                    <ReactTooltip delayShow={100}  id='textarea_4' aria-haspopup='true' role='example'>
                        <p>Save data from api answer</p>
                        <p>Example:{'Context.param1=rootOfResponse.pathToData.Data,Context.param2=rootOfResponse.pathToData2[1].Data2'}</p>
                        <p>'rootOfResponse' contains the body of the response</p>
                    </ReactTooltip>

                </div>
            </form>
        )
    }

    BtnBlock() {
        var options = this.props.BotMsgs.map((msg)=>
            <option value={msg._id}>{msg.Name +"(#"+msg._id+")"}</option>
        )
        var ok = false;
        for(var i=0;i<this.props.BotMsgs.length;i++)
        {
            if(this.props.BotMsgs[i]._id==this.props.BotMsg)
            {
                options.splice(i,1);
            }
            else if(this.state.Url == this.props.BotMsgs[i]._id) {
                ok = true;
            }
        }

        if(!ok && this.props.id.length<3) {
            for (var i = 0; i < this.props.BotMsgs.length; i++) {
                if (this.props.BotMsgs[i]._id != this.props.BotMsg) {
                    this.state.Url = this.props.BotMsgs[i]._id;
                }
            }

            this.props.handleForContent(this.state.Index,this.state.Text
                ,this.state.typingTime,this.state.Link,this.state.myFile,this.state.Type,this.state.Url,this.state.ReqType,this.state.Field,
                0,this.state.Macros,this.state.BtnType,this.state.BtnUrl);
        }
        return (
            <form>
                <div className="api_block">
                    <input type="text" className="chat_input" name="Text" value={this.state.Text} onChange={this.handleInputChange}/>
                    { this.state.BtnType=="postBack" ?
                        <select className="chat_input" name="Url" value={this.state.Url}
                                onChange={this.handleInputChange}>
                            {options}
                        </select> :
                        <input className="chat_input" type="text" value={this.state.BtnUrl} name="BtnUrl" onChange={this.handleInputChange}/>
                    }
                    <div className="wrap_filter_laber">
                        <label htmlFor="enable_filter">Url</label>
                        <input id="enable_filter"  type="checkbox" checked={this.state.BtnType=="openUrl"} name={"BtnType"} onChange={this.onCheckMsg}/>
                    </div>
                </div>
            </form>
        )
    }

    BtnBlockToGroup() {
        if(this.props.Groups && this.props.Groups.length>0) {
            var options = this.props.Groups.map((msg) =>
                <option value={msg._id}>{msg.Name + "(#" + msg._id + ")"}</option>
            )
            var ok = false;
            for (var i = 0; i < this.props.Groups.length; i++) {
                if (this.state.Url == this.props.Groups[i]._id) {
                    ok = true;
                }
            }
            if (!ok) {
                this.state.Url = this.props.Groups[0]._id;
                this.props.handleForContent(this.state.Index, this.state.Text
                    , this.state.typingTime, this.state.Link, this.state.myFile, this.state.Type, this.state.Url, this.state.ReqType, this.state.Field,
                    0, this.state.Macros, this.state.BtnType, this.state.BtnUrl);
            }
        }
        return (
            <form>
                <div className="api_block">
                    <input type="text" className="chat_input" name="Text" value={this.state.Text} onChange={this.handleInputChange}/>
                    { this.state.BtnType=="postBack" ?
                        <select className="chat_input" name="Url" value={this.state.Url}
                                onChange={this.handleInputChange}>
                            {options}
                        </select> :
                        <input className="chat_input" type="text" value={this.state.BtnUrl} name={"BtnUrl"} onChange={this.handleInputChange}/>
                    }
                    <div className="wrap_filter_laber">
                        <label htmlFor="enable_filter_1">Url</label>
                        <input id="enable_filter_1" className=" border_textarea_" type="checkbox" checked={this.state.BtnType=="openUrl"} name={"BtnType"} onChange={this.onCheckGroup}/>
                    </div>
                </div>
            </form>
        )
    }
    /*{upload video}*/
    UploadVideo(){
        return(
            <Player>
                <source src={this.state.Url} />
            </Player>
        )
    }
    /*{End upload video}*/




    /*{ upload audio}*/
    UploadAudio(){
        var key = "";
        var chars = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "z", "x", "c", "v", "b", "n", "m", "a", "s", "d", "f", "g", "h", "j",
            "k", "l", "q", "w", "e", "r", "t", "y", "u", "i", "o", "p"];
        for (var i = 0; i < 12; i++) {
            var x = Math.floor(Math.random() * (chars.length - 1));
            key += chars[x];
        }
        var id = key + "AudioPlayerId";

        return(
            <div className="uploadAudio">
                <Audio id={id} src={this.state.Url}/>
            </div>
        )
    }

    /*{End upload audio}*/

    TypingTime(){
        return (
            <div className="TypingTime">
                <span>Typing time</span>
                <input type="range" min="0" max="10" placeholder={''} value={this.state.typingTime} onChange={this.handleInputChange} name="typingTime"/>
                <span>{this.state.typingTime}s</span>
            </div>
        )
    }
    MessageBlock(){
//Enter you text here...
        return(
            <div className="EditMessage">
                {this.TypingTime()}
                <TextareaAutosize id={"TextContent"}
                    placeholder={""} rows = { 3 } value={this.state.Text} onChange={this.handleInputChange} name="Text" ></TextareaAutosize>
            </div>
        )
    }
    CondBlock() {
        var options = this.props.BotMsgs.map((msg)=>
            <option value={msg._id}>{msg.Name +"(#"+msg._id+")"}</option>
        )
        var ok = false;
        for(var i=0;i<this.props.BotMsgs.length;i++)
        {
            if(this.props.BotMsgs[i]._id==this.props.BotMsg)
            {
                options.splice(i,1);
            }
            else if(this.state.CondRedirect == this.props.BotMsgs[i]._id) {
                ok = true;
            }
        }

        if(!ok && this.props.id.length<3) {
            for (var i = 0; i < this.props.BotMsgs.length; i++) {
                if (this.props.BotMsgs[i]._id != this.props.BotMsg) {
                    this.state.CondRedirect = this.props.BotMsgs[i]._id;
                }
            }

            this.props.handleForContent(this.state.Index,this.state.Text
                ,this.state.typingTime,this.state.Link,this.state.myFile,this.state.Type,this.state.Url,this.state.ReqType,this.state.Field,
                0,this.state.Macros,this.state.BtnType,this.state.BtnUrl,
                this.state.Headers,this.state.Save,this.state.Cond,this.state.CondRedirect);
        }
        return (
            <form>
                <div className="api_block">
                    <input type="text" placeholder={"parseInt(Context.age)>=30 && parseInt(Context.age)<40"} className="chat_input" name="Cond" value={this.state.Cond} onChange={this.handleInputChange}/>
                    <select className="chat_input" name="CondRedirect" value={this.state.CondRedirect}
                            onChange={this.handleInputChange}>
                        {options}
                    </select>
                </div>
            </form>
        )
    }

    DefaultLinkBlock(){
        var options = this.props.BotMsgs.map((msg)=>
            <option value={msg._id}>{msg.Name +"(#"+msg._id+")"}</option>
        )
        for(var i=0;i<this.props.BotMsgs.length;i++)
        {
            if(this.props.BotMsgs[i]._id==this.props.BotMsg)
            {
                options.splice(i,1);
            }

        }

        return (
            <form>
                <div className="api_block">
                    <label>Default Link</label>
                    <select className="chat_input" name="Url" value={this.state.Url} onChange={this.handleInputChange}>
                        <option value="-">-</option>
                        {options}
                    </select>
                </div>
            </form>
        )
    }

    DefaultLinkBlockToGroup(){
        var options = this.props.Groups.map((g)=>
            <option value={g._id}>{g.Name +"(#"+g._id+")"}</option>
        )
        return (
            <form>
                <div className="api_block">
                    <label>Default Link</label>
                    <select className="chat_input" name="Url" value={this.props.Url ? this.props.Url : "-"} onChange={this.handleInputChange}>
                        <option value="-">-</option>
                        {options}
                    </select>
                </div>
            </form>
        )
    }

    render() {
        return (<div className="container2 mt_c15">
                {this.props.Type=="Text" ? this.MessageBlock() : null}
                {this.props.Type=="Video" ? this.UploadVideo() : null}
                {this.props.Type=="Audio" ? this.UploadAudio() : null}
                {this.props.Type=="Image" ? this.UploadImage() : null}
                {this.props.Type=="Api" ? this.ApiBlock() : null}
                {this.props.Type=="Btn" ? this.BtnBlock() : null}
                {this.props.Type=="Cond" ? this.CondBlock() : null}
                {this.props.Type=="BtnToGroup" ? this.BtnBlockToGroup() : null}
                {this.props.Type=="DefaultLink" ? this.DefaultLinkBlock() : null}
                {this.props.Type=="DefaultLinkToGroup" ? this.DefaultLinkBlockToGroup() : null}
                {this.props.Type!="Text" && this.props.Type!="DefaultLink" && this.props.Type!="DefaultLinkToGroup"
                    ? <div className="button_delete_block"><span className="right_delete_icon" onClick={()=>this.props.onDelete(this.props.Index,this.state.Type)}></span></div>: null}
            </div>
        );
    }
}

export default  Content;