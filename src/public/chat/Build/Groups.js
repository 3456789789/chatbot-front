import React, { Component}  from 'react';
import jQuery from 'jquery';
import Cookies from 'universal-cookie';
import Draggable, {DraggableCore} from 'react-draggable'; // Both at the same time
import { Scrollbars } from 'react-custom-scrollbars';
import TextareaAutosize from 'react-autosize-textarea';
import SvgComponent from "./SvgComponent";
import LeftPanel from "../../LeftPanel";
import TopPanel from "../../TopPanel";
import Popup from "../../Popup";
import Loading from '../../../Loading.js';
import Tags from "./Tags";
import Loader from 'react-loader-spinner';
import ContainerDimensions from 'react-container-dimensions'

const cookies = new Cookies();
var config = require('../../../config.js');

const btnBlue = {
    background: 'orange',
    color: 'white'
}

class Group extends Component {
    constructor(props) {

        super(props);
        this.state = {
            x: +this.props.x,
            y: +this.props.y,
            Name: this.props.Name,
            _id: this.props._id,
        };

        this.onStop=this.onStop.bind(this);
    }

    handleDrag = (e, { deltaX, deltaY }) => {
        this.setState({
            x: this.state.x + deltaX,
            y: this.state.y + deltaY,
        });

    };

    onStop()
    {
        var url = config.Backend + '/group/edit?';
        if (!(cookies.get('Id') === undefined))
        {
            url += '&CurrentUserId=' + cookies.get('Id').toString();
        }

        url+='&Id='+this.state._id;
        url+='&Pos='+parseFloat(this.state.x)+"*"+parseFloat(this.state.y);

        this.setState({Wait:++this.state.Wait});jQuery.getJSON(url, function (res) {
            if (!res.Error) {this.setState({Wait:--this.state.Wait});

            } else {
                this.setState({Wait:--this.state.Wait});alert(JSON.stringify(res));
            }
        }.bind(this));
    }

    render() {
        var p =this.props;
        return (
            <foreignObject>
                <div className="canva-block">
                    <span className={'canva_group'}></span>
                    <h2 className="title">
                        <a onClick={this.props.onEdit}>{this.props.Name}</a>
                        <span onClick={this.props.onRename} className="canva-block_edit"></span>
                    </h2>

                    <div className="canva_btn">
                        <button onClick={this.props.onEdit} className="Canva_btn_edit">Edit</button>
                        {p.nlu ?
                            <button onClick={this.props.onNluEdit} className="Canva_btn_edit">Intent</button>
                        : null}
                        <button onClick={this.props.onDelete} className="Canva_btn_del"><i class="fas fa-trash-alt"></i></button>
                    </div>
                </div></foreignObject>
        )
    }
}

class Groups extends Loading {
    constructor(props) {
        super(props);
        this.state = {
            User: {},
            Bot: {},
            Groups: [],
            GroupsWasLoad: false,
            Group: "-",
            TextContents: [],
            BtnContents: [],
            ShowDeleteGroupPopup: false,
            ShowRenameGroupPopup: false,
            NluEditIndex: -1,
            Wait: 0,
            Commands: [],
            CommandIndex: 0,
            Intents:{},
        }

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleInputFileChange = this.handleInputFileChange.bind(this);
        this.addGroup = this.addGroup.bind(this);
        this.chooseGroup = this.chooseGroup.bind(this);
        this.deleteGroup = this.deleteGroup.bind(this);
        this.renameGroup = this.renameGroup.bind(this);
        this.saveBotMsg = this.saveBotMsg.bind(this);
        this.wantDeleteGroup = this.wantDeleteGroup.bind(this);
        this.wantRenameGroup = this.wantRenameGroup.bind(this);
        this.getGroupName = this.getGroupName.bind(this);
        this.hideAllPopups = this.hideAllPopups.bind(this);
        this.loadGroups = this.loadGroups.bind(this);
        this.handleForContent = this.handleForContent.bind(this);
        this.groupIsSelected = this.groupIsSelected.bind(this);
        this.refreshGroupPos = this.refreshGroupPos.bind(this);
        this.addRule = this.addRule.bind(this);
        this.onDeleteLink = this.onDeleteLink.bind(this);
        this.onDeleteTag = this.onDeleteTag.bind(this);
        this.onAddTag = this.onAddTag.bind(this);
        this.linkChanged = this.linkChanged.bind(this);
        this.dragLink = this.dragLink.bind(this);
        this.undragLink = this.undragLink.bind(this);
        this.checkLink = this.checkLink.bind(this);
        this.ctrl = this.ctrl.bind(this);
        this.restore = this.restore.bind(this);
        this.pushCommand = this.pushCommand.bind(this);
        this.stateBot = this.stateBot.bind(this);
        this.change = config.change.bind(this);
    }
    pushCommand(undo,redo)
    {
        var arr = [];
        for(var i =0;i<this.state.CommandIndex;i++)
        {
            arr.push(this.state.Commands[i]);
        }
        arr.push({Redo:redo,Undo:undo});
        this.setState({Commands:arr,CommandIndex:this.state.CommandIndex+1})
    }
ctrl(e)
{
    var s =this.state;
    if( e.ctrlKey && s.Commands.length>0) {
        if (e.keyCode == 90 && s.CommandIndex>0)//ctrl+z
        {
            this.setState({CommandIndex:--this.state.CommandIndex});
            s.Commands[s.CommandIndex].Undo();
        }
        else if (e.keyCode == 89 && s.CommandIndex<s.Commands.length)//ctrl+y
        {
            s.Commands[s.CommandIndex].Redo();
            this.setState({CommandIndex:s.CommandIndex+1});
        }
    }
}
    dragLink(from,to)
    {
        var s =this.state;
        if(config.indexOf(s.Bot.AiLinks,{Link:to})==-1)
        {
            this.addRule(to);
        }
    }
    undragLink(from,to)
    {
        var s =this.state;
        if(config.indexOf(s.Bot.AiLinks,{Link:to})!=-1)
        {
            this.onDeleteLink(config.indexOf(s.Bot.AiLinks,{Link:to}),1);
        }
    }
    checkLink(from,to)
    {
        var s =this.state;
        return (config.indexOf(s.Bot.AiLinks,{Link:to})!=-1);
    }
    onDeleteTag(i,ti) {
        var url = config.Backend + '/bot/edit?';
        if (!(cookies.get('Id') === undefined))
        {
            url += '&CurrentUserId=' + cookies.get('Id').toString();
        }
        url+= '&Id='+ this.state.Bot._id;
        url+= '&EditRule=1';
        url+= '&DeleteTag=1';
        url+= '&Index='+i;
        url+= '&TagIndex='+ti;
        this.setState({Wait:++this.state.Wait});jQuery.getJSON(url, function (res) {
            if (!res.Error) {this.setState({Wait:--this.state.Wait});
                const bot = JSON.parse(JSON.stringify(this.state.Bot));
                const new_bot = JSON.parse(JSON.stringify(res));
                this.pushCommand(()=>this.stateBot(bot),()=>this.stateBot(new_bot))
                this.setState({Bot:res});
            } else {
                this.setState({Wait:--this.state.Wait});alert(JSON.stringify(res));
            }
        }.bind(this));
    }

    onAddTag(i,tag)
    {
        var url = config.Backend + '/bot/edit?';
        if (!(cookies.get('Id') === undefined))
        {
            url += '&CurrentUserId=' + cookies.get('Id').toString();
        }
        url+= '&Id='+ this.state.Bot._id;
        url+= '&EditRule=1';
        url+= '&Tag='+tag;
        url+= '&Index='+i;
        this.setState({Wait:++this.state.Wait});jQuery.getJSON(url, function (res) {
            if (!res.Error) {this.setState({Wait:--this.state.Wait});
                const bot = JSON.parse(JSON.stringify(this.state.Bot));
                const new_bot = JSON.parse(JSON.stringify(res));
                    this.pushCommand(()=>this.stateBot(bot),()=>this.stateBot(new_bot))
                this.setState({Bot:res});
            } else {
                this.setState({Wait:--this.state.Wait});alert(JSON.stringify(res));
            }
        }.bind(this));
    }

    onDeleteLink(i,not_save_state) {
        var url = config.Backend + '/bot/edit?';
        if (!(cookies.get('Id') === undefined))
        {
            url += '&CurrentUserId=' + cookies.get('Id').toString();
        }
        url+= '&Id='+ this.state.Bot._id;
        url+= '&DeleteRule=1';
        url+= '&Index='+i;
        this.setState({Wait:++this.state.Wait});jQuery.getJSON(url, function (res) {
            if (!res.Error) {
                this.setState({Wait:--this.state.Wait});
                const bot = JSON.parse(JSON.stringify(this.state.Bot));
                if(!(not_save_state===1))
                {
                    this.pushCommand(()=>this.stateBot(bot),()=>this.stateBot(res))
                }
                var b = this.state.Bot;
                b.AiLinks = [];
                this.setState({Bot:b},()=>this.setState({Bot:res}));
            } else {
                this.setState({Wait:--this.state.Wait});alert(JSON.stringify(res));
            }
        }.bind(this));
    }



    linkChanged(i,newLink) {
        var url = config.Backend + '/bot/edit?';
        if (!(cookies.get('Id') === undefined))
        {
            url += '&CurrentUserId=' + cookies.get('Id').toString();
        }
        url+= '&Id='+ this.state.Bot._id;
        url+= '&EditRule=1';
        url+= '&Index='+i;
        url+= '&Link='+newLink;
        jQuery.getJSON(url, function (res) {
            if (!res.Error) {
                this.setState({Bot:res});
            } else {
                alert(JSON.stringify(res));
            }
        }.bind(this));
    }
    stateBot(b)
    {
        this.setState({Wait:++this.state.Wait})
        config.getData('bot/edit',{Bot:b}).then(function (ok) {
            this.setState({Wait:--this.state.Wait})
            var b = this.state.Bot;
            b.AiLinks = [];
            this.setState({Bot:b},()=>this.setState({Bot:ok}));
        }.bind(this),function (err) {
            this.setState({Wait:--this.state.Wait})
            alert(JSON.stringify(err))
        }.bind(this))
    }
    addRule(link) {
        var url = config.Backend + '/bot/edit?';
        if (!(cookies.get('Id') === undefined))
        {
            url += '&CurrentUserId=' + cookies.get('Id').toString();
        }
        url+= '&Id='+ this.state.Bot._id;
        url+= '&AddRule=1';
        if(typeof link=="string")
        {
            url+= '&Link='+link;
        }

        this.setState({Wait:++this.state.Wait});jQuery.getJSON(url, function (res) {
            if (!res.Error) {this.setState({Wait:--this.state.Wait});
                if(typeof link!="string")
                {
                    const b =JSON.parse(JSON.stringify(this.state.Bot));
                    this.pushCommand(()=>this.stateBot(b),()=>this.stateBot(res));
                }
                this.setState({Bot:res});
            } else {
                this.setState({Wait:--this.state.Wait});alert(JSON.stringify(res));
            }
        }.bind(this));
    }


    renameGroup(id,Name,not_save_state) {
        var url = config.Backend + '/group/edit?';
        if (!(cookies.get('Id') === undefined))
        {
            url += '&CurrentUserId=' + cookies.get('Id').toString();
        }
        url+= '&Bot='+ this.state.Bot._id;
        url+= '&Id='+id;
        url+= '&Name='+Name;

        this.setState({Wait:++this.state.Wait});jQuery.getJSON(url, function (res) {
            if (!res.Error) {this.setState({Wait:--this.state.Wait});
            var oldName = "";
                for(var i=0;i<this.state.Groups.length;i++)
                {
                    if(this.state.Groups[i]._id==id)
                    {
                        oldName = this.state.Groups[i].Name;
                        this.state.Groups[i].Name = Name;
                    }
                }
                var old = this.state.Groups;
                if(!(not_save_state===1))
                {
                    this.pushCommand(()=>this.renameGroup(id,oldName,1),()=>this.renameGroup(id,Name,1))
                }
                //var arr = [];
                //this.setState({Groups:arr});
                this.setState({Groups:old});
            } else {
                this.setState({Wait:--this.state.Wait});alert(JSON.stringify(res));
            }
        }.bind(this));


        this.hideAllPopups();
    }
    saveGroup() {
        var s=this.state;
        var i =s.NluEditIndex;
        var url = config.Backend + '/group/edit?';
        if (!(cookies.get('Id') === undefined))
        {
            url += '&CurrentUserId=' + cookies.get('Id').toString();
        }
        url+= '&Bot='+ s.Bot._id;
        url+= '&Id='+s.Groups[i]._id;
        url+= '&Name='+s.Groups[i].Name;
        if(s.Groups[i].NLU)
        url+= '&Intent='+s.Groups[i].Intent;

        this.setState({Wait:++this.state.Wait});
        jQuery.getJSON(url, function (res) {
            this.setState({Wait:--this.state.Wait});
            if (!res.Error) {
                this.setState({NluEditIndex:-1});
            } else {
                alert(JSON.stringify(res));
            }
        }.bind(this));
        this.hideAllPopups();
    }
    refreshGroupPos(id,newPos) {
        for(var i=0;i<this.state.Groups.length;i++)
        {
            if(id==this.state.Groups[i]._id)
            {
                this.state.Groups[i].Pos = newPos;
            }
        }
        if(id=="-1")
        {
            var bot = this.state.Bot;
            bot.AiPos=newPos;
            this.setState({Bot:this.state.Bot})
        }
    }

    handleForContent(index,Text,Time,Link,File,Type,Url,ReqType,Field,Refresh) {
        if(Type=="Text")
        {
            this.state.TextContents[index].Text = Text;
            this.state.TextContents[index].Time = Time;
        }
        else if(Type=="DefaultLink")
        {
            for(var i=0;i<this.state.BotMsgs.length;i++)
            {
                if(this.state.BotMsgs[i]._id==this.state.BotMsg)
                {
                    this.state.BotMsgs[i].DefaultLink = Url;
                }
            }
        }
        if(Refresh==1)
        {
            var arr = this.state.BotMsgs;
            this.setState({BotMsgs:[]})
            this.setState({BotMsgs:arr})
        }

    }

    handleInputFileChange(event) {
        this.setState({Image:event.target.files[0]});
    }

    hideAllPopups() {
        this.setState({ShowRenameGroupPopup:false})
        this.setState({ShowDeleteGroupPopup:false})
    }

    getGroupName() {
        for(var i=0;i<this.state.Groups.length;i++)
        {
            if (this.state.Groups[i]._id==this.state.Group) {
                return this.state.Groups[i].Name;
            }
        }
    }

    wantRenameGroup(id) {
        this.hideAllPopups();
        this.setState({ShowRenameGroupPopup:true,Group:id},()=>{
           document.getElementById("PopupInput").focus();
        })
    }

    wantDeleteGroup(id) {
        this.hideAllPopups();
        this.setState({ShowDeleteGroupPopup:true});
        this.setState({Group:id});
    }

    saveBotMsg(id) {
        var url = config.Backend + '/botMsg/edit?';
        if (!(cookies.get('Id') === undefined))
        {
            url += '&CurrentUserId=' + cookies.get('Id').toString();
        }
        url+= '&Id='+this.state.BotMsg;
        url+= '&Name='+this.state.BotMsgName;

        for(var i=0;i<this.state.BotMsgs.length;i++)
        {
            if(this.state.BotMsgs[i]._id == this.state.BotMsg)
                url+='&DefaultLink='+this.state.BotMsgs[i].DefaultLink;
        }



        this.setState({Wait:++this.state.Wait});jQuery.getJSON(url, function (res) {
            if (!res.Error) {this.setState({Wait:--this.state.Wait});
                this.setState({BotMsg:"-1"});
            } else {
                this.setState({Wait:--this.state.Wait});alert(JSON.stringify(res));
            }
        }.bind(this));

        this.saveContents();
    }

    loadGroups() {
        var url = config.Backend + '/bot/groups?';
        if (!(cookies.get('Id') === undefined))
        {
            url += '&CurrentUserId=' + cookies.get('Id').toString();
        }
        url+='&Id=' + this.state.Bot._id;

        this.setState({Wait:++this.state.Wait});jQuery.getJSON(url, function (res) {
            if (!res.Error) {this.setState({Wait:--this.state.Wait});
                this.setState({Groups: res},()=>{
                    this.setState({GroupsWasLoad:true})
                });
            } else {
                this.setState({Wait:--this.state.Wait});alert(JSON.stringify(res));
            }
        }.bind(this));
    }

    componentWillMount() {
        document.onkeydown = this.ctrl;
        var url = config.Backend + '/users/findById?';
        if (!(cookies.get('Id') === undefined))
        {
            url += 'Id=' + cookies.get('Id').toString();
        }

        this.setState({Wait:++this.state.Wait});jQuery.getJSON(url, function (res) {
            if (!res.Error) {this.setState({Wait:--this.state.Wait});

                this.setState({User: res});
            } else {
                window.location.replace(window.location.origin + '/login');
            }
        }.bind(this));

        url = config.Backend + '/bot/findById?';
        if (!(cookies.get('Id') === undefined))
        {
            url += 'CurrentUserId=' + cookies.get('Id').toString();
        }
        url += '&Id=' + cookies.get('BotId').toString();


        this.setState({Wait:++this.state.Wait});jQuery.getJSON(url, function (res) {
            if (!res.Error) {this.setState({Wait:--this.state.Wait});
                if(!config.checkRights(cookies.get('Id').toString(),res,"Build"))
                {
                    config.accessDenied(cookies.get('Id').toString(),res);
                }
                        this.setState({Bot: res});
                this.setState({Wait:++this.state.Wait});
                config.getData('nlu/all_entity').then(ok=>{
                    this.setState({Wait:--this.state.Wait,Intents:ok});
                },err=>{
                    this.setState({Wait:--this.state.Wait});
                    alert(JSON.stringify(err))
                })
                this.loadGroups();
            } else {
                this.setState({Wait:--this.state.Wait});alert(JSON.stringify(res));
            }
        }.bind(this));
    }

    handleInputChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    RightBar() {
        return (
            <div className="overflow_main_bot">
                <LeftPanel User={this.state.User}/>
                <div className="overflow_main_bot_center">
                {this.state.Bot ? this.Canva() : null}
                </div>
                        <div class="rightPanel">
                            <Scrollbars
                                style={{ height: "90%" }}>
                                <div className="right_container_content">
                                    {this.Right()}
                                </div>
                            </Scrollbars>
                        </div>
                        </div>

        )
    }

    Right() {
        if(this.state.NluEditIndex==-1) return this.EditPanel();
        else return this.NluEdit();
    }
//Anton
    addGroup(nlu) {
        var url = config.Backend + '/group/add?';
        if (!(cookies.get('Id') === undefined))
        {
            url += '&CurrentUserId=' + cookies.get('Id').toString();
        }
        url+='&Bot=' + this.state.Bot._id;
        if(nlu)
            url+='&NLU=' + true;
        url+= '&Pos='+Math.floor(Math.random() * (200))+'*'+Math.floor(Math.random() * (200));
        var g = this.state.Groups;
        var names = [];
        for(var i=0;i<g.length;i++)
        {
            names.push(g[i].Name);
        }
        var newName = "Group";
        var x = 1;
        do
        {
            newName ="Group" + (x++);
        }
        while(names.indexOf(newName)!=-1);
        url+='&Name='+newName;

        this.setState({Wait:++this.state.Wait});jQuery.getJSON(url, function (res) {
            if (!res.Error) {this.setState({Wait:--this.state.Wait});
                this.state.Groups.push(res.Group);
                this.pushCommand(()=>this.deleteGroup(1,res.Group._id),()=>this.restore(res.Group._id));
                this.setState({Groups:this.state.Groups });
            } else {
                this.setState({Wait:--this.state.Wait});alert(JSON.stringify(res));
            }
        }.bind(this));
    }
    restore(id)
    {
        var url = config.Backend + '/group/restore?';
        if (!(cookies.get('Id') === undefined))
        {
            url += '&CurrentUserId=' + cookies.get('Id').toString();
        }
        url+='&Bot=' + this.state.Bot._id;
        url+= '&Id='+id;
        this.setState({Wait:++this.state.Wait});jQuery.getJSON(url, function (res) {
        if (!res.Error) {this.setState({Wait:--this.state.Wait});
            this.state.Groups.push(res.Group);
            this.setState({Groups:this.state.Groups });
        } else {
            this.setState({Wait:--this.state.Wait});alert(JSON.stringify(res));
        }
    }.bind(this));
    }

    deleteGroup(not_save_state,id) {
        var url = config.Backend + '/group/delete?';
        if (!(cookies.get('Id') === undefined))
        {
            url += '&CurrentUserId=' + cookies.get('Id').toString();
        }
        url+='&Bot=' + this.state.Bot._id;
        var gId = id || this.state.Group;
        url+='&Id='+gId;

        this.setState({Wait:++this.state.Wait});jQuery.getJSON(url, function (res) {
            if (!res.Error) {this.setState({Wait:--this.state.Wait});
                var index = 0;
                for(var i=0;i<this.state.Groups.length;i++)
                {
                    if(this.state.Groups[i]._id==gId)
                    {
                        index = i;
                    }
                }
                this.state.Groups.splice(index,1);
                this.setState({Groups:this.state.Groups });
                this.setState({ShowDeleteGroupPopup:false,NluEditIndex:-1});
                if(!(not_save_state===1)) {
                    this.pushCommand(()=>this.restore(gId),()=>this.deleteGroup(1,gId));
                }
            } else {
                this.setState({Wait:--this.state.Wait});alert(JSON.stringify(res));
            }
        }.bind(this));
    }

    chooseGroup(id) {
        //var stateObj = { id: id  };
        //window.history.pushState(stateObj, "page", window.location.origin + '/chat/build/'+id);
        window.location.href=(window.location.origin + '/chat/build/'+id);
    }

    /*{Right group}*/
    NluEdit() {
        var s=this.state
        var i =s.NluEditIndex;
        var intents = [];
        for(var k in s.Intents) {
            var key =k;
            intents.push(<option value={key}>{key}</option>)
        }
        var entities = s.Intents[s.Groups[i].Intent] || [];
        var text="Entities:"+JSON.stringify(entities);
        return (
            <div>
                <div className="right_group">
                        Choose intent
                        <select className={'chat_input borderR'} value={s.Groups[i].Intent} name={"Groups.Intent"} onChange={(e)=>this.change(e,i)}>
                           <option value={""}></option>
                            {intents}
                        </select>
                        {text}
                    <div className="form_group_right_panel">
                        <button onClick={()=>this.saveGroup()} className="btnSave">Save</button>
                        <button onClick={()=>{this.setState({NluEditIndex:-1})}} className="btnCansel">Cancel</button>
                    </div>
                </div>
            </div>
        )
    }

    groupIsSelected(id) {
        return this.state.Group == id;
    }


    /*{End Right group}*/



    /*{Right edit panel}*/

    EditPanel(){
        var links;
        if(this.state.Bot.AiLinks && this.state.GroupsWasLoad) {
            links = this.state.Bot.AiLinks.map(function(elem, ind) {
                const l =elem;
                const i = ind;
                return (<Tags onDeleteLink={this.onDeleteLink}
                                                               Link={l.Link} onAddTag={this.onAddTag}
                                                               onDeleteTag={this.onDeleteTag}
                                                               AiLink={l} Index={i} linkChanged={this.linkChanged}
                                                               Groups={this.state.Groups}/>)
        }.bind(this))
        } else {
            links = null;
        }
        return(
            <div>
                <div className="EditAddBlock">
                    <div className="form_group_right_panel">
                        <button className="addRule" onClick={this.addRule}><span>Add rule</span></button>
                        <button onClick={()=>this.addGroup()} className="addGroup"><span>Add group</span></button>
                        <button onClick={()=>this.addGroup(true)} className="addNLUGroup"><span>Add NLU group</span></button>
                    </div>
                </div>
                {links}
            </div>
        )
    }

    /*{End edit panel}*/

    /*{Canva block}*/
    nluEdit=(g,i)=>{
        this.setState({NluEditIndex:i})
    }
    Canva() {
        const ai = <foreignObject>
            <div className="canva-block level" >
                <div className="canva_t ai_canva">
                    <arrow  className="btn_icon">АI</arrow>
                    <arrow className={'ti-arrow-left arrow_1'}></arrow>
                    <arrow className={'ti-arrow-up arrow_2'}></arrow>
                    <arrow className={'ti-arrow-right arrow_3'}></arrow>
                    <arrow className={'ti-arrow-down arrow_4'}></arrow>
                    {/*<span>Ai</span>*/}

                </div>
            </div></foreignObject>
        const groups = this.state.Groups.map((g,i)=><Group x={g.Pos.split('*')[0]} y={g.Pos.split('*')[1]} key={g._id}
                                                         _id={g._id} Name={g.Name}
                                                         onDelete={()=>this.wantDeleteGroup(g._id)}
                                                         onRename={()=>this.wantRenameGroup(g._id)}
                                                         onNluEdit={()=>this.nluEdit(g,i)} nlu={g.NLU}
                                                         onEdit={()=>this.chooseGroup(g._id)}/>);



        return (

                <SvgComponent pushCommand={this.pushCommand} undragLink={this.undragLink} checkLink={this.checkLink}
                    dragLink={this.dragLink} Bot={this.state.Bot} Ai={ai} childrens={groups} arr={this.state.Groups} isGroups={true} Refresh={this.refreshGroupPos}/>

        )
    }

    /*{End Canva block}*/

    /*{End Setting RightBat}*/

    render() {
        return (
            <div>
                {this.Load()}
                <TopPanel/>
                {this.RightBar()}
                {this.state.ShowDeleteGroupPopup ?
                    <Popup text={"Are you sure?"} Yes={this.deleteGroup}
                           No={()=>{this.setState({ShowDeleteGroupPopup:false})}}/> : null}
                {this.state.ShowRenameGroupPopup ?
                    <Popup text={"Enter new name"} Bot = {this.state.Bot} Group = {this.state.Group}
                           RenameGroup={true} Rename={this.renameGroup}
                           chooseGroup={this.chooseGroup}  Name={this.getGroupName()}     No={()=>{this.setState({ShowRenameGroupPopup:false})}}/> : null}

            </div>
        )
    }
};

export default Groups;
