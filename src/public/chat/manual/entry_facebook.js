import React, { Component}  from 'react';
import jQuery from 'jquery';
import Loading from '../../../Loading.js';
import ManualTop from "./manual_top";
var config = require('../../../config.js');



class ManualConfigureFacebook extends Loading {
    constructor(props) {
        super(props);
        this.state = {
            Group: this.props.Group,
        };
    }

    render(){

        return (
            <div className="wrap_manual">
              <ManualTop/>
                <div className="container">
                    <div className="row">

                        <ol class="breadcrumb  mt_25">
                            <li><a href="/manual/">Manual</a></li>
                            <li><a href="/manual/configure">Configure</a></li>
                            <li><a href="/manual/configure/facebook">facebook</a></li>
                        </ol>

                        <div className="entry_content mt_25">
                            <h2>Facebook</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis, recusandae!</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet at consequuntur cupiditate deleniti dolores fugit, id iure libero maxime natus, nisi non nulla porro quam recusandae repudiandae sint tenetur, veritatis.</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab aspernatur, commodi cum deserunt, dolore eligendi harum in laudantium nisi non optio perspiciatis, possimus quasi ratione repudiandae sapiente tempore vitae voluptatibus?</p>

                        </div>

                    </div>
                </div>
            </div>
        )
    }
}
export default ManualConfigureFacebook;
