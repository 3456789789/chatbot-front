import React, { Component}  from 'react';
import jQuery from 'jquery';
import Loading from '../../../Loading.js';
import ManualTop from "../manual/manual_top";
var config = require('../../../config.js');


class ManualConfigureSlack extends Loading {
    constructor(props) {
        super(props);
        this.state = {
            Group: this.props.Group,
        };
    }

    render(){
        return (
            <div className="wrap_manual">
              <ManualTop/>
                <div className="container">
                    <div className="row">
                        <ol class="breadcrumb  mt_25">
                            <li><a href="/manual/">Manual</a></li>
                            <li><a href="/manual/configure">Configure</a></li>
                            <li><a href="/manual/configure/slack">slack</a></li>
                        </ol>
                        <div className="entry_content mt_25">
                       <h4> If you do not have an account, create an account and your channel slack.</h4>
                        <h3>1:Please log in.</h3>

                            <div class="manual_image">
                                <div class="manual_img border_img">
                                    <img src="/img/Manual/slack/1.png"/>
                                </div>
                            </div>
                            <h3>2: Authorize</h3>
                            <h3>3: You will see in the applications slack, the added application ChatBot</h3>
                            <div class="manual_image">
                                <div class="manual_img border_img">
                                    <img src="/img/Manual/slack/2.png"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default ManualConfigureSlack;
