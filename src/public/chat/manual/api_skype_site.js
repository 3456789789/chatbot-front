import React, { Component}  from 'react';
import jQuery from 'jquery';
import ManualTop from "./manual_top";
import Loading from '../../../Loading.js';
const ReactMarkdown = require('react-markdown')
var config = require('../../../config.js');


class ManualApiSkype extends Loading {
    constructor(props) {
        super(props);
        this.state = {
            Group: this.props.Group,
        };
    }

    render(){
        const input2 =
            '\t{\n\n' +
            '\t"attachments": [' +
            '        {\n\n' +
            '            "contentType": "application/vnd.microsoft.card.adaptive",\n\n' +
            '            "content": {\n\n' +
            '                "type": "AdaptiveCard",\n\n' +
            '                "body": [\n\n' +
            '                    {\n' +
            '                        "type": "TextBlock",\n\n' +
            '                        "text": "<Result.Text>",\n' +
            '                        "size": "large",\n' +
            '                        "weight": "bolder"\n\n' +
            '                    },\n' +
            '                    {\n' +
            '                        "type": "TextBlock",\n\n' +
            '                        "text": "Conf Room 112/3377 (10)"\n' +
            '                    },\n' +
            '                    {\n' +
            '                        "type": "TextBlock",\n' +
            '                        "text": "12:30 PM - 1:30 PM"\n' +
            '                    },\n' +
            '                    {\n' +
            '                        "type": "TextBlock",\n\n' +
            '                        "text": "Snooze for"\n\n' +
            '                    },\n' +
            '                    {\n' +
            '                        "type": "Input.ChoiceSet",\n' +
            '                        "id": "snooze",\n' +
            '                        "style": "compact",\n' +
            '                        "choices": [\n' +
            '                            {\n' +
            '                                "title": "5 minutes",\n' +
            '                                "value": "5",\n' +
            '                                "isSelected": true\n' +
            '                            },\n' +
            '                            {\n' +
            '                                "title": "15 minutes",\n' +
            '                                "value": "15"\n' +
            '                            },\n' +
            '                            {\n' +
            '                                "title": "30 minutes",\n\n' +
            '                                "value": "30"\n' +
            '                            }\n' +
            '                        ]\n' +
            '                    }\n' +
            '                ],\n' +
            '                "actions": [\n' +
            '                    {\n' +
            '                        "type": "Action.Http",\n\n' +
            '                        "method": "POST",\n' +
            '                        "url": "http://foo.com",\n\n' +
            '                        "title": "Snooze"\n' +
            '                    },\n' +
            '                    {\n' +
            '                        "type": "Action.Http",\n' +
            '                        "method": "POST",\n' +
            '                        "url": "http://foo.com",\n' +
            '                        "title": "I\'ll be late"\n' +
            '                    },\n' +
            '                    {\n' +
            '                        "type": "Action.Http",\n' +
            '                        "method": "POST",\n' +
            '                        "url": "http://foo.com",\n' +
            '                        "title": "Dismiss"\n' +
            '                    }\n' +
            '                ]\n' +
            '            }\n' +
            '        }\n' +
            '    ]\n' +
            '\t}'

        var res  = '\tres.json({\n' +
            '            \tResult: {Text: "Some text"},\n' +
            '            \tImages: ["https://back.i-solutions.org/content/getFile?&CurrentUserId=5a29c38d2ed2852067505afe&Link=uploads/5hcx3a8hsbhq-1512758220062.png"],\n' +
            '            \tVideo: "https://back.i-solutions.org/content/getFile?&CurrentUserId=5a29c38d2ed2852067505afe&Link=uploads/fxcnddn7mkz2-1512757701334.webm"\n' +
            '            \t,\n' +
            '            \tAudio: "https://back.i-solutions.org/content/getFile?&CurrentUserId=5a29c38d2ed2852067505afe&Link=uploads/ynbvfw21185t-1512757663654.mp3"\n' +
            '        \t});'

        return (
            <div className="wrap_manual">
                <ManualTop/>
                <div className="container">
                    <div className="row">
                        <ol class="breadcrumb  mt_25">
                            <li><a href="/manual/">Manual</a></li>
                            <li><a href="/manual/build">Build</a></li>
                            <li><a href="/manual/build/api-skype">Skype,Webchat Macros</a></li>
                        </ol>

                        <div className="entry_content mt_25">
                            <h3>Samples here</h3>
                            <h4>
                                <a href={" https://docs.microsoft.com/en-us/bot-framework/rest-api/bot-framework-rest-connector-add-rich-cards"}>
                                https://docs.microsoft.com/en-us/bot-framework/rest-api/bot-framework-rest-connector-add-rich-cards</a>
                            </h4>
                            <p>they need not be completely copied, but only the attachments field and wrap it in the object</p>
                           <div><ReactMarkdown source={input2} /></div>

                            <p>For a test, you can do a post and get request on the url</p>
                            <p><a href={config.Backend+'/skype/test?'}>{config.Backend+'/skype/test?'}</a></p>
                            <p>it returns the following answer</p>
                            <br/>
                            <div><ReactMarkdown source={res} /></div>
                        </div>

                    </div>
                </div>
            </div>
        )
    }
}
export default ManualApiSkype;
