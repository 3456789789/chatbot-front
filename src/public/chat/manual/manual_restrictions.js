import React, { Component}  from 'react';
import jQuery from 'jquery';
import ManualTop from "../manual/manual_top";
import Loading from '../../../Loading.js';
var config = require('../../../config.js');

class ManualRestrictions extends Loading {
    constructor(props) {
        super(props);
        this.state = {
            Group: this.props.Group,
        };
    }

    render(){
        return (
            <div className="wrap_manual">
                <ManualTop/>
                <div className="container">
                    <div className="row">

                        <ol class="breadcrumb  mt_25">
                            <li><a href="/manual/">Manual</a></li>
                            <li><a href="/manual/restrictions">Restrictions</a></li>
                        </ol>

                        <div className="article_main">
                            <ul className="article_list">
                                <li className="article_item">
                                    <a href="/manual/restrictions/fb">
                                        <div className="article_item_content">
                                            <h3>Facebook</h3>
                                        </div>
                                    </a>
                                </li>
                                <li className="article_item">
                                    <a href="/manual/restrictions/skype">
                                        <div className="article_item_content">
                                            <h3>Skype</h3>
                                        </div>
                                    </a>
                                </li>
                                <li className="article_item">
                                    <a href="/manual/restrictions/slack">
                                        <div className="article_item_content">
                                            <h3>Slack</h3>
                                        </div>
                                    </a>
                                </li>
                                <li className="article_item">
                                    <a href="/manual/restrictions/site">
                                        <div className="article_item_content">
                                            <h3>Web chat</h3>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        )
    }
}
export default ManualRestrictions;
