import React, { Component}  from 'react';
import jQuery from 'jquery';
import ManualTop from "./manual_top";
import Loading from '../../../Loading.js';
var config = require('../../../config.js');


class ManualConfigureVk extends Loading {
    constructor(props) {
        super(props);
        this.state = {
            Group: this.props.Group,
        };
    }

    render(){

        return (
            <div className="wrap_manual">
                <ManualTop/>
                <div className="container">
                    <div className="row">

                        <ol class="breadcrumb  mt_25">
                            <li><a href="/manual/">Manual</a></li>
                            <li><a href="/manual/configure">Configure</a></li>
                            <li><a href="/manual/configure/vk">Vk</a></li>
                        </ol>

                        <div className="entry_content mt_25">
                            <h3>1. Создайте группу или публичную страницу</h3>
                            <h3>2. Зайдите в группу и выберите пункт меню "Управление сообществом" -> "Настройки" -> "Работа с API"</h3>

                            <div className="manual_image">
                                <div class="manual_img border_img">
                                    <img src="/img/Manual/vk/1.png"/>
                                </div>
                            </div>
                            <div class="manual_image">
                                <div class="manual_img border_img">
                                    <img src="/img/Manual/vk/2.png"/>
                                </div>
                            </div>

                            <h3>3. Создайте ключ доступа сообщества(этот ключ вам понадобится для привязки вашего бота к сообществу)</h3>
                            <div class="manual_image">
                                <div class="manual_img border_img">
                                    <img src="/img/Manual/vk/3.png"/>
                                </div>
                            </div>

                            <h3>4: Перейдите на вкладку Callback API,скопируйте оттуда id вашей группы и код который должен вернуть сервер
                            а также введите адрес сервера(но не нажимайте кнопку потвердить!)</h3>
                            <div class="manual_image">
                                <div class="manual_img border_img">
                                    <img src="/img/Manual/vk/4.png"/>
                                </div>
                            </div>

                            <h3>5: Введите ключ доступа,id группы и код в форму на нашем сайте и нажмите "Привязать"</h3>
                            <h3>6: Потвердите адрес сервера в окне с которым вы работали в пункте 4</h3>
                            <h3>7: Далее перейдите во вкладку "Типы событий" и установите галочку "Входящее сообщение"</h3>
                            <div class="manual_image">
                                <div class="manual_img border_img">
                                    <img src="/img/Manual/vk/5.png"/>
                                </div>
                            </div>
                            <br/>
                            <br/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default ManualConfigureVk;
