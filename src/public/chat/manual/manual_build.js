import React, { Component}  from 'react';
import jQuery from 'jquery';
import ManualTop from "../manual/manual_top";
import Loading from '../../../Loading.js';
var config = require('../../../config.js');

class ManualBuild extends Loading {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render(){

        return (
            <div className="wrap_manual">
                <ManualTop/>
                <div className="container">
                    <div className="row">
                        <ol class="breadcrumb  mt_25">
                            <li><a href="/manual/">Manual</a></li>
                            <li><a href="/manual/configure">Build</a></li>
                        </ol>
                        <div className="article_main">
                            <ul className="article_list">
                                <li className="article_item">
                                    <a href="/manual/build/api-skype">
                                        <div className="article_item_content">
                                            <h3>Macros for Skype/Webchat</h3>
                                            <p>Learn how to create API macros for Skype/Webchat bot</p>
                                        </div>
                                    </a>
                                </li>
                                <li className="article_item">
                                    <a href="/manual/build/api-slack">
                                        <div className="article_item_content">
                                            <h3>Macros for Slack</h3>
                                            <p>Learn how to create API macros for Slack</p>
                                        </div>
                                    </a>
                                </li>
                                <li className="article_item">
                                    <a href="/manual/build/api-fb">
                                        <div className="article_item_content">
                                            <h3>Macros for Facebook</h3>
                                            <p>Learn how to create API macros for Facebook</p>
                                        </div>
                                    </a>
                                </li>
                                <li className="article_item">
                                    <a href="/manual/build/keywords">
                                        <div className="article_item_content">
                                            <h3>Keywords in messages</h3>
                                            <p>Learn how to add keyword in your message</p>
                                        </div>
                                    </a>
                                </li>
                                <li className="article_item">
                                    <a href="/manual/build/check-api">
                                        <div className="article_item_content">
                                            <h3>Api call status</h3>
                                            <p>Learn how to check api call status</p>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default ManualBuild;
