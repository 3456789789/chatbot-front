import React, { Component}  from 'react';
import jQuery from 'jquery';
import Loading from '../../../Loading.js';
import ManualTop from "../manual/manual_top";
var config = require("../../../config.js");

class ManualConfigureSlack extends Loading {
    constructor(props) {
        super(props);
        this.state = {
            Group: this.props.Group,
        };
    }

    render(){
        //
        return (
            <div className="wrap_manual">
                <ManualTop/>
                <div className="container">
                    <div className="row">
                        <ol class="breadcrumb  mt_25">
                            <li><a href="/manual/">Manual</a></li>
                            <li><a href="/manual/configure">Configure</a></li>
                            <li><a href="/manual/configure/slack">slack</a></li>
                        </ol>
                        <div className="entry_content mt_25">
                            <h4> If you do not have an account, create an account and your channel slack.</h4>
                            <h3>1:Create a Slack App.</h3>
                            <div class="manual_image">
                                <div class="manual_img border_img">
                                    <img src="/img/Manual/slack_new/1.png"/>
                                </div>
                            </div>
                            <h3>2: Go to Basic Information->Interactive Components</h3>
                            <div class="manual_image">
                                <div class="manual_img border_img">
                                    <img src="/img/Manual/slack_new/2.png"/>
                                </div>
                            </div>
                            <h3>3: Click enable button and paste the next url({config.Backend+"/slack/hook/btns"}) in input fields,then click enable button</h3>
                            <div class="manual_image">
                                <div class="manual_img border_img">
                                    <img src="/img/Manual/slack_new/3.png"/>
                                </div>
                            </div>
                            <div class="manual_image">
                                <div class="manual_img border_img">
                                    <img src="/img/Manual/slack_new/4.png"/>
                                </div>
                            </div>
                            <h3>4: Go to Basic Information->Event Subscriptions</h3>
                            <div class="manual_image">
                                <div class="manual_img border_img">
                                    <img src="/img/Manual/slack_new/5.png"/>
                                </div>
                            </div>
                            <h3>5:Click enable button and paste the next url({config.Backend+"/slack/hook"}) in Request URL field,
                                then add 3 worspace events(message.groups,message.im,message.channels) and save changes</h3>
                            <div class="manual_image">
                                <div class="manual_img border_img">
                                    <img src="/img/Manual/slack_new/6.png"/>
                                </div>
                            </div>
                            <h3>6: Go to Basic Information->Bots and create a Bot User</h3>
                            <div class="manual_image">
                                <div class="manual_img border_img">
                                    <img src="/img/Manual/slack_new/7.png"/>
                                </div>
                            </div>
                            <div class="manual_image">
                                <div class="manual_img border_img">
                                    <img src="/img/Manual/slack_new/8.png"/>
                                </div>
                            </div>
                            <div class="manual_image">
                                <div class="manual_img border_img">
                                    <img src="/img/Manual/slack_new/9.png"/>
                                </div>
                            </div>
                            <h3>7: Go to Basic Information->Permissions</h3>
                            <div class="manual_image">
                                <div class="manual_img border_img">
                                    <img src="/img/Manual/slack_new/13.png"/>
                                </div>
                            </div>
                            <h3>8:Add the next redirect url({window.location.origin+"/chat/configure"}),
                                then save changes</h3>
                            <div class="manual_image">
                                <div class="manual_img border_img">
                                    <img src="/img/Manual/slack_new/14.png"/>
                                </div>
                            </div>
                            <h3>9:Copy you Client Id,Secret and Verification Token and paste it into configure page,then click connect button</h3>
                            <div class="manual_image">
                                <div class="manual_img border_img">
                                    <img src="/img/Manual/slack_new/16.png"/>
                                </div>
                            </div>
                            <div class="manual_image">
                                <div class="manual_img border_img">
                                    <img src="/img/Manual/slack_new/17.png"/>
                                </div>
                            </div>
                            <h3>10:Authorize and you can se bot in you slack</h3>
                            <div class="manual_image">
                                <div class="manual_img border_img">
                                    <img src="/img/Manual/slack_new/15.png"/>
                                </div>
                            </div>
                            <div class="manual_image">
                                <div class="manual_img border_img">
                                    <img src="/img/Manual/slack_new/12.png"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default ManualConfigureSlack;
