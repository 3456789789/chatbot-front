import React, { Component}  from 'react';
import jQuery from 'jquery';
import ManualTop from "./manual_top";
import Loading from '../../../Loading.js';
const ReactMarkdown = require('react-markdown')
var config = require('../../../config.js');


class ManualApiFb extends Loading {
    constructor(props) {
        super(props);
        this.state = {
            Group: this.props.Group,
        };
    }

    render(){
        var exam = '\n' +
            '\t{\n' +
            '    "attachment":{\n' +
            '      "type":"template",\n' +
            '      "payload":{\n' +
            '        "template_type":"generic",\n' +
            '        "elements":[\n' +
            '           {\n' +
            '            "title":"OK <Result.Text>",\n' +
            '            "image_url":"<Images[0]>",\n' +
            '            "subtitle":"We ve got the right hat for everyone.",\n' +
            '            "buttons":[\n' +
            '             {\n' +
            '                "type":"postback",\n' +
            '                "title":"Start Chatting",\n' +
            '                "payload":"DEVELOPER_DEFINED_PAYLOAD"\n' +
            '              }              \n' +
            '            ]      \n' +
            '          }\n' +
            '        ]\n' +
            '      }\n' +
            '    }\n' +
            '\t}'

        var res  = '\tres.json({\n' +
            '            \tResult: {Text: "Some text"},\n' +
            '            \tImages: ["https://back.i-solutions.org/content/getFile?&CurrentUserId=5a29c38d2ed2852067505afe&Link=uploads/5hcx3a8hsbhq-1512758220062.png"],\n' +
            '            \tVideo: "https://back.i-solutions.org/content/getFile?&CurrentUserId=5a29c38d2ed2852067505afe&Link=uploads/fxcnddn7mkz2-1512757701334.webm"\n' +
            '            \t,\n' +
            '            \tAudio: "https://back.i-solutions.org/content/getFile?&CurrentUserId=5a29c38d2ed2852067505afe&Link=uploads/ynbvfw21185t-1512757663654.mp3"\n' +
            '        \t});'

        return (
            <div className="wrap_manual">
                <ManualTop/>
                <div className="container">
                    <div className="row">

                        <ol class="breadcrumb  mt_25">
                            <li><a href="/manual/">Manual</a></li>
                            <li><a href="/manual/build">Build</a></li>
                            <li><a href="/manual/build/api-fb">Fb Macros</a></li>
                        </ol>

                        <div className="entry_content mt_25">
                            <p>Samples  <a href={'https://developers.facebook.com/docs/messenger-platform/send-messages/templates'}>
                                here</a></p>
                            <h3>Example</h3>
                            <div className="entry_content_coding">
                                <div><ReactMarkdown source={exam} /></div>
                                <p>for a test, you can do a post and get request on url</p>
                                <p><a href={config.Backend+'/skype/test?'}>{config.Backend+'/skype/test?'}</a></p>
                                <h3>it returns the following answer</h3>
                                <br/>
                                <br/>
                                <div><ReactMarkdown source={res} /></div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        )
    }
}
export default ManualApiFb;
