import React, { Component}  from 'react';
import jQuery from 'jquery';
import ManualTop from "../manual/manual_top";
import Loading from '../../../Loading.js';
var config = require('../../../config.js');

class ManualMain extends Loading {
    constructor(props) {
        super(props);
        this.state = {
            Group: this.props.Group,
        };
    }

    render(){
        /*
          <li>
                                    <a href="">
                                        <div className="item_manual">
                                            <div className="list_manual_left">
                                                <div className="list_manual_left_image manual_broadcast"></div>
                                            </div>
                                            <div className="list_manual_right">
                                                <div className="list_manual_right_content">
                                                    <h3>BroadCast</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="">
                                        <div className="item_manual">
                                            <div className="list_manual_left">
                                                <div className="list_manual_left_image manual_grow_up"></div>
                                            </div>
                                            <div className="list_manual_right">
                                                <div className="list_manual_right_content">
                                                    <h3>Grow Up</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="">
                                        <div className="item_manual">
                                            <div className="list_manual_left">
                                                <div className="list_manual_left_image manual_analyze"></div>
                                            </div>
                                            <div className="list_manual_right">
                                                <div className="list_manual_right_content">
                                                    <h3>Analyze</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </li>
         */

        return (
            <div className="wrap_manual">
                <ManualTop/>
            <div className="manual_list_content">
                <div className="container">
                    <div className="row">
                            <ul className="list_manual_item">
                                <li>
                                    <a href="/manual/build">
                                        <div className="item_manual">
                                            <div className="list_manual_left">
                                                <div className="list_manual_left_image manual_build"></div>
                                            </div>
                                            <div className="list_manual_right">
                                                <div className="list_manual_right_content">
                                                    <h3>Build</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </li>

                                <li>
                                    <a href="/manual/configure">
                                        <div className="item_manual">
                                            <div className="list_manual_left">
                                                <div className="list_manual_left_image manual_configure"></div>
                                            </div>
                                            <div className="list_manual_right">
                                                <div className="list_manual_right_content">
                                                    <h3>Configure</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="/manual/restrictions">
                                        <div className="item_manual">
                                            <div className="list_manual_left">
                                                <div className="list_manual_left_image manual_configure"></div>
                                            </div>
                                            <div className="list_manual_right">
                                                <div className="list_manual_right_content">
                                                    <h3>Restrictions</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default ManualMain;
