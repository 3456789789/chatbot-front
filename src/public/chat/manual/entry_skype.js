import React, { Component}  from 'react';
import jQuery from 'jquery';
import ManualTop from "./manual_top";
import Loading from '../../../Loading.js';
var config = require('../../../config.js');


class ManualConfigureSkype extends Loading {
    constructor(props) {
        super(props);
        this.state = {
            Group: this.props.Group,
        };
    }

    render(){

        return (
            <div className="wrap_manual">
                <ManualTop/>
                <div className="container">
                    <div className="row">

                        <ol class="breadcrumb  mt_25">
                            <li><a href="/manual/">Manual</a></li>
                            <li><a href="/manual/configure">Configure</a></li>
                            <li><a href="/manual/configure/skype">skype</a></li>
                        </ol>

                        <div className="entry_content mt_25">
                        <h3>1. Register a Microsoft record and push <a href={"https://dev.botframework.com/bots"}>Create a bot</a> button</h3>
                        <h3>2. Select "Bot Channels Registration" and push "Create"</h3>

                        <div class="manual_image">
                            <div class="manual_img border_img">
                                <img src="/img/Manual/skype/1.png"/>
                            </div>
                        </div>

                        <h3>3. Enter the endpoint "{config.Backend+'/skype/hook'}" and push "create"</h3>
                        <div class="manual_image">
                            <div class="manual_img border_img">
                                <img src="/img/Manual/skype/2.png"/>
                            </div>
                        </div>

                        <h3>4: Select "Resources groups"->the name of your Bot Service </h3>
                            <div class="manual_image">
                                <div class="manual_img border_img">
                                    <img src="/img/Manual/skype/3.png"/>
                                </div>
                            </div>

                            <h3>5: Select "Deployments" and copy AppId and AppSecret(AppPassword) </h3>
                            <div class="manual_image">
                                <div class="manual_img border_img">
                                    <img src="/img/Manual/skype/4.png"/>
                                </div>
                            </div>
                            <h3>6: Select "All Resources"->the name of your Bot Service</h3>
                            <div class="manual_image">
                                <div class="manual_img border_img">
                                    <img src="/img/Manual/skype/5.png"/>
                                </div>
                            </div>
                            <h3>7: Select "Channels"->push the Skype icon,then push "Save"</h3>
                            <div class="manual_image">
                                <div class="manual_img border_img">
                                    <img src="/img/Manual/skype/6.png"/>
                                </div>
                            </div>
                         <h3>8: Paste you skype application Id and secret on <a href={config.Frontend+'/chat/configure'} target={"_blank"}>this</a> page and press the connect button</h3>
                            <br/>
                            <br/>
                    </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default ManualConfigureSkype;
