import React, { Component}  from 'react';
import jQuery from 'jquery';
import ManualTop from "../manual/manual_top";
import Loading from '../../../Loading.js';
var config = require('../../../config.js');

class ManualConfigure extends Loading {
    constructor(props) {
        super(props);
        this.state = {
            Group: this.props.Group,
        };
    }

    render(){
        /*
        <li className="article_item">
                                   <a href="/manual/configure/facebook">
                                       <div className="article_item_content">
                                   <h3>Connect from Facebook</h3>
                                   <p>Learn how to quickly create a bot for Facebook</p>
                                       </div>
                                   </a>
                               </li>
         */

        return (
            <div className="wrap_manual">
               <ManualTop/>
               <div className="container">
                   <div className="row">

                       <ol class="breadcrumb  mt_25">
                           <li><a href="/manual/">Manual</a></li>
                           <li><a href="/manual/configure">Configure</a></li>
                       </ol>

                       <div className="article_main">
                           <ul className="article_list">

                               <li className="article_item">
                                   <a href="/manual/configure/skype">
                                       <div className="article_item_content">
                                           <h3>Connect from Skype</h3>
                                           <p>Learn how to quickly create a bot for Skype</p>
                                       </div>
                                   </a>
                               </li>
                               <li className="article_item">
                                   <a href="/manual/configure/slack">
                                       <div className="article_item_content">
                                           <h3>Connect from Slack</h3>
                                           <p>
                                               Learn how to quickly create a bot for Slack</p>
                                       </div>
                                   </a>
                               </li>
                               <li className="article_item">
                                   <a href="/manual/configure/site">
                                       <div className="article_item_content">
                                           <h3>Connect with web chat</h3>
                                           <p>Learn how to quickly create a bot for standalone site</p>
                                       </div>
                                   </a>
                               </li>
                           </ul>
                       </div>

                   </div>
               </div>
           </div>
        )
    }
}
export default ManualConfigure;
