import React, { Component}  from 'react';
import jQuery from 'jquery';
import Loading from '../../../Loading.js';
var config = require('../../../config.js');

class ManualTop extends Loading {
    constructor(props) {
        super(props);
        this.state = {
            Group: this.props.Group,
        };
    }

    render(){
        return (
            <div className="manual">
                <div className="container">
                    <div className="row">
                        <div className="header_manual">
                            <div className="header_left">
                                <h1><a href="/manual">ChatBot Manual</a></h1>
                            </div>
                            <div className="header_right">
                                <a href="/chat/build">Go to Chatbot</a>
                            </div>
                        </div>
                        <p>Tips and answers from our team</p>
                    </div>
                </div>
            </div>
        )
    }
}

export default ManualTop;
