import React, { Component}  from 'react';
import jQuery from 'jquery';
import ManualTop from "./manual_top";
import Loading from '../../../Loading.js';
const ReactMarkdown = require('react-markdown')
var config = require('../../../config.js');


class CheckApi extends Loading {
    constructor(props) {
        super(props);
        this.state = {
            Group: this.props.Group,
        };
    }

    render(){
        return (
            <div className="wrap_manual">
                <ManualTop/>
                <div className="container">
                    <div className="row">

                        <ol class="breadcrumb  mt_25">
                            <li><a href="/manual/">Manual</a></li>
                            <li><a href="/manual/build">Build</a></li>
                            <li><a href="/manual/build/check-api">Api call status</a></li>
                        </ol>

                        <div className="entry_content mt_25">
                            <p>There are two Context parameters for check api call status:</p>
                            <p>Context.APIStatus.code: contain http status code of api call</p>
                            <p>Context.APIStatus.description: contain http status code description of api call</p>
                            <div class="manual_image">
                                <div class="manual_img border_img">
                                    <img src="/img/Manual/all/1.png"/>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        )
    }
}
export default CheckApi;
