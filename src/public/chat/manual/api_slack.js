import React, { Component}  from 'react';
import jQuery from 'jquery';
import ManualTop from "./manual_top";
import Loading from '../../../Loading.js';
const ReactMarkdown = require('react-markdown')
var config = require('../../../config.js');


class ManualApiSlack extends Loading {
    constructor(props) {
        super(props);
        this.state = {
            Group: this.props.Group,
        };
    }

    render(){
var exam = '\t{\n' +
    '    "attachments": [\n' +
    '        {\n' +
    '            "fallback": "213",\n' +
    '            "title": "Network traffic (kb/s)",\n' +
    '            "text": "<Result.Text>",\n' +
    '            "image_url": "<Images[0]>",\n' +
    '            "color": "#764FA5"\n' +
    '        }\n' +
    '    ]\n' +
    '\t}';

        var res  = '\tres.json({\n' +
            '            \tResult: {Text: "Some text"},\n' +
            '            \tImages: ["https://back.i-solutions.org/content/getFile?&CurrentUserId=5a29c38d2ed2852067505afe&Link=uploads/5hcx3a8hsbhq-1512758220062.png"],\n' +
            '            \tVideo: "https://back.i-solutions.org/content/getFile?&CurrentUserId=5a29c38d2ed2852067505afe&Link=uploads/fxcnddn7mkz2-1512757701334.webm"\n' +
            '            \t,\n' +
            '            \tAudio: "https://back.i-solutions.org/content/getFile?&CurrentUserId=5a29c38d2ed2852067505afe&Link=uploads/ynbvfw21185t-1512757663654.mp3"\n' +
            '        \t});'

        return (
            <div className="wrap_manual">
                <ManualTop/>
                <div className="container">
                    <div className="row">

                        <ol class="breadcrumb  mt_25">
                            <li><a href="/manual/">Manual</a></li>
                            <li><a href="/manual/build">Build</a></li>
                            <li><a href="/manual/build/api-slack">Slack Macros</a></li>
                        </ol>

                        <div className="entry_content mt_25">
                            <p>Samples here</p>
                            <h4>
                                <a href={'https://api.slack.com/docs/message-attachments'}>
                                https://api.slack.com/docs/message-attachments</a>
                            </h4>
                            <p>Test message:</p>
                            <p><a href={'https://api.slack.com/docs/messages/builder?msg='}>
                                https://api.slack.com/docs/messages/builder?msg=</a></p>
                            <h3>Example</h3>
                            <div className="entry_content_coding">
                                <div><ReactMarkdown source={exam} /></div>
                                <p>for a test, you can do a post and get request on url</p>
                                <p><a href={config.Backend+'/skype/test?'}>{config.Backend+'/skype/test?'}</a></p>
                                <h3>it returns the following answer</h3>
                                <br/>
                                <br/>
                                <div><ReactMarkdown source={res} /></div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        )
    }
}
export default ManualApiSlack;
