import React, { Component}  from 'react';
import jQuery from 'jquery';
import ManualTop from "./manual_top";
import Loading from '../../../Loading.js';
var config = require('../../../config.js');


class ManualConfigureTelegram extends Loading {
    constructor(props) {
        super(props);
        this.state = {
            Group: this.props.Group,
        };
    }

    render(){

        return (
            <div className="wrap_manual">
                <ManualTop/>
                <div className="container">
                    <div className="row">

                        <ol class="breadcrumb  mt_25">
                            <li><a href="/manual/">Manual</a></li>
                            <li><a href="/manual/configure">Configure</a></li>
                            <li><a href="/manual/configure/telegram">Telegram</a></li>
                        </ol>

                        <div className="entry_content mt_25">
                            <h3>1. Создайте бота Telegram с помощью диалога с <a href={"https://telegram.me/botfather"}
                            target={"_blank"}>BotFather</a></h3>
                            <h3>2. Скопируйте токен доступа бота и вставте его в форму на нашем сайте</h3>

                            <div className="manual_image">
                                <div class="manual_img border_img">
                                    <img src="/img/Manual/telegram/1.png"/>
                                </div>
                            </div>
                            <br/>
                            <br/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default ManualConfigureTelegram;
