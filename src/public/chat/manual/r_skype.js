import React, { Component}  from 'react';
import ManualTop from "./manual_top";
import Loading from '../../../Loading.js';
var config = require('../../../config.js');

class ManualFb extends Loading {
    constructor(props) {
        super(props);
        this.state = {
            Group: this.props.Group,
        };
    }

    render(){
        return (
            <div className="wrap_manual">
                <ManualTop/>
                <div className="container">
                    <div className="row">

                        <ol class="breadcrumb  mt_25">
                            <li><a href="/manual/">Manual</a></li>
                            <li><a href="/manual/restrictions">Restrictions</a></li>
                            <li><a href="/manual/restrictions/skype">Skype</a></li>
                        </ol>

                        <div className="entry_content mt_25">
                            <h3>1: Buttons limit</h3>
                            <p>Your messages must contain no more than 3 buttons</p>
                            <h3>2: Media content</h3>
                            <p>Some platforms and Skype versions may not support audio and video,
                                see details in Skype documentation</p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default ManualFb;
