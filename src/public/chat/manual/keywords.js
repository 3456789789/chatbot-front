import React, { Component}  from 'react';
import jQuery from 'jquery';
import ManualTop from "./manual_top";
import Loading from '../../../Loading.js';
const ReactMarkdown = require('react-markdown')
var config = require('../../../config.js');


class ManualKeywrods extends Loading {
    constructor(props) {
        super(props);
        this.state = {
            Group: this.props.Group,
        };
    }

    render(){
        return (
            <div className="wrap_manual">
                <ManualTop/>
                <div className="container">
                    <div className="row">

                        <ol class="breadcrumb  mt_25">
                            <li><a href="/manual/">Manual</a></li>
                            <li><a href="/manual/build">Build</a></li>
                            <li><a href="/manual/build/keywords">Keywords</a></li>
                        </ol>

                        <div className="entry_content mt_25">
                            <p>You can put keywords from this list in your message</p>
                            <p>Facebook:</p>
                            <p>{"{{first_name}}"}</p>
                            <p>{"{{last_name}}"}</p>
                            <p>{"{{gender}}"}</p>
                            <p>{"{{locale}}"}</p>
                            <p>Skype:</p>
                            <p>{"{{name}}"}</p>
                            <p>{"{{country}}"}</p>
                            <p>{"{{platform}}"}</p>
                            <p>{"{{locale}}"}</p>
                            <p>Slack:</p>
                            <p>{"{{name}}"}</p>
                        </div>

                    </div>
                </div>
            </div>
        )
    }
}
export default ManualKeywrods;
