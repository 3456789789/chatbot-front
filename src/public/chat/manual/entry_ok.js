import React, { Component}  from 'react';
import jQuery from 'jquery';
import ManualTop from "./manual_top";
import Loading from '../../../Loading.js';
var config = require('../../../config.js');


class ManualConfigureOk extends Loading {
    constructor(props) {
        super(props);
        this.state = {
            Group: this.props.Group,
        };
    }

    render(){
        return (
            <div className="wrap_manual">
                <ManualTop/>
                <div className="container">
                    <div className="row">

                        <ol class="breadcrumb  mt_25">
                            <li><a href="/manual/">Manual</a></li>
                            <li><a href="/manual/configure">Configure</a></li>
                            <li><a href="/manual/configure/telegram">Ok</a></li>
                        </ol>

                        <div className="entry_content mt_25">
                            <h3>1. Получите ключ доступа сообщества следуя <a href={"https://apiok.ru/dev/graph_api/bot_api"}
                                                                                 target={"_blank"}>этой</a> инструкции</h3>
                            <h3>2. Скопируйте ключ доступа сообщества и вставте его в форму на нашем сайте</h3>
                            <br/>
                            <br/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default ManualConfigureOk;
