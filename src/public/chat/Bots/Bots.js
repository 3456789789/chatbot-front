import React, { Component}  from 'react';
import jQuery from 'jquery';
import Cookies from 'universal-cookie';
import Popup from "../../Popup";
import Animate from 'react-smooth';
import ReactTooltip from 'react-tooltip'
import { Nav, NavItem, Dropdown, DropdownItem, DropdownToggle, DropdownMenu, NavLink } from 'reactstrap';
import { Scrollbars } from 'react-custom-scrollbars';
import Loading from '../../../Loading.js';
import { Resizable, ResizableBox } from 'react-resizable';

const cookies = new Cookies();
var config = require('../../../config.js');

class Link extends React.Component {
    state = {
        open: false
    }
    handleClick = () => {
        this.setState({ open: !this.state.open });
    }

    exit(event) {
        cookies.set('Exit',"true", { path: '/' });
        window.location.replace(window.location.origin + '/login');
    }

    render () {
        var s =this.state;
        const { open } = this.state;
        return (
            <div className="link">
                <span onClick={this.handleClick}>
                    <div className={`img_picture_user ${open ? 'borderR2' : ''}`}>
                                            <img src={this.props.icon || "/img/profile/images.png"} alt=""/>
                                        </div></span>
                <div className={`menu ${open ? 'open' : ''}`}>
                    <ul className={'list_top_menu'}>
                        <li><a href="/profile/setting"><span class="ti-settings mr_2"></span>Setting</a></li>
                        <li><a target={'_blank'} href="/manual"><span className={'ti-help-alt mr_2'}></span>Help</a></li>
                        <li><a href={'#'} onClick={this.exit}><span className={'ti-power-off mr_2'}></span>Logout</a></li>
                    </ul>
                </div>
            </div>
        )
    }
}
class Bots extends Loading {
    constructor(props) {
        super(props);
        this.state = {
            User:{},
            Bots:[],
            DftBots:[],
            ShowRenamePopup: false,
            ShowDeletePopup: false,
            ShowSharePopup: false,
            BotId:"",
            ShareEmail: "",
            dropdownOpen: false,
            userOpen: false,
            height: null,
            Wait: 0
        };

        this.renameBot = this.renameBot.bind(this);
        this.deleteBot= this.deleteBot.bind(this);
        this.editBot= this.editBot.bind(this);
        this.addBot = this.addBot.bind(this);
        this.wantDeleteBot = this.wantDeleteBot.bind(this);
        this.getBotName = this.getBotName.bind(this);
        this.toggle = this.toggle.bind(this);
        this.addTemplateBot = this.addTemplateBot.bind(this);
        this.repair = this.repair.bind(this);
        this.share = this.share.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.addIcon = this.addIcon.bind(this);
        this.clearInput = this.clearInput.bind(this);
        this.UserToggle = this.UserToggle.bind(this);
    }

    UserToggle() {
        this.setState({
            userOpen: !this.state.userOpen
        });
    }



    addIcon(e,bot,i) {
        e.preventDefault();
        let reader = new FileReader();
        let file = e.target.files[0];
        reader.onloadend = () => {
            var url = reader.result;
            if(file) {
                var fd = new FormData;
                fd.append('File', file);
                this.setState({Wait:++this.state.Wait});
                jQuery.ajax({
                    method: "POST",
                    processData: false,
                    contentType: false,
                    url: config.Backend + "/content/file",
                    success: function (msg) {
                        config.getData("bot/setup_icon",{Icon:msg,Bot:bot._id}).then(function (ok) {
                            this.state.Bots[i].Icon = msg;
                            this.setState({Wait:--this.state.Wait,Bots:this.state.Bots})
                        }.bind(this),function (err) {
                            this.setState({Wait:--this.state.Wait})
                            alert(JSON.stringify(err))
                        }.bind(this))

                    }.bind(this),
                    data: fd,
                })
            }
        }
        reader.readAsDataURL(file)
    }
    clearInput(e) {
        e.target.value = null;
    }
    share()
    {
        var s= this.state;
        this.setState({Wait:++this.state.Wait})
        jQuery.ajax({
            method: "POST",
            contentType: 'application/json',
            url: config.Backend + '/share/add',
            success: function (res) {
                if (!res.Error) {
                    window.location.href = (window.location.origin + '/chat/share');
                    this.setState({Wait:--this.state.Wait})
                    //alert(JSON.stringify(res));
                } else {
                    this.setState({Wait:--this.state.Wait})
                    alert(JSON.stringify(res));
                }
            }.bind(this),
            data: JSON.stringify({Email:this.state.ShareEmail,
                               BotId: this.state.BotId,
            Rights:"RTC"}),
        })
    }
    handleInputChange(event) {
        this.setState({
            [event.target.name]: event.target.value.trim()
        })
    }
    getBotName() {
        for(var i=0;i<this.state.Bots.length;i++)
        {
            if(this.state.Bots[i]._id==this.state.BotId)
                return this.state.Bots[i].Name;
        }
        return "-";
    }





    toggle(node) {
        this.setState({
            dropdownOpen: !this.state.dropdownOpen
        },()=>{
            var url = config.Backend + '/bot/dftBots?';
            if (!(cookies.get('Id') === undefined))
            {
                url += 'CurrentUserId=' + cookies.get('Id').toString();
            }
            var s= this.state;
            this.setState({Wait:++this.state.Wait})
            jQuery.getJSON(url, function (res) {
                if (!res.Error) {
                    //alert(JSON.stringify(res));
                    if(res.Data.length>4 && !this.state.dropdownOpen)
                    {
                        this.setState({DftBots: res.Data.splice(0,4),Wait:--this.state.Wait})
                    }
                    else {
                        this.setState({DftBots: res.Data,Wait:--this.state.Wait})
                    }
                } else {
                    this.setState({Wait:--this.state.Wait})
                    alert(JSON.stringify(res));
                }
            }.bind(this));
        });
            if(node && !this.state.height){
              var grid =   this.setState({
                    height: node.offsetHeight
                });
              console.log(grid)
            }
    }

    addBot() {
        var url = config.Backend +'/bot/add?';
        if (!(cookies.get('Id') === undefined))
        {
            url += 'CurrentUserId=' + cookies.get('Id').toString();
        }
        var g = this.state.Bots;
            var names = [];
            for(var i=0;i<g.length;i++)
            {
                names.push(g[i].Name);
            }
            var newName = "Bot";
            var x = 1;
            do
            {
                newName ="Bot" + (x++);
            }
            while(names.indexOf(newName)!=-1);
        url+='&Name='+newName;
        url+='&AiPos=50*50';
        var s= this.state;
        this.setState({Wait:++this.state.Wait})
        jQuery.getJSON(url, function (res) {
            if (!res.Error) {
                this.state.Bots.push(res.Bot);
                this.setState({Bots:this.state.Bots,Wait:--this.state.Wait});
            } else {
                this.setState({Wait:--this.state.Wait})
                alert(JSON.stringify(res));
            }
        }.bind(this));
    }
    editBot(b) {
        cookies.set('BotId', b._id, {path: '/'});
        if(b.User==cookies.get('Id').toString())
        {
            window.location.href = window.location.origin + '/chat/build';
        }
        else {
            var r = config.firstRight(cookies.get('Id').toString(), b);
            if (r) {
                window.location.href = window.location.origin + '/chat/' + r.toLowerCase();
            }
            else {
                window.location.href = window.location.origin + '/profile/setting';
            }
        }
    }
    renameBot(id,name){
        var url = config.Backend +'/bot/edit?';
        if (!(cookies.get('Id') === undefined))
        {
            url += 'CurrentUserId=' + cookies.get('Id').toString();
        }
        url+='&Id='+this.state.BotId;
        url+='&Name='+name;
        var s= this.state;
        this.setState({Wait:++this.state.Wait})
        jQuery.getJSON(url, function (res) {
            if (!res.Error) {
                for(var i=0;i<this.state.Bots.length;i++)
                {
                    if(this.state.BotId==this.state.Bots[i]._id && name!="")
                        this.state.Bots[i].Name=name;
                }
                this.setState({Bots:this.state.Bots,ShowRenamePopup:false,Wait:--this.state.Wait});
            } else {
                this.setState({Wait:--this.state.Wait})
                alert(JSON.stringify(res));
            }
        }.bind(this));
    }
    deleteBot(){
        var url = config.Backend +'/bot/delete?';
        if (!(cookies.get('Id') === undefined))
        {
            url += 'CurrentUserId=' + cookies.get('Id').toString();
        }
        url+='&Id='+this.state.BotId;
        var s= this.state;
        this.setState({Wait:++this.state.Wait})
        jQuery.getJSON(url, function (res) {
            if (!res.Error) {
                for(var i=0;i<this.state.Bots.length;i++)
                {
                    if(this.state.BotId==this.state.Bots[i]._id)
                        this.state.Bots.splice(i,1);
                }
                this.setState({Bots:this.state.Bots,ShowDeletePopup:false,Wait:--this.state.Wait});
            } else {
                this.setState({Wait:--this.state.Wait})
                alert(JSON.stringify(res));
            }
        }.bind(this));
    }
    wantDeleteBot(id) {
        this.setState({BotId:id,ShowDeletePopup:true})
    }
    componentWillMount() {
        this.preload();
        var url = config.Backend +'/users/findById?';
        if (!(cookies.get('Id') === undefined))
        {
            url += 'Id=' + cookies.get('Id').toString();
        }
        var s= this.state;
        this.setState({Wait:++this.state.Wait})
        jQuery.getJSON(url, function (res) {
            if (!res.Error) {
                if(res.Type=="Super")
                    window.location.replace(window.location.origin + '/admin/main');

                this.setState({User: res,Wait:--this.state.Wait},()=>{
                    var url = config.Backend + '/bot/dftBots?';
                    if (!(cookies.get('Id') === undefined))
                    {
                        url += 'CurrentUserId=' + cookies.get('Id').toString();
                    }
                    this.setState({Wait:++this.state.Wait})
                    jQuery.getJSON(url, function (res) {
                        if (!res.Error) {
                            //alert(JSON.stringify(res));
                            if(res.Data.length>4)
                            {
                                this.setState({DftBots: res.Data.splice(0,4),Wait:--this.state.Wait})
                            }
                            else {
                                this.setState({DftBots: res.Data,Wait:--this.state.Wait})
                            }
                        } else {
                            this.setState({Wait:--this.state.Wait})
                            alert(JSON.stringify(res));
                        }
                    }.bind(this));

                    url = config.Backend + '/users/showBots?';
                    if (!(cookies.get('Id') === undefined))
                    {
                        url += 'Id=' + cookies.get('Id').toString();
                    }
                    url+="&NotTemplates=1";
                    this.setState({Wait:++this.state.Wait})
                    jQuery.getJSON(url, function (res) {
                        if (!res.Error) {
                            this.setState({Bots: res,Wait:--this.state.Wait});
                        } else {
                            this.setState({Wait:--this.state.Wait})
                            alert(JSON.stringify(res));
                        }
                    }.bind(this));
                });
            } else {
                window.location.replace(window.location.origin + '/login');
            }
        }.bind(this));
    }

    repair(id) {
        var s =this.state;
        var url = config.Backend +'/bot/repairBot?';
        if (!(cookies.get('Id') === undefined))
        {
            url += 'CurrentUserId=' + cookies.get('Id').toString();
        }
        url+='&Id='+id;
        jQuery.getJSON(url, function (res) {
            if (!res.Error) {
                this.state.Bots.push(res.Bot);
                this.setState({Bots:this.state.Bots,Wait:--this.state.Wait});
            } else {
                this.setState({Wait:--this.state.Wait})
                alert(JSON.stringify(res));
            }
        }.bind(this));
    }

    addTemplateBot(id) {
        var s =this.state;
        var url = config.Backend +'/bot/copyBot?';
        if (!(cookies.get('Id') === undefined))
        {
            url += 'CurrentUserId=' + cookies.get('Id').toString();
        }
        url+='&Id='+id;
        this.setState({Wait:++this.state.Wait})
        jQuery.getJSON(url, function (res) {
            if (!res.Error) {
                setTimeout(()=>this.repair(res.Bot._id),1500)
                //this.setState({Bots:this.state.Bots});
            } else {
                this.setState({Wait:--this.state.Wait})
                alert(JSON.stringify(res));
            }
        }.bind(this));

    }
    exit(event) {
        cookies.set('Exit',"true", { path: '/' });
        window.location.replace(window.location.origin + '/login');
    }


    topBots(){
        var s =this.state;
        var bots = this.state.DftBots.map(function (b) {
            var src = config.Backend + '/content/getFile?';
            if (!(cookies.get('Id') === undefined)) {
                src += '&CurrentUserId=' + cookies.get('Id').toString();
            }
            src += '&Link=' + b.Icon;

            return <div className="col-sm-3 _bot_client">
                    <div className="dashboard-bot">
                        <div className="dashboard-bot_image">
                            <div className="admin_bot_image">
                                <img src={src} alt={""}/>
                            </div>
                        </div>
                        <div className="admin_bot_settings">
                            <a href="#" onClick={e=>{e.preventDefault();this.addTemplateBot(b._id)}}><span className="dashboard_title">{b.Name}</span></a>
                        </div>
                        <div  onClick={()=>this.addTemplateBot(b._id)} className="span_ov">
                            <span className="span_ov_icon"></span>
                            <span className="span_ov_add"><span>Create bot from templates</span></span>
                        </div>
                    </div>
            </div>
        }.bind(this))

        const steps = [{
            style: {
                height: 0,
            },
            duration: 200,
        }, {
            style: {
                height: 200,
                transform: 'translate(0, 0)',
            },
            duration: 600,
        }];
        //ref={(node) => this.calcHeight(node)} style={{height: this.state.height}}
        var icon = ""
        if(s.User && s.User.Icon)
        {
            if(s.User.Icon.indexOf("uploads")!=-1) {
                icon = config.Backend + '/content/getFile?';
                if (!(cookies.get('Id') === undefined)) {
                    icon += '&CurrentUserId=' + cookies.get('Id').toString();
                }
                icon += '&Link=' + s.User.Icon;
            }
            else {
                icon = s.User.Icon;
            }
        }



        var logo = "";
        if(s.Preload && s.Preload['Logo'])
        {
            if(s.Preload['Logo'].indexOf("uploads")!=-1) {
                logo = config.Backend + '/content/getFile?';
                logo += '&Link=' +  s.Preload['Logo'];
            }
            else {
                logo = s.Preload['Logo'];
            }
        }

        return(
                <div className="top_dasboard">
                    <div className="dashboard_top_menu">
                        <div className="dashboard-left">
                            <div className="dashboard-logo">
                                <a href="/dashboard"><img src={logo} alt=""/></a>
                            </div>
                            <div>
                                {s.Preload && s.Preload['Title'] ? s.Preload['Title'] : null}
                            </div>
                        </div>
                        <div className="dashboard-right">
                            <Link icon={icon} />
                        </div>
                    </div>
            <Animate steps={steps}>
            <div  className={this.state.dropdownOpen ? "top_bots extend": "top_bots"}>
                 <span className="icon_top_position" onClick={() => this.toggle()}>
                     <span  className={!this.state.dropdownOpen ? "class_list_icon " : "class_list_icon toggled"}></span>
                 </span>
                    <div className="client_bot_wrap">
                        <div className="client_list_admin">
                            {bots}
                        </div>
                    </div>
            </div>
            </Animate>
                </div>
        )
    }

// <span onClick={() => this.toggle()} className={this.state.dropdownOpen ? "class_list_add toggled" : "class_list_add"}>
//     calcHeight(node){
//         if(node && !this.state.height){
//             this.setState({
//                 height: node.offsetHeight
//             });
//         }
//     }

    sharePopup()
    {
        return ( <div>
            <div className='filterPopup'>
                <div className='filter_delete'>
                    <h1>{"Share this bot"}</h1>
                    <input className="chat_input mb10" type="text" value={this.state.ShareEmail}
                           name="ShareEmail" placeholder={'Input user email for share...'}
                           onChange={this.handleInputChange}/>
                    <button className={ "bg_shadow button"}
                            onClick={this.share}>
                        Ok
                    </button>
                    <button className="grey button" onClick={()=>{this.setState({ShowSharePopup:false})}}> {"Cancel"}</button>
                </div>
            </div>
        </div>);
    }


    render() {
        var s =this.state;
        const bots = this.state.Bots.map(function(bot,i) {
            var showShare = config.checkRights(cookies.get('Id').toString(),bot,"Share")
            var src = config.Backend + '/content/getFile?';
            if (!(cookies.get('Id') === undefined)) {
                src += '&CurrentUserId=' + cookies.get('Id').toString();
            }
            src += '&Link=' + bot.Icon;

            /*
            return <div className="col-sm-3 _bot_client">
                <a href="#">
                    <div className="client_bot_admin">
                        <div className="admin_bot_image">
                            <img src={src} alt={""}/>
                        </div>*/
            return (
                <div className="col-sm-3 list__item">
                    <div className="dashboard-bot">
                        <div className="dashboard-bot_image">
                            {bot.Icon ? <div className="admin_bot_image">

                                <img src={src} alt={""}/>
                            </div> :
                            <div className="dashboard-bot_image_1"></div> }
                        </div>
                        <div className="dashboard-bot-settings">
                            <a onClick={() => this.editBot(bot)}>
                                <span className="dashboard_title">{bot.Name}</span>
                            </a>



                            <div className="dashboard_btn_group">
                                {bot.User==cookies.get('Id').toString() ?
                                <button className="edit"
                                        data-tip data-for='edit_bot'
                                        onClick={() => this.setState({BotId: bot._id, ShowRenamePopup: true},()=>{
                                            document.getElementById("PopupInput").focus();
                                        })}></button> : null}
                                <button className="copy"
                                        data-tip data-for='copy_bot'
                                        onClick={() => this.addTemplateBot(bot._id)}></button>

                                <ReactTooltip delayShow={100}  id='edit_bot' aria-haspopup='true' role='example'>
                                    <p>Change name</p>
                                </ReactTooltip>

                                <ReactTooltip delayShow={100}  id='copy_bot' aria-haspopup='true' role='example'>
                                    <p>Copy bot</p>
                                </ReactTooltip>

                                <ReactTooltip delayShow={100} id='delete_bot' aria-haspopup='true' role='example'>
                                <p>Delete bot</p>
                                </ReactTooltip>

                                <ReactTooltip delayShow={100}vv id='share_access' aria-haspopup='true' role='example'>
                                    <p>Share access</p>
                                </ReactTooltip>



                                {bot.User==cookies.get('Id').toString() ?

                                    <button className="delete" data-tip data-for='delete_bot' onClick={() => this.wantDeleteBot(bot._id)}></button> : null}
                                {showShare ?<button data-tip data-for='share_access' className="share" onClick={() => this.setState({ShowSharePopup: true,BotId:bot._id})}>
                                </button> : null}
                                <div className="form-group inputDnD">
                                    <input type="file" accept="image/*" onChange={(e)=>this.addIcon(e,bot,i)} onClick={this.clearInput}   className="add_image_avatar_bot text-primary "  />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>)
        }.bind(this))

        return (
            <div>
                {this.Load()}
                <div className="dashboard_main">
                    <Scrollbars
                        style={{ height: "100%" }}
                        autoHide
                        autoHideTimeout={1000}
                        autoHideDuration={200}
                    >
                {this.topBots()}
                <div className="dashboard_list_bot">
                    <div className="client_bot_wrap">
                        <div className="row">
                            <div className="dashboard_add_button">
                                <div className="col-sm-12 mt_25">
                                        <button className="btn_all bg_shadow" onClick={this.addBot}>Add default bot</button>
                                </div>
                            </div>
                            <div className="list_bots_dashboard col-sm-12 row">
                                {bots}
                            </div>
                        </div>
                    </div>
                </div>


                {this.state.ShowDeletePopup ?
                    <Popup text={"Are you sure?"} Yes={this.deleteBot}  isDashboard={true}
                           No={()=>{this.setState({ShowDeletePopup:false})}}/> : null}
                {this.state.ShowRenamePopup ?

                    <Popup text={"Enter new name"}  Bot = {this.state.Bot} Group = "-"
                           RenameGroup={true} Rename={this.renameBot} isDashboard={true}
                           Name={this.getBotName()}   No={()=>{this.setState({ShowRenamePopup:false})}}/> : null}
                        {this.state.ShowSharePopup ? this.sharePopup() : null}
                {/*<LeftPanel isDashboard={true} User={this.state.User}/>*/}
                    </Scrollbars>
                </div>
            </div>
        )
    }

}
export default Bots;