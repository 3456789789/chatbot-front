import React, { Component}  from 'react';
import jQuery from 'jquery';
import Cookies from 'universal-cookie';
import Popup from "../../Popup";
import Animate from 'react-smooth';
import { Scrollbars } from 'react-custom-scrollbars';


class BotsDescription extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render(){
        return(
            <div>
                <div className='filterPopup'>
                    <div className='filter_delete'>
                        <form>
                            <h1>Bots name</h1>
                            <div className="content_bots">
                                <p>Group <span>name</span></p>
                                <p>channel <span>name</span></p>
                                <p>Status <span>Off/On</span></p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            )
    }
}

export default BotsDescription;