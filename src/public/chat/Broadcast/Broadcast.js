import React, { Component}  from 'react';
import jQuery from 'jquery';
import Cookies from 'universal-cookie';
import LeftPanel from "../../LeftPanel";
import TopPanel from "../../TopPanel";
import Switch from 'react-toggle-switch';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import Content from "../Build/Content";
import Popup from "../../Popup";
import DatePicker from 'react-date-picker';
import ReactTooltip from 'react-tooltip'
import { Scrollbars } from 'react-custom-scrollbars';
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import Loading from '../../../Loading.js';

const cookies = new Cookies();
var config = require('../../../config.js');

// deliver your message now  center panel
class Deliver extends Component {
    constructor(props) {
        super(props);
        this.state = {
            None: {},
            switched: false,
            User: this.props.User,
            Bot: this.props.Bot,
            Trigger: 0,
            TextContents: [],
            AudioContents: [],
            VideoContents: [],
            ImageContents: [],
            BtnContents: [],
            ApiContents: [],
            Groups: [],
            BotMsg: this.props.BotMsg,
            RepeatCustom: false,
            RepeatDays: [true,true,true,true,true,true,true],
            DefLink: [],
            Wait: 0
        }
        this.loadGroups = this.loadGroups.bind(this);
        this.loadContent = this.loadContent.bind(this);
        this.handleForContent = this.handleForContent.bind(this);
        this.addAudioContent = this.addAudioContent.bind(this);
        this.addVideoContent = this.addVideoContent.bind(this);
        this.addImageContent = this.addImageContent.bind(this);
        this.addApiContent = this.addApiContent.bind(this);
        this.addBtnContent = this.addBtnContent.bind(this);
        this.saveContents = this.saveContents.bind(this);
        this.deleteContent = this.deleteContent.bind(this);
        this.deleteMsg = this.deleteMsg.bind(this);
        this.send = this.send.bind(this);
        this.changeTrigger = this.changeTrigger.bind(this);
        this.changeTriggerCond = this.changeTriggerCond.bind(this);
        this.changeTriggerVal = this.changeTriggerVal.bind(this);
        this.changeTriggerAttr = this.changeTriggerAttr.bind(this);
        this.changeTime = this.changeTime.bind(this);
        this.changeTimeType = this.changeTimeType.bind(this);

        this.cleanAndSaveFilters = this.cleanAndSaveFilters.bind(this);
        this.saveFilters = this.saveFilters.bind(this);
        this.changeRepeat = this.changeRepeat.bind(this);
        this.changeTimezone = this.changeTimezone.bind(this);
        this.changeDate = this.changeDate.bind(this);
        this.changeRepeatDays = this.changeRepeatDays.bind(this);
        this.changeWaitAnswer = this.changeWaitAnswer.bind(this);
        this.changeAnswerName = this.changeAnswerName.bind(this);
    }
    changeAnswerName(e) {
                this.state.BotMsg.AnswerName = e.target.value;
                this.setState({BotMsg:this.state.BotMsg});
    }
    changeWaitAnswer(e) {
                this.state.BotMsg.WaitAnswer = e.target.checked;
                this.setState({BotMsg:this.state.BotMsg});
    }
    changeDate(e){
        this.state.BotMsg.Date = new Date(e).toISOString();
        this.setState({BotMsg:this.state.BotMsg});
        this.props.ChangeSave(false);
    }
    changeTimezone(e){
        this.state.BotMsg.Timezone = e.target.value;
        this.setState({BotMsg:this.state.BotMsg});
        this.props.ChangeSave(false);
    }
    changeRepeatDays(i)
    {
        this.state.RepeatDays[i] = !this.state.RepeatDays[i];
        this.setState({RepeatDays:this.state.RepeatDays})
        this.props.ChangeSave(false);
        var str = "";
        for(var i=0;i<this.state.RepeatDays.length;i++)
        {
            if(this.state.RepeatDays[i]==true)
                str+= i+";";
        }
        this.state.BotMsg.Days = str;
        this.setState({BotMsg:this.state.BotMsg});
    }
    changeRepeat(e) {
        var val = e.target.value;
        if(val!="Custom") {
            this.state.BotMsg.Days = val;
            this.setState({RepeatCustom:false})
        } else {
            var str = "";
            for(var i=0;i<this.state.RepeatDays.length;i++)
            {
                if(this.state.RepeatDays[i]==true)
                    str+= i+";";
            }
            this.state.BotMsg.Days = str;
            this.setState({RepeatCustom:true})
        }
        this.setState({BotMsg:this.state.BotMsg})
        this.props.ChangeSave(false);
    }
    getRepeatType() {
        var val = this.state.BotMsg.Days;
        if((val=="None") || (val=="Workdays") || (val=="Every Day") || (val=="Every Month") || (val=="Weekends") || (val=="Workdays"))
        {
            return val;
        }
        else return "Custom";
    }
    cleanAndSaveFilters() {
        var url = config.Backend + '/botMsg/cleanFilters?';
        if (!(cookies.get('Id') === undefined))
        {
            url += '&CurrentUserId=' + cookies.get('Id').toString();
        }
        url+='&Id=' + this.state.BotMsg._id;
        this.props.addWait();
        jQuery.getJSON(url, function (res) {
            if (!res.Error) {
                this.props.delWait();
                this.saveFilters();
            } else {
                this.props.delWait();
                alert(JSON.stringify(res));
            }
        }.bind(this));
    }
    saveFilters() {
        var url1 = config.Backend + '/botMsg/edit?';
        if (!(cookies.get('Id') === undefined))
        {
            url1 += '&CurrentUserId=' + cookies.get('Id').toString();
        }
        url1+='&Id=' + this.state.BotMsg._id;
        url1+='&Filter=' + this.state.BotMsg.Filters[0].Attr;
        url1+='&Edit=true';
        url1+='&Index=0';
        url1+='&Val=' + this.state.BotMsg.Filters[0].Val;
        url1+='&Cond=' + this.state.BotMsg.Filters[0].Cond;
        url1+='&FilterEnable=' + (this.state.BotMsg.FilterEnable ? "True" : "False");
        var s=this.state;
        this.props.addWait();
        jQuery.getJSON(url1, function (res) {
            if (!res.Error) {
                this.props.delWait();
            } else {
                this.props.delWait();
                alert(JSON.stringify(res));
            }
        }.bind(this));

        for(var i=1;i<this.state.BotMsg.Filters.length;i++)
        {
            if(this.state.BotMsg.Filters[i].Val!="" && this.state.BotMsg.Filters[i].Attr!="") {
                var url = config.Backend + '/botMsg/edit?';
                if (!(cookies.get('Id') === undefined)) {
                    url += '&CurrentUserId=' + cookies.get('Id').toString();
                }
                url += '&Id=' + this.state.BotMsg._id;
                url += '&Filter=' + this.state.BotMsg.Filters[i].Attr;
                url += '&Add=true';
                url += '&Val=' + this.state.BotMsg.Filters[i].Val;
                url += '&Cond=' + this.state.BotMsg.Filters[i].Cond;
                this.props.addWait();
                jQuery.getJSON(url, function (res) {
                    if (!res.Error) {
                        this.props.delWait();
                        this.props.loadMsgs(this.state.Bot);
                    } else {
                        this.props.delWait();
                        alert(JSON.stringify(res));
                    }
                }.bind(this));
            }
        }
    }

    changeTimeType(e) {
        this.state.BotMsg.TimeType = e.target.value;
        this.setState({BotMsg:this.state.BotMsg});
        this.props.ChangeSave(false);
    }
    changeTime(e) {
        this.state.BotMsg.Time = e.target.value;
        this.setState({BotMsg:this.state.BotMsg});
        this.props.ChangeSave(false);
    }
    changeTrigger(e) {
        this.state.BotMsg.Trigger = e.target.value;
        this.setState({BotMsg:this.state.BotMsg});
        this.props.ChangeSave(false);
    }
    changeTriggerCond(e) {
        this.state.BotMsg.TriggerCond = e.target.value;
        this.setState({BotMsg:this.state.BotMsg});
        this.props.ChangeSave(false);
    }
    changeTriggerAttr(e) {
        if(e) {
            this.state.BotMsg.TriggerAttr = e.value;
        }
        else
        {
            this.state.BotMsg.TriggerAttr = "";
        }
        //this.state.BotMsg.TriggerAttr = e.target.value;
        this.setState({BotMsg:this.state.BotMsg});
        this.props.ChangeSave(false);
    }
    changeTriggerVal(e) {
        this.state.BotMsg.TriggerVal = e.target.value;
        this.setState({BotMsg:this.state.BotMsg});
        this.props.ChangeSave(false);
    }
    send() {
        if(this.state.Bot.BindTo) {
            var x = config.Backend;
            if (this.state.Bot.BindTo == "Slack") {
                x += "/Slack/sendMsg?" +
                    "&Id=" + this.state.BotMsg._id
                    + "&BotToken=" + this.state.Bot.SlackBotToken
                    + "&TeamId=" + this.state.Bot.SlackTeamId;
                if (!(cookies.get('Id') === undefined)) {
                    x += '&CurrentUserId=' + cookies.get('Id').toString();
                }
            }
            else if (this.state.Bot.BindTo == "FB") {
                x += "/sendMsg?";
                x += "PageId=" + this.state.Bot.Page
                    + "&Id=" + this.state.BotMsg._id
                    + "&PageToken=" + this.state.Bot.PageToken;
                if (!(cookies.get('Id') === undefined)) {
                    x += '&CurrentUserId=' + cookies.get('Id').toString();
                }
            }
            else if (this.state.Bot.BindTo == "Skype" || this.state.Bot.BindTo == "Standalone") {
                x += "/skype/sendMsg?";
                x += "SkypeAppId=" + this.state.Bot.SkypeId
                    + "&Id=" + this.state.BotMsg._id
                    + "&SkypeAppPass=" + this.state.Bot.SkypePass;
                if (!(cookies.get('Id') === undefined)) {
                    x += '&CurrentUserId=' + cookies.get('Id').toString();
                }
            }
            this.props.addWait();
            jQuery.getJSON(x, function (res) {
                if (!res.Error) {
                    this.props.delWait();
                    this.props.changeMain("Sent");
                }
                else {
                    this.props.delWait();
                    alert(JSON.stringify(res));
                }
            }.bind(this));
        } else {
            alert("Bot not connected!");
        }
    }
    deleteMsg(msg) {
        this.setState({ShowDeletebotMsgPopup:false});
        this.props.DeleteMsg(msg);
    }
    saveContents() {
        this.props.ChangeSave(true);
        this.cleanAndSaveFilters();
        var url = config.Backend + '/botMsg/edit?';
        if (!(cookies.get('Id') === undefined)) {
            url += '&CurrentUserId=' + cookies.get('Id').toString();
        }
        url += '&Id='+this.state.BotMsg._id;
        url += '&DefaultLink='+this.state.BotMsg.DefaultLink;
        url += '&Status='+this.state.BotMsg.Status;
        url += '&Time='+this.state.BotMsg.Time;
        url += '&TimeType='+this.state.BotMsg.TimeType;
        url += '&Trigger='+this.state.BotMsg.Trigger;
        url += '&TriggerCond='+this.state.BotMsg.TriggerCond;
        url += '&TriggerVal='+this.state.BotMsg.TriggerVal;
        url += '&TriggerAttr='+this.state.BotMsg.TriggerAttr;
        url += '&FilterType='+this.state.BotMsg.FilterType;
        url += '&Date='+this.state.BotMsg.Date;
        url += '&Days='+this.state.BotMsg.Days;
        url += '&Timezone='+this.state.BotMsg.Timezone;
        url += '&TimezoneOffset='+new Date().getTimezoneOffset();
        if(this.state.BotMsg.WaitAnswer) {
            url += '&WaitAnswer=true';
        } else {
            url += '&WaitAnswer=false';
        }
        url += '&AnswerName=' + this.state.BotMsg.AnswerName;
        this.props.addWait();
        jQuery.getJSON(url, function (res) {
            if (!res.Error) {
                this.props.delWait();
                this.props.loadMsgs(this.state.Bot);
            } else {
                this.props.delWait();
                alert(JSON.stringify(res));
            }
        }.bind(this));


/*
        url = config.Backend + '/content/edit?';
        if (!(cookies.get('Id') === undefined)) {
            url += '&CurrentUserId=' + cookies.get('Id').toString();
        }
        url += '&BotMsg='+this.state.BotMsg._id;
        url += '&Id=' +this.state.TextContents[0]._id;
        url += '&Text='+this.state.TextContents[0].Text;
        url += '&Time='+this.state.TextContents[0].Time;
        url += '&Link='+this.state.TextContents[0].Link;
        url += '&DefaultLink='+this.state.BotMsg.DefaultLink;

        jQuery.getJSON(url, function (res) {
            if (!res.Error) {
            } else {
                alert(JSON.stringify(res));
            }
        }.bind(this));*/

        var fd = {};
        fd.Id=this.state.TextContents[0]._id;
        fd.Text= this.state.TextContents[0].Text;
        fd.Type= "TextContent";
        fd.Time= this.state.TextContents[0].Time;
        fd.Link= this.state.TextContents[0].Link;
        this.props.addWait();
        jQuery.ajax({
            method: "POST",
            contentType: 'application/json',
            url: config.Backend +"/content/edit",
            success: function (res) {
                if (!res.Error) {
                    this.props.delWait();
                } else {
                    this.props.delWait();
                    alert(JSON.stringify(res));
                }
            }.bind(this),
            data: JSON.stringify(fd),
        })

        for(var ind=0;ind<this.state.ImageContents.length;ind++) {
            const i = ind;
            if(this.state.ImageContents[i].Deleted != true) {
                if (this.state.ImageContents[i]._id == "-1") {
                    this.state.ImageContents[i]._id = "1";
                    var fd = new FormData;
                    fd.append('File', this.state.ImageContents[i].File);
                    /*if (this.state.ImageContents[i].Link!="-1") {
                        jQuery.ajax({
                            async: false,
                            dataType: "json",
                            url: "http://localhost:4800/content/deleteFile?Link="+this.state.ImageContents[i].Link,
                        })
                    }*/
                    this.props.addWait();
                    jQuery.ajax({
                        method: "POST",
                        async: false,
                        processData: false,
                        contentType: false,
                        url: config.Backend + "/content/file",
                        success: function (msg) {
                            this.props.delWait();
                            this.state.ImageContents[i].Link = msg;
                        }.bind(this),
                        data: fd,
                    })
                    var url = config.Backend + '/content/add?';
                    if (!(cookies.get('Id') === undefined)) {
                        url += '&CurrentUserId=' + cookies.get('Id').toString();
                    }
                    url += '&BotMsg=' + this.state.BotMsg._id;
                    url += '&Type=' + this.state.ImageContents[i].Type;
                    url += '&Text=' + this.state.ImageContents[i].Text;
                    url += '&Time=' + this.state.ImageContents[i].Time;
                    url += '&Link=' + this.state.ImageContents[i].Link;
                    this.props.addWait();
                    jQuery.getJSON(url, function (res) {
                        if (!res.Error) {
                            this.props.delWait();
                            this.state.ImageContents[i] = res.Content;
                            this.setState({ImageContents:this.state.ImageContents});
                        } else {
                            this.state.ImageContents[i]._id = "-1";
                            this.setState({ImageContents:this.state.ImageContents});
                            this.props.delWait();
                            alert(JSON.stringify(res));
                        }
                    }.bind(this));
                }
            }
        }

        for(var ind=0;ind<this.state.AudioContents.length;ind++) {
            const i = ind;
            if(this.state.AudioContents[i].Deleted != true) {
                if (this.state.AudioContents[i]._id == "-1") {
                    this.state.AudioContents[i]._id = "1";
                    var fd = new FormData;
                    fd.append('File', this.state.AudioContents[i].File);
                    this.props.addWait();
                    jQuery.ajax({
                        method: "POST",
                        async: false,
                        processData: false,
                        contentType: false,
                        url: config.Backend + "/content/file",
                        success: function (msg) {
                            this.props.delWait();
                            this.state.AudioContents[i].Link = msg;
                        }.bind(this),
                        data: fd,
                    })
                    var url = config.Backend + '/content/add?';
                    if (!(cookies.get('Id') === undefined)) {
                        url += '&CurrentUserId=' + cookies.get('Id').toString();
                    }
                    url += '&BotMsg=' + this.state.BotMsg._id;
                    url += '&Type=' + this.state.AudioContents[i].Type;
                    url += '&Text=' + this.state.AudioContents[i].Text;
                    url += '&Time=' + this.state.AudioContents[i].Time;
                    url += '&Link=' + this.state.AudioContents[i].Link;
                    this.props.addWait();
                    jQuery.getJSON(url, function (res) {
                        if (!res.Error) {
                            this.props.delWait();
                            this.state.AudioContents[i] = res.Content;
                            this.setState({AudioContents:this.state.AudioContents});
                        } else {
                            this.props.delWait();
                            this.state.AudioContents[i]._id = "-1";
                            this.setState({AudioContents:this.state.AudioContents});
                            alert(JSON.stringify(res));
                        }
                    }.bind(this));
                }
            }
        }

        for(var ind=0;ind<this.state.VideoContents.length;ind++) {
            const i = ind;

            if(this.state.VideoContents[i].Deleted != true) {
                if (this.state.VideoContents[i]._id == "-1") {
                    this.state.VideoContents[i]._id = "1";
                    var fd = new FormData;
                    fd.append('File', this.state.VideoContents[i].File);
                    this.props.addWait();
                    jQuery.ajax({
                        method: "POST",
                        async: false,
                        processData: false,
                        contentType: false,
                        url: config.Backend + "/content/file",
                        success: function (msg) {
                            this.props.delWait();
                            this.state.VideoContents[i].Link = msg;
                        }.bind(this),
                        data: fd,
                    })
                    var url = config.Backend + '/content/add?';
                    if (!(cookies.get('Id') === undefined)) {
                        url += '&CurrentUserId=' + cookies.get('Id').toString();
                    }
                    url += '&BotMsg=' + this.state.BotMsg._id;
                    url += '&Type=' + this.state.VideoContents[i].Type;
                    url += '&Text=' + this.state.VideoContents[i].Text;
                    url += '&Time=' + this.state.VideoContents[i].Time;
                    url += '&Link=' + this.state.VideoContents[i].Link;
                    this.props.addWait();
                    jQuery.getJSON(url, function (res) {
                        if (!res.Error) {
                            this.props.delWait();
                            this.state.VideoContents[i] = res.Content;
                            this.setState({VideoContents:this.state.VideoContents});
                        } else {
                            this.props.delWait();
                            this.state.VideoContents[i]._id = "-1";
                            this.setState({VideoContents:this.state.VideoContents});
                            alert(JSON.stringify(res))
                        }
                    }.bind(this));
                }
            }
        }

        for(var ind=0;ind<this.state.ApiContents.length;ind++) {
            const i = ind;
            if(this.state.ApiContents[i].Deleted != true) {
                if (this.state.ApiContents[i]._id == "-1") {
                    this.state.ApiContents[i]._id = "1";
                    var fd = {};
                    if (!(cookies.get('Id') === undefined)) {
                        fd.CurrentUserId = cookies.get('Id').toString();
                    }
                    fd.BotMsg = this.state.BotMsg;
                    fd.Type = this.state.ApiContents[i].Type;
                    fd.ReqType = this.state.ApiContents[i].ReqType;
                    fd.Field = this.state.ApiContents[i].Field;
                    fd.Macros = this.state.ApiContents[i].Macros;
                    fd.Headers = this.state.ApiContents[i].Headers;
                    fd.Save = this.state.ApiContents[i].Save;
                    fd.Url = this.state.ApiContents[i].Url;
                    this.props.addWait();
                    jQuery.ajax({
                        method: "POST",
                        contentType: 'application/json',
                        url: config.Backend + "/content/add",
                        success: function (res) {
                            if (!res.Error) {
                                this.props.delWait();
                                this.state.ApiContents[i] = res.Content;
                                this.setState({ApiContents:this.state.ApiContents});
                            } else {
                                this.props.delWait();
                                this.state.ApiContents[i]._id = "-1";
                                this.setState({ApiContents:this.state.ApiContents});
                                alert(JSON.stringify(res));
                            }
                        }.bind(this),
                        data: JSON.stringify(fd),
                    })
                } else {
                    var fd = {};
                    if (!(cookies.get('Id') === undefined)) {
                        fd.CurrentUserId = cookies.get('Id').toString();
                    }
                    fd.BotMsg = this.state.BotMsg;
                    fd.Id = this.state.ApiContents[i]._id;
                    fd.ReqType = this.state.ApiContents[i].ReqType;
                    fd.Field = this.state.ApiContents[i].Field;
                    fd.Macros = this.state.ApiContents[i].Macros;
                    fd.Headers = this.state.ApiContents[i].Headers;
                    fd.Save = this.state.ApiContents[i].Save;
                    fd.Url = this.state.ApiContents[i].Url;
                    this.props.addWait();
                    jQuery.ajax({
                        method: "POST",
                        contentType: 'application/json',
                        url: config.Backend + "/content/edit",
                        success: function (res) {
                            if (!res.Error) {
                                this.props.delWait();
                                // alert(JSON.stringify(res));
                            } else {
                                this.props.delWait();
                                alert(JSON.stringify(res));
                            }
                        }.bind(this),
                        data: JSON.stringify(fd),
                    })
                }
            }
        }

        for(var ind=0;ind<this.state.BtnContents.length;ind++) {
            const i = ind;
            if(this.state.BtnContents[i].Deleted != true) {
                if (this.state.BtnContents[i]._id != "-1") {
                    this.state.BtnContents[i]._id = "1";
                    var url = config.Backend + '/botMsg/edit?';
                    if (!(cookies.get('Id') === undefined)) {
                        url += '&CurrentUserId=' + cookies.get('Id').toString();
                    }
                    url += '&Id=' + this.state.BotMsg._id;
                    url += '&Btn=' + this.state.BtnContents[i].Name;
                    url += '&Redirect=' + this.state.BtnContents[i].Redirect;
                    url += '&Type=' + this.state.BtnContents[i].Type;
                    url += '&Url=' + this.state.BtnContents[i].Url;
                    url += '&Index=' + i;
                    this.props.addWait();
                    jQuery.getJSON(url, function (res) {
                        if (!res.Error) {
                            this.props.delWait();
                            this.state.BtnContents[i] = res.Content;
                            this.setState({BtnContents:this.state.BtnContents});
                            // alert(JSON.stringify(res));
                        } else {
                            this.props.delWait();
                            this.state.BtnContents[i]._id = "-1";
                            this.setState({BtnContents:this.state.BtnContents});
                            alert(JSON.stringify(res));
                        }
                    }.bind(this));
                } else {
                    var url = config.Backend + '/botMsg/edit?';
                    if (!(cookies.get('Id') === undefined)) {
                        url += '&CurrentUserId=' + cookies.get('Id').toString();
                    }
                    url += '&Id=' + this.state.BotMsg._id;
                    url += '&Btn=' + this.state.BtnContents[i].Name;
                    url += '&Redirect=' + this.state.BtnContents[i].Redirect;
                    url += '&Type=' + this.state.BtnContents[i].Type;
                    url += '&Url=' + this.state.BtnContents[i].Url;
                    this.props.addWait();
                    jQuery.getJSON(url, function (res) {
                        if (!res.Error) {
                            this.props.delWait();
                            // alert(JSON.stringify(res));
                        } else {
                            this.props.delWait();
                            alert(JSON.stringify(res));
                        }
                    }.bind(this));
                }
            }
        }
        //this.props.changeMsg(this.state.BotMsg);
    }
    clearInput(e) {
        e.target.value = null;
    }
    deleteContent(i,type,not_save_state) {
        this.setState({Saved:false})
        if(type=="Image")
        {
            if(this.state.ImageContents[i]._id!="-1")
            {
                var url = config.Backend + '/content/delete?';
                if (!(cookies.get('Id') === undefined))
                {
                    url += '&CurrentUserId=' + cookies.get('Id').toString();
                }

                url+='&Id='+this.state.ImageContents[i]._id
                this.props.addWait();
                jQuery.getJSON(url, function (res) {
                    if (!res.Error) {
                        this.props.delWait();
                        // alert(JSON.stringify(res));
                    } else {
                        this.props.delWait();
                        alert(JSON.stringify(res));
                    }
                }.bind(this));
                if(not_save_state!==1)
                {
                    this.props.pushCommand(()=>this.restore_content(i,"ImageContents"),
                        ()=>this.deleteContent(i,type,1));
                }
            }
            else
            {
                this.props.pushCommand(()=>this.restoreTempContent(i,"ImageContents"),
                    ()=>this.deleteTempContent(i,"ImageContents"));
            }
            this.state.ImageContents[i].Deleted=true;
            this.setState({ImageContents:this.state.ImageContents});
        }
        else if(type=="Video")
        {
            if(this.state.VideoContents[i]._id!="-1")
            {
                var url = config.Backend + '/content/delete?';
                if (!(cookies.get('Id') === undefined))
                {
                    url += '&CurrentUserId=' + cookies.get('Id').toString();
                }

                url+='&Id='+this.state.VideoContents[i]._id
                this.props.addWait();
                jQuery.getJSON(url, function (res) {
                    if (!res.Error) {
                        this.props.delWait();
                        // alert(JSON.stringify(res));
                    } else {
                        this.props.delWait();
                        alert(JSON.stringify(res));
                    }
                }.bind(this));
                if(not_save_state!==1)
                {
                    this.props.pushCommand(()=>this.restore_content(i,"VideoContents"),
                        ()=>this.deleteContent(i,type,1));
                }
            }
            else
            {
                this.props.pushCommand(()=>this.restoreTempContent(i,"VideoContents"),
                    ()=>this.deleteTempContent(i,"VideoContents"));
            }
            this.state.VideoContents[i].Deleted=true;
            this.setState({VideoContents:this.state.VideoContents});
        }
        else if(type=="Audio")
        {
            if(this.state.AudioContents[i]._id!="-1")
            {
                var url = config.Backend + '/content/delete?';
                if (!(cookies.get('Id') === undefined))
                {
                    url += '&CurrentUserId=' + cookies.get('Id').toString();
                }

                url+='&Id='+this.state.AudioContents[i]._id
                this.props.addWait();
                jQuery.getJSON(url, function (res) {
                    if (!res.Error) {
                        this.props.delWait();
                        // alert(JSON.stringify(res));
                    } else {
                        this.props.delWait();
                        alert(JSON.stringify(res));
                    }
                }.bind(this));
                if(not_save_state!==1)
                {
                    this.props.pushCommand(()=>this.restore_content(i,"AudioContents"),
                        ()=>this.deleteContent(i,type,1));
                }
            }
            else
            {
                this.props.pushCommand(()=>this.restoreTempContent(i,"AudioContents"),
                    ()=>this.deleteTempContent(i,"AudioContents"));
            }
            this.state.AudioContents[i].Deleted=true;
            this.setState({AudioContents:this.state.AudioContents});
        }
        else if(type=="Api")
        {
            if(this.state.ApiContents[i]._id!="-1")
            {
                var url = config.Backend + '/content/delete?';
                if (!(cookies.get('Id') === undefined))
                {
                    url += '&CurrentUserId=' + cookies.get('Id').toString();
                }

                url+='&Id='+this.state.ApiContents[i]._id
                this.props.addWait();
                jQuery.getJSON(url, function (res) {
                    if (!res.Error) {
                        this.props.delWait();
                        // alert(JSON.stringify(res));
                    } else {
                        this.props.delWait();
                        alert(JSON.stringify(res));
                    }
                }.bind(this));
                if(not_save_state!==1)
                {
                    this.props.pushCommand(()=>this.restore_content(i,"ApiContents"),
                        ()=>this.deleteContent(i,type,1));
                }
            }
            else
            {
                this.props.pushCommand(()=>this.restoreTempContent(i,"ApiContents"),
                    ()=>this.deleteTempContent(i,"ApiContents"));
            }
            this.state.ApiContents[i].Deleted=true;
            this.setState({ApiContents:this.state.ApiContents});
        }
        else if(type=="BtnToGroup")
        {
            if(this.state.BtnContents[i]._id!="-1")
            {
                var url = config.Backend + '/botMsg/edit?';
                if (!(cookies.get('Id') === undefined))
                {
                    url += '&CurrentUserId=' + cookies.get('Id').toString();
                }
                url+='&Id='+this.state.BotMsg;
                url+='&Btn='+this.state.BtnContents[i].Name;
                url+='&Redirect='+this.state.BtnContents[i].Redirect;
                url+='&Index='+i;
                url+='&Delete=true';
                this.props.addWait();
                jQuery.getJSON(url, function (res) {
                    if (!res.Error) {
                        this.props.delWait();
                        // alert(JSON.stringify(res));
                    } else {
                        this.props.delWait();
                        alert(JSON.stringify(res));
                    }
                }.bind(this));
                if(not_save_state!==1)
                {
                    this.props.pushCommand(()=>this.restoreTempContent(i,"BtnContents"),
                        ()=>this.deleteContent(i,type,1));
                }
            }
            else
            {
                this.props.pushCommand(()=>this.restoreTempContent(i,"BtnContents"),
                    ()=>this.deleteTempContent(i,"BtnContents"));
            }
            this.state.BtnContents[i].Deleted=true;
            this.setState({BtnContents:this.state.BtnContents});
        }
    }
    addBtnContent() {
        this.props.ChangeSave(false);
        var btn = {Name: "Button", Redirect: "-", _id: "-1",Type:"postBack",Url:"http://"}
        this.state.BtnContents.push(btn);
        var ind  = this.state.BtnContents.length-1;
        this.props.pushCommand(()=>this.deleteTempContent(ind,"BtnContents")
            ,()=>this.restoreTempContent(ind,"BtnContents"));
        this.setState({BtnContents:this.state.BtnContents})
    }
    addVideoContent(e) {
        this.props.ChangeSave(false);
        e.preventDefault();
        let reader = new FileReader();
        let file = e.target.files[0];
        reader.onloadend = () => {
            var Content = {_id:"-1",Type:"Video",Link:"-1",File:file,Url:reader.result}
            this.state.VideoContents.push(Content);
            var ind  = this.state.VideoContents.length-1;
            this.props.pushCommand(()=>this.deleteTempContent(ind,"VideoContents")
                ,()=>this.restoreTempContent(ind,"VideoContents"));
            this.setState({VideoContents:this.state.VideoContents});
        }
        reader.readAsDataURL(file)
    }
    addApiContent() {
        var Content = {_id:"-1",Type:"Api",Url:"",ReqType:"Get",Field:'',Macros: "",Headers:"",Save:""};
        this.state.ApiContents.push(Content);
        var ind  = this.state.ApiContents.length-1;
        this.props.pushCommand(()=>this.deleteTempContent(ind,"ApiContents")
            ,()=>this.restoreTempContent(ind,"ApiContents"));
        this.setState({ApiContents:this.state.ApiContents});
    }
    addImageContent(e) {
        this.props.ChangeSave(false);
        e.preventDefault();
        let reader = new FileReader();
        let file = e.target.files[0];
        reader.onloadend = () => {
            var Content = {_id:"-1",Type:"Image",Link:"-1",File:file,Url:reader.result}
            this.state.ImageContents.push(Content);
            var ind  = this.state.ImageContents.length-1;
            this.props.pushCommand(()=>this.deleteTempContent(ind,"ImageContents")
                ,()=>this.restoreTempContent(ind,"ImageContents"));
            this.setState({ImageContents:this.state.ImageContents});
        }
        reader.readAsDataURL(file)
    }
    addAudioContent(e) {
        this.props.ChangeSave(false);
        e.preventDefault();
        let reader = new FileReader();
        let file = e.target.files[0];
        reader.onloadend = () => {
            var Content = {_id:"-1",Type:"Audio",Link:"-1",File:file,Url:reader.result}
            this.state.AudioContents.push(Content);
            var ind  = this.state.AudioContents.length-1;
            this.props.pushCommand(()=>this.deleteTempContent(ind,"AudioContents")
                ,()=>this.restoreTempContent(ind,"AudioContents"));
            this.setState({AudioContents:this.state.AudioContents});
        }
        reader.readAsDataURL(file)
    }
    deleteTempContent = (i,content) => {
        this.state[content][i].Deleted=true;
        this.setState({[content]:this.state[content]});
    }
    restoreTempContent = (i,content) => {
        this.state[content][i]._id="-1";
        this.state[content][i].Deleted=undefined;
        this.setState({[content]:this.state[content]});
    }
    restore_content = (i,content) => {
        var url = config.Backend + '/content/restore?';
        url += '&CurrentUserId=' + cookies.get('Id');
        url+='&Id='+this.state[content][i]._id;

        this.setState({Wait:++this.state.Wait});jQuery.getJSON(url, function (res) {
            if (!res.Error) {
                this.state[content][i].Deleted=undefined;
                this.setState({[content]:this.state[content]});
                this.setState({Wait:--this.state.Wait})
            } else {
                this.setState({Wait:--this.state.Wait});alert(JSON.stringify(res));
            }
        }.bind(this));
    }
    handleForContent(index,Text,Time,Link,File,Type,Url,ReqType,Field,Refresh,Macros,BtnType,BtnUrl,Headers,Save) {
        this.props.ChangeSave(false);
        if(Type=="Text")
        {
            this.state.TextContents[index].Text = Text;
            this.state.TextContents[index].Time = Time;
        }
        else if (Type=="Image")
        {
            this.state.ImageContents[index].Link = Link;
            this.state.ImageContents[index].File = File;
        }
        else if (Type=="Audio")
        {
            this.state.AudioContents[index].Link = Link;
            this.state.AudioContents[index].File = File;
        }
        else if (Type=="Video")
        {
            this.state.VideoContents[index].Link = Link;
            this.state.VideoContents[index].File = File;
        }
        else if (Type=="Api")
        {
            this.state.ApiContents[index].Url = Url;
            this.state.ApiContents[index].ReqType = ReqType;
            this.state.ApiContents[index].Field = Field;
            this.state.ApiContents[index].Macros = Macros;
            this.state.ApiContents[index].Headers = Headers;
            this.state.ApiContents[index].Save = Save;
        }
        else if(Type=="BtnToGroup")
        {
            this.state.BtnContents[index].Name = Text;
            this.state.BtnContents[index].Redirect = Url;
            this.state.BtnContents[index].Type = BtnType;
            this.state.BtnContents[index].Url = BtnUrl;
            this.state.BotMsg.Btns = this.state.BtnContents;
        }
        else if(Type=="DefaultLinkToGroup")
        {
            this.state.DefLink[0] = Url;
            this.state.BotMsg.DefaultLink = Url;
        }
    }
    loadContent(msg) {
        this.state.DefLink = [];
        this.setState({DefLink:this.state.DefLink});
        this.state.DefLink.push(msg.DefaultLink);
        this.setState({DefLink:this.state.DefLink});

        var url = config.Backend + '/botMsg/showContents?';
        if (!(cookies.get('Id') === undefined))
        {
            url += '&CurrentUserId=' + cookies.get('Id').toString();
        }
        url+='&Id=' + msg._id;
        this.props.addWait();
        jQuery.getJSON(url, function (res) {
            if (!res.Error) {
                this.state.TextContents = [];
                this.state.ImageContents = [];
                this.state.VideoContents = [];
                this.state.AudioContents = [];
                this.state.ApiContents = [];

                this.setState({TextContents: this.state.TextContents});
                this.setState({ImageContents: this.state.ImageContents});
                this.setState({VideoContents: this.state.VideoContents});
                this.setState({AudioContents: this.state.AudioContents});
                this.setState({ApiContents: this.state.ApiContents});

                res.forEach(function (content,i,arr) {
                    if(content.Type=="Text")
                        this.state.TextContents.push(content)
                    if(content.Type=="Image")
                        this.state.ImageContents.push(content)
                    if(content.Type=="Video")
                        this.state.VideoContents.push(content)
                    if(content.Type=="Audio")
                        this.state.AudioContents.push(content)
                    if(content.Type=="Api")
                        this.state.ApiContents.push(content)
                }.bind(this))
                this.setState({TextContents: this.state.TextContents},()=>{
                    document.getElementById("TextContent").focus();
                });
                this.setState({ImageContents: this.state.ImageContents});
                this.setState({VideoContents: this.state.VideoContents});
                this.setState({AudioContents: this.state.AudioContents});
                this.setState({ApiContents: this.state.ApiContents});
                this.props.delWait();
            } else {
                this.props.delWait();
                alert(JSON.stringify(res))
            }
        }.bind(this));

        url = config.Backend + '/botMsg/findById?';
        if (!(cookies.get('Id') === undefined))
        {
            url += '&CurrentUserId=' + cookies.get('Id').toString();
        }
        url+='&Id=' + msg._id;
        this.props.addWait();
        jQuery.getJSON(url, function (res) {
            if(!res.Error) {
                this.state.BtnContents = [];
                this.setState({BtnContents: this.state.BtnContents});
                res.Btns.forEach(function (btn, i, arr) {
                    btn._id = i+"";
                    this.state.BtnContents.push(btn);
                }.bind(this))
                this.setState({BtnContents: this.state.BtnContents});
                this.props.delWait();

            } else {
                this.props.delWait();
                alert(JSON.stringify(res))
            }
        }.bind(this))


    }
    loadGroups() {
        var url = config.Backend + '/bot/groups?';
        if (!(cookies.get('Id') === undefined))
        {
            url += '&CurrentUserId=' + cookies.get('Id').toString();
        }
        url+='&Id=' + this.state.Bot._id;
        this.props.addWait();
        jQuery.getJSON(url, function (res) {
            if (!res.Error) {
                this.props.delWait();
                this.setState({Groups: res});
            } else {
                this.props.delWait();
                alert(JSON.stringify(res))
            }
        }.bind(this));
    }
    componentWillReceiveProps(nextProps) {
        if(nextProps.BotMsg._id != this.props.BotMsg._id) {
            if(nextProps.BotMsg._id) {
                this.loadContent(nextProps.BotMsg);
            }

            this.setState({BotMsg: nextProps.BotMsg}, () => {
                if (this.state.BotMsg.Days !== undefined) {
                    var arr = this.state.BotMsg.Days.split(";");
                    for (var i = 0; i < this.state.RepeatDays.length; i++) {
                        this.state.RepeatDays[i] = false;
                    }
                    for (var i = 0; i < arr.length; i++) {
                        if(arr[i]!="")
                        this.state.RepeatDays[+arr[i].replace(";", "")] = true;
                    }
                }
            });
        }
    }

    condition(user,attr,val,cond) {
        if(cond=="is")
        {
            return user[attr] == val;
        }
        else if(cond=="is not")
        {
            return user[attr] != val;
        }
        else if(cond==">")
        {
            return user[attr] > val;
        }
        else if(cond=="<")
        {
            return user[attr] < val;
        }
        else
        {
            return (user[attr] || "").indexOf(val || "")==0;
        }
    }

    componentWillMount() {
        this.loadGroups();
        this.loadContent(this.props.BotMsg);
        this.setState({BotMsg:this.props.BotMsg},()=>{
            var arr = this.state.BotMsg.Days.split(";");
            for(var i=0;i<this.state.RepeatDays.length;i++)
            {
                this.state.RepeatDays[i]=false;
            }
            for(var k=0;k<arr.length;k++)
            {
                if(arr[k]!="") {
                    this.state.RepeatDays[+arr[k].replace(";", "")] = true;
                }
            }
        });
    }
    toggleSwitch = () => {
        this.props.ChangeSave(false);
        this.state.BotMsg.Status = this.state.BotMsg.Status == "0" ? "1" : "0";
        this.setState({BotMsg: this.state.BotMsg});
    };
    AddItems(){
        /* <div className="form-group inputDnD">
                        <span className="form-control-file text-primary " data-title="Api"  onClick={this.addApiContent} >Api</span>
                    </div>*/
        return(
            <div className="EditAddBlock">
                <span>Add items</span>

                <div className="list_button">

                    <div className="form-group inputDnD">
                        <input type="file" accept="video/*" onChange={this.addVideoContent} onClick={this.clearInput} className="form-control-file text-primary "  data-title="Video"/>
                    </div>

                    <div className="form-group inputDnD">
                        <input type="file" accept="audio/*" onChange={this.addAudioContent} onClick={this.clearInput}  className="form-control-file text-primary " data-title="Audio" />
                    </div>

                    <div className="form-group inputDnD">
                        <input type="file" accept="image/*" onChange={this.addImageContent} onClick={this.clearInput}   className="form-control-file text-primary " data-title="Image" />
                    </div>
                    <div className="form-group inputDnD">
                        <span className="form-control-file text-primary " data-title="Api"  onClick={this.addApiContent} >Api</span>
                    </div>
                    <div className="form-group inputDnD">
                        <span className="form-control-file text-primary " data-title="Button"  onClick={this.addBtnContent} >Button</span>
                    </div>
                </div>
                <div className="broadCast_btn">
                    <button onClick={this.saveContents} className="btn_save">Save</button>
                    <button  data-tip data-for='btn_d' onClick={this.props.ShowDelPopup} className="btn_delete">
                        <ReactTooltip id='btn_d' type='error'>
                            <span>Delete this block</span>
                        </ReactTooltip>

                    </button>


                </div>


            </div>)
    }


    make_arr = (arr) => {
        var s =this.state;
        var res =[];
        for(var x =0;x<s[arr].length;x++)
        {
            if(!s[arr][x].Deleted)
            {
                s[arr][x].Index = x;
                res.push(s[arr][x]);
            }
        }
        return res;
    }

    EditPanel(){
        var s =this.state;
        var ic = this.make_arr("ImageContents");
        var vc = this.make_arr("VideoContents");
        var auc = this.make_arr("AudioContents");
        var ac = this.make_arr("ApiContents");
        var bc = this.make_arr("BtnContents");

        const txtContents = this.state.TextContents.map((content,i)=><Content Index={i} handleForContent={this.handleForContent}
                                                                              Text={content.Text} Type={content.Type}
                                                                              File={content.File} Url={content.Url}
                                                                              onDelete={this.deleteContent}           Link={content.Link} typingTime={content.Time}/>);
        const imgContents = ic.map((content)=><Content Index={content.Index} handleForContent={this.handleForContent}
                                                                               onDelete={this.deleteContent}              File={content.File} Url={content.Url}
                                                                               Text={content.Text} Type={content.Type} Link={content.Link} typingTime={content.Time}/>);
        const videoContents = vc.map((content)=><Content Index={content.Index} handleForContent={this.handleForContent}
                                                                                 onDelete={this.deleteContent}      Text={content.Text} Type={content.Type}
                                                                                 File={content.File} Url={content.Url}
                                                                                 Link={content.Link} typingTime={content.Time}/>);

        const audioContents = auc.map((content)=><Content Index={content.Index} handleForContent={this.handleForContent}
                                                                                 onDelete={this.deleteContent}     Text={content.Text} Type={content.Type}
                                                                                 File={content.File} Url={content.Url}
                                                                                 Link={content.Link} typingTime={content.Time}/>);
        const apiContents = ac.map((content)=><Content Index={content.Index} handleForContent={this.handleForContent}
                                                                         Macros={content.Macros}    onDelete={this.deleteContent}     Text={content.Text} Type={content.Type}
                                                                         Headers={content.Headers} Save={content.Save}   Bot={this.props.Bot} ReqType={content.ReqType} Url={content.Url}
                                                                             Field={content.Field} typingTime={content.Time}/>);

        const btnContents = bc.map(function(content){ return <Content Index={content.Index} handleForContent={this.handleForContent}
                                                                                     BtnUrl={content.Url}    BtnType={content.Type}   onDelete={this.deleteContent}     Text={content.Name} Type={"BtnToGroup"}
                                                                                            ReqType={content.ReqType} Url={content.Redirect}
                                                                                            BotMsg={this.state.BotMsg}  Groups={this.state.Groups} typingTime={content.Time}/>}.bind(this));

        var link="";
        for(var i=0;i<this.state.Groups.length;i++)
        {
            if(this.state.Groups[i]._id==this.state.BotMsg)
                link = this.state.BotMsgs[i].DefaultLink;
        }
        var defaultLink = this.state.DefLink.map((d)=><Content handleForContent={this.handleForContent} Type={"DefaultLinkToGroup"}
                                                               Url={d}
                                                               Groups={this.state.Groups}/>)
        return(
            <div>
                <div className="wrap_filter_laber mt_10">
                {this.state.BotMsg ? <label htmlFor="enable_filter_2">Wait answer</label>
                    : null}
                {this.state.BotMsg ?
                    <input id="enable_filter_2" type={"checkbox"} checked={this.state.BotMsg.WaitAnswer} onChange={this.changeWaitAnswer}/>
                    : null}
                </div>
                {this.state.BotMsg && this.state.BotMsg.WaitAnswer ?
                    <div>
                        <label htmlFor="param"><span>Context.</span> </label>
                        <input className="borderR chat_input mb6" id="param" type="text" onChange={this.changeAnswerName} value={this.state.BotMsg.AnswerName} />
                    </div>
                    : null}
                {txtContents}

                <div className="test">
                    {defaultLink}
                </div>
                <div className="testAudio">
                    {imgContents}
                </div>
                <div className="right_video mt_c15">
                    {videoContents}
                </div>
                <div className="audio"> {audioContents}</div>

                <div className="test">
                    {apiContents}
                </div>

                <div className="test">
                    {btnContents}
                </div>
                {this.AddItems()}
            </div>
        )
    }

    ScheduleForLater(){
        return(
            <div className="list_schedule_later">
                <div className="shedule_overlay_settings">
                    <div className="schedule_input_1">
                        <DatePicker className="bred"
                                    onChange={this.changeDate}
                                    value={new Date(this.state.BotMsg.Date)}
                        />
                    </div>
                    <div className="schedule_input_1">
                        <input className="borderR" type="text" placeholder="HH:mm" value={this.state.BotMsg.Time} onChange={this.changeTime}/>
                    </div>

                    <div className="schedule_input_1">
                    <select className="borderR" value={this.state.BotMsg.Timezone} onChange={this.changeTimezone}>
                        <option value="user">Local timezone</option>
                        <option value="-12">UTC -12:00</option>
                        <option value="-11">UTC -11:00</option>
                        <option value="-10">UTC -10:00</option>
                        <option value="-9">UTC -9:00</option>
                        <option value="-8">UTC -8:00</option>
                        <option value="-7">UTC -7:00</option>
                        <option value="-6">UTC -6:00</option>
                        <option value="-5">UTC -5:00</option>
                        <option value="-4">UTC -4:00</option>
                        <option value="-3">UTC -3:00</option>
                        <option value="-2">UTC -2:00</option>
                        <option value="-1">UTC -1:00</option>
                        <option value="0">UTC +0:00</option>
                        <option value="1">UTC +1:00</option>
                        <option value="2">UTC +2:00</option>
                        <option value="3">UTC +3:00</option>
                        <option value="4">UTC +4:00</option>
                        <option value="5">UTC +5:00</option>
                        <option value="6">UTC +6:00</option>
                        <option value="7">UTC +7:00</option>
                        <option value="8">UTC +8:00</option>
                        <option value="9">UTC +9:00</option>
                        <option value="10">UTC +10:00</option>
                        <option value="12">UTC +11:00</option>
                        <option value="12">UTC +12:00</option>
                    </select>
                    </div>
                    <div className="schedule_input_1">
                    <select className="borderR" onChange={this.changeRepeat} value={this.getRepeatType()}>
                        <option value="None">Repeat: None</option>
                        <option value="Every Day">Repeat: Every Day</option>
                        <option value="Weekends">Repeat: Weekends</option>
                        <option value="Every Month">Repeat: Every Month</option>
                        <option value="Workdays">Repeat: Workdays</option>
                        <option value="Custom">Repeat: Custom</option>
                    </select>
                    </div>
                </div>
                { this.getRepeatType()=="Custom" ?
                    <div className="block_list_schedule_custom">
                        <ul className="list_shedule">
                            <li onClick={()=>this.changeRepeatDays(0)}>
                                <span>Sun{this.state.RepeatDays[0] ? "+" : ""}</span>
                            </li>
                            <li onClick={()=>this.changeRepeatDays(1)}>
                                <span>Mon{this.state.RepeatDays[1] ? "+" : ""}</span>
                            </li>
                            <li onClick={()=>this.changeRepeatDays(2)}>
                                <span>Tue{this.state.RepeatDays[2] ? "+" : ""}</span>
                            </li>
                            <li onClick={()=>this.changeRepeatDays(3)}>
                                <span>Wed{this.state.RepeatDays[3] ? "+" : ""}</span>
                            </li>
                            <li onClick={()=>this.changeRepeatDays(4)}>
                                <span>Thu{this.state.RepeatDays[4] ? "+" : ""}</span>
                            </li>
                            <li onClick={()=>this.changeRepeatDays(5)}>
                                <span>Fri{this.state.RepeatDays[5] ? "+" : ""}</span>
                            </li>
                            <li onClick={()=>this.changeRepeatDays(6)}>
                                <span>Sat{this.state.RepeatDays[6] ? "+" : ""}</span>
                            </li>
                        </ul>
                    </div> : null
                }
            </div>
        )
    }


    render(){
        if(this.props.BotMsg===undefined)
            return null;
        var attrs = [];
        //alert(JSON.stringify(this.props.Bot))
        if(this.props.Bot.BindTo=="FB") {
             attrs = ["first_name", "last_name", "gender", "locale", "timezone"].map(function (attr) {
                return {value: attr, label: attr}
            });
        } else if(this.props.Bot.BindTo=="Skype") {
            attrs = ["name", "platform","locale","country"].map(function (attr) {
                return {value: attr, label: attr}
            });
        } else if(this.props.Bot.BindTo=="Slack") {
            attrs = [].map(function (attr) {
                return {value: attr, label: attr}
            });
        }
//                                            <option value="After user add bot to contacts">After user add bot to contacts(working only for skype bot)</option>


        return(
            <div className="overlay_main col-sm-8">


                {this.props.Saved() ? <span className="oSave">Saved</span> : <span className="nSave">Not saved</span>}
                <div className="trigger form-group">
                    {this.state.BotMsg.Type != "Now" ?
                        <div className="switch_select">
                            <Switch onClick={this.toggleSwitch} on={this.state.BotMsg.Status=="1"}/>
                        </div> :
                        <button className="sendMessage orange-rounded" onClick={this.send}>Send</button>
                    }
                    {this.state.BotMsg.Type=="Now" ? <span>Deliver Your Message Now</span> : null}
                    {this.state.BotMsg.Type=="Trigger" ? <span>Add a trigger</span> : null}
                    {this.state.BotMsg.Type=="Sch" ? <span>Schedule for later</span> : null}
                    {this.state.BotMsg.Type=="Auto" ? <span>Autopost from external source</span> : null}

                    <div className="user_filter_send">
                        <button className="userFilter" onClick={this.props.ShowUserFilter}>User filter</button>
                    </div>

                </div>
                <div className="deliver_txt">
            <span><b>{this.props.FilterCount+""}</b> users (all) match this filter.<br />
            This message will only be sent to users with matching User Filter attributes at the time of sending.</span>

                </div>
                <div className="trigger_settings">
                    {
                        this.state.BotMsg.Type=="Trigger" ?
                            <div className="settings_trigger">
                                <div className="left">
                                    <span>Trigger</span>
                                </div>
                                <div className="right">
                                    <div className="right_overlay">
                                        <select className="chat_input borderR" value={this.state.BotMsg.Trigger} onChange={this.changeTrigger}>
                                            <option value="After first interaction">After first interaction</option>
                                            <option value="After last interaction">After last interaction</option>
                                            <option value="After user attribute is set">After user attribute is set</option>
                                            <option value="After user join to channel">After user join to channel(working only for slack bot)</option>
                                        </select>
                                    </div>

                                    { this.state.BotMsg.Trigger=="After user attribute is set" ?
                                        <div className="profile_pic">
                                            <Select
                                                className="borderRs"
                                                placeholder={"Type attribute name"}
                                                name="form-field-name borderR"
                                                value={this.state.BotMsg.TriggerAttr}
                                                options={attrs}
                                                onChange={this.changeTriggerAttr}
                                            />

                                            <select value={this.state.BotMsg.TriggerCond} onChange={this.changeTriggerCond} className="chat_input borderR">
                                                <option value="is">is</option>
                                                <option value="is not">is not</option>
                                                <option value="start with">start with</option>
                                                <option value=">">{">"}</option>
                                                <option value="<">{"<"}</option>
                                            </select>
                                            <input value={this.state.BotMsg.TriggerVal} onChange={this.changeTriggerVal}
                                                   placeholder="Choose value of the attribute" className="chat_input borderR" type="text"/>
                                        </div> : null
                                    }
                                    {
                                        this.state.BotMsg.Trigger=="After first interaction" ||
                                        this.state.BotMsg.Trigger=="After last interaction" ?<span>Note:each user will receive the message only once</span>
                                            : null
                                    }

                                </div>
                                {this.state.BotMsg.Trigger!="After user join to channel" && this.state.BotMsg.Trigger!="After user add bot to contacts" ?
                                <div className="left">
                                    <span>Send time in</span>
                                </div> : null}
                                {this.state.BotMsg.Trigger!="After user join to channel" && this.state.BotMsg.Trigger!="After user add bot to contacts" ?
                                <div className="right">
                                    <div className="right_overlay">
                                        <input value={this.state.BotMsg.Time} onChange={this.changeTime} className="chat_input borderR" type="number"/>
                                        <select className="chat_input borderR" value={this.state.BotMsg.TimeType} onChange={this.changeTimeType}>
                                            <option value="Minutes">Minutes</option>
                                            <option value="Hours">Hours</option>
                                            <option value="Days">Days</option>
                                        </select>
                                    </div>
                                </div> : null}
                            </div> : null
                    }
                    {
                        this.state.BotMsg.Type=="Sch" ?
                            this.ScheduleForLater() : null
                    }
                </div>
                <div className="addCard">
                    {this.EditPanel()}
                </div>
            </div>
        )
    }
}

class BroadcastCentrPanel extends Component {
    constructor(props) {
        super(props);
        this.state = {
            None: {},
            User: this.props.User,
        }
    }
    MesseageSend(){
        return(
            <div className="message_block_center">
                <div className="text_center_block_success">
                    <h2>Message was sent!</h2>
                    <p>You can also use Schedule for later section in the left<br/> panel to manage your chatbot future posts.</p>
                </div>
            </div>
        )
    }

    CenterPanelText(){
        return(
            <div className="message_block_center">
                <div className="text_center_block">
                    <p>Broadcast functionality allows you to proactively reach out to your audience. Filter your users based on their attributes
                        <br/>and send personalized messages. Start by choosing the type <br/>   of broadcast you want to send on the left.</p>
                </div>
            </div>
        )
    }

    render(){
        return(
            <div className="broad_main col-sm-9">
                <Scrollbars
                    style={{ height: "90%" }}>
                    <div className="broad_main_content_block">
                        {this.props.Main!="Start" && this.props.Main!="Sent" && !(this.props.BotMsg===undefined) ?
                            <Deliver ChangeSave={this.props.ChangeSave} changeMsg={this.props.changeMsg} FilterCount={this.props.FilterCount}
                                     Saved={this.props.Saved} ShowDelPopup={this.props.ShowDelPopup} ShowUserFilter={this.props.ShowUserFilter}
                                     changeMain={this.props.changeMain} loadMsgs={this.props.loadMsgs}
                                     addWait={this.props.addWait} delWait={this.props.delWait} pushCommand={this.props.pushCommand}
                                     BotMsg={this.props.BotMsg} Bot={this.props.Bot} User={this.props.User} DeleteMsg={this.props.DeleteMsg}/>
                            : null}
                        {this.props.Main=="Start" ? this.CenterPanelText() : null}
                        {this.props.Main=="Sent" ? this.MesseageSend() : null}
                    </div>
                </Scrollbars>
            </div>
        )
    }
}


class Broadcast extends Loading {
    constructor(props) {
        super(props);
        this.state = {
            User: {},
            Bot: {},
            Main: "Start",
            CurrentMsg: {},
            Show:false,
            NowMsgs: [],
            TriggerMsgs: [],
            SchMsgs: [],
            AutoMsgs: [],
            ShowDeletebotMsgPopup: false,
            ShowUserFilter: false,
            Saved:true,
            BotUsers:[],
            FilterCount: 0,
            Wait: 0,
            Commands:[],
            CommandIndex:0,
        }
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleInputFocus = this.handleInputFocus.bind(this);
        this.changeMain = this.changeMain.bind(this);
        this.changeMsg = this.changeMsg.bind(this);
        this.addNowMsg = this.addNowMsg.bind(this);
        this.addTriMsg = this.addTriMsg.bind(this);
        this.addSchMsg = this.addSchMsg.bind(this);
        this.addAutoMsg = this.addAutoMsg.bind(this);
        this.loadMsgs = this.loadMsgs.bind(this);
        this.deleteMsg = this.deleteMsg.bind(this);
        this.changeFilterCond = this.changeFilterCond.bind(this);
        this.changeFilterAttr = this.changeFilterAttr.bind(this);
        this.changeFilterVal = this.changeFilterVal.bind(this);
        this.changeFilterType = this.changeFilterType.bind(this);
        this.addFilter = this.addFilter.bind(this);
        this.deleteFilter = this.deleteFilter.bind(this);
        this.changeSave = this.changeSave.bind(this);
        this.changeFilterEnable = this.changeFilterEnable.bind(this);
        this.refreshFilterUsers = this.refreshFilterUsers.bind(this);
        this.addWait = this.addWait.bind(this);
        this.delWait = this.delWait.bind(this);
        this.ctrl = this.ctrl.bind(this);
        this.pushCommand = this.pushCommand.bind(this);
        this.restore = this.restore.bind(this);
    }
    ctrl(e)
    {
        var s =this.state;
        if( e.ctrlKey && s.Commands.length>0) {
            if (e.keyCode == 90 && s.CommandIndex>0)//ctrl+z
            {
                this.setState({CommandIndex:--this.state.CommandIndex});
                s.Commands[s.CommandIndex].Undo();
            }
            else if (e.keyCode == 89 && s.CommandIndex<s.Commands.length)//ctrl+y
            {
                s.Commands[s.CommandIndex].Redo();
                this.setState({CommandIndex:s.CommandIndex+1});
            }
        }
    }
    restore(id)
    {
        var url = config.Backend + '/botMsg/restore?';
        if (!(cookies.get('Id') === undefined))
        {
            url += '&CurrentUserId=' + cookies.get('Id');
        }
        url+= '&Id='+id;
        this.setState({Wait:++this.state.Wait});jQuery.getJSON(url, function (res) {
        this.setState({Wait:--this.state.Wait})
        if (!res.Error) {
            //this.state.BotMsgs.push(res.BotMsg);
            //this.setState({BotMsgs: this.state.BotMsgs});
            this.loadMsgs(this.state.Bot);
        } else {
            alert(JSON.stringify(res));
        }
    }.bind(this));
    }
    pushCommand(undo,redo)
    {
        var arr = [];
        for(var i =0;i<this.state.CommandIndex;i++)
        {
            arr.push(this.state.Commands[i]);
        }
        arr.push({Redo:redo,Undo:undo});
        this.setState({Commands:arr,CommandIndex:this.state.CommandIndex+1})
    }
    addWait()
    {
        this.setState({Wait:++this.state.Wait});;
    }
    delWait()
    {
        this.setState({Wait:--this.state.Wait});;
    }
    refreshFilterUsers(msg) {
        var cnt = 0;
        for(var i=0;i<this.state.BotUsers.length;i++)
        {
            var filter = msg.FilterType == "AND" ? true : false;
            for (var f = 0;msg.Filters &&  f < msg.Filters.length; f++) {
                if (msg.FilterType == "AND") {
                    if (this.state.Bot.BindTo == "Skype") {
                        filter = filter && this.condition(this.state.BotUsers[i], msg.Filters[f].Attr, msg.Filters[f].Val, msg.Filters[f].Cond);
                    }
                    else if (this.state.Bot.BindTo == "FB") {
                        filter = filter && this.condition(this.state.BotUsers[i], msg.Filters[f].Attr, msg.Filters[f].Val, msg.Filters[f].Cond);
                    }
                    else if (this.state.Bot.BindTo == "Slack") {
                        //filter = filter &&  this.condition(this.state.BotUsers[i], msg.Filters[f].Attr, msg.Filters[f].Val, msg.Filters[f].Cond);
                    }
                } else {
                    if (this.state.Bot.BindTo == "Skype") {
                        filter = filter || this.condition(this.state.BotUsers[i], msg.Filters[f].Attr, msg.Filters[f].Val, msg.Filters[f].Cond);
                    }
                    else if (this.state.Bot.BindTo == "FB") {
                        filter = filter || this.condition(this.state.BotUsers[i], msg.Filters[f].Attr, msg.Filters[f].Val, msg.Filters[f].Cond);
                    }
                    else if (this.state.Bot.BindTo == "Slack") {
                        filter = filter || this.condition(this.state.BotUsers[i], msg.Filters[f].Attr, msg.Filters[f].Val, msg.Filters[f].Cond);
                    }
                }
            }
            if (filter || !msg.FilterEnable) {
                cnt++;
            }

        }
        this.setState({FilterCount:cnt});
    }
    changeFilterEnable(e){
        this.state.CurrentMsg.FilterEnable = e.target.checked;
        this.changeMsg(this.state.CurrentMsg);
        this.setState({Saved:false})
    }
    changeSave(x){
        this.setState({Saved:x});
    }
    deleteFilter(i){
        this.state.CurrentMsg.Filters.splice(i,1);
        this.changeMsg(this.state.CurrentMsg);
        this.setState({Saved:false})
    }
    addFilter() {
        this.state.CurrentMsg.Filters.push({Attr:"Attribute",Val:"",Cond:"is"});
        this.changeMsg(this.state.CurrentMsg);
        this.setState({Saved:false})
    }
    changeFilterType(e) {
        this.state.CurrentMsg.FilterType = e.target.value;
        this.changeMsg(this.state.CurrentMsg);
        this.setState({Saved:false})
    }
    changeFilterCond(e,i) {
        this.state.CurrentMsg.Filters[i].Cond = e.target.value;
        this.changeMsg(this.state.CurrentMsg);
        this.setState({Saved:false})
    }
    changeFilterAttr(e,i) {
        if(e)
        {
            this.state.CurrentMsg.Filters[i].Attr = e.value;
        }
        else {
            this.state.CurrentMsg.Filters[i].Attr = "";
        }
        //this.state.CurrentMsg.Filters[i].Attr = e.target.value;
        this.changeMsg(this.state.CurrentMsg);
        this.setState({Saved:false})
    }
    changeFilterVal(e,i) {
        this.state.CurrentMsg.Filters[i].Val = e.target.value;
        this.changeMsg(this.state.CurrentMsg);
        this.setState({Saved:false})
    }
    BroadCastFilter(){
        var attrs = [];
        if(this.state.Bot.BindTo=="FB") {
            attrs = ["first_name", "last_name", "gender", "locale", "timezone"].map(function (attr) {
                return {value: attr, label: attr}
            });
        } else if(this.state.Bot.BindTo=="Skype") {
            attrs = ["name", "platform","locale","country"].map(function (attr) {
                return {value: attr, label: attr}
            });
        } else if(this.state.Bot.BindTo=="Slack") {
            attrs = [].map(function (attr) {
                return {value: attr, label: attr}
            });
        }


        var firstFilter =<div className="filter_group">

            <div className="filter_width">


            <div className="filter_score">
                <span>If</span>
            </div>

                <div className="filter_f_width">
            <Select
                className="borderF"
                placeholder={"Type attribute name"}
                name="form-field-name"
                value={this.state.CurrentMsg.Filters[0].Attr}
                options={attrs}
                onChange={(e)=>this.changeFilterAttr(e,0)}
            />
                </div>

            <div className="filter_score">
                <select className="filter_is select_w" value={this.state.CurrentMsg.Filters[0].Cond} onChange={(e)=>this.changeFilterCond(e,0)}>
                    <option value="is">is</option>
                    <option value="is not">is not</option>
                    <option value="starts with">starts with</option>
                    <option value="<">{"<"}</option>
                    <option value=">">{">"}</option>
                </select>
            </div>
                <div className="filter_f_width">
                    <input className="attr_input" type="text" value={this.state.CurrentMsg.Filters[0].Val} onChange={(e)=>this.changeFilterVal(e,0)}/>
                </div>
            </div>
        </div>;

        var otherFilters = this.state.CurrentMsg.Filters.slice(1).map(function(f,i){
            return (
                <div className="add_filter_group">
                    <div className="filter_width mt_5">
                    <div className="filter_score">
                        <select className="select_w" value={this.state.CurrentMsg.FilterType} onChange={this.changeFilterType}>
                            <option value="AND">AND</option>
                            <option value="OR">OR</option>
                        </select>
                    </div>


                    <div className="filter_f_width">
                    <Select
                        className="borderF"
                        placeholder={"Type attribute name"}
                        name="form-field-name"
                        value={this.state.CurrentMsg.Filters[i+1].Attr}
                        options={attrs}
                        onChange={(e)=>this.changeFilterAttr(e,i+1)}
                    />
                    </div>



                    <div className="filter_score">
                        <select className="filter_is select_w" value={this.state.CurrentMsg.Filters[i+1].Cond} onChange={(e)=>this.changeFilterCond(e,i+1)}>
                            <option value="is">is</option>
                            <option value="is not">is not</option>
                            <option value="starts with">starts with</option>
                            <option value="<">{"<"}</option>
                            <option value=">">{">"}</option>
                        </select>
                    </div>

                        <div className="filter_f_width">
                    <input className="attr_input" type="text" value={this.state.CurrentMsg.Filters[i+1].Val} onChange={(e)=>this.changeFilterVal(e,i+1)}/>
                        </div>
                    </div>
                    
                    <div className="btn_filter_w">
                        <button className="btn_filter_delete mt_5" onClick={()=>this.deleteFilter(i+1)}></button>
                    </div>
                </div> )
        }.bind(this))

        return(
            <div className='filterPopup'>
                <Scrollbars
                    style={{ height: "90%" }}>
                    <div className='popup_filter_content'>
                        <div className="filter_title">
                            <p>User Filter</p>
                        </div>
                        <div className="popup_filter_text">
                            <p>Choose which users will receive your message. Use one of the predefined filters, or your own filters in the dropdown below, to reach a specific group of people.</p>
                        </div>
                        <div className="popup_filter_setting">
                            <p>Who will receive this broadcast:</p>
                            {firstFilter}
                            {otherFilters}

                            <button onClick={this.addFilter} className="btn_all mt_15">Add filter</button>

                            <div className="popup_filter_bottom mt_15">
                                <div className="bottom_left">
                                    <span>Total recipients: <b>{this.state.FilterCount}</b>of {this.state.BotUsers.length}</span>
                                    <br/>
                                        <div className="wrap_filter_laber">
                                            <label htmlFor="enable_filter">Enable filter</label>
                                            <input id="enable_filter" type="checkbox" checked={this.state.CurrentMsg.FilterEnable} onChange={this.changeFilterEnable}/>
                                        </div>
                                    </div>

                                <div className="bottom_right">
                                    <button onClick={()=>{this.setState({ShowUserFilter:false})}} className="btn_all orange-rounded">Done</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </Scrollbars>
            </div>
        )
    }
    deleteMsg(msg,not_save_state) {
        this.setState({ShowDeletebotMsgPopup:false});
        var url = config.Backend + '/botMsg/delete?';
        if (!(cookies.get('Id') === undefined))
        {
            url += '&CurrentUserId=' + cookies.get('Id').toString();
        }
        url+= '&Id='+msg._id;
        this.setState({Wait:++this.state.Wait});
        jQuery.getJSON(url, function (res) {
            if (!res.Error) {
                this.setState({Wait:--this.state.Wait});
                this.changeMsg({});
                this.changeMain("Start");
                if(not_save_state!==1)
                {
                    this.pushCommand(()=>this.restore(msg._id),()=>this.deleteMsg(msg,1));
                }
                /*
                if(msg.Type=="Trigger")
                {
                    var ind = 0;
                    for(var i=0;i<this.state.TriggerMsgs.length;i++)
                    {
                        if(this.state.TriggerMsgs[i]==msg._id)
                            ind = i;
                    }
                    this.state.TriggerMsgs.splice(ind,1);
                    this.setState({TriggerMsgs:this.state.TriggerMsgs})
                }
                else if(msg.Type=="Sch")
                {
                    var ind = 0;
                    for(var i=0;i<this.state.SchMsgs.length;i++)
                    {
                        if(this.state.SchMsgs[i]==msg._id)
                            ind = i;
                    }
                    this.state.SchMsgs.splice(ind,1);
                    this.setState({SchMsgs:this.state.SchMsgs})
                }
                else if(msg.Type=="Auto")
                {
                    var ind = 0;
                    for(var i=0;i<this.state.AutoMsgs.length;i++)
                    {
                        if(this.state.AutoMsgs[i]==msg._id)
                            ind = i;
                    }
                    this.state.AutoMsgs.splice(ind,1);
                    this.setState({AutoMsgs:this.state.AutoMsgs})
                }
                else if(msg.Type=="Now")
                {
                    var ind = 0;
                    for(var i=0;i<this.state.NowMsgs.length;i++)
                    {
                        if(this.state.NowMsgs[i]==msg._id)
                            ind = i;
                    }
                    this.state.NowMsgs.splice(ind,1);
                    this.setState({NowMsgs:this.state.NowMsgs})
                }*/
                this.loadMsgs(this.state.Bot);
            } else {
                this.setState({Wait:--this.state.Wait});
                alert(JSON.stringify(res))
            }
        }.bind(this));
    }
    loadMsgs(bot) {
        var url = config.Backend + '/bot/broadcastMsgs?';
        if (!(cookies.get('Id') === undefined))
        {
            url += '&CurrentUserId=' + cookies.get('Id').toString();
        }
        url+= '&Id='+bot._id;
        this.setState({Wait:++this.state.Wait});;
        jQuery.getJSON(url, function (res) {
            if (!res.Error) {
                this.state.NowMsgs = [];
                this.state.TriggerMsgs = [];
                this.state.SchMsgs = [];
                this.state.AutoMsgs = [];
                this.setState({NowMsgs:this.state.NowMsgs})
                this.setState({TriggerMsgs:this.state.TriggerMsgs})
                this.setState({SchMsgs:this.state.SchMsgs})
                this.setState({AutoMsgs:this.state.AutoMsgs})
                for(var i=0;i<res.length;i++)
                {
                    if(res[i].Type=="Trigger")
                    {
                        this.state.TriggerMsgs.push(res[i]);
                        this.setState({TriggerMsgs: this.state.TriggerMsgs});
                    }
                    else if(res[i].Type=="Sch")
                    {
                        this.state.SchMsgs.push(res[i]);
                        this.setState({SchMsgs: this.state.SchMsgs});
                    }
                    else if(res[i].Type=="Auto")
                    {
                        this.state.AutoMsgs.push(res[i]);
                        this.setState({AutoMsgs: this.state.AutoMsgs});
                    }
                    else if(res[i].Type=="Now")
                    {
                        this.state.NowMsgs.push(res[i]);
                        this.setState({NowMsgs: this.state.NowMsgs});
                    }
                }
                this.setState({Wait:--this.state.Wait});;
            } else {
                this.setState({Wait:--this.state.Wait});;
                //alert(JSON.stringify(res));
            }
        }.bind(this));
    }
    addNowMsg() {
        var url = config.Backend + '/botMsg/add?';
        if (!(cookies.get('Id') === undefined))
        {
            url += '&CurrentUserId=' + cookies.get('Id').toString();
        }
        url+= '&Bot='+this.state.Bot._id;
        url+= '&Type=Now';
        url+= '&Name=Block';
        this.setState({Wait:++this.state.Wait});;
        jQuery.getJSON(url, function (res) {
            if (!res.Error) {
                this.changeMsg(res.BotMsg);
                this.state.NowMsgs.push(res.BotMsg);
                this.pushCommand(()=>this.deleteMsg(res.BotMsg,1),()=>this.restore(res.BotMsg._id));
                this.setState({NowMsgs:this.state.NowMsgs,Wait:--this.state.Wait});
            } else {
                this.setState({Wait:--this.state.Wait});;
                alert(JSON.stringify(res));
            }
        }.bind(this));
    }
    addTriMsg() {
        var url = config.Backend + '/botMsg/add?';
        if (!(cookies.get('Id') === undefined))
        {
            url += '&CurrentUserId=' + cookies.get('Id').toString();
        }
        url+= '&Bot='+this.state.Bot._id;
        url+= '&Type=Trigger';
        url+= '&Name=Block';
        this.setState({Wait:++this.state.Wait});;
        jQuery.getJSON(url, function (res) {
            if (!res.Error) {
                this.changeMsg(res.BotMsg);
                this.state.TriggerMsgs.push(res.BotMsg);
                this.pushCommand(()=>this.deleteMsg(res.BotMsg,1),()=>this.restore(res.BotMsg._id));
                this.setState({TriggerMsgs:this.state.TriggerMsgs,Wait:--this.state.Wait});
            } else {
                this.setState({Wait:--this.state.Wait});;
                alert(JSON.stringify(res));
            }
        }.bind(this));
    }
    addSchMsg() {
        var url = config.Backend + '/botMsg/add?';
        if (!(cookies.get('Id') === undefined))
        {
            url += '&CurrentUserId=' + cookies.get('Id').toString();
        }
        url+= '&Bot='+this.state.Bot._id;
        url+= '&Type=Sch';
        url+= '&Name=Block';
        this.setState({Wait:++this.state.Wait});;
        jQuery.getJSON(url, function (res) {
            if (!res.Error) {
                this.changeMsg(res.BotMsg);
                this.state.SchMsgs.push(res.BotMsg);
                this.pushCommand(()=>this.deleteMsg(res.BotMsg,1),()=>this.restore(res.BotMsg._id));
                this.setState({SchMsgs:this.state.SchMsgs,Wait:--this.state.Wait});
            } else {
                this.setState({Wait:--this.state.Wait});;
                alert(JSON.stringify(res));
            }
        }.bind(this));
    }
    addAutoMsg() {
        var url = config.Backend + '/botMsg/add?';
        if (!(cookies.get('Id') === undefined))
        {
            url += '&CurrentUserId=' + cookies.get('Id').toString();
        }
        url+= '&Bot='+this.state.Bot._id;
        url+= '&Type=Auto';
        url+= '&Name=Block';
        this.setState({Wait:++this.state.Wait});;
        jQuery.getJSON(url, function (res) {
            if (!res.Error) {
                this.changeMsg(res.BotMsg);
                this.state.AutoMsgs.push(res.BotMsg);
                this.setState({AutoMsgs:this.state.AutoMsgs,Wait:--this.state.Wait});
            } else {
                this.setState({Wait:--this.state.Wait});;
                alert(JSON.stringify(res));
            }
        }.bind(this));
    }
    BroadcastLeftPanel() {
        var nowMsgs=this.state.NowMsgs.map((msg)=><div onClick={()=>this.changeMsg(msg)} className="tags-cell">
            {msg.Status == "1" ? <span className="tags_off"></span> : null}
            <span>Message</span>
        </div>)
        var triggerMsgs=this.state.TriggerMsgs.map(function(msg) {
            var str=msg.Time+" "+msg.TimeType.toLowerCase().substr(0,msg.TimeType.length-1)+" after ";
            if(msg.Trigger.indexOf("first")!=-1)str+="first interaction";
            else if(msg.Trigger.indexOf("last")!=-1)str+="last interaction";
            else str+="("+ msg.TriggerAttr + " " +msg.TriggerCond + " "+ msg.TriggerVal+")";
            if(msg.Trigger=="After user join to channel")
            {
                str = "After user join to channel";
            }
            else if(msg.Trigger=="After user add bot to contacts")
            {
                str = "After user add bot to contacts";
            }

            return (<div onClick={()=>this.changeMsg(msg)} className="tags-cell">
                {msg.Status == "1" ? <span className="tags_off"></span> : null}
                <span>{str}</span>
            </div>)
        }.bind(this))

        var schMsgs=this.state.SchMsgs.map(function(msg){
            var str= "";
            if(msg.Days=="None")
            {
                var correct = new Date(msg.Date);
                correct.setDate(correct.getDate()+1);
                var correctDate = JSON.stringify(correct);
                str+=correctDate.substr(1,correctDate.indexOf("T")-1);
            }
            else if(msg.Days=="Every Day")
            {
                str+=msg.Days;
            }
            else if(msg.Days=="Weekends")
            {
                str+=msg.Days;
            }
            else if(msg.Days=="Workdays")
            {
                str+=msg.Days;
            }
            else if(msg.Days=="Every Month")
            {
                str+="Every ";
                var x=+msg.Date.substr(8,2)+1
                str+=x;
                str+=" Day of the Month"
            }
            else
            {
                var arr = msg.Days.split(";");
                for(var i=0;i<arr.length-1;i++)
                {
                    var days = ["Sun","Mon","Tue","Wed","Thu","Fri","Sat"]
                    str+= days[+arr[i].replace(";","")]+", ";
                }
                str=str.substr(0,str.length-2);
            }
            str+=" "+msg.Time;
            return (
                <div onClick={()=>this.changeMsg(msg)} className="tags-cell">
                    {msg.Status == "1" ? <span className="tags_off"></span> : null}
                    <span>{str}</span>
                </div>)
        }.bind(this))

        var autoMsgs=this.state.AutoMsgs.map(function(msg){
            var str= "Autopost";

            return (
                <div onClick={()=>this.changeMsg(msg)} className="tags-cell">
                    {msg.Status == "1" ? <span className="tags_off"></span> : null}
                    <span>{str}</span>
                </div>)
        }.bind(this))





        return (
            <div className="broadcast_left_panel col-sm-3">
                <Scrollbars
                    style={{ height: "90%" }}>
                    <div className="broadcast_left_panel_post">
                        <div className="post_message_now">
                            <span className="post_title">Deliver Your Message now</span>
                            <div className="tags">
                                {nowMsgs}
                                <div onClick={this.addNowMsg} className="tags-cell">
                                    <span className="post_add_button"></span>
                                </div>
                            </div>
                        </div>
                        <div className="post_add_trigger">
                            <span className="post_title">Add a Trigger</span>
                            <div className="tags">
                                {triggerMsgs}
                                <div onClick={this.addTriMsg} className="tags-cell">
                                    <span className="post_add_button"></span>
                                </div>
                            </div>
                        </div>
                        <div className="post_schedule_later">
                            <span className="post_title">Schedule for later</span>
                            <div className="tags">
                                {schMsgs}
                                <div onClick={this.addSchMsg} className="tags-cell">
                                    <span className="post_add_button"></span>
                                </div>
                            </div>
                        </div>
                        {/*<div className="post_sourse">*/}
                            {/*<span className="post_title">Autopost From external source</span>*/}
                            {/*<div className="tags">*/}
                                {/*{autoMsgs}*/}
                                {/*<div onClick={this.addAutoMsg}  className="tags-cell">*/}
                                    {/*<span className="post_add_button"></span>*/}
                                {/*</div>*/}
                            {/*</div>*/}
                        {/*</div>*/}
                    </div>
                </Scrollbars>
            </div>
        )
    }
    changeMsg(msg) {
        this.setState({CurrentMsg:msg},()=>{
            if(msg._id)
            {
                this.refreshFilterUsers(msg) ;
                this.changeMain(msg.Type)
            }
        });
    }
    changeMain(newMain) {
        this.setState({Main:newMain});
    }
    condition(user,attr,val,cond) {
        if(cond=="is")
        {
            return user[attr] == val;
        }
        else if(cond=="is not")
        {
            return user[attr] != val;
        }
        else if(cond==">")
        {
            return user[attr] > val;
        }
        else if(cond=="<")
        {
            return user[attr] < val;
        }
        else
        {
            return (user[attr] || "").indexOf(val || "")==0;
        }
    }
    componentWillMount() {
        document.onkeydown = this.ctrl;
        this.preload();
        var url = config.Backend +'/users/findById?';
        if (!(cookies.get('Id') === undefined))
        {
            url += 'Id=' + cookies.get('Id').toString();
        }
        this.setState({Wait:++this.state.Wait});;
        jQuery.getJSON(url, function (res) {
            if (!res.Error) {

                this.setState({User: res},()=>{
                    var url = config.Backend + '/bot/findById?';
                    if (!(cookies.get('Id') === undefined))
                    {
                        url += 'CurrentUserId=' + cookies.get('Id').toString();
                    }
                    url += '&Id=' + cookies.get('BotId').toString();
                    jQuery.getJSON(url, function (res) {
                        if (!res.Error) {
                            if(!config.checkRights(cookies.get('Id').toString(),res,"Broadcast"))
                            {
                                config.accessDenied(cookies.get('Id').toString(),res);

                            }
                                    this.setState({Bot: res},() => {
                                        var route = "fb";
                                        if(this.state.Bot.BindTo=="Skype" || this.state.Bot.BindTo=="Standalone")
                                        {
                                            route="skype";
                                        }
                                        else if(this.state.Bot.BindTo=="Slack")
                                        {
                                            route="slack";
                                        }
                                        var url = config.Backend + "/"+route+"/getUsers?BotId="+this.state.Bot._id;
                                        if (!(cookies.get('Id') === undefined))
                                        {
                                            url += '&CurrentUserId=' + cookies.get('Id').toString();
                                        }
                                        jQuery.getJSON(url,function (res) {
                                            if(!res.Error)
                                            {
                                                this.setState({BotUsers:res,Wait:--this.state.Wait})
                                            }
                                            else
                                            {
                                                this.setState({Wait:--this.state.Wait});
                                                
                                                alert(JSON.stringify(res))
                                            }
                                        }.bind(this));
                                        this.setState({Show:true})
                                        this.loadMsgs(res)
                                    });

                        } else {
                            this.setState({Wait:--this.state.Wait});
                            alert(JSON.stringify(res))
                        }
                    }.bind(this));
                });
            } else {
                window.location.replace(window.location.origin + '/login');
            }
        }.bind(this));


    }
    handleInputChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }
    handleInputFocus(event) {
        this.setState({
            [event.target.name + '_valid']: true
        })
    }
    render() {
        return (
            <div>
                {this.Load()}
                {this.state.Show ?
                    <div className="container-fluid"><div className="row">
                        <TopPanel />
                        {this.state.Show ? <LeftPanel User={this.state.User}/> : null}
                        <div className="mainBroadCast">
                            {this.BroadcastLeftPanel()}
                            <BroadcastCentrPanel loadMsgs={this.loadMsgs} changeMsg={this.changeMsg} changeMain={this.changeMain} DeleteMsg={this.deleteMsg}
                                                 Saved={()=>{return this.state.Saved}} ChangeSave={this.changeSave} addWait={this.addWait}
                                                 delWait={this.delWait} pushCommand={this.pushCommand}
                                                 ShowDelPopup={()=>{this.setState({ShowDeletebotMsgPopup:true})}}     ShowUserFilter={()=>{this.setState({
                                ShowUserFilter: true
                            })}}  FilterCount={this.state.FilterCount}
                                                 BotMsg={this.state.CurrentMsg} User={this.state.User} Bot={this.state.Bot} Main={this.state.Main}/>
                        </div></div></div> : null}
                {this.state.ShowDeletebotMsgPopup ?
                    <Popup text={"Are you sure?"} FromBroadcast={true}
                           Yes={()=>this.deleteMsg(this.state.CurrentMsg)}
                           No={()=>{this.setState({ShowDeletebotMsgPopup:false})}}/> : null}
                {this.state.ShowUserFilter ? this.BroadCastFilter() : null}
            </div>
        )
    }
}
;
export default Broadcast;