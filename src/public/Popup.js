import React, { Component}  from 'react';
import jQuery from 'jquery';
import Cookies from 'universal-cookie';

const cookies = new Cookies();
var config = require('../config.js');
class Popup extends Component {
    constructor(props) {

        super(props);
        this.state = {
            Bot: this.props.Bot,
            Group: this.props.Group,
            Name: this.props.Name,
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.onSumbit = this.onSumbit.bind(this);
    }

    handleInputChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    onSumbit(e)
    {
        e.preventDefault();
       if( this.props.RenameGroup)
       {
           this.props.Rename(this.state.Group, this.state.Name)
       }
       else
       {
       this.props.Yes();
    }
    }

    render() {
        // onClick={this.props.RenameGroup ? () => this.props.Rename(this.state.Group, this.state.Name) : this.props.Yes}>
        //  onClick={this.props.RenameGroup ? () => this.props.Rename(this.state.Group, this.state.Name) : this.props.Yes}>
        //  onClick={this.props.RenameGroup ? () => this.props.Rename(this.state.Group, this.state.Name) : this.props.Yes}>


        return (
            <div>
                {
                    this.props.isDashboard ?
                        <div className='filterPopup'>
                            <div className='filter_delete'>
                                <form onSubmit={this.onSumbit}>
                                <h1>{this.props.text}</h1>
                                {this.props.RenameGroup ?
                                    <input id={"PopupInput"} className="chat_input mb10" type="text" value={this.state.Name} name="Name"
                                           onChange={this.handleInputChange}/>
                                    : null
                                }
                                <button className={this.props.RenameGroup ? true : "redButton"} type="submit">
                                    Yes
                                </button>

                                <button className="grey" onClick={this.props.No}>No</button>

                                </form>
                            </div>
                        </div> : null

                }
                { this.props.FromBroadcast ?
                    <div className='filterPopup'>
                        <div className='filter_delete'>
                            <form onSubmit={this.onSumbit}>
                            <h1>{this.props.text}</h1>
                            {this.props.RenameGroup ?
                                <input id={"PopupInput"} className="chat_input mb10" type="text" value={this.state.Name} name="Name"
                                       onChange={this.handleInputChange}/>
                                : null
                            }
                            <button className={this.props.RenameGroup ? true : "redButton"} type="submit">
                                Yes
                            </button>
                            <button className="grey" onClick={this.props.No}>No</button>
                            </form>
                        </div>
                    </div> : null

                }

                {
                    !this.props.FromBroadcast && !this.props.isDashboard ?
                        <div className='popup'>
                            <div className='popup_inner'>
                                <form onSubmit={this.onSumbit}>
                                <h1>{this.props.text}</h1>
                                {this.props.RenameGroup ?
                                    <input  id={"PopupInput"} className="chat_input mb10" type="text" value={this.state.Name} name="Name"
                                           onChange={this.handleInputChange}/>
                                    : null
                                }
                                <button className={this.props.RenameGroup ? true : "redButton"} type="submit">
                                    Yes
                                </button>
                                <button  onClick={this.props.No}>No</button>
                                </form>
                            </div>
                        </div> : null
                }
            </div>
        );
    }
}


export default Popup;