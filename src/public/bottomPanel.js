import React, { Component}  from 'react';
import jQuery from 'jquery';
import Cookies from 'universal-cookie';

const cookies = new Cookies();
var config = require('../config.js');

var styleTop = {
    height: '70px'
}

// если закончились то этот стиль
var styleTopTwo = {
    height: '140px'
}



class BottomPanel extends React.Component {

    render(){
        return (
            <div className="fixed_bottom_panel">
            <div className="chatbot">
                <div className=" top_nav_danger" style={styleTop}>
                    <p>Работа Ваших ботов временно преостановлена. Для возобновления подключите <a href="/tariffs">тариф</a>.</p>
                </div>
            </div>
            </div>
        )
    }
}

export default BottomPanel;
