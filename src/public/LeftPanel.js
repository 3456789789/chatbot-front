import React, { Component}  from 'react';
import jQuery from 'jquery';
import Cookies from 'universal-cookie';
import { Scrollbars } from 'react-custom-scrollbars';
const cookies = new Cookies();
var config = require('../config.js');

class LeftPanel extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            User: null,
            Bot: null,
            OpenChats: 0,
        };
        this.refresh = this.refresh.bind(this);
    }

    exit(event) {
        cookies.set('Exit',"true", { path: '/' });
        window.location.replace(window.location.origin + '/login');
    }

    refresh() {
        var s=this.state;
        var route = "fb";
        if (this.state.Bot.BindTo == "Skype" || this.state.Bot.BindTo == "Standalone") {
            route = "skype";
        }
        else if (this.state.Bot.BindTo == "Slack") {
            route = "slack";
        }
        var url = config.Backend + "/" + route + "/getUsers?BotId=" + this.state.Bot._id;
        url += '&ChatWithAdmin=1';
        if (!(cookies.get('Id') === undefined)) {
            url += '&CurrentUserId=' + cookies.get('Id').toString();
        }
        jQuery.getJSON(url, function (res) {
            if (!res.Error) {
               
                this.setState({OpenChats:res.length})
            }
            else alert(JSON.stringify(res))
        }.bind(this));
    }

    componentWillMount() {
        var url = config.Backend +'/users/findById?';
        if (!(cookies.get('Id') === undefined))
        {
            url += 'Id=' + cookies.get('Id').toString();
        }

        jQuery.getJSON(url, function (res) {
            if (!res.Error) {
                this.setState({User: res},()=>{
                    var url = config.Backend + '/bot/findById?';
                    if (!(cookies.get('Id') === undefined))
                    {
                        url += 'CurrentUserId=' + cookies.get('Id').toString();
                    }
                    url += '&Id=' + cookies.get('BotId').toString();
                    jQuery.getJSON(url, function (res) {
                        if (!res.Error) {
                                    this.setState({Bot: res},()=>{
                                        if(res.BindTo && res.BindTo!="-") {
                                            this.refresh();
                                            setInterval(this.refresh, 10 * 1000);
                                        }
                                    })
                        } else {
                            alert(JSON.stringify(res))
                        }
                    }.bind(this))
                });
            } else {
                window.location.replace(window.location.origin + '/login');
            }
        }.bind(this));
    }
    render(){
        var uId = cookies.get('Id').toString();
        var b = this.state.Bot;
        var u = this.state.User;
        var showBuild = b && config.checkRights(uId,b,'Build');
        var showBroadcast = (u && u.Type!="Super" && b && config.checkRights(uId,b,'Broadcast')) || (b && b.Basic) || (b && b.isTemplate);
        var showConfigure = (u && u.Type!="Super" && b && config.checkRights(uId,b,'Configure')) || (b && b.Basic);
        var showGrow = (u && u.Type!="Super" && b && config.checkRights(uId,b,'Grow')) || (b && b.Basic) || (b && b.isTemplate);
        var showAnalyze = (u && u.Type!="Super" && b && config.checkRights(uId,b,'Analyze')) || (b && b.Basic);
        var showRtc = (u && u.Type!="Super" && b && config.checkRights(uId,b,'RTC')) || (b && b.Basic);
        var showShare = (u && u.Type!="Super" && b && config.checkRights(uId,b,'Share')) || (b && b.Basic);

        /*
        <li>
                                <span>{this.props.User.Name}</span>
                            </li>
         */
            // bot_left_panel
            return (
                <div className="bot_left_panel">
                    <Scrollbars
                        style={{ height: "90%" }}>
                    <div className="left_panel_top_block">
                    {this.props.isDashboard ? null : <ul className="top_navigator mt_25">
                        {showBuild ?
                        <li className={window.location.href.indexOf("/chat/build")!=-1 ? "active" : ""}>
                            <a href="/chat/build">
                                <div className="block_image build">
                                </div>
                                <span>Build</span>
                            </a>
                        </li> : null}
                        {showBroadcast ?
                        <li className={window.location.href.indexOf("/chat/broadcast")!=-1 ? "active" : ""}>
                            <a href="/chat/broadcast">
                                <div className="block_image broadcast">
                                </div>
                                <span>Broadcast</span>
                            </a>
                        </li> : null}
                        {showConfigure ?
                        <li className={window.location.href.indexOf("/chat/configure")!=-1 ? "active" : ""}>
                            <a href="/chat/configure">
                                <div className="block_image configurate">
                                </div>
                                <span>Integrate</span>
                            </a>
                        </li> : null}
                        {showGrow ?
                        <li className={window.location.href.indexOf("/chat/grow")!=-1 ? "active" : ""}>
                            <a href="/chat/grow">
                                <div className="block_image  growUp">
                                </div>
                                <span>Plugins</span>
                            </a>
                        </li> : null}
                        {showAnalyze ?
                        <li className={window.location.href.indexOf("/chat/analyze")!=-1 ? "active" : ""}>
                            <a href="/chat/analyze">
                                <div className="block_image analyze">
                                </div>
                                <span>Analytics</span>
                            </a>
                        </li> : null}
                        {showRtc ?
                            <li className={window.location.href.indexOf("/chat/rtc")!=-1 ? "active" : ""}>
                                <a href="/chat/rtc">
                                <div className="block_image rtc"></div>
                                    <span>RTC</span>
                                    <span className={'check_rtc'}>{this.state.OpenChats}</span></a></li> : null}

                        {showShare ?
                            <li  className={window.location.href.indexOf("/chat/share")!=-1 ? "active" : ""}>
                                <a href="/chat/share">
                                <div className="block_image share"></div>
                                    <span>Share</span></a>
                            </li> : null}


                        {showShare ? <li className={window.location.href.indexOf("/chat/history")!=-1 ? "active" : ""}>
                            <a href="/chat/history">
                            <div className="block_image history"></div>
                            <span href="/chat/history">History</span></a>
                        </li> : null}

                    </ul> }

                    </div>

                    </Scrollbars>
                </div>
            )
    }
}

export default LeftPanel;
