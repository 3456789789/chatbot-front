import React, { Component}  from 'react';
import jQuery from 'jquery'; 
import Cookies from 'universal-cookie';
import { Switch, Route, Link } from 'react-router-dom'

const cookies = new Cookies();

class UsersForm extends Component{
    constructor(props) {
    super(props);

    this.state = {
      CurUser: {},
      CurUserDoctor: {},
      users: [],
      Name: '',
      Gender: '',
      City: '',
      DateOfBirth: '',
      Email: '',
      Response_txt: '',
      Error: false,
    }
this.Search = this.Search.bind(this);
this.handleInputChange = this.handleInputChange.bind(this);
this.handleInputFocus = this.handleInputFocus.bind(this);
  }
 
  Search() {
      var url = 'http://localhost:4800/users/search?';
      if (this.state.Name) {
        url += '&FullName='+ this.state.Name;   
      }
      if (this.state.Gender) {
        url += '&Gender='+ this.state.Gender;   
      }
      if (this.state.Email) {
        url += '&Email='+ this.state.Email;   
      }
      if (this.state.DateOfBirth) {
        url += '&DateOfBirth='+ this.state.DateOfBirth;   
      }
      if (this.state.City) {
        url += '&City='+ this.state.City;   
      }
        if (!(cookies.get('Id') === undefined))
        {
            url += '&Id='+ cookies.get('Id').toString();   
        }
               
        jQuery.getJSON(url,function (res){
             if (res.Error)
            {
                this.setState({Response_txt : res.Error});
                this.setState({Error : true});
            } else {
               this.setState({Error: false});
               console.log(res);
               this.setState({users:res});
            }
        }.bind(this));
  
  }
  
  handleInputChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    })
  }

  handleInputFocus(event) {
    this.setState({
      [event.target.name+'_valid'] : true
    })
  }
 
  componentWillMount() {
       var url = 'http://localhost:4800/users/findById?';
        if (!(cookies.get('Id') === undefined))
        {
            url += 'Id='+ cookies.get('Id').toString();   
        }
               
        jQuery.getJSON(url,function (res){
             if (!res.Error) {
               this.setState({CurUser: res});
               
               //this.setState({Id: res._id});
               console.log(res);
            }
        }.bind(this));
      var url = 'http://localhost:4800/users/search';
        if (!(cookies.get('Id') === undefined))
        {
            url += '?Id='+ cookies.get('Id').toString();   
        }
               
        jQuery.getJSON(url,function (res){
             if (res.Error)
            {
                this.setState({Response_txt : res.Error});
                this.setState({Error : true});
            } else {
               this.setState({Error: false});
               console.log(res);
               this.setState({users:res});
            }
        }.bind(this));
    }
    
    UsersHeader() {
        let users = this.state.CurUser.Type == 'Doctor' ? 'Patients' : 'Doctors';
        if (this.state.CurUser.Doctor !== undefined)
        {
            users += '(You doctor is '+ this.state.CurUser.Doctor.FullName + ")";
        }
        return <h1>{users}</h1>
    }
     
    Response () {
        const danger = this.state.Error ? <div className="text-danger">{this.state.Response_txt}</div> : '';
        return (<div className="form-group">
    {danger}
    </div>
                        )
        }
        
        

TableData (props) {
    const users = props.users ? props.users.map((user) => <tr>
    <td>{user.FullName}</td>
    <td>{user.Gender}</td>
    <td>{user.Email}</td>
    <td>{user.City}</td>   
    <td>{user.DateOfBirth.substr(0,10)}</td>
    <td>
    {user.Type == 'Patient' ? <Link to={`/appointments/${user._id}`}><button className="btn btn-success">Appointments</button></Link> : null}
    {props.CurUser.Type == 'Admin' ?
    <Link to={`/patients/${user._id}`}><button className="btn btn-success">Patients</button></Link> : null}
        {props.CurUser.Type == 'Admin' ?
    <Link to={`/resetPassword/${user.Email}`}><button className="btn btn-success">Reset password</button></Link> : null}
    {props.CurUser.Type == 'Admin' || props.CurUser.Type == 'Doctor' ?
    <Link to={`/users/edit/${user.Email}/${user.Type}`}><button className="btn btn-success">Edit</button></Link>  : null}
    {props.CurUser.Type == 'Admin' || props.CurUser.Type == 'Doctor' ?
    <Link to={`/users/delete/${user.Email}/${user.Type}`}><button className="btn btn-success">Delete</button></Link> : null}
    {props.CurUser.Type == 'Patient' ?
    <Link to={`/bind/${props.CurUser._id}/${user._id}`}><button className="btn btn-success">Set this doctor</button></Link> : null}        
            </td>        </tr>
            ) : null;
    return users;
}
    
        
        Table() {
            return (<table class="table table-striped table-bordered" >
        <thead>
        <tr>
    <th>Name</th>
    <th>Gender</th>
    <th>Email</th>
    <th>City</th>
    <th>DateOfBirth</th>
    <th>
    {this.state.CurUser.Type == 'Admin' || this.state.CurUser.Type == 'Doctor' ?
         <Link to={`/users/add`}><button className="btn btn-success">Add</button></Link> : null}
    </th>
   </tr>
   </thead>
      <tbody>
      <tr>
        <td><input type="text" className="form-control" value={this.state.Name} name="Name" onChange={this.handleInputChange} onFocus={this.handleInputFocus}/></td>
   <td><input type="text" className="form-control" value={this.state.Gender} name="Gender" onChange={this.handleInputChange} onFocus={this.handleInputFocus}/></td>
   <td><input type="text" className="form-control" value={this.state.Email} name="Email" onChange={this.handleInputChange} onFocus={this.handleInputFocus}/></td>
   <td><input type="text" className="form-control" value={this.state.City} name="City" onChange={this.handleInputChange} onFocus={this.handleInputFocus}/></td>
   <td><input type="text" className="form-control" value={this.state.DateOfBirth} name="DateOfBirth" onChange={this.handleInputChange} onFocus={this.handleInputFocus}/></td>
   <td><button onClick={this.Search}>Search</button></td>
        </tr>
    <this.TableData users = {this.state.users} CurUser = {this.state.CurUser}/>
       </tbody>
    </table>)
                        
        }

  render () {
    return (
      <div className="container">
          {this.UsersHeader()}
          {this.Response()}
          {this.Table()}
      </div>
    );
  }
}

export default UsersForm;
