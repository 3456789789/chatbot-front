import React, { Component}  from 'react';
import Loader from 'react-loader-spinner';
var config = require('./config.js');

class Loading extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
        };
        this.preload = this.preload.bind(this);
    }
    preload()
    {
        config.getData('settings/all_public').then(data=>{
            var o = {}
            data.forEach((e,i,arr)=>{
                o[e.Key] = e.Value;
                if(e.Key=="Title")
                {
                    document.title = e.Value;
                }
            })
            this.setState({Preload:o})
        },err=>{
            alert(JSON.stringify(err))
        })
    }
    componentWillMount()
    {
        this.preload();
    }
    componentWillUpdate (p,s)
    {
        //console.log(this.state.Wait);
        //console.log(this.state.Wait);
        //console.log("-");
    }
    Load() {
        if (this.state.Wait) {
            return (<div className="preloader_bots">
                <div className="preloader_bots_2">
                    <Loader type="Grid" color="white" height={50} width={50}/>
                </div>
            </div>)
        }
        return null;
    }
}

export default Loading;
